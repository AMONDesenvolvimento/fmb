package domain_configuracao
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
//    "strconv"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
//	gr "github.com/mikeshimura/goreport"
    "../util"
)
type Record struct {
        ID int64 `json:"id"`
        Nome     string `json:"nome"`
	Descricao string `json:"descricao"`
        Exercicio    int64 `json:"exercicio"`
        MesReferencia string `json:"mesreferencia"`
        UltimoMes     int64 `json:"ultimomes"`
        RegimeDespesa string `json:"regimedespesa"`
        ContaDespesa string   `json:"contadespesa"`
        TodasSubcontasDespesa string `json:"todassubcontasdespesa"`
        RegimeReceita string `json:"regimereceita"`
        ContaReceita string   `json:"contareceita"`
        TodasSubcontasReceita string `json:"todassubcontasreceita"`
        RegimeOrcamento string `json:"regimeorcamento"`
        ContaOrcamento string   `json:"contaorcamento"`
        TodasSubcontasOrcamento string `json:"todassubcontasorcamento"`
        DataCriacao string `json:"datacriacao"` 
        DataExclusao string `json:"dataexclusao"`
 
}
type Filter struct {
        ID string `json:"id"`
        Nome     string `json:"nome"`
}
type Arr []Record
func GetAll(w http.ResponseWriter, r *http.Request) {
	rows := util.QueryDB(`SELECT id,
                                     nome,
                                     descricao,
                                     exercicio,
                                     mesreferencia,
                                     ultimomes,
                                     regimedespesa,
                                     contadespesa,
                                     todassubcontasdespesa,
                                     regimereceita,
                                     contareceita,
                                     todassubcontasreceita,
                                     regimeorcamento,
                                     contaorcamento,
                                     todassubcontasorcamento,
                                     datacriacao,
                                     dataexclusao FROM configuracao order by id`)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.Nome, 
                               &record.Descricao,
                               &record.Exercicio,
                               &record.MesReferencia,
                               &record.UltimoMes,
                               &record.RegimeDespesa,
                               &record.ContaDespesa, 
                               &record.TodasSubcontasDespesa,

                               &record.RegimeReceita,
                               &record.ContaReceita, 
                               &record.TodasSubcontasReceita,
                               &record.RegimeOrcamento,
                               &record.ContaOrcamento, 
                               &record.TodasSubcontasOrcamento,

                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB(`SELECT id,
                                       nome,
                                       descricao,
                                       exercicio,
                                       mesreferencia,
                                       ultimomes,
                                       regimedespesa,
                                       contadespesa,
                                       todassubcontasdespesa,
                                       regimereceita,
                                       contareceita,
                                       todassubcontasreceita,
                                       regimeorcamento,
                                       contaorcamento,
                                       todassubcontasorcamento,
                                       datacriacao,dataexclusao FROM configuracao where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome, 
                                &record.Descricao,
                                &record.Exercicio,
                                &record.MesReferencia,
                                &record.UltimoMes,
                                &record.RegimeDespesa,
                                &record.ContaDespesa,
                                &record.TodasSubcontasDespesa,     
                                &record.RegimeReceita,
                                &record.ContaReceita,
                                &record.TodasSubcontasReceita,     
                                &record.RegimeOrcamento,
                                &record.ContaOrcamento,
                                &record.TodasSubcontasOrcamento,     
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func RowByID(id int64) Record {
	stmt := util.PrepareDB(`SELECT id,
                                       nome,
                                       descricao,
                                       exercicio,
                                       mesreferencia,  
                                       ultimomes,
                                       regimedespesa,
                                       contadespesa,
                                       todassubcontasdespesa,
                                       regimereceita,
                                       contareceita,
                                       todassubcontasreceita,
                                       regimeorcamento,
                                       contaorcamento,
                                       todassubcontasorcamento,
                                       datacriacao,dataexclusao FROM configuracao where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome, 
                                &record.Descricao,
                                &record.Exercicio,
                                &record.MesReferencia,
                                &record.UltimoMes,
                                &record.RegimeDespesa,
                                &record.ContaDespesa,
                                &record.TodasSubcontasDespesa,     
                                &record.RegimeReceita,
                                &record.ContaReceita,
                                &record.TodasSubcontasReceita,     
                                &record.RegimeOrcamento,
                                &record.ContaOrcamento,
                                &record.TodasSubcontasOrcamento,     
                                &record.DataCriacao,
                                &record.DataExclusao)
		//checkErr(err)
	}
        return record
}

func Insert(w http.ResponseWriter, r *http.Request) {
	var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+string(record.ID))
        log.Println("nome---->"+record.Nome)
        record.DataCriacao = time.Now().Format("2006-01-02 15:04:05")
	stmt := util.PrepareDB(
                                   `INSERT INTO configuracao (
                                   id,
                                   nome, 
                                   descricao,
                                   exercicio,
                                   mesreferencia,
                                   ultimomes,
                                   regimedespesa,
                                   contadespesa,
                                   todassubcontasdespesa,
                                   regimereceita,
                                   contareceita,
                                   todassubcontasreceita,
                                   regimeorcamento,
                                   contaorcamento,
                                   todassubcontasorcamento,
                                   datacriacao) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`)
	result, errExec := stmt.Exec(
                           record.ID,
                           record.Nome,   
                           record.Descricao,
                           record.Exercicio,
                           record.MesReferencia,
                           record.UltimoMes,
                           record.RegimeDespesa,
                           record.ContaDespesa,
                           record.TodasSubcontasDespesa,
                           record.RegimeReceita,
                           record.ContaReceita,
                           record.TodasSubcontasReceita,
                           record.RegimeOrcamento,
                           record.ContaOrcamento,
                           record.TodasSubcontasOrcamento,
                           record.DataCriacao,
                           )
	util.CheckErr(errExec)
	newID, errLast := result.LastInsertId()
	util.CheckErr(errLast)
	record.ID = newID
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func UpdateByID(w http.ResponseWriter, r *http.Request) {
	var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+string(record.ID))
        log.Println("nome---->"+record.Nome)

	stmt := util.PrepareDB(`UPDATE configuracao SET (
                                     nome,
                                     descricao,
                                     exercicio,
                                     mesreferencia,
                                     ultimomes,
                                     regimedespesa,
                                     contadespesa,
                                     todassubcontasdespesa,  
                                     regimereceita,
                                     contareceita,
                                     todassubcontasreceita,  
                                     regimeorcamento,
                                     contaorcamento,
                                     todassubcontasorcamento)= (?,?,?,?,?,?,?,?,?,?,?,?,?,?)  
                                    WHERE id = ?`)
	result, errExec := stmt.Exec(
                                  record.Nome,
                                  record.Descricao,
                                  record.Exercicio,
                                  record.MesReferencia,
                                  record.UltimoMes,
                                  record.RegimeDespesa,
                                  record.ContaDespesa,
                                  record.TodasSubcontasDespesa,
                                  record.RegimeReceita,
                                  record.ContaReceita,
                                  record.TodasSubcontasReceita,
                                  record.RegimeOrcamento,
                                  record.ContaOrcamento,
                                  record.TodasSubcontasOrcamento,
                                  record.ID)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(record)
		util.CheckErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func DeleteByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB("DELETE FROM configuracao WHERE id = ?")
	result, errExec := stmt.Exec(id)
	util.CheckErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	util.CheckErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}





