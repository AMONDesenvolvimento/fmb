package domain_relatorio

import (
	"net/http"
	"io"
	"github.com/gorilla/mux"
	"fmt"
	"bytes"
)

func GetRelatorio(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
    relatorio := vars["relatorio"]

    url := fmt.Sprintf("http://localhost:8080/jasperserver/rest_v2/reports/reports/%s.pdf",relatorio)
    
    client := &http.Client{}
    req, _ := http.NewRequest("GET",url, nil)
    req.Header.Set("Authorization","Basic amFzcGVyYWRtaW46amFzcGVyYWRtaW4=")

    
    query := req.URL.Query()
    r.ParseForm()

    for key, value := range r.Form {
        if value[0] != "" {
            query.Add(key, value[0])
        }
    }
    req.URL.RawQuery = query.Encode()

    resp, err := client.Do(req)
    if err != nil {
    	 println(err.Error())
    }

    var buf bytes.Buffer
    
    _ , err = io.Copy(&buf,resp.Body)
    if err != nil {
         println(err.Error())
    }

    _ , err =  w.Write(buf.Bytes())
    if err != nil {
         println(err.Error())
    }
}
