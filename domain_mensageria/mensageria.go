package domain_mensageria
import (
    "fmt"
    "time"
    "encoding/json"
    "net/http"
    "io/ioutil"
    "github.com/gorilla/mux"
    "../util"
)

type Record struct {
        Id int64 `json:"id"`
        Nome string `json:"nome"`
        Apelido string `json:"apelido"` 
        DataNascimento string `json:"datanascimento"`
        Celular string `json:"celular"`
        Fixo string `json:"fixo"`
        Email string `json:"email"`
        Email2 string `json:"email2"`
        CEP string `json:"cep"`
        UF string `json:"uf"`
        Cidade string `json:"cidade"`
        Bairro string `json:"bairro"`
        Logradouro  string `json:"logradouro"`
        Numero string `json:"numero"` 
        Complemento string `json:"complemento"`
        CanalSMS string `json:"canalsms"`
        CanalEmail string `json:"canalemail"`
        Observacao string `json:"observacao"`
        Foto string `json:"foto"`
        PJ string `json:"pj"`
        Cargo string `json:"cargo"`
        DataHoraCadastro string `json:"datahoracadastro"`
        DataUltimaMsg string `json:"dataultimamsg"`
}

type RecordPJ struct {
        Id int64 `json:"id"`
        CNPJ string `json:"cnpj"`
        RazaoSocial string `json:"razaosocial"`
        Alias string `json:"alias"` 
        Celular string `json:"celular"`
        Fixo string `json:"fixo"`
        Fixo2 string `json:"fixo2"`
        Email string `json:"email"`
        Email2 string `json:"email2"`
        PaginaWEB string `json:"paginaweb"`
        CEP string `json:"cep"`
        UF string `json:"uf"`
        Cidade string `json:"cidade"`
        Bairro string `json:"bairro"`
        Logradouro  string `json:"logradouro"`
        Numero string `json:"numero"` 
        Complemento string `json:"complemento"`
        Observacao string `json:"observacao"`
        CanalSMS string `json:"canalsms"`
        CanalEmail string `json:"canalemail"`
        DataHoraCadastro string `json:"datahoracadastro"`
        DataUltimaMsg string `json:"dataultimamsg"`
}

type Arr []Record

type ArrPJ []RecordPJ



//Get

func GetAll_PF(w http.ResponseWriter, r *http.Request) {

  rows, err := util.MainDB.Query(`SELECT  id,
                                          nome,
                                          apelido,
                                          dataNascimento,
                                          celular,
                                          fixo,
                                          email,
                                          email2,
                                          cep,
                                          uf,
                                          cidade,
                                          bairro,
                                          logradouro,
                                          numero,
                                          complemento,
                                          canalSMS,
                                          canalEmail,
                                          observacao,
                                          foto,
                                          pj,
                                          cargo,
                                          datetime() as dataHoraCadastro,
                                          dataHoraUltimaMsg
                                    FROM contato_pf 
                                    ORDER BY nome`)

  util.CheckErr(err)
  var arr Arr

  for rows.Next() {
    var record Record
    err = rows.Scan(
                           &record.Id,
                           &record.Nome,
                           &record.Apelido, 
                           &record.DataNascimento,
                           &record.Celular,
                           &record.Fixo,
                           &record.Email,
                           &record.Email2,
                           &record.CEP,
                           &record.UF,
                           &record.Cidade,
                           &record.Bairro,
                           &record.Logradouro,
                           &record.Numero,
                           &record.Complemento, 
                           &record.CanalSMS,
                           &record.CanalEmail,
                           &record.Observacao,
                           &record.Foto,
                           &record.PJ,
                           &record.Cargo,
                           &record.DataHoraCadastro,
                           &record.DataUltimaMsg,
                       )

    arr = append(arr, record) 
  }

  jsonB, errMarshal := json.Marshal(arr)
  util.CheckErr(errMarshal)
  fmt.Printf("\nLISTA CONTATOS PF OBTIDA\n\n")
  fmt.Fprintf(w, "%s", string(jsonB))
}

func GetByID_PF(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]

    stmt := util.PrepareDB(`SELECT  id,
                                    nome,
                                    apelido,
                                    dataNascimento,
                                    celular,
                                    fixo,
                                    email,
                                    email2,
                                    cep,
                                    uf,
                                    cidade,
                                    bairro,
                                    logradouro,
                                    numero,
                                    complemento,
                                    canalSMS,
                                    canalEmail,
                                    observacao,
                                    foto,
                                    pj,
                                    cargo,
                                    dataHoraCadastro,
                                    dataHoraUltimaMsg
                            FROM contato_pf 
                            WHERE id = ?`)

    rows, errQuery := stmt.Query(id)
    util.CheckErr(errQuery)
    var record Record

    for rows.Next() {
        rows.Scan( &record.Id,
                   &record.Nome,
                   &record.Apelido, 
                   &record.DataNascimento,
                   &record.Celular,
                   &record.Fixo,
                   &record.Email,
                   &record.Email2,
                   &record.CEP,
                   &record.UF,
                   &record.Cidade,
                   &record.Bairro,
                   &record.Logradouro,
                   &record.Numero,
                   &record.Complemento, 
                   &record.CanalSMS,
                   &record.CanalEmail,
                   &record.Observacao,
                   &record.Foto,
                   &record.PJ,
                   &record.Cargo,
                   &record.DataHoraCadastro,
                   &record.DataUltimaMsg,
        )
    }

    fmt.Println("Contato encontrado: " + record.Nome)

    jsonB, errMarshal := json.Marshal(record)
    util.CheckErr(errMarshal)
    fmt.Fprintf(w, "%s", string(jsonB))
}


//Delete

func DeleteByID_PF(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
    fmt.Println("ID para exclusão -> " + id)
    stmt := util.PrepareDB("DELETE FROM contato_pf WHERE id = ?")
    result, errExec := stmt.Exec(id)
    util.CheckErr(errExec)
    rowAffected, errRow := result.RowsAffected()
    util.CheckErr(errRow)
    fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}


//Insert

func Insert_PF(w http.ResponseWriter, r *http.Request) {
    var record Record
    body, err := ioutil.ReadAll(r.Body)

    if err != nil {
      panic(fmt.Sprintf("%s", r.Body))
    }

    fmt.Println("\nDADOS RECEBIDOS PARA CADASTRO:\n" + string(body) + "\n")
    err = json.Unmarshal(body, &record)

    record.Celular = util.RemoveMascaraDeCampoNumerico(record.Celular)
    record.Fixo = util.RemoveMascaraDeCampoNumerico(record.Fixo)
    record.CEP = util.RemoveMascaraDeCampoNumerico(record.CEP)

    if err != nil {
      fmt.Printf("erro %s",err)
    }

    stmt := util.PrepareDB(`INSERT INTO contato_pf (
                                dataHoraCadastro,
                                foto,
                                cargo,
                                pj,
                                nome,
                                apelido,
                                dataNascimento,
                                celular,
                                fixo,
                                email,
                                email2,
                                cep,
                                uf,
                                cidade,
                                bairro,
                                logradouro,
                                numero,
                                complemento,
                                canalSMS,
                                canalEmail,
                                observacao
                            ) 
                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`)

    record.DataHoraCadastro = time.Now().Format("2006-01-02 15:04:05")

    result, errExec := stmt.Exec(
                    record.DataHoraCadastro,
                    record.Foto,
                    record.Cargo,
                    record.PJ,
                    record.Nome,
                    record.Apelido,
                    record.DataNascimento,
                    record.Celular,
                    record.Fixo,
                    record.Email,
                    record.Email2,
                    record.CEP,
                    record.UF,
                    record.Cidade,
                    record.Bairro,
                    record.Logradouro,
                    record.Numero,
                    record.Complemento,
                    record.CanalSMS,
                    record.CanalEmail,
                    record.Observacao,
    )

    util.CheckErr(errExec)
    newID, errLast := result.LastInsertId()
    util.CheckErr(errLast)
    record.Id = newID
    jsonB, errMarshal := json.Marshal(record)
    util.CheckErr(errMarshal)
    fmt.Fprintf(w, "%s", string(jsonB))
}


//Update

func UpdateByID_PF(w http.ResponseWriter, r *http.Request) {
    var record Record
    body, err := ioutil.ReadAll(r.Body)

    if err != nil {
      panic(fmt.Sprintf("%s", r.Body))
    }

    fmt.Println("\nDADOS RECEBIDOS PARA EDIÇÃO:\n" + string(body) + "\n\n")
    err = json.Unmarshal(body, &record)

    record.Celular = util.RemoveMascaraDeCampoNumerico(record.Celular)
    record.Fixo = util.RemoveMascaraDeCampoNumerico(record.Fixo)
    record.CEP = util.RemoveMascaraDeCampoNumerico(record.CEP)

    if err != nil {
      fmt.Printf("erro %s",err)
    }

    stmt := util.PrepareDB(`UPDATE contato_pf SET (
                                foto,
                                cargo,
                                pj,
                                nome,
                                apelido,
                                dataNascimento,
                                celular,
                                fixo,
                                email,
                                email2,
                                cep,
                                uf,
                                cidade,
                                bairro,
                                logradouro,
                                numero,
                                complemento,
                                canalSMS,
                                canalEmail,
                                observacao
                            ) = (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
                            WHERE id = ?`)

    result, errExec := stmt.Exec(
                    record.Foto,
                    record.Cargo,
                    record.PJ,
                    record.Nome,
                    record.Apelido,
                    record.DataNascimento,
                    record.Celular,
                    record.Fixo,
                    record.Email,
                    record.Email2,
                    record.CEP,
                    record.UF,
                    record.Cidade,
                    record.Bairro,
                    record.Logradouro,
                    record.Numero,
                    record.Complemento,
                    record.CanalSMS,
                    record.CanalEmail,
                    record.Observacao,
                    record.Id,
    )

    util.CheckErr(errExec)
    newID, errLast := result.LastInsertId()
    util.CheckErr(errLast)
    record.Id = newID
    jsonB, errMarshal := json.Marshal(record)
    util.CheckErr(errMarshal)
    fmt.Fprintf(w, "%s", string(jsonB))
}









// PJ ===========================================================================

//Get

func GetByID_PJ(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]

    stmt := util.PrepareDB(`SELECT  id,
                                    cnpj,
                                    razaoSocial,
                                    alias,
                                    celular,
                                    fixo,
                                    fixo2,
                                    email,
                                    email2,
                                    paginaWeb,
                                    cep,
                                    uf,
                                    cidade,
                                    bairro,
                                    logradouro,
                                    numero,
                                    complemento,
                                    observacao,
                                    canalSMS,
                                    canalEmail,
                                    datetime() as dataHoraCadastro,
                                    dataHoraUltimaMsg
                            FROM contato_pj 
                            WHERE id = ?`)

    rows, errQuery := stmt.Query(id)
    util.CheckErr(errQuery)
    var record RecordPJ

    for rows.Next() {
        rows.Scan( &record.Id,
                   &record.CNPJ,
                   &record.RazaoSocial,
                   &record.Alias, 
                   &record.Celular,
                   &record.Fixo,
                   &record.Fixo2,
                   &record.Email,
                   &record.Email2,
                   &record.PaginaWEB,
                   &record.CEP,
                   &record.UF,
                   &record.Cidade,
                   &record.Bairro,
                   &record.Logradouro,
                   &record.Numero,
                   &record.Complemento, 
                   &record.Observacao, 
                   &record.CanalSMS,
                   &record.CanalEmail,
                   &record.DataHoraCadastro,
                   &record.DataUltimaMsg,
        )
    }

    fmt.Println("Contato PJ encontrado: " + record.RazaoSocial)

    jsonB, errMarshal := json.Marshal(record)
    util.CheckErr(errMarshal)
    fmt.Fprintf(w, "%s", string(jsonB))
}

func GetAll_PJ(w http.ResponseWriter, r *http.Request) {

  rows, err := util.MainDB.Query(`SELECT  id,
                                          cnpj,
                                          razaoSocial,
                                          alias,
                                          celular,
                                          fixo,
                                          fixo2,
                                          email,
                                          email2,
                                          paginaWeb,
                                          cep,
                                          uf,
                                          cidade,
                                          bairro,
                                          logradouro,
                                          numero,
                                          complemento,
                                          observacao,
                                          canalSMS,
                                          canalEmail,
                                          datetime() as dataHoraCadastro,
                                          dataHoraUltimaMsg
                                    FROM contato_pj
                                    ORDER BY razaoSocial`)

  util.CheckErr(err)
  var arr ArrPJ

  for rows.Next() {
    var record RecordPJ
    err = rows.Scan(
                       &record.Id,
                       &record.CNPJ,
                       &record.RazaoSocial,
                       &record.Alias, 
                       &record.Celular,
                       &record.Fixo,
                       &record.Fixo2,
                       &record.Email,
                       &record.Email2,
                       &record.PaginaWEB,
                       &record.CEP,
                       &record.UF,
                       &record.Cidade,
                       &record.Bairro,
                       &record.Logradouro,
                       &record.Numero,
                       &record.Complemento, 
                       &record.Observacao, 
                       &record.CanalSMS,
                       &record.CanalEmail,
                       &record.DataHoraCadastro,
                       &record.DataUltimaMsg,
                   )

    arr = append(arr, record) 
  }

  jsonB, errMarshal := json.Marshal(arr)
  util.CheckErr(errMarshal)
  fmt.Printf("\nLISTA CONTATOS PJ OBTIDA\n\n")
  fmt.Fprintf(w, "%s", string(jsonB))
}


// Update

func UpdateByID_PJ(w http.ResponseWriter, r *http.Request) {
    var record RecordPJ
    body, err := ioutil.ReadAll(r.Body)

    if err != nil {
      panic(fmt.Sprintf("%s", r.Body))
    }

    fmt.Println("\nDADOS RECEBIDOS PARA EDIÇÃO:\n" + string(body) + "\n\n")
    err = json.Unmarshal(body, &record)

    record.Celular = util.RemoveMascaraDeCampoNumerico(record.Celular)
    record.Fixo = util.RemoveMascaraDeCampoNumerico(record.Fixo)
    record.Fixo2 = util.RemoveMascaraDeCampoNumerico(record.Fixo2)
    record.CNPJ = util.RemoveMascaraDeCampoNumerico(record.CNPJ)
    record.CEP = util.RemoveMascaraDeCampoNumerico(record.CEP)

    if err != nil {
      fmt.Printf("erro %s",err)
    }

    stmt := util.PrepareDB(`UPDATE contato_pj SET (
                                razaoSocial,
                                alias,
                                cnpj,
                                celular,
                                fixo,
                                fixo2,
                                email,
                                email2,
                                paginaweb,
                                cep,
                                uf,
                                cidade,
                                bairro,
                                logradouro,
                                numero,
                                complemento,
                                observacao,
                                canalSMS,
                                canalEmail
                            ) = (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
                            WHERE id = ?`)

    result, errExec := stmt.Exec(
                    record.RazaoSocial,
                    record.Alias,
                    record.CNPJ,
                    record.Celular,
                    record.Fixo,
                    record.Fixo2,
                    record.Email,
                    record.Email2,
                    record.PaginaWEB,
                    record.CEP,
                    record.UF,
                    record.Cidade,
                    record.Bairro,
                    record.Logradouro,
                    record.Numero,
                    record.Complemento,
                    record.CanalSMS,
                    record.CanalEmail,
                    record.Observacao,
                    record.Id,
    )

    util.CheckErr(errExec)
    newID, errLast := result.LastInsertId()
    util.CheckErr(errLast)
    record.Id = newID
    jsonB, errMarshal := json.Marshal(record)
    util.CheckErr(errMarshal)
    fmt.Fprintf(w, "%s", string(jsonB))
}


//Insert

func Insert_PJ(w http.ResponseWriter, r *http.Request) {
    var record RecordPJ
    body, err := ioutil.ReadAll(r.Body)

    if err != nil {
      panic(fmt.Sprintf("%s", r.Body))
    }

    fmt.Println("\nDADOS RECEBIDOS PARA CADASTRO:\n" + string(body) + "\n\n")
    err = json.Unmarshal(body, &record)

    record.Celular = util.RemoveMascaraDeCampoNumerico(record.Celular)
    record.Fixo = util.RemoveMascaraDeCampoNumerico(record.Fixo)
    record.Fixo2 = util.RemoveMascaraDeCampoNumerico(record.Fixo2)
    record.CEP = util.RemoveMascaraDeCampoNumerico(record.CEP)
    record.CNPJ = util.RemoveMascaraDeCampoNumerico(record.CNPJ)

    if err != nil {
      fmt.Printf("erro %s",err)
    }

    stmt := util.PrepareDB(`INSERT INTO contato_pj (
                                dataHoraCadastro,
                                razaoSocial,
                                alias,
                                cnpj,
                                celular,
                                fixo,
                                fixo2,
                                email,
                                email2,
                                paginaweb,
                                cep,
                                uf,
                                cidade,
                                bairro,
                                logradouro,
                                numero,
                                complemento,
                                canalSMS,
                                canalEmail,
                                observacao
                            ) 
                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`)

    record.DataHoraCadastro = time.Now().Format("2006-01-02 15:04:05")

    result, errExec := stmt.Exec(
                    record.DataHoraCadastro,
                    record.RazaoSocial,
                    record.Alias,
                    record.CNPJ,
                    record.Celular,
                    record.Fixo,
                    record.Fixo2,
                    record.Email,
                    record.Email2,
                    record.PaginaWEB,
                    record.CEP,
                    record.UF,
                    record.Cidade,
                    record.Bairro,
                    record.Logradouro,
                    record.Numero,
                    record.Complemento,
                    record.CanalSMS,
                    record.CanalEmail,
                    record.Observacao,
    )

    util.CheckErr(errExec)
    newID, errLast := result.LastInsertId()
    util.CheckErr(errLast)
    record.Id = newID
    jsonB, errMarshal := json.Marshal(record)
    util.CheckErr(errMarshal)
    fmt.Fprintf(w, "%s", string(jsonB))
}


//Delete

func DeleteByID_PJ(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
    fmt.Println("ID para exclusão -> " + id)
    stmt := util.PrepareDB("DELETE FROM contato_pj WHERE id = ?")
    result, errExec := stmt.Exec(id)
    util.CheckErr(errExec)
    rowAffected, errRow := result.RowsAffected()
    util.CheckErr(errRow)
    fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}






// API DE SMS ==========================================================================================

type CredenciaisSMS struct {
    conta string
    codigo string
}

func GetCredentials(w http.ResponseWriter, r *http.Request) {
    var a [2]string
    a[0] = "ado.api"
    a[1] = "CVcyUipMTC"
    jsonB, errMarshal := json.Marshal(a)
    util.CheckErr(errMarshal)
    fmt.Printf("\nCredenciais SMS -> %s\n\n",string(jsonB))
    fmt.Fprintf(w, "%s", string(jsonB))
}


//FIM
