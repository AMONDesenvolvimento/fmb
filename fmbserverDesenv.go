package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
        "strings"
	"log"
	"net/http"
	"strconv"
        "time"
        "github.com/gorilla/mux"
        "github.com/gorilla/securecookie"
	_ "github.com/mattn/go-sqlite3"
        "io/ioutil"
        "./domain_lancamento"
        "./domain_orcamento"
        "./domain_evento"
        "./domain_infografico"
//        "./domain_mensageria2"
        "./domain_eventom"
        "./domain_destinatario"
        "./domain_configuracao"
	"./domain_Despesas_Diretoria"
	"./domain_Despesas_Assessorias"
    "./domain_relatorio"
)

var cookieHandler = securecookie.New(
    securecookie.GenerateRandomKey(64),
    securecookie.GenerateRandomKey(32))

type Route struct {
    Name string
    Method string
    Pattern string
    HandlerFunc http.HandlerFunc
}
type Routes [] Route

var routes  = Routes {
    Route {"perfilDeleteByID","DELETE","/perfil/{id}",perfilDeleteByID,
    },
    Route {"perfilGetByID","GET","/perfil/{id}",perfilGetByID,
    },
    Route {"perfilGetAll","GET","/perfil",perfilGetAll,
    },
    Route {"perfilUpdateByID","PUT","/perfil/{id}",perfilUpdateByID,
    },
    Route {"perfilInsert","POST","/perfil",perfilInsert,
    },


    Route {"configuracaoDeleteByID","DELETE","/configuracao/{id}",domain_configuracao.DeleteByID,
    },
    Route {"configuracaoGetByID","GET","/configuracao/{id}",domain_configuracao.GetByID,
    },
    Route {"configuracaoGetAll","GET","/configuracao",domain_configuracao.GetAll,
    },
    Route {"configuracaoUpdateByID","PUT","/configuracao/{id}",domain_configuracao.UpdateByID,
    },
    Route {"configuracaoInsert","POST","/configuracao",domain_configuracao.Insert,
    },


    Route {"usuarioDeleteByID","DELETE","/usuario/{id}",usuarioDeleteByID,
    },
    Route {"usuarioGetByID","GET","/usuario/{id}",usuarioGetByID,
    },
    Route {"usuarioGetAll","GET","/usuario",usuarioGetAll,
    },
    Route {"usuarioUpdateByID","PUT","/usuario/{id}",usuarioUpdateByID,
    },
    Route {"usuarioInsert","POST","/usuario",usuarioInsert,
    },


    //Serviços REST de mensageria
    /*Route {"mensageriaGetAll_PF", "GET", "/mensageriaPF", domain_mensageria.GetAll_PF,
    },
    Route {"mensageriaGetAll_PJ", "GET", "/mensageriaPJ", domain_mensageria.GetAll_PJ,
    },
    Route {"mensageriaGetByID_PF","GET","/mensageriaPF/{id}",domain_mensageria.GetByID_PF,
    },
    Route {"mensageriaGetByID_PJ","GET","/mensageriaPJ/{id}",domain_mensageria.GetByID_PJ,
    },
    Route {"mensageriaDeleteByID_PF","DELETE","/mensageriaPF/{id}",domain_mensageria.DeleteByID_PF,
    },
    Route {"mensageriaDeleteByID_PJ","DELETE","/mensageriaPJ/{id}",domain_mensageria.DeleteByID_PJ,
    },
    Route {"mensageriaInsert_PF","POST","/mensageriaPF",domain_mensageria.Insert_PF,
    },
    Route {"mensageriaInsert_PJ","POST","/mensageriaPJ",domain_mensageria.Insert_PJ,
    },
    Route {"mensageriaUpdateByID_PF","PUT","/mensageriaPF/{id}",domain_mensageria.UpdateByID_PF,
    },
    Route {"mensageriaUpdateByID_PJ","PUT","/mensageriaPJ/{id}",domain_mensageria.UpdateByID_PJ,
    },
    Route {"mensageriaGetCredenciais","GET","/mensageriaGetCredenciais",domain_mensageria.GetCredentials,
    },*/
    //IMPLEMENTAÇÃO 2 MENSAGERIA
/*    Route {"mensageriaGetAll_PF", "GET", "/mensageriaPF", domain_mensageria2.GetAll_PF,
    },
    Route {"mensageriaGetByID_PF", "GET", "/mensageriaPF/{id}", domain_mensageria2.GetByID_PF,
    },
    Route {"mensageriaInsert_PF","POST","/mensageriaPF",domain_mensageria2.Insert_PF,
    },*/


    Route {"eventomDeleteByID","DELETE","/eventom/{id}",domain_eventom.DeleteByID,
    },
    Route {"eventomGetByID","GET","/eventom/{id}",domain_eventom.GetByID,
    },
    Route {"eventomGetAll","GET","/eventom",domain_eventom.GetAll,
    },
    Route {"eventomUpdateByID","PUT","/eventom/{id}",domain_eventom.UpdateByID,
    },
    Route {"eventomInsert","POST","/eventom",domain_eventom.Insert,
    },


    Route {"destinatarioDeleteByID","DELETE","/destinatario/{id}",domain_destinatario.DeleteByID,
    },
    Route {"destinatarioGetByID","GET","/destinatario/{id}",domain_destinatario.GetByID,
    },
    Route {"destinatarioGetAll","GET","/destinatario",domain_destinatario.GetAll,
    },
    Route {"destinatarioUpdateByID","PUT","/destinatario/{id}",domain_destinatario.UpdateByID,
    },
    Route {"destinatarioInsert","POST","/destinatario",domain_destinatario.Insert,
    },


    Route {"infograficoDeleteByID","DELETE","/infografico/{id}",domain_infografico.DeleteByID,
    },
    Route {"infograficoGetByID","GET","/infografico/{id}",domain_infografico.GetByID,
    },
    Route {"infograficoGetAll","GET","/infografico",domain_infografico.GetAll,
    },
    Route {"infograficoGetMenu","GET","/infograficomenu",domain_infografico.GetMenu,
    },
    Route {"infograficoUpdateByID","PUT","/infografico/{id}",domain_infografico.UpdateByID,
    },
    Route {"infograficoInsert","POST","/infografico",domain_infografico.Insert,
    },
    Route {"infograficoData","POST","/infograficodata",domain_infografico.Data,
    },

    Route {"projetoDeleteByID","DELETE","/projeto/{id}",projetoDeleteByID,
    },
    Route {"projetoGetByID","GET","/projeto/{id}",projetoGetByID,
    },
    Route {"projetoGetAll","GET","/projeto",projetoGetAll,
    },
    Route {"projetoUpdateByID","PUT","/projeto/{id}",projetoUpdateByID,
    },
    Route {"projetoInsert","POST","/projeto",projetoInsert,
    },

    Route {"lancamentoDeleteByID","DELETE","/lancamento/{id}",domain_lancamento.DeleteByID,
    },
    Route {"lancamentoGetByID","GET","/lancamento/{id}",domain_lancamento.GetByID,
    },
    Route {"lancamentoGetAll","GET","/lancamento",domain_lancamento.GetAll,
    },
    Route {"lancamentoGetByFilter","POST","/lancamentobyfilter",domain_lancamento.GetByFilter,
    },
    Route {"lancamentoUpdateByID","PUT","/lancamento/{id}",domain_lancamento.UpdateByID,
    },
    Route {"lancamentoInsert","POST","/lancamento",domain_lancamento.Insert,
    },
    Route {"lancamentoList","POST","/lancamentolist",domain_lancamento.List,
    },
    /*Route {"lancamentoRelatorio","POST","/lancamentorelatorio",domain_lancamento.Relatorio,
    },*/


    Route {"temaDeleteByID","DELETE","/tema/{id}",temaDeleteByID,
    },
    Route {"temaGetByID","GET","/tema/{id}",temaGetByID,
    },
    Route {"temaGetAll","GET","/tema",temaGetAll,
    },
    Route {"temaUpdateByID","PUT","/tema/{id}",temaUpdateByID,
    },
    Route {"temalInsert","POST","/tema",temaInsert,
    },


    Route {"eventoDeleteByID","DELETE","/evento/{id}",domain_evento.DeleteByID,
    },
    Route {"eventoGetByID","GET","/evento/{id}",domain_evento.GetByID,
    },
    Route {"eventoGetByIDLanc","GET","/eventol/{id}",domain_evento.GetByIDLanc,
    },
    Route {"eventoGetAll","GET","/evento",domain_evento.GetAll,
    },
    Route {"eventoUpdateByID","PUT","/evento/{id}",domain_evento.UpdateByID,
    },
    Route {"eventoInsert","POST","/evento",domain_evento.Insert,
    },


     Route {"gerarRelatorio","POST","/relatorio/{relatorio}",domain_relatorio.GetRelatorio,
    },


    //traz eventos da diretoria
    Route {"eventoGetByDiretoria","GET","/eventoD",domain_evento.GetByDiretoria,
    },

    Route {"planodecontasGetByCodred","GET","/planodecontasbycodred/{id}",planodecontasGetByCodred,
    },
    Route {"temaOrcamentoGenerate","POST","/temaorcamentogenerate",temaOrcamentoGenerate,
    },


    Route {"orcamentoGetAll","GET","/orcamento",domain_orcamento.GetAll,
    },
    Route {"orcamentoGetByExercicio","GET","/orcamento/{exercicio}",domain_orcamento.GetByExercicio,
    },
    Route {"orcamentoGetByIDExercicio","GET","/orcamento/{exercicio}/{id}",domain_orcamento.GetByIDExercicio,
    },
    Route {"orcamentoGetByID","GET","/orcamento/{id}",domain_orcamento.GetByID,
    },
    Route {"orcamentoUpdateByExercicioCodred","PUT","/orcamento/{exercicio}/{codred}",domain_orcamento.UpdateByExercicioCodred,
    },
    Route {"orcamentoList","POST","/orcamentolist",domain_orcamento.List,
    },
    Route {"orcamentoGetReceita","POST","/orcamentogetreceita",domain_orcamento.GetReceita,
    },
    Route {"orcamentoGetReceitaBubble","POST","/orcamentoreceitabubble",domain_orcamento.GetReceitaBubble,
    },
    Route {"orcamentoGetReceitaMensal","POST","/orcamentoreceitamensal",domain_orcamento.GetReceitaMensal,
    },
    Route {"orcamentoGetReceitaMensalDashBoard","POST","/orcamentoreceitamensaldashboard",domain_orcamento.GetReceitaMensalDashboard,
    },
    Route {"orcamentoGetDespesaBubble","POST","/orcamentodespesabubble",domain_orcamento.GetDespesaBubble,
    },
    Route {"orcamentoGetDespesaPartition","POST","/orcamentodespesapartition",domain_orcamento.GetDespesaPartition,
    },
    Route {"orcamentoGetDespesa","POST","/orcamentodespesa",domain_orcamento.GetDespesa,
    },
    Route {"orcamentoGetDespesaMensal","POST","/orcamentodespesamensal",domain_orcamento.GetDespesaMensal,
    },
    Route {"orcamentoGetDespesaMensalDashboard","POST","/orcamentodespesamensaldashboard",domain_orcamento.GetDespesaMensalDashboard,
    },
    Route {"orcamentoGetTipoDespesa","POST","/orcamentotipodespesa",domain_orcamento.GetTipoDespesa,
    },
    Route {"orcamentoGetTipoDespesaMensal","POST","/orcamentotipodespesamensal",domain_orcamento.GetTipoDespesaMensal,
    },
    Route {"orcamentoGetTipoDespesaMensalDashboard","POST","/orcamentotipodespesamensaldashboard",domain_orcamento.GetTipoDespesaMensalDashboard,
    },
    Route {"orcamentoGetTipoDespesaMensalBar","POST","/c3-master/htdocs/samples/orcamentotipodespesamensalbar",domain_orcamento.GetTipoDespesaMensalBar,
    },
    Route {"orcamentoGetOrcamento","POST","/orcamentoorcamento",domain_orcamento.GetOrcamento,
    },
    Route {"orcamentoGetOrcamentoTree","POST","/weightedTree/orcamentotree",domain_orcamento.GetOrcamentoTree,
    },
    Route {"orcamentoGetOrcamentoTree","POST","/weightedTree_Despesas_Diretoria/orcamentotree",domain_Despesas_Diretoria.GetOrcamentoTree,
    },
    Route {"orcamentoGetOrcamentoTree","POST","/weightedTree_Despesas_Assessorias/orcamentotree",domain_Despesas_Assessorias.GetOrcamentoTree,
    },
    Route {"orcamentoDeleteByID","DELETE","/orcamento/{id}",domain_orcamento.DeleteByID,
    },
    Route {"orcamentoInsert","POST","/orcamento/",domain_orcamento.Insert,
    },
    Route {"orcamentoAnoSeguinte","PUT","/orcamento/",domain_orcamento.CopiaOrcamento,
    },
}

func NewRouter() *mux.Router {
    router := mux.NewRouter().StrictSlash(true)
    for _, route := range routes {
        router.Methods(route.Method).
        Path(route.Pattern).
        Name(route.Name).
        Handler(route.HandlerFunc)
    }
/*
    router.HandleFunc("/",indexPageHandler)
    router.HandleFunc("/internal",internalPageHandler)
    router.HandleFunc("/login",loginHandler).Methods("POST")
    router.HandleFunc("/logout",logoutHandler).Methods("POST")
*/
    return router
}


var layout string = "2006-01-02T15:04:05.000Z"
//var Agora time.Time

type RetornoLista struct {
     Arquivo string `json:"arquivo"`
     Status  string  `json:"status"`
}

type Projeto struct {
	ID   int64  `json:"id"`
        IdPai     int64  `json:"idpai"`
        Exercicio int64   `json:"exercicio"`
        Conta     string `json:"conta"`
	Descricao string `json:"descricao"`
        Acao      string `json:"acao"`
        DataInicio time.Time  `json:"datainicio"`
        DataFim    time.Time  `json:"datafim"`
        Orcamento  float64    `json:"orcamento"`
        Despesa    float64    `json:"despesa"`
        Meta       float64    `json:"meta"`
        Realizado  float64    `json:"realizado"`
        Unidade    string     `json:"unidade"`
        Status     string     `json:"status"`
        DataCriacao string     `json:"datacriacao"`
        DataExclusao string    `json:"dataexclusao"`
}

/*
type Infografico struct {
	ID   int64  `json:"id"`
        Descricao     string  `json:"descricao"`
        Tema string `json:"tema"`
        Perspectivas string `json:"perspectivas"`
        Variaveis    string    `json:"variaveis"`
        Filtros string  `json:"filtros"`
        Tipo string   `json:"tipo"`
        Visibilidade string   `json:"visibilidade"`
        Abrangencia  string     `json:"abrangencia"`
        DataCriacao string     `json:"datacriacao"`
        DataExclusao string    `json:"dataexclusao"`
}
*/

type Tema struct {
	ID   int64  `json:"id"`
        Nome string `json:"nome"`
        Descricao     string  `json:"descricao"`
        Perspectivas string `json:"perspectivas"`
        Variaveis    string    `json:"variaveis"`
        Filtros string  `json:"filtros"`
        UltimaGeracao string   `json:"ultimageracao"`
        DataCriacao string     `json:"datacriacao"`
        DataExclusao string    `json:"dataexclusao"`
}

type TemaOrcamento struct {
        ID int64 `json:"id"`
        Exercicio int64 `json:"exercicio"`
        Conta1 string `json:"conta1"`
        Descricao1 string `json:"descricao1"`
        Conta2 string `json:"conta2"`
        Descricao2 string `json:"descricao2"`
        Conta3 string `json:"conta3"`
        Descricao3 string `json:"descricao3"`
        Conta4 string `json:"conta4"`
        Descricao4 string `json:"descricao4"`
        Orcamento float64 `json:"orcamento"`
        Realizado float64 `json:"realizado"`
}

type TemaDespesa struct {
        ID int64 `json:"id"`
        Exercicio int64 `json:"exercicio"`
        Mes       int64 `json:"mes"`
        Tipo     string `json:"tipo"`
        Conta1 string `json:"conta1"`
        Descricao1 string `json:"descricao1"`
        Conta2 string `json:"conta2"`
        Descricao2 string `json:"descricao2"`
        Conta3 string `json:"conta3"`
        Descricao3 string `json:"descricao3"`
        Conta4 string `json:"conta4"`
        Descricao4 string `json:"descricao4"`
        RegimeCaixa float64 `json:"regimecaixa"`
        RegimeCompetencia float64 `json:"regimecompetencia"`
}

type PlanoDeContas struct {
        ID int64 `json:"id"`
        IdPai     int64  `json:"idpai"`
        Exercicio int64  `json:"exercicio"`
        Conta     string `json:"conta"`
        Codred     int64  `json:"codred"`
	Descricao string `json:"descricao"`
        Analitica      string `json:"analitica"`
        SaldoAnterior float64 `json:"saldoanterior"`
        MovimentoDebito float64 `json:"movimentodebito"`
        MovimentoCredito float64 `json:"movimentocredito"`
        Orcamento  float64    `json:"orcamento"`
        DataCriacao string `json:"datacriacao"`
        DataExclusao string `json:"dataexclusao"`
     
}


type Perfil struct {
        ID int64    `json: "id"`
        Nome      string `json:"nome"`
        Descricao string `json: "descricao"`
        Login     string `json: "login"`
        Emissor    string `json: "emissor"`
        Destinatario string `json: "destinatario"`
        Responsavel  string `json: "responsavel"`
        DataCriacao string `json:"datacriacao"`
        DataExclusao string `json:"dataexclusao"`
//      DataCriacao time.Time `json:"datacriacao"`
//      DataExclusao time.Time `json:"dataexclusao"`
}

type Usuario struct {
        ID int64    `json: "id"`
        Nome      string `json:"nome"`
        Reduzido string `json: "reduzido"`
        Perfil     int64 `json: "perfil"`
        NomePerfil string `json: "nomeperfil"`
        CpfCnpj   string `json: "cpfcnpj"`
        Dia        int64 `json: "dia"`
        Mes        int64 `json: "mes"`
        Endereco    string `json: "endereco"`
        Telefones    string `json: "telefones"`
        Email        string `json: "email"`
        DataCriacao string `json:"datacriacao"`
        DataExclusao string `json:"dataexclusao"`
}

type UsuarioFiltro struct {
        ID string    `json: "id"`
        Nome      string `json:"nome"`
        Perfil     int64 `json: "perfil"`
        CpfCnpj   string `json: "cpfcnpj"`
}

type ProjetoArr []Projeto
type TemaArr []Tema

type PerfilArr []Perfil
type UsuarioArr []Usuario

type PlanoDeContasArr []PlanoDeContas


var mainDB *sql.DB

func maxAgeHandler(seconds int, h http.Handler) http.Handler{
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
       w.Header().Add("Cache-Control", fmt.Sprintf("max-age=%d,public,must-revalidate, proxy-revalidate",seconds))
       h.ServeHTTP(w,r)
       })
}
func main() {
	db, errOpenDB := sql.Open("sqlite3", "fmbtransparencia.db")
	checkErr(errOpenDB)
	mainDB = db

    r := NewRouter()
//  r.PathPrefix("/").Handler(http.StripPrefix("/",http.FileServer(http.Dir("static/"))))
  
    r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
    http.Handle("/static/", maxAgeHandler(60,http.FileServer(http.Dir("public")))) //retirar se nao der certo
    http.Handle("/",r)
var porta = 8000;

    log.Println(fmt.Sprintf("FMBSERVER v 1.6 (released at 2017-ago-16) running on %d- Amon(C) Todos os direitos reservados",porta))
    http.ListenAndServe(fmt.Sprintf(":%d",porta),r)
}

const indexPage = `
<h1>Login</h1>
<form method="post" action="/login">
    <label for="name">User name</label>
    <input type="text" id="name" name="name">
    <label for="password">Password</label>
    <input type="password" id="password" name="password">
    <button type="submit">Login</form>
</form>
`
func indexPageHandler(response http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(response, indexPage)
}

const internalPage = `
<h1>Internal</h1>
<hr>
<small>User: %s</small>
<form method="post" action="/logout">
    <button type="submit">Logout</button>
</form>
`

func internalPageHandler(response http.ResponseWriter, request *http.Request) {
    userName := getUserName(request)
    if userName != "" {
        fmt.Fprintf(response, internalPage, userName)
    } else {
        http.Redirect(response, request, "/", 302)
    }
}

func loginHandler(response http.ResponseWriter, request *http.Request) {
    name := request.FormValue("name")
    pass := request.FormValue("password")
    redirectTarget := "/"
    if name != "" && pass != "" {
       // ...check credentials...
       setSession(name, response)
//     redirectTarget = "/internal"
       redirectTarget = "/BubbleMenu2"
    }
    http.Redirect(response, request, redirectTarget, 302)
}

func logoutHandler(response http.ResponseWriter, request *http.Request) {
    clearSession(response)
    http.Redirect(response, request, "/", 302)
}

func setSession(userName string, response http.ResponseWriter) {
    value := map[string]string{
        "name" : userName,
    }
    if encoded, err := cookieHandler.Encode("session",value); err == nil {
        cookie := &http.Cookie {
            Name : "session",
            Value: encoded,
            Path: "/",
        }
        http.SetCookie(response, cookie)
    }
}

func getUserName(request *http.Request) (userName string) {
    if cookie, err := request.Cookie("session"); err == nil {
        cookieValue := make(map[string]string)
        if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
            userName = cookieValue["name"]
        }
    }
    return userName
}

func clearSession(response http.ResponseWriter) {
    cookie := &http.Cookie{
        Name: "session",
        Value: "",
        Path: "",
        MaxAge: -1,
    }
    http.SetCookie(response, cookie)
}

///////////////////////////////////////////////////////////////////////////////
func projetoGetAll(w http.ResponseWriter, r *http.Request) {
	rows, err := mainDB.Query("SELECT * FROM projeto")
	checkErr(err)
	var projetoArr ProjetoArr
	for rows.Next() {
		var projeto Projeto
		err = rows.Scan(
                               &projeto.ID,
                               &projeto.IdPai,
                               &projeto.Conta,
                               &projeto.Descricao,
                               &projeto.Acao,
                               &projeto.DataInicio,
                               &projeto.DataFim,
                               &projeto.Orcamento,
                               &projeto.Despesa,
                               &projeto.Meta,
                               &projeto.Realizado,
                               &projeto.Unidade,
                               &projeto.Status,
                               &projeto.DataCriacao,
                               &projeto.DataExclusao,
                               )
		//checkErr(err)
		projetoArr = append(projetoArr, projeto)
	}
	jsonB, errMarshal := json.Marshal(projetoArr)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func projetoGetByID(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get(":id")
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare(" SELECT * FROM projeto where id = ?")
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var projeto Projeto
	for rows.Next() {
		err = rows.Scan(&projeto.ID,
                                &projeto.IdPai,
                                &projeto.Conta,
                                &projeto.Descricao,
                                &projeto.Acao,
                                &projeto.DataInicio,
                                &projeto.DataFim,
                                &projeto.Orcamento,
                                &projeto.Despesa,
                                &projeto.Meta,
                                &projeto.Realizado,
                                &projeto.Unidade,
                                &projeto.Status,
                                &projeto.DataCriacao,
                                &projeto.DataExclusao)
		checkErr(err)
	}
	jsonB, errMarshal := json.Marshal(projeto)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func projetoInsert(w http.ResponseWriter, r *http.Request) {
        //agora := time.Now()
        idPai := r.FormValue("idpai")
        conta := r.FormValue("conta")
	descricao := r.FormValue("descricao")
        acao      := r.FormValue("acao")
        dataInicio:= r.FormValue("datainicio")
        dataFim   := r.FormValue("datafim")
        orcamento := r.FormValue("orcamento")
        despesa   := r.FormValue("despesa")
        meta      := r.FormValue("meta")
        realizado := r.FormValue("realizado")
        unidade   := r.FormValue("unidade")
        status    := r.FormValue("status")
        dataCriacao := time.Now().Format("2006-01-02 15:04:05")
	var projeto Projeto
        projeto.IdPai, _ = strconv.ParseInt(idPai,10,0)
        projeto.Conta = conta
	projeto.Descricao = descricao
        projeto.Acao = acao
        projeto.DataInicio, _ = time.Parse(layout,dataInicio)
        projeto.DataFim, _    = time.Parse(layout,dataFim)
        projeto.Orcamento, _ = strconv.ParseFloat(orcamento, 64)
        projeto.Despesa, _   = strconv.ParseFloat(despesa,64)
        projeto.Meta, _      = strconv.ParseFloat(meta, 64)
        projeto.Realizado, _ = strconv.ParseFloat(realizado, 64)
        projeto.Unidade   = unidade
        projeto.Status    = status
        projeto.DataCriacao = dataCriacao
	stmt, err := mainDB.Prepare(
                                   `INSERT INTO projeto (
                                   idpai,
                                   conta,
                                   descricao,
                                   acao,
                                   datainicio,
                                   datafim,
                                   orcamento,
                                   despesa,
                                   meta,
                                   realizado,
                                   unidade,
                                   status,
                                   datacriacao) values (?,?,?,?,?,?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           projeto.IdPai,
                           projeto.Conta,
                           projeto.Descricao,
                           projeto.Acao,
                           projeto.DataInicio,
                           projeto.DataFim,
                           projeto.Orcamento,
                           projeto.Despesa,
                           projeto.Meta,
                           projeto.Realizado,
                           projeto.Unidade,
                           projeto.Status,
                           projeto.DataCriacao,
                           )
	checkErr(errExec)
	newID, errLast := result.LastInsertId()
	checkErr(errLast)
	projeto.ID = newID
	jsonB, errMarshal := json.Marshal(projeto)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func projetoUpdateByID(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get(":id")
        idPai := r.FormValue("idpai")
        conta := r.FormValue("conta")
	descricao := r.FormValue("descricao")
        acao      := r.FormValue("acao")
        dataInicio:= r.FormValue("datainicio")
        dataFim   := r.FormValue("datafim")
        orcamento := r.FormValue("orcamento")
        despesa   := r.FormValue("despesa")
        meta      := r.FormValue("meta")
        realizado := r.FormValue("realizado")
        unidade   := r.FormValue("unidade")
        status    := r.FormValue("status")

	var projeto Projeto
	ID, _ := strconv.ParseInt(id, 10, 0)
	projeto.ID = ID
        projeto.IdPai, _ = strconv.ParseInt(idPai,10,0)
        projeto.Conta = conta
	projeto.Descricao = descricao
        projeto.Acao = acao
        projeto.DataInicio, _ = time.Parse(layout, dataInicio)
        projeto.DataFim, _ = time.Parse(layout, dataFim)
        projeto.Orcamento, _ = strconv.ParseFloat(orcamento,64)
        projeto.Despesa, _ = strconv.ParseFloat(despesa, 64)
        projeto.Meta, _ = strconv.ParseFloat(meta, 64)
        projeto.Realizado, _ = strconv.ParseFloat(realizado,64)
        projeto.Unidade = unidade
        projeto.Status = status
	stmt, err := mainDB.Prepare(`UPDATE projeto
                                    SET idpai = ?,
                                    SET conta = ?,
                                    SET descricao = ?,
                                    SET acao = ?,
                                    SET datainicio = ?,
                                    SET datafim = ?,
                                    SET orcamento = ?,
                                    SET despesa = ?,
                                    SET meta = ?,
                                    SET realizado = ?,
                                    SET unidade = ?,
                                    SET status = ?
                                    WHERE id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  projeto.IdPai,
                                  projeto.Conta,
                                  projeto.Descricao,
                                  projeto.Acao,
                                  projeto.DataInicio,
                                  projeto.DataFim,
                                  projeto.Orcamento,
                                  projeto.Despesa,
                                  projeto.Meta,
                                  projeto.Realizado,
                                  projeto.Unidade,
                                  projeto.Status,
                                  projeto.ID)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(projeto)
		checkErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func projetoDeleteByID(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get(":id")
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare("DELETE FROM projeto WHERE id = ?")
	checkErr(err)
	result, errExec := stmt.Exec(id)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
func temaGetAll(w http.ResponseWriter, r *http.Request) {
	rows, err := mainDB.Query(`SELECT id,
                                          nome,
                                          descricao,
                                          perspectivas,
                                          variaveis,
                                          filtros,
                                          ultimageracao,
                                          datacriacao,
                                          dataexclusao FROM tema`)
	checkErr(err)
	var temaArr TemaArr
	for rows.Next() {
		var tema Tema
		err = rows.Scan(
                               &tema.ID,
                               &tema.Nome,
                               &tema.Descricao,
                               &tema.Perspectivas,
                               &tema.Variaveis,
                               &tema.Filtros,
                               &tema.UltimaGeracao,
                               &tema.DataCriacao,
                               &tema.DataExclusao,
                               )
		//checkErr(err)

		temaArr = append(temaArr, tema)
	}
	jsonB, errMarshal := json.Marshal(temaArr)
	checkErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func temaGetByID(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get(":id")
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare(` SELECT id,
                                             nome,
                                             descricao,
                                             perspectivas,
                                             variaveis,
                                             filtros,
                                             ultimageracao,
                                             datacriacao,
                                             dataexclusao FROM tema where id = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var  tema Tema
	for rows.Next() {
		err = rows.Scan(&tema.ID,
                                &tema.Nome,
                                &tema.Descricao,
                                &tema.Perspectivas,
                                &tema.Variaveis,
                                &tema.Filtros,
                                &tema.UltimaGeracao,
                                &tema.DataCriacao,
                                &tema.DataExclusao)
		//checkErr(err)
	}
	jsonB, errMarshal := json.Marshal(tema)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func temaGetByID2(id int64) Tema {
	stmt, err := mainDB.Prepare(` SELECT id,
                                             nome,
                                             descricao,
                                             perspectivas,
                                             variaveis,
                                             filtros,
                                             ultimageracao,
                                             datacriacao,
                                             dataexclusao FROM tema where id = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var  tema Tema
	for rows.Next() {
		err = rows.Scan(&tema.ID,
                                &tema.Nome,
                                &tema.Descricao,
                                &tema.Perspectivas,
                                &tema.Variaveis,
                                &tema.Filtros,
                                &tema.UltimaGeracao,
                                &tema.DataCriacao,
                                &tema.DataExclusao)
		//checkErr(err)
	}
        return tema
}


func temaInsert(w http.ResponseWriter, r *http.Request) {
        var tema Tema
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &tema)
        if err != nil {
            fmt.Printf("erro %s",err)
//          panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(tema.ID,10))
        log.Println("nome---->"+tema.Nome)
        log.Println("descricao---->"+tema.Descricao)
        log.Println("perspectivas---->"+tema.Perspectivas)
        log.Println("variaveis---->"+tema.Variaveis)
        log.Println("filtros---->"+tema.Filtros)
        log.Println("ultimageracao---->"+tema.UltimaGeracao)
        tema.DataCriacao = time.Now().Format("2006-01-02 15:04:05")


	stmt, err := mainDB.Prepare(
                                   `INSERT INTO tema (
                                   nome,
                                   descricao,
                                   perspectivas,
                                   variaveis,
                                   filtros,
                                   ultimageracao,
                                   datacriacao) values (?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           tema.Nome,
                           tema.Descricao,
                           tema.Perspectivas,
                           tema.Variaveis,
                           tema.Filtros,
                           tema.UltimaGeracao,
                           tema.DataCriacao,
                           )
	checkErr(errExec)
	newID, errLast := result.LastInsertId()
	checkErr(errLast)
	tema.ID = newID
	jsonB, errMarshal := json.Marshal(tema)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func temaUpdateByID(w http.ResponseWriter, r *http.Request) {
      var tema Tema
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &tema)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(tema.ID,10))
        log.Println("nome---->"+tema.Nome)
        log.Println("descricao---->"+tema.Descricao)
        log.Println("perspectivas---->"+tema.Perspectivas)
        log.Println("variaveis---->"+tema.Variaveis)
        log.Println("filtros---->"+tema.Filtros)
        log.Println("ultimageracao---->"+tema.UltimaGeracao)


	stmt, err := mainDB.Prepare(`UPDATE tema
                                    SET (nome, descricao, perspectivas, variaveis, filtros, ultimageracao)=
                                    (?,?,?,?,?,?)
                                    WHERE id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  tema.Nome,
                                  tema.Descricao,
                                  tema.Perspectivas,
                                  tema.Variaveis,
                                  tema.Filtros,
                                  tema.UltimaGeracao,
                                  tema.ID)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(tema)
		checkErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func temaDeleteByID(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get(":id")
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare("DELETE FROM tema WHERE id = ?")
	checkErr(err)
	result, errExec := stmt.Exec(id)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func perfilGetAll(w http.ResponseWriter, r *http.Request) {
	rows, err := mainDB.Query(`SELECT id,
                                          nome,
                                          descricao,
                                          login,
                                          emissor,
                                          destinatario,
                                          responsavel,
                                          datacriacao,
                                          dataexclusao FROM perfil order by id`)
	checkErr(err)
	var perfilArr PerfilArr
	for rows.Next() {
		var perfil Perfil
		err = rows.Scan(
                               &perfil.ID,
                               &perfil.Nome,
                               &perfil.Descricao,
                               &perfil.Login,
                               &perfil.Emissor,
                               &perfil.Destinatario,
                               &perfil.Responsavel,
                               &perfil.DataCriacao,
                               &perfil.DataExclusao,
                               )
		//checkErr(err)
		perfilArr = append(perfilArr, perfil)
	}
	jsonB, errMarshal := json.Marshal(perfilArr)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func perfilGetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare(`SELECT id,
                                            nome,
                                            descricao,
                                            login,
                                            emissor,
                                            destinatario,
                                            responsavel,
                                            datacriacao,
                                            dataexclusao FROM perfil where id = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var perfil Perfil
	for rows.Next() {
		err = rows.Scan(&perfil.ID,
                                &perfil.Nome,
                                &perfil.Descricao,
                                &perfil.Login,
                                &perfil.Emissor,
                                &perfil.Destinatario,
                                &perfil.Responsavel,
                                &perfil.DataCriacao,
                                &perfil.DataExclusao)
		//checkErr(err)
	}
	jsonB, errMarshal := json.Marshal(perfil)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func perfilGetByID2(id int64) Perfil {
//      log.Println("id->"+id)
	stmt, err := mainDB.Prepare(`SELECT id,
                                            nome,
                                            descricao,
                                            login,
                                            emissor,
                                            destinatario,
                                            responsavel,
                                            datacriacao,
                                            dataexclusao FROM perfil where id = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var perfil Perfil
	for rows.Next() {
		err = rows.Scan(&perfil.ID,
                                &perfil.Nome,
                                &perfil.Descricao,
                                &perfil.Login,
                                &perfil.Emissor,
                                &perfil.Destinatario,
                                &perfil.Responsavel,
                                &perfil.DataCriacao,
                                &perfil.DataExclusao)
		//checkErr(err)
	}
        return perfil
}
func perfilInsert(w http.ResponseWriter, r *http.Request) {
	var perfil Perfil
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &perfil)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(perfil.ID,10))
        log.Println("nome---->"+perfil.Nome)
        perfil.DataCriacao = time.Now().Format("2006-01-02 15:04:05")
	stmt, err := mainDB.Prepare(
                                   `INSERT INTO perfil (
                                   id,
                                   nome,
                                   descricao,
                                   login,
                                   emissor,
                                   destinatario,
                                   responsavel,
                                   datacriacao) values (?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           perfil.ID,
                           perfil.Nome,
                           perfil.Descricao,
                           perfil.Login,
                           perfil.Emissor,
                           perfil.Destinatario,
                           perfil.Responsavel,
                           perfil.DataCriacao,
                           )
	checkErr(errExec)
	newID, errLast := result.LastInsertId()
	checkErr(errLast)
	perfil.ID = newID
	jsonB, errMarshal := json.Marshal(perfil)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func perfilUpdateByID(w http.ResponseWriter, r *http.Request) {
	var perfil Perfil
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &perfil)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+string(perfil.ID))
        log.Println("nome---->"+perfil.Nome)



//	ID, _ := strconv.ParseInt(id, 10, 0)
//	perfil.ID = ID
	//perfil.Descricao = descricao
	stmt, err := mainDB.Prepare(`UPDATE perfil
                                    SET (nome, descricao, login, emissor, destinatario, responsavel) =
                                        (?,?,?,?,?,?)
                                    WHERE id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  perfil.Nome,
                                  perfil.Descricao,
                                  perfil.Login,
                                  perfil.Emissor,
                                  perfil.Destinatario,
                                  perfil.Responsavel,
                                  perfil.ID)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(perfil)
		checkErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func perfilDeleteByID(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get(":id")
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare("DELETE FROM perfil WHERE id = ?")
	checkErr(err)
	result, errExec := stmt.Exec(id)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

///////////////////////////////////////////////////////////////////////////////
func usuarioGetAll(w http.ResponseWriter, r *http.Request) {
	rows, err := mainDB.Query(`SELECT id,
                                          nome,
                                          reduzido,
                                          perfil,
                                          cpfcnpj,
                                          dia,
                                          mes,
                                          endereco,
                                          telefones,
                                          email,
                                          datacriacao,
                                          dataexclusao FROM usuario`)
	checkErr(err)
	var usuarioArr UsuarioArr
	for rows.Next() {
		var usuario Usuario
		err = rows.Scan(
                               &usuario.ID,
                               &usuario.Nome,
                               &usuario.Reduzido,
                               &usuario.Perfil,
                               &usuario.CpfCnpj,
                               &usuario.Dia,
                               &usuario.Mes,
                               &usuario.Endereco,
                               &usuario.Telefones,
                               &usuario.Email,
                               &usuario.DataCriacao,
                               &usuario.DataExclusao,
                               )
		//checkErr(err)

		usuarioArr = append(usuarioArr, usuario)
	}
	jsonB, errMarshal := json.Marshal(usuarioArr)
	checkErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func usuarioGetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare(` SELECT id,
                                             nome,
                                             reduzido,
                                             perfil,
                                             cpfcnpj,
                                             dia,
                                             mes,
                                             endereco,
                                             telefones,
                                             email,
                                             datacriacao,
                                             dataexclusao FROM usuario where id = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var  usuario Usuario
	for rows.Next() {
		err = rows.Scan(&usuario.ID,
                                &usuario.Nome,
                                &usuario.Reduzido,
                                &usuario.Perfil,
                                &usuario.CpfCnpj,
                                &usuario.Dia,
                                &usuario.Mes,
                                &usuario.Endereco,
                                &usuario.Telefones,
                                &usuario.Email,
                                &usuario.DataCriacao,
                                &usuario.DataExclusao)
		//checkErr(err)
	}
	jsonB, errMarshal := json.Marshal(usuario)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func usuarioGetByID2(id int64) Usuario {
	stmt, err := mainDB.Prepare(` SELECT id,
                                             nome,
                                             reduzido,
                                             perfil,
                                             cpfcnpj,
                                             dia,
                                             mes,
                                             endereco,
                                             telefones,
                                             email,
                                             datacriacao,
                                             dataexclusao FROM usuario where id = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var  usuario Usuario
	for rows.Next() {
		err = rows.Scan(&usuario.ID,
                                &usuario.Nome,
                                &usuario.Reduzido,
                                &usuario.Perfil,
                                &usuario.CpfCnpj,
                                &usuario.Dia,
                                &usuario.Mes,
                                &usuario.Endereco,
                                &usuario.Telefones,
                                &usuario.Email,
                                &usuario.DataCriacao,
                                &usuario.DataExclusao)
		//checkErr(err)
	}
        return usuario
}


func usuarioInsert(w http.ResponseWriter, r *http.Request) {
        var usuario Usuario
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &usuario)
        if err != nil {
            fmt.Printf("erro %s",err)
//          panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("nome---->"+usuario.Nome)
        usuario.DataCriacao = time.Now().Format("2006-01-02 15:04:05")


	stmt, err := mainDB.Prepare(
                                   `INSERT INTO usuario (
                                   nome,
                                   reduzido,
                                   perfil,
                                   cpfcnpj,
                                   dia,
                                   mes,
                                   endereco,
                                   telefones,
                                   email,
                                   datacriacao) values (?,?,?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           usuario.Nome,
                           usuario.Reduzido,
                           usuario.Perfil,
                           usuario.CpfCnpj,
                           usuario.Dia,
                           usuario.Mes,
                           usuario.Endereco,
                           usuario.Telefones,
                           usuario.Email,
                           usuario.DataCriacao,
                           )
	checkErr(errExec)
	newID, errLast := result.LastInsertId()
	checkErr(errLast)
	usuario.ID = newID
	jsonB, errMarshal := json.Marshal(usuario)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func usuarioUpdateByID(w http.ResponseWriter, r *http.Request) {
      var usuario Usuario
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &usuario)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(usuario.ID,10))
        log.Println("nome---->"+usuario.Nome)


	stmt, err := mainDB.Prepare(`UPDATE usuario
                                    SET (nome, reduzido, perfil, cpfcnpj, dia, mes, endereco, telefones, email)=
                                    (?,?,?,?,?,?,?,?,?)
                                    WHERE id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  usuario.Nome,
                                  usuario.Reduzido,
                                  usuario.Perfil,
                                  usuario.CpfCnpj,
                                  usuario.Dia,
                                  usuario.Mes,
                                  usuario.Endereco,
                                  usuario.Telefones,
                                  usuario.Email,
                                  usuario.ID)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(usuario)
		checkErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func usuarioDeleteByID(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get(":id")
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare("DELETE FROM usuario WHERE id = ?")
	checkErr(err)
	result, errExec := stmt.Exec(id)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func usuarioList(w http.ResponseWriter, r *http.Request) {
        var usuarioFiltro UsuarioFiltro
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &usuarioFiltro)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   


	rows, err := mainDB.Query(`SELECT id,
                                          descricao,
                                          tema,
                                          perspectivas,
                                          variaveis,
                                          filtros,
                                          tipo,
                                          visibilidade,
                                          abrangencia,
                                          datacriacao,
                                          dataexclusao FROM infografico`)
	checkErr(err)
	var usuarioArr UsuarioArr
        var usuario Usuario
	for rows.Next() {
		err = rows.Scan(
                               &usuario.ID,
                               &usuario.Nome,
                               &usuario.Reduzido,
                               &usuario.Perfil,
                               &usuario.CpfCnpj,
                               &usuario.Dia,
                               &usuario.Mes,
                               &usuario.Endereco,
                               &usuario.Telefones,
                               &usuario.Email,
                               &usuario.DataCriacao,
                               &usuario.DataExclusao,
                               )
		//checkErr(err)

		usuarioArr = append(usuarioArr, usuario)
	}

        var contents string = ""
/*
        for _,infografico = range infograficoArr {
           contents+=strconv.FormatInt(infografico.ID, 10)+"\t"
           contents+=infografico.Descricao+"\t"
           contents+=infografico.Tema+"\t"
           contents+=infografico.Perspectivas+"\t"
           contents+=infografico.Variaveis+"\t"
           contents+=infografico.Filtros+"\t"
           contents+=infografico.Tipo+"\t"
           contents+=infografico.Visibilidade+"\t"
           contents+=infografico.Abrangencia+"\t"
           contents+=infografico.DataCriacao+"\t"
           contents+=infografico.DataExclusao+"\n"
         }
*/
        d1 := []byte(contents)
        err = ioutil.WriteFile("infograficolist.txt",d1,0644)
        checkErr(err)
        var retornoLista RetornoLista
        retornoLista.Arquivo="infograficolist.txt"
        retornoLista.Status="ok"
	jsonB, errMarshal := json.Marshal(retornoLista)
	checkErr(errMarshal)
        fmt.Printf("Lista-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}


///////////////////////////////////////////////////////////////////////////////////////
func planodecontasGetByCodred(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get(":id")
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt, err := mainDB.Prepare("SELECT id,idpai,exercicio,conta,codred,descricao,analitica FROM planodecontas where codred = ?")
	checkErr(err)
	rows, errQuery := stmt.Query(id)
	checkErr(errQuery)
	var planoDeContas PlanoDeContas
	for rows.Next() {
		err = rows.Scan(&planoDeContas.ID,
                                &planoDeContas.IdPai,
                                &planoDeContas.Exercicio,
                                &planoDeContas.Conta,
                                &planoDeContas.Codred,
                                &planoDeContas.Descricao,
                                &planoDeContas.Analitica)
//                                &planoDeContas.DataCriacao,
//                                &planoDeContas.DataExclusao)
		//checkErr(err)
        log.Println(planoDeContas.ID)
	}
        log.Println("Descrição : "+planoDeContas.Descricao)
	jsonB, errMarshal := json.Marshal(planoDeContas)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}
func planodecontasRowByCodred(codred int64) string {
	stmt, err := mainDB.Prepare("SELECT id,idpai,exercicio,conta,codred,descricao,analitica FROM planodecontas where codred = ?")
	checkErr(err)
	rows, errQuery := stmt.Query(codred)
	checkErr(errQuery)
	var planoDeContas PlanoDeContas
	for rows.Next() {
		err = rows.Scan(&planoDeContas.ID,
                                &planoDeContas.IdPai,
                                &planoDeContas.Exercicio,
                                &planoDeContas.Conta,
                                &planoDeContas.Codred,
                                &planoDeContas.Descricao,
                                &planoDeContas.Analitica)
//                                &planoDeContas.DataCriacao,
//                                &planoDeContas.DataExclusao)
		//checkErr(err)
        log.Println(planoDeContas.ID)
	}
        log.Println("Descrição : "+planoDeContas.Descricao)
	jsonB, errMarshal := json.Marshal(planoDeContas)
	checkErr(errMarshal)
	return string(jsonB)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
func dateFilterToText(date string) string {
    var r string
    idts:=strings.Split(date,";")
    for _,idt := range idts {
        pos :=strings.Index(idt,"-")
        if pos < 0 {
            r+=idt
        } else {
            r+="de "+string(idt[0:pos])+" até "+string(idt[pos+1])
        }
        r+=" "
    }
    return r
}
//////////////////////////////////////////////////////////////////////////////////////////////////

func temaOrcamentoGenerate(w http.ResponseWriter, r *http.Request) {

}

