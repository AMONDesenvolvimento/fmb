package main

import (
    "database/sql"
  _ "github.com/mattn/go-sqlite3"
    "time"
    "github.com/tealeg/xlsx"
    "fmt"
//  "log"
    "unicode/utf8"
//  "strconv"
    "strings"
)

var mainDB *sql.DB

type Evento struct {
    ID string
    Nome string
    Descricao string 
    Tipo    string 
    Recdes  string
    Debito  string
    Credito string
    Gerencial string
    IsReceita bool 
}

func main() {


    db, errOpenDB := sql.Open("sqlite3", "fmbtransparencia.db")
    checkErr(errOpenDB)
    mainDB = db

    var evento Evento
    var detailNumber int
    excelFileName := "Tabelaeventosfinanceiros2017Mai23FMB.xlsx"
    xlFile, err := xlsx.OpenFile(excelFileName)
    if err != nil {
        panic(err)
    }
    for _, sheet := range xlFile.Sheets {
        for i, row := range sheet.Rows {
            fmt.Printf("ind da linha->%d length->%d",i,len(row.Cells))
            if (len(row.Cells)==0) {
                break;
            } 
            cab, _ := row.Cells[0].String()
            fmt.Printf("Cab da linha->%s",cab)
            if i<3 {
               continue
            }
            if strings.HasPrefix(cab,"ID") {
                if utf8.RuneCountInString(evento.ID)>0 {
                    fmt.Printf("Vai persistir evento: %s",evento.ID)
                    tratarEvento(evento)
                }  
                fmt.Printf("Novo detalhe na linha %d",i)
                detailNumber=0;
                tipo,_ := row.Cells[2].String()
                evento.Tipo = tipo
                if strings.HasPrefix(evento.Tipo,"TIPO") {
                    evento.Tipo=""  
                } 
 
                evento.IsReceita=false;
                continue
            }
            detailNumber++
            for j, cell := range row.Cells {
                text, _ := cell.String()
//              ct :=cell.Type()
                if detailNumber==1 && j==0 {
                    fmt.Printf("[ID %s]",text)
                    evento.ID = strings.Trim(text," ")
                } else if detailNumber==1 && j==1 {
                    fmt.Printf("[Nome %s]",text)
                     evento.Nome = text
                } else if detailNumber==1 && j==2 {
                    fmt.Printf("[Tipo %s]",text)
                    evento.Tipo = text
                } else if detailNumber==1 && j==3 && utf8.RuneCountInString(text)>0 {
                    evento.Recdes = text  
                    fmt.Printf("[Receita %s]",text)
                    evento.IsReceita=true
                } else if detailNumber==1 && j==4 && utf8.RuneCountInString(text)>0 {
                    evento.Recdes = text
                    fmt.Printf("[Despesa %s]",text) 
                } else if detailNumber==1 && j==5 && utf8.RuneCountInString(text)>0 {
                    evento.Gerencial=text
                    fmt.Printf("[Gerencial %s]",text)   
                } else if detailNumber==3 && j==1 {
                    evento.Descricao=text
                    fmt.Printf("[Descrição %s]",text)
                } else if detailNumber==3 && j==3 {
                    evento.Debito=text
                    fmt.Printf("[Debito %s]",text)   
                } else if detailNumber==3 && j==4 {
                    evento.Credito=text
                    fmt.Printf("[Credito %s]",text)
                } else if detailNumber>3 &&  j==1 {
                    evento.Descricao+=text
                    fmt.Printf("[Descricao cont %s]",text)
                }
            }

        }

                if utf8.RuneCountInString(evento.ID)>0 {
                    fmt.Printf("Vai persistir evento final: %s",evento.ID)
                    tratarEvento(evento)
                }  

    }
}

func tratarEvento(evento Evento) {
            if utf8.RuneCountInString(evento.Recdes)==0 {
                evento.Recdes="0"
            } 
            if utf8.RuneCountInString(evento.Debito)==0 {
                evento.Debito="0"
            } 
            if utf8.RuneCountInString(evento.Credito)==0 {
                evento.Credito="0"
            } 
            eventoInsert(evento)
            evento.ID="";
            evento.Nome=""
            evento.Descricao=""
            evento.Recdes=""
            evento.Debito=""
            evento.Credito=""
            evento.Gerencial="" 
            evento.Tipo="" 
            evento.IsReceita=false            
}

func eventoInsert(evento Evento) {
	stmt, err := mainDB.Prepare(
                                   `INSERT INTO evento (
                                   id,
                                   nome,
                                   tipo,  
                                   descricao,
                                   recdes,
                                   debito,
                                   credito,
                                   gerencial,
                                   projeto,  
                                   datacriacao) values (?,?,?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           evento.ID,
                           evento.Nome,
                           evento.Tipo,
                           evento.Descricao,
                           evento.Recdes,
                           evento.Debito,
                           evento.Credito,
                           evento.Gerencial,
                           0,
                           time.Now().Format("2006-01-02 15:04:05"),
                           )
	checkErr(errExec)
//	newID, errLast := result.LastInsertId()
	_, errLast := result.LastInsertId()
	checkErr(errLast)


}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

