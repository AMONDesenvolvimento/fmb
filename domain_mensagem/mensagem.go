package domain_mensagem
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
    "strconv"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
//	gr "github.com/mikeshimura/goreport"
    "../util"
    "../domain_eventom"
    "../domain_destinatario"

)

type Record struct {
	ID   int64  `json:"id"`
        Nome string  `json:"nome"`  
        Descricao     string  `json:"descricao"`
        Evento        int64 `json:"evento"`
        NomeEvento    string `json:"nomevento"`
        DescricaoEvento    string `json:"nomevento"`
        DisparoEvento    string `json:"disparovento"`
        Destinatario  int64 `json:"destinatario"` 
        NomeDestinatario string `json:"nomedestinatario"`
        DescricaoDestinatario string `json:"descricaodestinatario"`
        ExpressaoDestinatario string `json:"expressaodestinatario"`
        Meio    string    `json:"meio"`
        Modelo string  `json:"modelo"`
        DataCriacao string     `json:"datacriacao"` 
        DataExclusao string    `json:"dataexclusao"`
}
type Filter struct {
	ID   string  `json:"id"`
        Nome     string  `json:"nome"`
        Evento        string `json:"evento"`
        Destinatario        string `json:"destinatario"` 
        Meio    string    `json:"meio"`
}

type Arr []Record


//OBTENÇÃO DE PARÂMETROS PARA USO DA API ZENVIA
type CredenciaisSMS struct {
    conta string
    codigo string
}

func GetCredentials(w http.ResponseWriter, r *http.Request) {
    var a [2]string
    a[0] = "ado.api"
    a[1] = "CVcyUipMTC"
    jsonB, errMarshal := json.Marshal(a)
    util.CheckErr(errMarshal)

    fmt.Println(a[0], a[1])
    fmt.Printf("\n\nCredenciais SMS -> %s\n\n",string(jsonB))
    fmt.Fprintf(w, "%s", string(jsonB))
}
//=============================================



func GetAll(w http.ResponseWriter, r *http.Request) {
	rows := util.QueryDB(`SELECT id,
                                          nome,
                                          descricao,
                                          evento,
                                          destinatario,
                                          meio,
                                          modelo,
                                          datacriacao,
                                          dataexclusao FROM mensagem`)
	var arr Arr
	for rows.Next() {
  		var record Record
  		rows.Scan(
                 &record.ID,
                 &record.Nome,
                 &record.Descricao,
                 &record.Evento, 
                 &record.Destinatario,
                 &record.Meio,
                 &record.Modelo,
                 &record.DataCriacao,
                 &record.DataExclusao, 
               )
                eventom :=domain_eventom.RowByID(record.Evento)
                record.NomeEvento = eventom.Nome
                record.DescricaoEvento = eventom.Descricao 
                record.DisparoEvento = eventom.Disparo
                destinatario := domain_destinatario.RowByID(record.Destinatario)
                record.NomeDestinatario = destinatario.Nome
                record.DescricaoDestinatario = destinatario.Descricao
                record.ExpressaoDestinatario = destinatario.Expressao  

		  arr = append(arr, record)
	}

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB(` SELECT id,
                                             nome,
                                             descricao,
                                             evento,
                                             destinatario,
                                             meio,
                                             modelo,
                                             datacriacao,
                                             dataexclusao FROM mensagem where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var  record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome,
                                &record.Descricao,
                                &record.Evento, 
                                &record.Destinatario,
                                &record.Meio,
                                &record.Modelo,
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
        eventom :=domain_eventom.RowByID(record.Evento)
        record.NomeEvento = eventom.Nome
        record.DescricaoEvento = eventom.Descricao 
        record.DisparoEvento = eventom.Disparo
        destinatario := domain_destinatario.RowByID(record.Destinatario)
        record.NomeDestinatario = destinatario.Nome
        record.DescricaoDestinatario = destinatario.Descricao
        record.ExpressaoDestinatario = destinatario.Expressao  

	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func RowByID(id int64) Record {
	stmt := util.PrepareDB(` SELECT id,
                                             nome,
                                             descricao,
                                             evento,
                                             destinatario,
                                             meio,
                                             modelo,
                                             datacriacao,
                                             dataexclusao FROM mensagem where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var  record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome,
                                &record.Descricao,
                                &record.Evento, 
                                &record.Destinatario,
                                &record.Meio,
                                &record.Modelo,
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
        eventom :=domain_eventom.RowByID(record.Evento)
        record.NomeEvento = eventom.Nome
        record.DescricaoEvento = eventom.Descricao 
        record.DisparoEvento = eventom.Disparo
        destinatario := domain_destinatario.RowByID(record.Destinatario)
        record.NomeDestinatario = destinatario.Nome
        record.DescricaoDestinatario = destinatario.Descricao
        record.ExpressaoDestinatario = destinatario.Expressao  
        return record
}


func Insert(w http.ResponseWriter, r *http.Request) {
        var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            fmt.Printf("erro %s",err)
//          panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("nome---->"+record.Nome)
        record.DataCriacao = time.Now().Format("2006-01-02 15:04:05")


	stmt := util.PrepareDB(
                                   `INSERT INTO mensagem (
                                   nome,
                                   descricao,
                                   evento,
                                   destinatario,
                                   meio,
                                   modelo,
                                   datacriacao) values (?,?,?,?,?,?,?)`)
	result, errExec := stmt.Exec(
                           record.Nome, 
                           record.Descricao,
                           record.Evento,
                           record.Destinatario,
                           record.Meio,
                           record.Modelo,
                           record.DataCriacao,
                           )
	util.CheckErr(errExec)
	newID, errLast := result.LastInsertId()
	util.CheckErr(errLast)
	record.ID = newID
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func UpdateByID(w http.ResponseWriter, r *http.Request) {
      var record Record 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("nome---->"+record.Nome)


	stmt := util.PrepareDB(`UPDATE mensagem 
                                    SET (nome,descricao, evento, destinatario, meio, modelo)=
                                    (?,?,?,?,?,?)     
                                    WHERE id = ?`)
	result, errExec := stmt.Exec(
                                  record.Nome,
                                  record.Descricao,
                                  record.Evento,
                                  record.Destinatario,
                                  record.Meio,
                                  record.Modelo,
                                  record.ID)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(record)
		util.CheckErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func DeleteByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB("DELETE FROM mensagem WHERE id = ?")
	result, errExec := stmt.Exec(id)
	util.CheckErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	util.CheckErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func List(w http.ResponseWriter, r *http.Request) {
        var filter Filter 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+filter.ID)
        log.Println("nome---->"+filter.Nome)


	rows := util.QueryDB(`SELECT id,
                                          nome,
                                          descricao,
                                          evento,
                                          destinatario,
                                          meio,
                                          modelo,
                                          datacriacao,
                                          dataexclusao FROM mensagem`)
	var arr Arr
        var record Record
	for rows.Next() {
		rows.Scan(
                               &record.ID,
                               &record.Descricao,
                               &record.Evento, 
                               &record.Destinatario,
                               &record.Meio,
                               &record.Modelo,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}

        var contents string = ""
        for _,record = range arr {
           contents+=strconv.FormatInt(record.ID, 10)+"\t"
           contents+=record.Nome+"\t"
           contents+=record.Descricao+"\t"
           contents+=strconv.FormatInt(record.Evento,10)+"\t"
           contents+=strconv.FormatInt(record.Destinatario,10)+"\t"
           contents+=record.Meio+"\t"
           contents+=record.Modelo+"\t"
           contents+=record.DataCriacao+"\t"
           contents+=record.DataExclusao+"\n"
         } 
        d1 := []byte(contents)
        err = ioutil.WriteFile("mensagemlist.txt",d1,0644)
        util.CheckErr(err)
        var retornoLista util.RetornoLista
        retornoLista.Arquivo="mensagemlist.txt"
        retornoLista.Status="ok"
	jsonB, errMarshal := json.Marshal(retornoLista)
	util.CheckErr(errMarshal)
        fmt.Printf("Lista-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}





