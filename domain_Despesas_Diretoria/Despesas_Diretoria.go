package domain_Despesas_Diretoria
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
    "strconv"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
    "strings"
	gr "github.com/mikeshimura/goreport"
    "../util"
    "../domain_lancamento"

)
type Record struct {
        ID int64 `json:"id"`
        IdPai     int64  `json:"idpai"`
        Exercicio int64  `json:"exercicio"` 
        Conta     string `json:"conta"`
        Codred     int64  `json:"codred"`
	Descricao string `json:"descricao"`
        Analitica      string `json:"analitica"`
        SaldoAnterior float64 `json:"saldoanterior"`
        MovimentoDebitoCaixa float64 `json:"movimentodebitocaixa"`
        MovimentoCreditoCaixa float64 `json:"movimentocreditocaixa"`
        MovimentoDebitoCompetencia float64 `json:"movimentodebitocompetencia"`
        MovimentoCreditoCompetencia float64 `json:"movimentocreditocompetencia"`
        Orcamento  float64    `json:"orcamento"`
        DataCriacao string `json:"datacriacao"` 
        DataExclusao string `json:"dataexclusao"`
}
type ReceitaDiretoria struct {
	Nome  	        string 	 `json:"nome"`
        Descricao       string   `json:"Descricao"`
	Conta     	string   `json:"conta"`
        Valor       	float64  `json:"Valor"` 
 	Total  		float64  `json:"Total"`
}

type Filter struct {
        Exercicio string  `json:"exercicio"` 
        Codred    string  `json:"codred"`
}

type Parameters struct{
    Exercicio string `json:"exercicio"`
    MesReferencia string `json:"mesreferencia"`
    UltimoMes string `json:"ultimomes"`
    Conta string `json:"conta"`
    Regime string `json:"regime"`
    ArvoreCompleta string `json:"arvorecompleta"`
}
type ReceitaReduce struct {
    Conta string `json:"conta"`
    Descricao string `json:"descricao"`
    Valor     float64 `json:"valor"`
}

type ReceitaMensalReduce struct {
    Mes   int64  `json:"mes"`  
    Conta string `json:"conta"`
    Descricao string `json:"descricao"`
    Valor     float64 `json:"valor"`
}
type DespesaReduce struct {
    Conta string `json:"conta"`
    Descricao string `json:"descricao"`
    Valor     float64 `json:"valor"`
}

type DespesaMensalReduce struct {
    Mes   int64  `json:"mes"`  
    Conta string `json:"conta"`
    Descricao string `json:"descricao"`
    Valor     float64 `json:"valor"`
}

type OrcamentoReduce struct {
    Conta string `json:"conta"`
    Descricao string `json:"descricao"`
    Valor     float64 `json:"valor"`
    Realizado float64 `json:"realizado"`
}
type OrcamentoTreeReduce struct {
    Categoria string `json:"categoria"`
    Nivel1 string `json:"nivel1"`
    Nivel2 string `json:"nivel2"`
    Nivel3 string `json:"nivel3"`
    Nivel4 string `json:"nivel4"`
    Valor  float64 `json:"valor"`
    Realizado float64 `json:"realizado"`
    Saldo     float64 `json:"realizado"` 
}

type TipoDespesaReduce struct {
    Fixa float64 `json:"fixa"`
    Variavel float64 `json:"variavel"`
}
type TipoDespesaMensalReduce struct {
    Mes int64 `json:"mes"`
    Fixa float64 `json:"fixa"`
    Variavel float64 `json:"variavel"`
}


type Arr []Record


//var agora time.Time
/*
func GetAll(w http.ResponseWriter, r *http.Request) {
	rows, err := util.MainDB.Query(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM orcamento order by conta`)
	util.CheckErr(err)
	var arr Arr
	for rows.Next() {
		var record Record
		err = rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}*/
///////////////////////////////////////////RECEITA///////////////////////////////////////////////////////////////
func GetRowsReceita(parameters Parameters) [] ReceitaReduce{
	selectText :=`SELECT id,
                             idpai,
                             exercicio,
                             conta,
                             codred,
                             descricao,
                             analitica,
                             saldoanterior,
                             movimentodebitocaixa,
                             movimentocreditocaixa,
                             movimentodebitocompetencia,
                             movimentocreditocompetencia,
                             orcamento,
                             datacriacao,
                             dataexclusao FROM orcamento
                      `
                       
        whereText := generateReceitaWhereText(parameters)

        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
        selectText+=" order by conta"
        log.Println("where text->"+whereText)
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
	var arr [] ReceitaReduce
	for rows.Next() {
		var record Record
                var receitaReduce ReceitaReduce
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                          )
               receitaReduce.Conta = record.Conta
               receitaReduce.Descricao = record.Descricao 
//             intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
                if parameters.Regime == "caixa" {
                receitaReduce.Valor = domain_lancamento.GetSumByContaCaixa(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,
                                                receitaReduce.Conta,
                                                )
                } else {
                receitaReduce.Valor = domain_lancamento.GetSumByContaCompetencia(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,
                                                receitaReduce.Conta,
                                                )
                }
		arr = append(arr, receitaReduce)
	}
        return arr
}

func GetRowsReceitaMensal(parameters Parameters) [] ReceitaMensalReduce{
	selectText :=`SELECT id,
                             idpai,
                             exercicio,
                             conta,
                             codred,
                             descricao,
                             analitica,
                             saldoanterior,
                             movimentodebitocaixa,
                             movimentocreditocaixa,
                             movimentodebitocompetencia,
                             movimentocreditocompetencia,
                             orcamento,
                             datacriacao,
                             dataexclusao FROM orcamento
                      `
                       
        whereText := generateReceitaWhereText(parameters)

        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
        selectText+=" order by conta"
        log.Println("where text->"+whereText)
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
	var arr [] ReceitaMensalReduce
	for rows.Next() {
		var record Record
                var receitaMensalReduce ReceitaMensalReduce
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                          )
               receitaMensalReduce.Conta = record.Conta
               receitaMensalReduce.Descricao = record.Descricao
               arr = append(arr,receitaMensalReduce)
       }           
       var arr2 []ReceitaMensalReduce
//             intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
               intMes,_:=strconv.ParseInt(parameters.UltimoMes,10,64)

       var i int64
       for i=1;i<=intMes;i++ {
           for _,receitaMensalReduce := range arr {         
                    receitaMensalReduce.Mes = i
                    if parameters.Regime =="caixa" {
                    receitaMensalReduce.Valor = domain_lancamento.GetSumByContaCaixa(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    receitaMensalReduce.Conta,
                                                    )
                    } else {
                    receitaMensalReduce.Valor = domain_lancamento.GetSumByContaCompetencia(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    receitaMensalReduce.Conta,
                                                    )
                    }
		    arr2 = append(arr2, receitaMensalReduce)
           }
	}
        return arr2
}

func generateReceitaWhereText(parameters Parameters) string {
    r := ""
    if len(parameters.Exercicio)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleInteger("exercicio", parameters.Exercicio))
    }
    if len(parameters.Conta)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleString("conta", parameters.Conta+"%"))
    } else {
        r = util.ConcatWhere(r,util.GenerateSingleString("conta", "3%"))
    }
    if strings.ToLower(parameters.ArvoreCompleta)=="n" {
        dots := strings.Count(parameters.Conta,".")
        if dots==0 {
            r = util.ConcatWhere(r,"length(conta) <=3 and length(conta)>1")
        } else if dots==1 {
            r = util.ConcatWhere(r,"length(conta) <=5 and length(conta)>3")
        } else {
        r = util.ConcatWhere(r,"length(conta)>5")
        }
    }
    return r

}

func GetReceita(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultmes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

        arr := GetRowsReceita(parameters)

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getByFilter-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetReceitaBubble(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultimomes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)
        parameters.ArvoreCompleta="S"

        arr := GetRowsReceita(parameters)
/*
        tmp={  
        "name": "despesas",
         "children": [
         {"name": "FolhaPagamento", "children":[{"name":"Salários","size": 3000},{"name":"Encargos","children":[{"name":"FGTS","size": 1500},{"name":"GRPS","size": 876}]}]},
          {"name": "CusteioInstalacoes","children":[{"name":"Base","size": 1500},{"name":"Comissão","size": 876}] },
         {"name": "Assessorias", "size": 6714},
         {"name": "Diretoria", "size": 743}
         ]	  
         }
*/
        currentDeep := 0
        var currentValue float64
        in :=`{"name": "despesas",`
        for i:=1;i< len(arr);i++ {
            dots := strings.Count(arr[i].Conta,".") 
            if dots>currentDeep {
                in+=`"children":[{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor     
            } else if dots==currentDeep {
                in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`},`
                in+=`{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor   
            } else {
                in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`}`
                for j:=1; j<=currentDeep-dots;j++ {
                    in+=`]}`
                }
                in+=`,`
                in+=`{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor
            } 
            currentDeep=dots
        }
        in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`}`
        for j:=1;j<=currentDeep;j++ { 
        in+="]}"
        }
       rawIn := json.RawMessage(in)
       jsonB, err := rawIn.MarshalJSON()
       if err != nil {
           panic(err)
       }

       fmt.Printf("Data-> %s",string(jsonB))
       fmt.Fprintf(w, "%s", string(jsonB))
}


func GetReceitaMensal(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultmes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

        arr := GetRowsReceitaMensal(parameters)

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getByFilter-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetReceitaMensalDashboard(w http.ResponseWriter, r *http.Request) {
    var parameters Parameters 
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        panic(fmt.Sprintf("%s", r.Body))
    }
    log.Println("body---->"+string(body))
    err = json.Unmarshal(body, &parameters)
    log.Println("exercicio---->"+parameters.Exercicio)
    log.Println("mesref---->"+parameters.MesReferencia)
    log.Println("ultmes---->"+parameters.UltimoMes)
    log.Println("conta---->"+parameters.Conta)
    log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

    arr := GetRowsReceitaMensal(parameters)
/*
        	var freqData=[{p1:'Jan',p2:{FolhaPagamento: 4035, CusteioInstalacoes: 5014.21,Assessorias: 23765,Diretoria:7263.51,Telefones:318.37,Provisionamento: 0}}
		   ,{p1:'Fev',p2:{FolhaPagamento: 4035, CusteioInstalacoes: 5284.02, Assessorias:22885,Diretoria:1386.65,Telefones:318.37,Provisionamento:0}}];
           var strFreqData = JSON.stringify(freqData);
           localStorage.setItem("data", "dashboard;"+"Despesa Mensal Regime Caixa"+";"+strFreqData+";"+"FolhaPagamento#CusteioInstacoes#Assessorias#Diretoria#Telefones#Provisionamento");
           window.open("Dashboard.html");
*/
    mapLegendas := make(map[string]float64) 
    var strLegendas=""
    var legendArr []string;
    for _,row := range arr {
       log.Println("Valor->"+strconv.FormatFloat(row.Valor,'f',2,64))
        _,exists:=mapLegendas[row.Descricao]
        if !exists {
            mapLegendas[row.Descricao]=0.0
        }
    }
    for key,value :=range mapLegendas {
        legendArr = append(legendArr,key);       
        log.Println("key->"+key)
        log.Println("value->"+strconv.FormatFloat(value,'f',2,64))
        strLegendas+=key+"#"
    }
    if strings.HasSuffix(strLegendas,"#") {
       strLegendas=strLegendas[0:len(strLegendas)-1]
    } 


   in :=`{"legendas":"`+strLegendas+`", "dados":[`
    var tagMes int64  = 0 
    for key,_ :=range mapLegendas {
        mapLegendas[key]=0.0
    }
    for _,row := range arr {
        if row.Mes != tagMes {
            if tagMes>0 {
                in+=`{"p1":"`+util.MonthDescription(strconv.FormatInt(tagMes,10))+`","p2":{`
                /*
                for key,value := range mapLegendas {
                     in+=`"`+key+`":`+ strconv.FormatFloat(value,'f',2,64)+`,`
                }
                */
                for _,l := range legendArr {
                    in+=`"`+l+`":` + strconv.FormatFloat(mapLegendas[l],'f',2,64)+`,`
                } 
                if strings.HasSuffix(in,",") {
                    in=in[0:len(in)-1]
                }
                in+=`}},`
            }
            for key,_ := range mapLegendas {
                mapLegendas[key] = 0.0
            }
            mapLegendas[row.Descricao]=row.Valor
        } else { 
            mapLegendas[row.Descricao]=row.Valor
        } 
        tagMes=row.Mes
    } 
    in+=`{"p1":"`+util.MonthDescription(strconv.FormatInt(tagMes,10))+`","p2":{`
    /*
    for key,value := range mapLegendas {
        in+=`"`+key+`":`+ strconv.FormatFloat(value,'f',2,64)+`,`
    }
    */
    for _,l := range legendArr {
        in+=`"`+l+`":` + strconv.FormatFloat(mapLegendas[l],'f',2,64)+`,`
    } 

    if strings.HasSuffix(in,",") {
        in=in[0:len(in)-1]
    }
    in+=`}}`
    in+="]}"

   rawIn := json.RawMessage(in)
    jsonB, err := rawIn.MarshalJSON()
    if err != nil {
        panic(err)
    }

   fmt.Printf("Data-> %s",string(jsonB))
   fmt.Fprintf(w, "%s", string(jsonB))
}
/////////////////////////////////////////DESPESA//////////////////////////////////////////////////////
func GetRowsDespesa(parameters Parameters) [] DespesaReduce{
	selectText :=`SELECT id,
                             idpai,
                             exercicio,
                             conta,
                             codred,
                             descricao,
                             analitica,
                             saldoanterior,
                             movimentodebitocaixa,
                             movimentocreditocaixa,
                             movimentodebitocompetencia,
                             movimentocreditocompetencia,
                             orcamento,
                             datacriacao,
                             dataexclusao FROM orcamento
                      `
        whereText := generateDespesaWhereText(parameters)

        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
        selectText+=" order by conta"
        log.Println("where text->"+whereText)
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
	var arr [] DespesaReduce
	for rows.Next() {
		var record Record
                var despesaReduce DespesaReduce
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                          )
               despesaReduce.Conta = record.Conta
               despesaReduce.Descricao = record.Descricao 
//             intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
                if parameters.Regime=="caixa" {
                despesaReduce.Valor = domain_lancamento.GetSumByContaCaixa(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,
                                                despesaReduce.Conta,
                                                )
                } else {
                despesaReduce.Valor = domain_lancamento.GetSumByContaCompetencia(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,
                                                despesaReduce.Conta,
                                                )
                } 
		arr = append(arr, despesaReduce)
	}
        return arr
}

func GetRowsDespesaMensal(parameters Parameters) [] DespesaMensalReduce{
	selectText :=`SELECT id,
                             idpai,
                             exercicio,
                             conta,
                             codred,
                             descricao,
                             analitica,
                             saldoanterior,
                             movimentodebitocaixa,
                             movimentocreditocaixa,
                             movimentodebitocompetencia,
                             movimentocreditocompetencia,
                             orcamento,
                             datacriacao,
                             dataexclusao FROM orcamento
                      `
                       
        whereText := generateDespesaWhereText(parameters)

        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
        selectText+=" order by conta"
        log.Println("where text->"+whereText)
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
	var arr [] DespesaMensalReduce
	for rows.Next() {
		var record Record
                var despesaMensalReduce DespesaMensalReduce
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                          )
               despesaMensalReduce.Conta = record.Conta
               despesaMensalReduce.Descricao = record.Descricao
               arr = append(arr,despesaMensalReduce)
       }           
       var arr2 []DespesaMensalReduce
//             intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
               intMes,_:=strconv.ParseInt(parameters.UltimoMes,10,64)

       var i int64
       for i=1;i<=intMes;i++ {
           for _,despesaMensalReduce := range arr {         
                    despesaMensalReduce.Mes = i
                    if parameters.Regime=="caixa" {
                    despesaMensalReduce.Valor = domain_lancamento.GetSumByContaCaixa(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    despesaMensalReduce.Conta,
                                                    )
                    } else {
                    despesaMensalReduce.Valor = domain_lancamento.GetSumByContaCompetencia(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    despesaMensalReduce.Conta,
                                                    )
                    }
		    arr2 = append(arr2, despesaMensalReduce)
           }
	}
        return arr2
}


func generateDespesaWhereText(parameters Parameters) string {
    r := ""
    if len(parameters.Exercicio)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleInteger("exercicio", parameters.Exercicio))
    }
    if len(parameters.Conta)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleString("conta", parameters.Conta+"%"))
    } else {
        r = util.ConcatWhere(r,util.GenerateSingleString("conta", "4%"))
    }
    if strings.ToLower(parameters.ArvoreCompleta)=="n" {
        dots := strings.Count(parameters.Conta,".")
        if dots==0 {
            r = util.ConcatWhere(r,"length(conta) <=3 and length(conta)>1")
        } else if dots==1 {
            r = util.ConcatWhere(r,"length(conta) <=5 and length(conta)>3")
        } else {
        r = util.ConcatWhere(r,"length(conta)>5")
        }
    }
    return r

}

func GetDespesa(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultmes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

        arr := GetRowsDespesa(parameters)

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getByFilter-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))


}

func GetDespesaBubble(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultimomes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)
        parameters.ArvoreCompleta="S"

        arr := GetRowsDespesa(parameters)
/*
        tmp={  
        "name": "despesas",
         "children": [
         {"name": "FolhaPagamento", "children":[{"name":"Salários","size": 3000},{"name":"Encargos","children":[{"name":"FGTS","size": 1500},{"name":"GRPS","size": 876}]}]},
          {"name": "CusteioInstalacoes","children":[{"name":"Base","size": 1500},{"name":"Comissão","size": 876}] },
         {"name": "Assessorias", "size": 6714},
         {"name": "Diretoria", "size": 743}
         ]	  
         }
*/
        currentDeep := 0
        var currentValue float64
        in :=`{"name": "despesas",`
        for i:=1;i< len(arr);i++ {
            dots := strings.Count(arr[i].Conta,".") 
            if dots>currentDeep {
                in+=`"children":[{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor     
            } else if dots==currentDeep {
                in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`},`
                in+=`{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor   
            } else {
                in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`}`
                for j:=1; j<=currentDeep-dots;j++ {
                    in+=`]}`
                }
                in+=`,`
                in+=`{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor
            } 
            currentDeep=dots
        }
        in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`}`
        for j:=1;j<=currentDeep;j++ { 
        in+="]}"
        }
       rawIn := json.RawMessage(in)
       jsonB, err := rawIn.MarshalJSON()
       if err != nil {
           panic(err)
       }

       fmt.Printf("Data-> %s",string(jsonB))
       fmt.Fprintf(w, "%s", string(jsonB))
}
func GetDespesaPartition(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultimomes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)
        parameters.ArvoreCompleta="S"

        arr := GetRowsDespesa(parameters)
/*
        tmp={  
        "name": "despesas",
         "children": [
         {"name": "FolhaPagamento", "children":[{"name":"Salários","size": 3000},{"name":"Encargos","children":[{"name":"FGTS","size": 1500},{"name":"GRPS","size": 876}]}]},
          {"name": "CusteioInstalacoes","children":[{"name":"Base","size": 1500},{"name":"Comissão","size": 876}] },
         {"name": "Assessorias", "size": 6714},
         {"name": "Diretoria", "size": 743}
         ]	  
         }
*/
        currentDeep := 0
        var currentValue float64
        in :=`{"name": "despesas",`
        for i:=1;i< len(arr);i++ {
            dots := strings.Count(arr[i].Conta,".") 
            if dots>currentDeep {
                in+=`"children":[{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor     
            } else if dots==currentDeep {
                in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`},`
                in+=`{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor   
            } else {
                in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`}`
                for j:=1; j<=currentDeep-dots;j++ {
                    in+=`]}`
                }
                in+=`,`
                in+=`{"name":"`+arr[i].Descricao+`",`
                currentValue=arr[i].Valor
            } 
            currentDeep=dots
        }
        in+=`"size":`+strconv.FormatFloat(currentValue,'f',2,64)+`}`
        for j:=1;j<=currentDeep;j++ { 
        in+="]}"
        }
       rawIn := json.RawMessage(in)
       jsonB, err := rawIn.MarshalJSON()
       if err != nil {
           panic(err)
       }

       fmt.Printf("Data-> %s",string(jsonB))
       fmt.Fprintf(w, "%s", string(jsonB))
}



func GetDespesaMensal(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultmes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

        arr := GetRowsDespesaMensal(parameters)

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getByFilter-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}


func GetDespesaMensalDashboard(w http.ResponseWriter, r *http.Request) {
    var parameters Parameters 
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        panic(fmt.Sprintf("%s", r.Body))
    }
    log.Println("body---->"+string(body))
    err = json.Unmarshal(body, &parameters)
    log.Println("exercicio---->"+parameters.Exercicio)
    log.Println("mesref---->"+parameters.MesReferencia)
    log.Println("ultmes---->"+parameters.UltimoMes)
    log.Println("conta---->"+parameters.Conta)
    log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

    arr := GetRowsDespesaMensal(parameters)
/*
        	var freqData=[{p1:'Jan',p2:{FolhaPagamento: 4035, CusteioInstalacoes: 5014.21,Assessorias: 23765,Diretoria:7263.51,Telefones:318.37,Provisionamento: 0}}
		   ,{p1:'Fev',p2:{FolhaPagamento: 4035, CusteioInstalacoes: 5284.02, Assessorias:22885,Diretoria:1386.65,Telefones:318.37,Provisionamento:0}}];
           var strFreqData = JSON.stringify(freqData);
           localStorage.setItem("data", "dashboard;"+"Despesa Mensal Regime Caixa"+";"+strFreqData+";"+"FolhaPagamento#CusteioInstacoes#Assessorias#Diretoria#Telefones#Provisionamento");
           window.open("Dashboard.html");
*/
    mapLegendas := make(map[string]float64) 
    var strLegendas=""
    var legendArr []string
    for _,row := range arr {
       log.Println("Valor->"+strconv.FormatFloat(row.Valor,'f',2,64))
        _,exists:=mapLegendas[row.Descricao]
        if !exists {
            mapLegendas[row.Descricao]=0.0
        }
    }
    for key,value :=range mapLegendas {
        legendArr = append(legendArr,key)
        log.Println("key->"+key)
        log.Println("value->"+strconv.FormatFloat(value,'f',2,64))
        strLegendas+=key+"#"
    }
    if strings.HasSuffix(strLegendas,"#") {
       strLegendas=strLegendas[0:len(strLegendas)-1]
    } 


   in :=`{"legendas":"`+strLegendas+`", "dados":[`
    var tagMes int64  = 0 
    for key,_ :=range mapLegendas {
        mapLegendas[key]=0.0
    }
    for _,row := range arr {
        if row.Mes != tagMes {
            if tagMes>0 {
                in+=`{"p1":"`+util.MonthDescription(strconv.FormatInt(tagMes,10))+`","p2":{`
                /*
                for key,value := range mapLegendas {
                     in+=`"`+key+`":`+ strconv.FormatFloat(value,'f',2,64)+`,`
                }
                */
                for _,l := range legendArr {
                     in+=`"`+l+`":`+ strconv.FormatFloat(mapLegendas[l],'f',2,64)+`,`
                }
                if strings.HasSuffix(in,",") {
                    in=in[0:len(in)-1]
                }
                in+=`}},`
            }
            for key,_ := range mapLegendas {
                mapLegendas[key] = 0.0
            }
            mapLegendas[row.Descricao]=row.Valor
        } else { 
            mapLegendas[row.Descricao]=row.Valor
        } 
        tagMes=row.Mes
    } 
    in+=`{"p1":"`+util.MonthDescription(strconv.FormatInt(tagMes,10))+`","p2":{`
    /*
    for key,value := range mapLegendas {
        in+=`"`+key+`":`+ strconv.FormatFloat(value,'f',2,64)+`,`
    }
    */
    for _,l := range legendArr {
        in+=`"`+l+`":`+ strconv.FormatFloat(mapLegendas[l],'f',2,64)+`,`
    }
 
    if strings.HasSuffix(in,",") {
        in=in[0:len(in)-1]
    }
    in+=`}}`
    in+="]}"

   rawIn := json.RawMessage(in)
    jsonB, err := rawIn.MarshalJSON()
    if err != nil {
        panic(err)
    }

   fmt.Printf("Data-> %s",string(jsonB))
   fmt.Fprintf(w, "%s", string(jsonB))
}
/////////////////////////////////////////TIPO DESPESA//////////////////////////////////////////////////////
func GetRowsTipoDespesa(parameters Parameters) TipoDespesaReduce{
    var tipoDespesaReduce TipoDespesaReduce
//  intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
    if parameters.Regime=="caixa" {
    tipoDespesaReduce.Fixa = domain_lancamento.GetSumByTipoCaixa(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes, 
                                                "DF",
                                                )
    tipoDespesaReduce.Variavel = domain_lancamento.GetSumByTipoCaixa(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,   
                                                "DV",
                                                )
    } else {
    tipoDespesaReduce.Fixa = domain_lancamento.GetSumByTipoCompetencia(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes, 
                                                "DF",
                                                )
    tipoDespesaReduce.Variavel = domain_lancamento.GetSumByTipoCompetencia(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,   
                                                "DV",
                                                )
    }
    return tipoDespesaReduce
}

func GetRowsTipoDespesaMensal(parameters Parameters) [] TipoDespesaMensalReduce{
    var arr []TipoDespesaMensalReduce
    var tipoDespesaMensalReduce TipoDespesaMensalReduce
//  intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
    intMes,_:=strconv.ParseInt(parameters.UltimoMes,10,64)
    var i int64
    for i=1;i<=intMes;i++ {
        tipoDespesaMensalReduce.Mes  = i
        if parameters.Regime =="caixa" {
        tipoDespesaMensalReduce.Fixa = domain_lancamento.GetSumByTipoCaixa(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    "DF",
                                                    )
        tipoDespesaMensalReduce.Variavel = domain_lancamento.GetSumByTipoCaixa(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    "DV",
                                                    )
        } else {
        tipoDespesaMensalReduce.Fixa = domain_lancamento.GetSumByTipoCompetencia(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    "DF",
                                                    )
        tipoDespesaMensalReduce.Variavel = domain_lancamento.GetSumByTipoCompetencia(
                                                    parameters.Exercicio,
                                                    strconv.FormatInt(i,10),
                                                    parameters.UltimoMes,
                                                    "DV",
                                                    )
        }
	arr = append(arr, tipoDespesaMensalReduce)
    }
    return arr
}

func GetTipoDespesa(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("utmes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

        arr := GetRowsTipoDespesa(parameters)

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getByFilter-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetTipoDespesaMensal(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultmes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

        arr := GetRowsTipoDespesaMensal(parameters)

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getTipoDespesaMensal-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}


func GetTipoDespesaMensalDashboard(w http.ResponseWriter, r *http.Request) {
    var parameters Parameters 
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        panic(fmt.Sprintf("%s", r.Body))
    }
    log.Println("body---->"+string(body))
    err = json.Unmarshal(body, &parameters)
    log.Println("exercicio---->"+parameters.Exercicio)
    log.Println("mesref---->"+parameters.MesReferencia)
    log.Println("ultmes---->"+parameters.UltimoMes)
    log.Println("conta---->"+parameters.Conta)
    log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

    arr := GetRowsTipoDespesaMensal(parameters)

/*
[{p1:'Jan',p2:{Fixo: 31724.35, Var: 8671.74}}
		   ,{p1:'Fev',p2:{Fixo: 31114.16, Var: 2794.88}}]
*/
   in :="["
    for _,row := range arr {

        in+=`{"p1":"`+util.MonthDescription(strconv.FormatInt(row.Mes,10))+`","p2":{"Fixo":`+strconv.FormatFloat(row.Fixa,'f',2,64)
        in+=`, "Var":`+strconv.FormatFloat(row.Variavel,'f',2,64)+`}},`
    }
   if strings.HasSuffix(in,",") {
       in=in[0:len(in)-1]
   }  
   in +="]" 

   rawIn := json.RawMessage(in)
    jsonB, err := rawIn.MarshalJSON()
    if err != nil {
        panic(err)
    }

   fmt.Printf("Data-> %s",string(jsonB))
   fmt.Fprintf(w, "%s", string(jsonB))
}
func GetTipoDespesaMensalBar(w http.ResponseWriter, r *http.Request) {
    var parameters Parameters 
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        panic(fmt.Sprintf("%s", r.Body))
    }
    log.Println("body---->"+string(body))
    err = json.Unmarshal(body, &parameters)
    log.Println("exercicio---->"+parameters.Exercicio)
    log.Println("mesref---->"+parameters.MesReferencia)
    log.Println("ultmes---->"+parameters.UltimoMes)
    log.Println("conta---->"+parameters.Conta)
    log.Println("regime---->"+parameters.Regime)
    log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

    arr := GetRowsTipoDespesaMensal(parameters)


/*
[
            ['x', 'jan', 'fev', 'mar'],
            ['Fixa', 33152.58, 32522.39, 23972.82],
            ['Variavel', 7263.51, 1386.65, 13103.34],
          ]
*/
   in :=`[["x",`
   intUltimoMes,_ :=strconv.ParseInt(parameters.UltimoMes,10,64)
   for i:= int64(1);i<=intUltimoMes;i++ {
       in+=`"`+util.MonthDescription(strconv.FormatInt(i,10))+`",` 
   }
   if strings.HasSuffix(in,",") {
       in = in[0:len(in)-1] 
   }
   in +=`],`
 
    strFixa:=``
    strVariavel:=``

    for _,row := range arr {
        strFixa+=strconv.FormatFloat(row.Fixa,'f',2,64)+`,`
        strVariavel+=strconv.FormatFloat(row.Variavel,'f',2,64)+`,`
    }
   if strings.HasSuffix(strFixa,",") {
       strFixa=strFixa[0:len(strFixa)-1]
   }  
   if strings.HasSuffix(strVariavel,",") {
       strVariavel=strVariavel[0:len(strVariavel)-1]
   }  
   in +=`["Fixa",`+strFixa+`],`
   in +=`["Variavel",`+strVariavel+`]`
   in +=`]` 

   rawIn := json.RawMessage(in)
    jsonB, err := rawIn.MarshalJSON()
    if err != nil {
        panic(err)
    }

   fmt.Printf("Data-> %s",string(jsonB))
   fmt.Fprintf(w, "%s", string(jsonB))
}


///////////////////////////////////////////ORCAMENTO///////////////////////////////////////////////////////////////
func GetRowsOrcamento(parameters Parameters) [] OrcamentoReduce{
	selectText :=`SELECT id,
                             idpai,
                             exercicio,
                             conta,
                             codred,
                             descricao,
                             analitica,
                             saldoanterior,
                             movimentodebitocaixa,
                             movimentocreditocaixa,
                             movimentodebitocompetencia,
                             movimentocreditocompetencia,
                             orcamento,
                             datacriacao,
                             dataexclusao FROM orcamento
                      `
                       
        whereText := generateOrcamentoWhereText(parameters)

        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
        selectText+=" order by conta"
        log.Println("where text->"+whereText)
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
	var arr [] OrcamentoReduce
	for rows.Next() {
		var record Record
                var orcamentoReduce OrcamentoReduce
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                          )
               orcamentoReduce.Conta = record.Conta
               orcamentoReduce.Descricao = record.Descricao 
               orcamentoReduce.Valor = record.Orcamento
//             intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
               if parameters.Regime=="caixa" {
               orcamentoReduce.Realizado = domain_lancamento.GetSumByContaCaixa(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,
                                                orcamentoReduce.Conta,
                                                )
                } else {
               orcamentoReduce.Realizado = domain_lancamento.GetSumByContaCompetencia(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes,
                                                orcamentoReduce.Conta,
                                                )
                } 
		arr = append(arr, orcamentoReduce)
	}
        return arr
}


func generateOrcamentoWhereText(parameters Parameters) string {
    r := ""
    if len(parameters.Exercicio)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleInteger("exercicio", parameters.Exercicio))
    }
    if len(parameters.Conta)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleString("conta", parameters.Conta+"%"))
    } else {
        r = util.ConcatWhere(r,util.GenerateSingleString("conta", "3%"))
    }
    if strings.ToLower(parameters.ArvoreCompleta)=="n" {
        dots := strings.Count(parameters.Conta,".")
        if dots==0 {
            r = util.ConcatWhere(r,"length(conta) <=3 and length(conta)>1")
        } else if dots==1 {
            r = util.ConcatWhere(r,"length(conta) <=5 and length(conta)>3")
        } else {
        r = util.ConcatWhere(r,"length(conta)>5")
        }
    }
    return r

}

func GetOrcamento(w http.ResponseWriter, r *http.Request) {
        var parameters Parameters 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parameters)
        log.Println("exercicio---->"+parameters.Exercicio)
        log.Println("mesref---->"+parameters.MesReferencia)
        log.Println("ultmes---->"+parameters.UltimoMes)
        log.Println("conta---->"+parameters.Conta)
        log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

        arr := GetRowsOrcamento(parameters)

	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getByFilter-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))


}

func GetRowsOrcamentoTree(parameters Parameters) [] OrcamentoTreeReduce{
	selectText :=`SELECT id,
                             idpai,
                             exercicio,
                             conta,
                             codred,
                             descricao,
                             analitica,
                             saldoanterior,
                             movimentodebitocaixa,
                             movimentocreditocaixa,
                             movimentodebitocompetencia,
                             movimentocreditocompetencia,
                             orcamento,
                             datacriacao,
                             dataexclusao FROM orcamento where exercicio = `+parameters.Exercicio+
                             ` and conta like "4.4%" and analitica = 'X' order by conta`
                       
        rows := util.QueryDB(selectText)
	var arr [] OrcamentoTreeReduce
	for rows.Next() {
		var record Record
                var orcamentoTreeReduce OrcamentoTreeReduce
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                          )

               dots :=strings.Count(record.Conta,".") 
                   orcamentoTreeReduce.Categoria="Despesa" 
               if dots==1 {
                   orcamentoTreeReduce.Nivel2=record.Descricao
               } else if dots ==2 {
                   orcamentoTreeReduce.Nivel3=record.Descricao
               } else if dots ==3 {
                   orcamentoTreeReduce.Nivel4=record.Descricao
               }
               intExercicio,_ :=strconv.ParseInt(parameters.Exercicio,10,64)
               parentArr := extractAllParents(intExercicio,record.Conta)
               for i := len(parentArr)-1;i>=0;i-- {
                   delta :=len(parentArr)-1 - i
                   if delta ==0 {
                       orcamentoTreeReduce.Nivel1 = parentArr[i].Descricao
                   }  else if delta==1 {
                       orcamentoTreeReduce.Nivel2 = parentArr[i].Descricao
                   } else {
                       orcamentoTreeReduce.Nivel3 = parentArr[i].Descricao
                   }
               }
 
               orcamentoTreeReduce.Valor = record.Orcamento
	       if parameters.Regime == "caixa" {
               orcamentoTreeReduce.Realizado = domain_lancamento.GetSumByContaCaixa(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes, 
                                                record.Conta,
                                                )
                } else {
               orcamentoTreeReduce.Realizado = domain_lancamento.GetSumByContaCompetencia(
                                                parameters.Exercicio,
                                                parameters.MesReferencia,
                                                parameters.UltimoMes, 
                                                record.Conta,
                                                )
                }
                               
		arr = append(arr, orcamentoTreeReduce)
	}
        return arr
}


func GetOrcamentoTree(w http.ResponseWriter, r *http.Request) {
    var parameters Parameters 
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        panic(fmt.Sprintf("%s", r.Body))
    }
    log.Println("body---->"+string(body))
    err = json.Unmarshal(body, &parameters)
    log.Println("exercicio---->"+parameters.Exercicio)
    log.Println("mesref---->"+parameters.MesReferencia)
    log.Println("ultmes---->"+parameters.UltimoMes)
    log.Println("conta---->"+parameters.Conta)
    log.Println("arvorecompleta---->"+parameters.ArvoreCompleta)

    arr := GetRowsOrcamentoTree(parameters)



/*
in :=`[
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Folha de Pagamento",
"Level3":"",
"Level4":"",
"Federal":42200.00,
"":"b",
"GovXFer":"",
"State":"8070.00",
"Local":"34130",
"Total":42200.00
},
*/

stmt := util.PrepareDB(` select v.nome,v.descricao,o.conta,l.valor,sum(l.valor)  as Total 
		 from lancamento l
		inner join evento v on v.id = l.evento
		inner join orcamento o on v.gerencial = o.codred
		where o.conta like "4.4%" and o.descricao like ?
		and l.exercicio = `+parameters.Exercicio+
		` and v.id not in (29,30,32)
		 group by v.descricao,o.conta 
		 order by v.descricao`)
    in :=`[`

    for _,row := range arr {
		rows, errQuery := stmt.Query(row.Nivel3)
		util.CheckErr(errQuery)
	for rows.Next() {
		var records ReceitaDiretoria
			rows.Scan(
                               &records.Nome,
                               &records.Descricao,
                               &records.Conta, 
                               &records.Valor,
                               &records.Total,)
        in+=`{`
        in+=`"Category":"-`+row.Categoria+`",`
        in+=`"Level1":"`+row.Nivel2+`",`
        in+=`"Level2":"`+row.Nivel3+`",`
        in+=`"Level3":"`+records.Descricao+`",`
        //in+=`"Level4":"`+records.Descricao+`",`
        in+=`"Federal":`+strconv.FormatFloat(records.Total,'f',2,64)+`,`//+strconv.FormatFloat(row.Valor,'f',2,64)+`,`
        //in+=`"":"b",`
        in+=`"GovXFer":"",`//+strconv.FormatFloat(records.Total,'f',2,64)+`",`
        in+=`"State":`+strconv.FormatFloat(records.Total,'f',2,64)+`,`
        in+=`"Local":"",`//+strconv.FormatFloat(records.Total,'f',2,64)+`,`//+strconv.FormatFloat(row.Valor-row.Realizado,'f',2,64)+`,`
        in+=`"Total":`+strconv.FormatFloat(records.Total,'f',2,64)//+strconv.FormatFloat(row.Valor,'f',2,64)
        in+=`},` 
	}
    } 
    if strings.HasSuffix(in,",") {
        in = in[0:len(in)-1]
    }

    in+=`]`

    rawIn := json.RawMessage(in)
    jsonB, err := rawIn.MarshalJSON()
    if err != nil {
        panic(err)
    }

   fmt.Printf("Data-> %s",string(jsonB))
   fmt.Fprintf(w, "%s", string(jsonB))
}
////////////////////////////////////GETROWBYID/////////////////////////////////////////////////////
func GetRowByID(id int64) Record{
	stmt := util.PrepareDB(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitocaixa,
                                            movimentocreditocaixa,
                                            movimentodebitocompetencia,
                                            movimentocreditocompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao FROM orcamento where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.IdPai,
                                &record.Exercicio, 
                                &record.Conta,
                                &record.Codred,
                                &record.Descricao,
                                &record.Analitica,
                                &record.SaldoAnterior,
                                &record.MovimentoDebitoCaixa,
                                &record.MovimentoCreditoCaixa,
                                &record.MovimentoDebitoCompetencia,
                                &record.MovimentoCreditoCompetencia,
                                &record.Orcamento,    
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
        return record
}

/////////////////////////////////GETBYID////////////////////////////////////////////////////////////
func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitocaixa,
                                            movimentocreditocaixa,
                                            movimentodebitocompetencia,
                                            movimentocreditocompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao FROM orcamento where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.IdPai,
                                &record.Exercicio, 
                                &record.Conta,
                                &record.Codred,
                                &record.Descricao,
                                &record.Analitica,
                                &record.SaldoAnterior,
                                &record.MovimentoDebitoCaixa,
                                &record.MovimentoCreditoCaixa,
                                &record.MovimentoDebitoCompetencia,
                                &record.MovimentoCreditoCompetencia,
                                &record.Orcamento,    
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func UpdateByExercicioCodred(w http.ResponseWriter, r *http.Request) {
      var record Record 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("exercicio----->"+strconv.FormatInt(record.Exercicio,10))
        log.Println("codred---->"+strconv.FormatInt(record.Codred,10))
        log.Println("valor---->"+strconv.FormatFloat(record.Orcamento,'f',-1,64))

	stmt := util.PrepareDB(`UPDATE orcamento 
                                    SET (orcamento)=
                                    (?)     
                                    WHERE exercicio = ? and codred = ?`)
	result, errExec := stmt.Exec(
                                  record.Orcamento,
                                  record.Exercicio,
                                  record.Codred)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(record)
		util.CheckErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}
        Init(record.Exercicio)
        Sum(record.Exercicio)
}

func GetByConta(exercicio int64, conta string) Record {
	stmt := util.PrepareDB(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitocaixa,
                                            movimentocreditocaixa,
                                            movimentodebitocompetencia,
                                            movimentocreditocompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao  
                                            FROM orcamento where exercicio = ? and conta = ?`)
	rows, errQuery := stmt.Query(exercicio,conta)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.IdPai,
                                &record.Exercicio,
                                &record.Conta, 
                                &record.Codred,
                                &record.Descricao,
                                &record.Analitica,
                                &record.SaldoAnterior,
                                &record.MovimentoDebitoCaixa,
                                &record.MovimentoCreditoCaixa,
                                &record.MovimentoDebitoCompetencia,
                                &record.MovimentoCreditoCompetencia,
                                &record.Orcamento, 
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	return record
}
func Init(exercicio int64) {
	stmt := util.PrepareDB(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM orcamento where exercicio = ?`)
	rows, errQuery := stmt.Query(exercicio)
	util.CheckErr(errQuery)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
        for _,record := range arr {
            if len(strings.Trim(record.Analitica," "))==0 {
                continue
            }
            parent := extractParent(record.Conta) 
            for len(parent)>0 {
                orcamentoParent := GetByConta(record.Exercicio,parent) 
//              sum := orcamentoParent.Orcamento + record.Orcamento
                UpdateByID(orcamentoParent.ID, 0.0)
                parent = extractParent(parent)
            }
        }
}

func Sum(exercicio int64) {
	stmt := util.PrepareDB(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM orcamento where exercicio = ?`)
	rows, errQuery := stmt.Query(exercicio)
	util.CheckErr(errQuery)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
        for _,record := range arr {
            if len(strings.Trim(record.Analitica," "))==0 {
                continue
            }
            parent := extractParent(record.Conta) 
            for len(parent)>0 {
                orcamentoParent := GetByConta(record.Exercicio,parent) 
                sum := orcamentoParent.Orcamento + record.Orcamento
                UpdateByID(orcamentoParent.ID, sum)
                parent = extractParent(parent)
            }
        }
}
func extractAllParents(exercicio int64, conta string) []Record {
    var r []Record
    p := extractParent (conta)
    for len(p) >0 {
        parentRecord := GetByConta(exercicio, p)
        r = append(r,parentRecord) 
        p = extractParent(p)
    }
    return r;
}
func extractParent(conta string) string {
    pos:=strings.LastIndex(conta,".") 
    if pos<0 {
        return ""
    } else {
        return conta[0:pos]
    }
}

func UpdateByID(id int64, orcamento float64) {
	stmt := util.PrepareDB(`UPDATE orcamento 
                                    SET (orcamento)=
                                    (?)     
                                    WHERE id = ?`)
	result, errExec := stmt.Exec(
                                  orcamento,
                                  id)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
 	if rowAffected > 0 {
		//jsonB, errMarshal := json.Marshal(gerencial)
		//checkErr(errMarshal)
		//fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		//fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}
}

func List(w http.ResponseWriter, r *http.Request) {
        var filter Filter 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("exercicio----->"+filter.Exercicio)
        log.Println("codred---->"+filter.Codred)


	rows := util.QueryDB(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao, 
                                          dataexclusao FROM orcamento order by conta`)
	var arr Arr
        var record Record 
	for rows.Next() {
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao, 
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}

        var contents string = ""
        for _,record = range arr {
           contents+=strconv.FormatInt(record.ID, 10)+"\t"
           contents+=strconv.FormatInt(record.IdPai, 10)+"\t"
           contents+=strconv.FormatInt(record.Exercicio, 10)+"\t"
           contents+=record.Conta+"\t"
           contents+=strconv.FormatInt(record.Codred, 10)+"\t"
           contents+=record.Descricao+"\t"
           contents+=record.Analitica+"\t"
           contents+=strconv.FormatFloat(record.SaldoAnterior,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoDebitoCaixa,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoCreditoCaixa,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoDebitoCompetencia,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoCreditoCompetencia,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.Orcamento,'f',-1,64)+"\t"
           contents+=record.DataCriacao+"\t"
           contents+=record.DataExclusao+"\n"
         } 
        d1 := []byte(contents)
        err = ioutil.WriteFile("orcamentolist.txt",d1,0644)
        util.CheckErr(err)
        Report()


        var retornoLista util.RetornoLista
        retornoLista.Arquivo="orcamentolist.pdf"
        retornoLista.Status="ok"
	jsonB, errMarshal := json.Marshal(retornoLista)
	util.CheckErr(errMarshal)
        fmt.Printf("Lista-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func Report() {
        util.Agora = time.Now()
	r := gr.CreateGoReport()
	r.SumWork["amountcum="] = 0.0
	font1 := gr.FontMap{
		FontName: "IPAexG",
		FileName: "ttf//ipaexg.ttf",
	}
	fonts := []*gr.FontMap{&font1}
	r.SetFonts(fonts)
	d := new(S1Detail)
	r.RegisterBand(gr.Band(*d), gr.Detail)
	h := new(S1Header)
	r.RegisterBand(gr.Band(*h), gr.PageHeader)
	s := new(S1Summary)
	r.RegisterBand(gr.Band(*s), gr.Summary)

	f := new(S1Footer)
	r.RegisterBand(gr.Band(*f), gr.PageFooter)


	r.Records = gr.ReadTextFile("orcamentolist.txt", 7) // 09 e 0A no final
//	fmt.Printf("Records %v \n", r.Records)
	r.SetPage("A4", "mm", "L")
	r.SetFooterY(190)
	r.Execute("static/orcamentolist.pdf")
	r.SaveText("static/orcamentosavetext.txt")
}

type S1Detail struct {
}

func (h S1Detail) GetHeight(report gr.GoReport) float64 {
	return 5
}
func (h S1Detail) Execute(report gr.GoReport) {
	cols := report.Records[report.DataPos].([]string)
	report.Font("IPAexG", 12, "")
	y := 5.0
	report.Cell(15, y, cols[3])
	report.Cell(50, y, util.IdentDescription(cols[3],cols[5]))
	report.CellRight(205, y,15, util.TransformFloat(cols[12],2))
//	amt := ParseFloatNoError(cols[5]) * ParseFloatNoError(cols[6])
        report.CellRight(240, y,6, util.TransformCodred(cols[4])) 
	report.SumWork["amountcum="] += 1
//	report.CellRight(180, y, 30, strconv.FormatFloat(amt, 'f', 2, 64))
}

type S1Header struct {
}

func (h S1Header) GetHeight(report gr.GoReport) float64 {
	return 30
}
func (h S1Header) Execute(report gr.GoReport) {
	report.Font("IPAexG", 14, "")
	report.Cell(80, 15, "Transparência - Plano Gerencial com Orçamento")
	report.Font("IPAexG", 12, "")
	report.Cell(255, 15, "Página")
	report.Cell(275, 15, strconv.Itoa(report.Page))
        y := 23.0
        filter := "Exercício: 2017"
        var x float64
        x = float64((265.0 - len(filter))/2.0)
        report.Cell(15+x,y,filter)
	y = 28.0
	report.Cell(15, y, "Conta")
	report.Cell(50, y, "Descrição")
	report.CellRight(205, y,15, "Orçamento")
	report.Cell(240, y, "Cod.Red.")
	report.LineType("straight", 0.2)
        report.LineH(15, y+5.0, 280)

      report.Image("static/Content/img/fmb.jpeg", 15, 10, 80, 20)
}

type S1Summary struct {
}

func (h S1Summary) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Summary) Execute(report gr.GoReport) {
	report.CellRight(100, 12,25, "Contas Listadas:")
	report.CellRight(130, 12, 30, strconv.FormatFloat(
		report.SumWork["amountcum="], 'f', 0, 64))
}

type S1Footer struct {
}

func (h S1Footer) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Footer) Execute(report gr.GoReport) {
	report.CellRight(200, 8,80, "Emitido por MASTER em "+util.FormatTime(util.Agora))
}



