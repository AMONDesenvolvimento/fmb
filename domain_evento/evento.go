package domain_evento
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
    "../util"
)
type Record struct {
        ID int64 `json:"id"`
        Nome     string `json:"nome"`
	      Descricao string `json:"descricao"`
        Recdes    int64 `json:"recdes"`
        DescricaoRecdes string `json:"descricaorecdes"`
        Debito    int64 `json:"debito"`
        DescricaoDebito string `json:"descricaodebito"`
        Credito int64   `json:"credito"`
        DescricaoCredito string `json:"descricaocredito"`
        Projeto int64   `json:"projeto"`
        DescricaoProjeto string `json:"descricaoprojeto"`
        Gerencial int64 `json:"gerencial"`
        DescricaoGerencial string `json:"descricaogerencial"`
        Tipo string     `json:"tipo"`
        DataCriacao string `json:"datacriacao"` 
        DataExclusao string `json:"dataexclusao"`
 
}
type Filter struct {
        ID string `json:"id"`
        Nome     string `json:"nome"`
        Tipo string     `json:"tipo"`
}

type Lanc struct {
        ID string `json:"id"`
}

type Arr []Record
func GetAll(w http.ResponseWriter, r *http.Request) {
	rows := util.QueryDB(`SELECT id,
                                     nome,
                                     descricao,
                                     recdes,
                                     debito,
                                     credito,
                                     projeto,
                                     gerencial,
                                     tipo,
                                     datacriacao,
                                     dataexclusao FROM evento order by id`)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.Nome, 
                               &record.Descricao,
                               &record.Recdes,
                               &record.Debito,
                               &record.Credito,
                               &record.Projeto,
                               &record.Gerencial, 
                               &record.Tipo,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB("SELECT id,nome,descricao,recdes,debito,credito,projeto,gerencial,tipo,datacriacao,dataexclusao FROM evento where id = ?")
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome, 
                                &record.Descricao,
                                &record.Recdes,
                                &record.Debito,
                                &record.Credito,
                                &record.Projeto,
                                &record.Gerencial,
                                &record.Tipo,     
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
  log.Println(string(jsonB))
}

func GetByIDLanc(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
  stmt := util.PrepareDB("SELECT id FROM lancamento where evento= ?")
  rows, errQuery := stmt.Query(id)
  util.CheckErr(errQuery)
  var record Lanc
  for rows.Next() {
    rows.Scan(&record.ID)
  }
  jsonB, errMarshal := json.Marshal(record)
  util.CheckErr(errMarshal)
  fmt.Fprintf(w, "%s", string(jsonB))
}

func RowByID(id int64) Record {
	stmt := util.PrepareDB("SELECT id,nome,descricao,recdes,debito,credito,projeto,gerencial,tipo,datacriacao,dataexclusao FROM evento where id = ?")
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome, 
                                &record.Descricao,
                                &record.Recdes,
                                &record.Debito,
                                &record.Credito,
                                &record.Projeto,
                                &record.Gerencial,
                                &record.Tipo,     
                                &record.DataCriacao,
                                &record.DataExclusao)
		//checkErr(err)
	}
        return record
}

func Insert(w http.ResponseWriter, r *http.Request) {
	var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+string(record.ID))
        log.Println("nome---->"+record.Nome)
        record.DataCriacao = time.Now().Format("2006-01-02 15:04:05")
	stmt := util.PrepareDB(
                                   `INSERT INTO evento (
                                   id,
                                   nome, 
                                   descricao,
                                   recdes,
                                   debito,
                                   credito,
                                   projeto,
                                   tipo,   
                                   datacriacao) values ((select id + 1
                                                          from evento
                                                          order by id desc
                                                          limit 0,1),?,?,?,?,?,?,?,?)`)
	result, errExec := stmt.Exec(
                           record.Nome,   
                           record.Descricao,
                           record.Recdes,
                           record.Debito,
                           record.Credito,
                           record.Projeto,
                           record.Tipo,
                           record.DataCriacao,
                           )

  log.Println(stmt.Exec)
	util.CheckErr(errExec)
	newID, errLast := result.LastInsertId()
	util.CheckErr(errLast)
	record.ID = newID
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}




func UpdateByID(w http.ResponseWriter, r *http.Request) {
	var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+string(record.ID))
        log.Println("nome---->"+record.Nome)

	stmt := util.PrepareDB(`UPDATE evento 
                                    SET (nome,descricao)=
                                    (?,?)
                                    WHERE id = ?`)
	result, errExec := stmt.Exec(
                                  record.Nome,
                                  record.Descricao,
                                  record.ID)
  log.Println(stmt)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(record)
		util.CheckErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func DeleteByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB("DELETE FROM evento WHERE id = ?")
	result, errExec := stmt.Exec(id)
	util.CheckErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	util.CheckErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func GetByDiretoria(w http.ResponseWriter, r *http.Request) {
  rows := util.QueryDB(`SELECT v.id,
                                     v.nome,
                                     v.descricao,
                                     v.recdes,
                                     v.debito,
                                     v.credito,
                                     v.projeto,
                                     v.gerencial,
                                     v.tipo,
                                     v.datacriacao,
                                     v.dataexclusao FROM evento v inner join orcamento o on v.gerencial = o.codred  where o.conta like '4.4%'
                                     order by v.id`)
  var arr Arr
  for rows.Next() {
    var record Record
    rows.Scan(
                               &record.ID,
                               &record.Nome, 
                               &record.Descricao,
                               &record.Recdes,
                               &record.Debito,
                               &record.Credito,
                               &record.Projeto,
                               &record.Gerencial, 
                               &record.Tipo,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
    arr = append(arr, record)
  }
  jsonB, errMarshal := json.Marshal(arr)
  util.CheckErr(errMarshal)
  fmt.Fprintf(w, "%s", string(jsonB))
}


/*func UpdateByID(w http.ResponseWriter, r *http.Request) {
  var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+string(record.ID))
        log.Println("nome---->"+record.Nome)

  stmt := util.PrepareDB(`UPDATE evento 
                                    SET nome = ?,
                                    SET descricao = ?,
                                    SET recdes = ?,
                                    SET debito = ?,
                                    SET credito = ?,
                                    SET projeto = ?,
                                    SET tipo = ?   
                                    WHERE id = ?`)
  result, errExec := stmt.Exec(
                                  record.Nome,
                                  record.Descricao,
                                  record.Recdes,
                                  record.Debito,
                                  record.Credito,
                                  record.Projeto,
                                  record.Tipo,
                                  record.ID)
  util.CheckErr(errExec)
  rowAffected, errLast := result.RowsAffected()
  util.CheckErr(errLast)
  if rowAffected > 0 {
    jsonB, errMarshal := json.Marshal(record)
    util.CheckErr(errMarshal)
    fmt.Fprintf(w, "%s", string(jsonB))
  } else {
    fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
  }

}*/


