package main
import (
    "io"
    "net/http"
)
func hello(w http.ResponseWriter, r *http.Request) {
    io.WriteString(w, "FMB Transparência")
}
func main() {
    http.HandleFunc("/", hello)
    http.ListenAndServe(":8000", nil)
}
