package main
import (
    "text/template"
    "log"
    "os"
    "fmt"
    "bytes"
    "io/ioutil"
    "encoding/json"
)
type Recipient struct {
    Name string `json:"name"`
    Gift string `json:"gift"`
    Attended bool `json:"attended"`
}
func main() {
    echoLogo()
/*
    const letter = `
    Dear {{.Name}},
    {{if .Attended}}
    It was a pleasure to see you at the wedding.
    {{- else}}
    It is a shame you couldn't make it to the wedding. 
    {{- end}}
    {{with .Gift -}}
    Thank you for the lovely {{.}}.
    {{end}}
    Best wishes,
    Josie
    `
*/
    letter := loadTemplate()

/* 
    var recipients = []Recipient {
        {"Aunt Mildred","bone china tea set",true},
        {"Uncle John","moleskin pants",false},
        {"Cousin Rodney","",false},
    }
*/
    t := template.Must(template.New("letter").Parse(letter))

    recipients := getRecipients()
    var doc bytes.Buffer

    for _, r := range recipients {
        err := t.Execute(os.Stdout, r)
        err = t.Execute(&doc,r)
        ss:= doc.String()
        fmt.Println("string->"+ss);
        if err != nil {
            log.Println("executing template",err)
        }
    }

    err := ioutil.WriteFile("./outfile.txt",[]byte(doc.String()),0644)
    check(err)
    log.Print("DONE")
    echoLogo()
}
func check(e error) {
    if e != nil {//99049125 3201 6623 3321 1700 3321 1707 internacao
        panic(e)
    }

} 

func loadTemplate() string {
    b, err := ioutil.ReadFile("./template.txt")
    if err != nil {
        fmt.Print(err)
    }
    return string(b)
}
 
func getRecipients() []Recipient {
    raw, err := ioutil.ReadFile("./recipients.json")
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)

    }
    fmt.Println("read ok")
    fmt.Println(string(raw))
    var c []Recipient 
    json.Unmarshal(raw, &c)
    fmt.Println("length: "+string(len(c)))
    for _, recipient := range c {
        fmt.Println("Name: "+recipient.Name)
    }
    return c
}
func echoLogo() {
fmt.Println("Goh: a typical amazonic fish: simple, small and cheap but appreciated and nutritious.")
fmt.Println("..´¯`..¸¸..´¯`..¸¸.><((((º> ..´¯`..¸¸..´¯`..¸¸. ><((((º>")
fmt.Println("¯`..¸¸.><((((º > ¯`..¸¸.><((((º>¯`..¸¸. .><((((º>")
fmt.Println("¯`..¸¸.><((((º> ¯`..¸¸.><((((º> ><((((º>")
fmt.Println("..´¯`..¸¸..´¯`..¸¸. ><((((º> ¯`..¸¸.><((((º> ¯`..¸¸.")
fmt.Println("><((((º>¯`..¸¸. ><((((º>")
} 
