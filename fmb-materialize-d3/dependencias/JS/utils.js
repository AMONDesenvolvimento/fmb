function identificarBrowser(){

	// Opera 8.0+
	var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

	// Firefox 1.0+
	var isFirefox = typeof InstallTrigger !== 'undefined';

	// Safari 3.0+ "[object HTMLElementConstructor]" 
	var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

	// Internet Explorer 6-11
	var isIE = /*@cc_on!@*/false || !!document.documentMode;

	// Edge 20+
	var isEdge = !isIE && !!window.StyleMedia;

	// Chrome 1+
	var isChrome = !!window.chrome && !!window.chrome.webstore;

	if(isFirefox) return ("Firefox");
	if(isIE) return("IE");
	if(isEdge) return("Edge");
	if(isChrome) return("Chrome");
	if(isSafari) return("Safari");
	if(isOpera) return("Opera");
}

function formatarData(str){
	var data = new Date(str);
	var dia = data.getDate(), 
	    mes = data.getMonth(), 
	    ano = data.getYear() + 1900;

	if(data.getDate() <= 9)
		dia = "0" + data.getDate(); 

	if((data.getMonth() + 1) <= 9)
		mes = "0" + (data.getMonth() + 1); 
	
	return dia + "/" + mes + "/" + ano;
}

function intToMonth(int){
	var meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
	return meses[(int - 1)];
}

function formatarDinheiro(valor) { 
	var partes = valor.toString().split(".");
	var saida = partes[0];

	if(partes.length == 1){
		saida += "00";
	}
	else if(partes.length == 2){
		saida += (partes[1].length == 1) ?
			partes[1] + "0" : partes[1];
	}

	var tmp = saida.toString();
	tmp = tmp.toString().replace(/([0-9]{2})$/g, ",$1"); 
	if(tmp.length > 6)
		tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2"); 
	return "R$ " + tmp;
}

function desformatarDinheiro(valor){
	valor = valor.replace('R$ ', '');
	valor = valor.replace(',', '.');
	return parseFloat(valor);
}