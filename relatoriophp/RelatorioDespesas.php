<?php

require_once __DIR__ . "/vendor/autoload.php";

include ("geradorSubtitulo.php");
include ("geradorFiltro.php");
include ("filtro.php");

use Jaspersoft\Client\Client;
use Jaspersoft\Exception\RESTRequestException;
use Jaspersoft\Service\ReportService;

$ano = $_GET['exercicio'];
$mesreferencia=$_GET['mesreferencia'];
$datainicial=converteData($_GET['datainicial']);
$datafinal=converteData($_GET['datafinal']);
$tipo="'D%'";

$jasperclient = new Client(
                "http://200.98.70.20:8080/jasperserver",
                "jasperadmin",
                "jasperadmin"
            );

$filtro = new Filtro($ano,$mesreferencia,$datainicial,$datafinal,null,null);

$consulta='select sum(l.valor),v.nome,v.recdes, l.exercicio ,v.tipo, historico, l.mesreferencia, o.conta from lancamento l inner join   (evento v inner join orcamento o on v.gerencial = o.codred) 
on l.evento = v.id where v.tipo like '.$tipo;

$condicional = gerarFiltro($filtro);
$subtitulo= gerarSubtitulo($filtro);

if(!empty($condicional)){
	$consulta.= " and ".$condicional;
}
$consulta.=" group by (v.recdes) order by o.conta";

$controls = array(
	'consulta' =>array ($consulta),
	'subtitulo'=>array ($subtitulo)
	);

$report = $jasperclient->reportService()->runReport('/reports/Rel_despesas', 'pdf',null, null, $controls);

/*header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename=report.xls');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . strlen($report));
header('Content-Type: application/xls');*/
header('Content-Type: application/pdf');
echo $report;

var_dump($consulta);

?>