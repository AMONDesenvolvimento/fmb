<?php
 
 class Filtro {
	private $ano;
	private $mesreferencia;
	private $datainicial;
	private $datafinal;
	private $evento;
	private $tipo;
	private $conta;


	function __construct($ano = null,$mesreferencia=null,$datainicial=null,$datafinal=null,$evento=null,$tipo=null,$conta=null){
		$this->ano=$ano;
		$this->mesreferencia=$mesreferencia;
		$this->datainicial=$datainicial;
		$this->datafinal=$datafinal;
		$this->evento=$evento;
		$this->tipo=$tipo;
		$this->conta=$conta;
	} 


	function getAno(){
		return $this->ano;
	}

	function getTipo(){
		return $this->tipo;
	}

	function getMesreferencia(){
		return $this->mesreferencia;
	}

	function getDatainicial(){
		return $this->datainicial;
	}

	function getDatafinal(){
		return $this->datafinal;
	}

	function getEvento(){
		return $this->evento;
	}

	function getConta(){
		return $this->conta;
	}

	function setAno($ano){
		$this->ano=$ano;
	}

	function setTipo($tipo){
		$this->tipo=$tipo;
	}

	function setMesreferencia($mesreferencia){
		$this->mesreferencia=$mesreferencia;
	}

	function setDatainicial($datainicial){
		$this->datainicial=$datainicial;
	}

	function setDatafinal($datafinal){
		$this->datafinal=$datafinal;

	}

	function setEvento($evento){
		$this->evento=$evento;

	}

	function setConta($conta){
		$this->conta=$conta;

	}

}

?>