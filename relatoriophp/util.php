<?php

function converteData($data){
       if (strstr($data, "/")){//verifica se tem a barra /
           $d = explode ("/", $data);//tira a barra
           $rstData = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
           return $rstData;
       }else if(strstr($data, "-")){
          $data = substr($data, 0, 10);
          $d = explode ("-", $data);
          $rstData = "$d[2]/$d[1]/$d[0]";
          return $rstData;
       }
       else{
           return '';
      }
}

/*function gerarFiltro(Filtro $filtro) {
	$r = "";

 /* if (!empty($filtro->getConta())){
    $r = concatAnd($r, " o.conta like '".$filtro->getConta()."'");
  }
  if (!empty($filtro->getTipo())){
    $r = concatAnd($r, " v.tipo like '".$filtro->getTipo()."'");
  }
	if (!empty($filtro->getAno())){
		$r = concatAnd($r, " l.exercicio like ".$filtro->getAno());
	}
	if (!empty($filtro->getMesreferencia())){
		$r = concatAnd($r, " l.mesreferencia like ".$filtro->getMesreferencia());
	}
	if (!empty($filtro->getDatainicial()) && !empty($filtro->getDatafinal())){
		$r = concatAnd($r, " l.data between '".$filtro->getDatainicial()."' and '".$filtro->getDatafinal()."'");
	}
  if (!empty($filtro->getEvento())){
    $r = concatAnd($r, " v.id like ".$filtro->getEvento());
  }
	//return substr($r, 0,-4);
  return $r;
}

function concatAnd ($clausule ,$element)  {
    $r = $clausule; 
    if (!empty($clausule)) {
        $r.=" and ";
    } 
     $r.= $element;
    return $r;
}*/

function formataTipoConta($tipo){
  $r="";
 // $tipo= str_replace("'","", $tipo);
  if ($tipo =="D%" or $tipo=="DF" or $tipo=="DV"){
    $r="'4.%'";
  }else {
    $r="'3.%'";
  }
 return $r;
}

function formataTipo($tipo){
  $r="";
  $tipo= str_replace("'","", $tipo);
  if ($tipo=="D%"){
    $r="Despesas";
  }elseif ($tipo=="DF") {
    $r="Despesas Fixas";
  }elseif ($tipo=="DV") {
    $r="Despesas Variaveis";
  }elseif ($tipo=="R%"){
    $r="Receitas";
  }elseif ($tipo=="RF") {
    $r="Receitas Fixas";
  }elseif ($tipo=="RV"){
    $r="Receitas Variaveis";
  }
 return $r;
}

function formataConta($conta){
  $r="";
  $conta= str_replace("'","", $conta);
  if ($conta=="4.4%"){
    $r="Despesas Totais";
  }elseif ($conta=="4.4.1") {
    $r="Despesa com Passagens";
  }elseif ($conta=="4.4.2") {
    $r="Despesas com Hospedagem";
  }elseif ($conta=="4.4.3"){
    $r="Despesas com Diárias";
  }
  return $r;
}

function formataMes($mes){
  $r="";
  if ($mes=="1"){
    $r="Janeiro";
  }elseif ($mes=="2"){
    $r="Fevereiro";   
  }elseif ($mes=="3"){
    $r="Março";
  }elseif ($mes=="4"){
    $r="Abril";
  }elseif ($mes="5"){
    $r="Maio";
  }elseif ($mes="6"){
    $r="Junho";
  }elseif ($mes=="7"){
    $r="Julho";
  }elseif ($mes=="8"){
    $r="Agosto";
  }elseif ($mes=="9") {
    $r="Setembro";
  }elseif ($mes=="10") {
    $r="Outubro"; 
  }elseif ($mes=="11") {
    $r="Novembro";
  }else {
    $r="Dezembro";
  }
  return $r;
}

?>