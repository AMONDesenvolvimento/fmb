<?php
include ("util.php");

function gerarSubtitulo(Filtro $filtro) {
	$r = "";
  if (!empty($filtro->getConta() && empty($filtro->getEvento()))){
    $r = concatsubtitulo($r,  "Tipo: ".formataConta($filtro->getConta()));
    var_dump($r);
  }
  if (!empty($filtro->getTipo())){
    $r = concatsubtitulo($r,  "Tipo: ".formataTipo($filtro->getTipo()));
  }
	if (!empty($filtro->getAno())){
		$r = concatsubtitulo($r,  "Exercicio: ".$filtro->getAno());
	}
	if (!empty($filtro->getMesreferencia())){
		$r = concatsubtitulo($r, "Mes Referencia: ".formataMes($filtro->getMesreferencia()));
	}
	if (!empty($filtro->getDatainicial()) && !empty($filtro->getDatafinal())){
		$r = concatsubtitulo($r, "Periodo: ".converteData($filtro->getDatainicial())." a ".converteData($filtro->getDatafinal()));
	}
	return $r;
}

function concatsubtitulo ($clausule ,$element)  {
    $r = $clausule;
    if (!empty($clausule)) {
        $r.=" / ";
    }
    $r.= $element;
    return $r;
}

?>