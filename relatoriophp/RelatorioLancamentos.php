<?php

require_once __DIR__ . "/vendor/autoload.php";

include ("geradorSubtitulo.php");
include ("geradorFiltro.php");
include ("filtro.php");

use Jaspersoft\Client\Client;
use Jaspersoft\Exception\RESTRequestException;
use Jaspersoft\Service\ReportService;

//parametros de filtro
$ano = $_GET['exercicio'];
$tipo = $_GET['tipoevento'];
$mesreferencia=$_GET['mesreferencia'];
$evento=$_GET['evento'];
$datainicial=converteData($_GET['datainicial']);
$datafinal=converteData($_GET['datafinal']);

//cria conexao com servidor jasper
$jasperclient = new Client(
                "http://200.98.70.20:8080/jasperserver",
                "jasperadmin",
                "jasperadmin"
            );
//consulta base do relatorio principal
$consultaRelPrincipal ='select DISTINCT conta, o.descricao, orcamento from orcamento o  inner join  evento v on v.gerencial = o.codred';

$filtro = new Filtro($ano,$mesreferencia,$datainicial,$datafinal,$evento,$tipo);

$condicional = gerarFiltro($filtro);
$subtitulo = gerarSubtitulo($filtro);

//verifica se a consulta será por evento ou tipo e conta
if (!empty($evento)) {
  $consultaRelPrincipal.= " where v.id =  " .$evento;
} elseif (!empty($tipo)){
  $consultaRelPrincipal.= " where conta like ".formataTipoConta($tipo)." and v.tipo like '".$tipo."'";
}


if (!empty($condicional)) {
  $consultaSubRel = " and ".$condicional;
}
  $consultaSubRel .=" order by v.nome , l.data";

if (empty($subtitulo)){
	$subtitulo = "Todos";
}

//controles que serão enviados para o relatório
$controls = array(
   'consultaprincipal'=> array($consultaRelPrincipal),
   'consulta' => array($consultaSubRel),
   'subtitulo'=>array($subtitulo),
   'idevento'=> array($evento)
   );

//realiza a chamada ao relatório
$report = $jasperclient->reportService()->runReport('/reports/Rel_lancamentos', 'pdf',null, null, $controls);
header('Content-Type: application/pdf');
echo $report;

?>