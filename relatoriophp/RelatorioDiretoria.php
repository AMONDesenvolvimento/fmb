<?php

require_once __DIR__ . "/vendor/autoload.php";

include ("geradorSubtitulo.php");
include ("geradorFiltro.php");
include ("filtro.php");

use Jaspersoft\Client\Client;
use Jaspersoft\Exception\RESTRequestException;
use Jaspersoft\Service\ReportService;

$ano = $_GET['exercicio'];
$tipo = $_GET['tipoevento'];
$mesreferencia=$_GET['mesreferencia'];
$evento=$_GET['evento'];
$datainicial=converteData($_GET['datainicial']);
$datafinal=converteData($_GET['datafinal']);

$jasperclient = new Client(
                "http://200.98.70.20:8080/jasperserver",
                "jasperadmin",
                "jasperadmin"
            );

if(empty($tipo)){
	$conta="'4.4%'";
}else {
	$conta= "'".$tipo."'";
}

$filtro = new Filtro($ano,$mesreferencia,$datainicial,$datafinal,$evento,null,$conta);

$consultaRelprincipal = 'select DISTINCT conta, o.descricao, orcamento from orcamento o  inner join  evento v on v.gerencial = o.codred where conta like '.$conta;

$condicional = gerarFiltro($filtro);
$subtitulo = gerarSubtitulo($filtro);

if (!empty($condicional)) {
  $consulta = " and ".$condicional;
}
  $consulta .=" order by v.nome , l.data";

$controls = array(
   'consultaprincipal'=> array($consultaRelprincipal),
   'consulta' => array($consulta),
   'subtitulo'=>array($subtitulo),
   'idevento'=> array($evento)
	);

$report = $jasperclient->reportService()->runReport('/reports/Rel_diretoria', 'pdf',null, null, $controls);
header('Content-Type: application/pdf');
echo $report;

?>