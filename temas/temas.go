package temas

import (
//  "encoding/json"
    "database/sql"
  _ "github.com/mattn/go-sqlite3"
    "time"
    "github.com/tealeg/xlsx"
//    "fmt"
    "log"
    "unicode/utf8"
    "strconv"
    "strings"
    "flag"
    "io/ioutil"
    gr "github.com/mikeshimura/goreport"

)

var mainDB *sql.DB
var agora time.Time

type Gerencial struct {
        ID int64 `json:"id"`
        IdPai     int64  `json:"idpai"`
        Exercicio int64  `json:"exercicio"` 
        Conta     string `json:"conta"`
        Codred     int64  `json:"codred"`
	Descricao string `json:"descricao"`
        Analitica      string `json:"analitica"`
        SaldoAnterior float64 `json:"saldoanterior"`
        MovimentoDebitoCaixa float64 `json:"movimentodebitocaixa"`
        MovimentoCreditoCaixa float64 `json:"movimentocreditocaixa"`
        MovimentoDebitoCompetencia float64 `json:"movimentodebitocompetencia"`
        MovimentoCreditoCompetencia float64 `json:"movimentocreditocompetencia"`
        Orcamento  float64    `json:"orcamento"`
        DataCriacao string `json:"datacriacao"` 
        DataExclusao string `json:"dataexclusao"`
}
type GerencialArr []Gerencial

type TemaOrcamento struct {
        ID int64 `json:"id"`
        Exercicio int64 `json:"exercicio"`
        Conta1 string `json:"conta1"`
        Descricao1 string `json:"descricao1"`
        Conta2 string `json:"conta2"`
        Descricao2 string `json:"descricao2"`
        Conta3 string `json:"conta3"`
        Descricao3 string `json:"descricao3"`
        Conta4 string `json:"conta4"`
        Descricao4 string `json:"descricao4"`
        Orcamento float64 `json:"orcamento"`
        RealizadoCaixa float64 `json:"realizadocaixa"`
        RealizadoCompetencia float64 `json:"realizadocompetencia"`
}

type TemaDespesa struct {
        ID int64 `json:"id"`
        Exercicio int64 `json:"exercicio"`
        Mes       int64 `json:"mes"`
        DesMes   string `json:"desmes"`  
        Tipo     string `json:"tipo"`    
        Conta1 string `json:"conta1"`
        Descricao1 string `json:"descricao1"`
        Conta2 string `json:"conta2"`
        Descricao2 string `json:"descricao2"`
        Conta3 string `json:"conta3"`
        Descricao3 string `json:"descricao3"`
        Conta4 string `json:"conta4"`
        Descricao4 string `json:"descricao4"`
        RegimeCaixa float64 `json:"regimecaixa"`
        RegimeCompetencia float64 `json:"regimecompetencia"`
}

type TemaReceita struct {
        ID int64 `json:"id"`
        Exercicio int64 `json:"exercicio"`
        Mes       int64 `json:"mes"`
        DesMes   string `json:"desmes"`  
        Tipo     string `json:"tipo"`    
        Conta1 string `json:"conta1"`
        Descricao1 string `json:"descricao1"`
        Conta2 string `json:"conta2"`
        Descricao2 string `json:"descricao2"`
        Conta3 string `json:"conta3"`
        Descricao3 string `json:"descricao3"`
        Conta4 string `json:"conta4"`
        Descricao4 string `json:"descricao4"`
        RegimeCaixa float64 `json:"regimecaixa"`
        RegimeCompetencia float64 `json:"regimecompetencia"`
}

type Lancamento struct {
	ID   int64  `json:"id"`
        Evento     int64  `json:"evento"`
        NomeEvento string `json:nomeevento"`
        DescricaoEvento string `json:"descricaoevento"` 
        TipoEvento      string `json:"tipoevento"` 
        Valor       float64    `json:"valor"`
        Data time.Time  `json:"data"`
        Exercicio int64   `json:"exercicio"`
        MesReferencia int64   `json:"mesreferencia"`
        Historico  string     `json:"historico"`         
        DataCriacao string     `json:"datacriacao"` 
        DataExclusao string    `json:"dataexclusao"`
}

type Evento struct {
        ID int64 `json:"id"`
        Nome     string `json:"nome"`
	Descricao string `json:"descricao"`
        Recdes    int64 `json:"recdes"`
        DescricaoRecdes string `json:"descricaorecdes"`
        Debito    int64 `json:"debito"`
        DescricaoDebito string `json:"descricaodebito"`
        Credito int64   `json:"credito"`
        DescricaoCredito string `json:"descricaocredito"`
        Projeto int64   `json:"projeto"`
        DescricaoProjeto string `json:"descricaoprojeto"`
        Gerencial int64 `json:"gerencial"`
        DescricaoGerencial string `json:"descricaogerencial"`
        Tipo string     `json:"tipo"`
        DataCriacao string `json:"datacriacao"` 
        DataExclusao string `json:"dataexclusao"`
     
}

/*
func main() {
    load :=flag.Bool( "l",false,"load file before sum")
    sum  :=flag.Bool("s",false,"sum")
    report := flag.Bool("r",false,"report")
    flag.Parse()
    if *load {
        log.Println("load")
    } else {
        log.Println("Do not load")
    }
    if *sum {
        log.Println("sum")
    } else {
        log.Println("Do not sum")
    }
    if *report {
         log.Println("generate report")
    } else {
         log.Println("Do not generate report")
    }


    db, errOpenDB := sql.Open("sqlite3", "fmbtransparencia.db")
    checkErr(errOpenDB)
    mainDB = db
    if *load {
    var entrada Entrada
    excelFileName := "PlanodeContasGerencial.xlsx"
    xlFile, err := xlsx.OpenFile(excelFileName)
    if err != nil {
        panic(err)
    }
    for _, sheet := range xlFile.Sheets {
        for _, row := range sheet.Rows {
            if len(row.Cells)==0 {
                break;
            } 
            conta, _ := row.Cells[0].String()
            if strings.HasPrefix(conta,"Cod.Red") {
                continue
            }
            for j, cell := range row.Cells {
                text, _ := cell.String()
                if j==0 {
                    entrada.Codred = text
                    entrada.Descricao=""
                    entrada.Conta=""
                    entrada.Analitica=""
                    entrada.Orcamento=""
                    continue
                } else if j==1 {
                    entrada.Conta=text
                } else if j==2 {
                    entrada.Descricao=text
                } else if j==3 {
                    entrada.Orcamento=text
                }
            }
            gerencialInsert(entrada) 
        }
    }
    }
    if *sum { 
        gerencialSum(2017)
    }
    if *report {
        gerencialList()
    }
}
*/

func TemaOrcamentoGenerate(w http.ResponseWriter, r *http.Request) {

} 

func temaOrcamentoInsert(temaOrcamento TemaOrcamento) {
       stmt, err := mainDB.Prepare(
                                   `INSERT INTO temaorcamento (
                                   exercicio,
                                   conta1,
                                   descricao1,
                                   conta2,
                                   descricao2,
                                   conta3,
                                   descricao3,  
                                   conta4,
                                   descricao4,
                                   orcamento,
                                   realizadocaixa, 
                                   realizadocompetencia) values (?,?,?,?,?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(C
                           temaOrcamento.Exercicio,
                           temaOrcamento.Conta1,
                           temaOrcamento.Descricao1,
                           temaOrcamento.Conta2,
                           temaOrcamento.Descricao2,
                           temaOrcamento.Conta3,
                           temaOrcamento.Descricao3,
                           temaOrcamento.Conta4,
                           temaOrcamento.Descricao4,  
                           temaOrcamento.Orcamento,
                           temaOrcamento.RealizadoCaixa,
                           temaOrcamento.RealizadoCompetencia,
                           )
	checkErr(errExec)
//	newID, errLast := result.LastInsertId()
	_, errLast := result.LastInsertId()
	checkErr(errLast)
}

func temaDespesaInsert(temaDespesa TemaDespesa) {
       stmt, err := mainDB.Prepare(
                                   `INSERT INTO temadespesa (
                                   exercicio,
                                   tipo,
                                   conta1,
                                   descricao1,
                                   conta2,
                                   descricao2,
                                   conta3,
                                   descricao3,  
                                   conta4,
                                   descricao4,
                                   regimecaixa,
                                   regimecompetencia) values (?,?,?,?,?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(C
                           temaDespesa.Exercicio,
                           temaDespesa.Tipo,
                           temaDespesa.Conta1,
                           temaDespesa.Descricao1,
                           temaDespesa.Conta2,
                           temaDespesa.Descricao2,
                           temaDespesa.Conta3,
                           temaDespesa.Descricao3,
                           temaDespesa.Conta4,
                           temaDespesa.Descricao4,  
                           temaDespesa.RegimeCaixa,
                           temaDespesa.RegimeCompetencia,
                           )
	checkErr(errExec)
//	newID, errLast := result.LastInsertId()
	_, errLast := result.LastInsertId()
	checkErr(errLast)
}


func temaReceitaInsert(temaReceita TemaReceita) {
       stmt, err := mainDB.Prepare(
                                   `INSERT INTO temareceita (
                                   exercicio,
                                   tipo,
                                   conta1,
                                   descricao1,
                                   conta2,
                                   descricao2,
                                   conta3,
                                   descricao3,  
                                   conta4,
                                   descricao4,
                                   regimecaixa,
                                   regimecompetencia) values (?,?,?,?,?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           temaReceita.Exercicio,
                           temaReceita.Tipo,
                           temaReceita.Conta1,
                           temaReceita.Descricao1,
                           temaReceita.Conta2,
                           temaReceita.Descricao2,
                           temaReceita.Conta3,
                           temaReceita.Descricao3,
                           temaReceita.Conta4,
                           temaReceita.Descricao4,  
                           temaReceita.RegimeCaixa,
                           temaReceita.RegimeCompetencia,
                           )
	checkErr(errExec)
//	newID, errLast := result.LastInsertId()
	_, errLast := result.LastInsertId()
	checkErr(errLast)
}



func gerencialGetByConta(conta string, exercicio int64) Gerencial {
	stmt, err := mainDB.Prepare(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitoCaixa,
                                            movimentocreditoCaixa,
                                            movimentodebitoCompetencia,
                                            movimentocreditoCompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao  
                                            FROM planodecontasgerencial where exercicio = ? and conta = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(exercicio,conta)
	checkErr(errQuery)
	var gerencial Gerencial
	for rows.Next() {
		err = rows.Scan(&gerencial.ID,
                                &gerencial.IdPai,
                                &gerencial.Exercicio,
                                &gerencial.Conta, 
                                &gerencial.Codred,
                                &gerencial.Descricao,
                                &gerencial.Analitica,
                                &gerencial.SaldoAnterior,
                                &gerencial.MovimentoDebitoCaixa,
                                &gerencial.MovimentoCreditoCaixa,
                                &gerencial.MovimentoDebitoCompetencia,
                                &gerencial.MovimentoCreditoCompetencia,
                                &gerencial.Orcamento, 
                                &gerencial.DataCriacao,
                                &gerencial.DataExclusao)
		//checkErr(err)
	}
        //log.Println("get by conta ID    : "+strconv.FormatInt(gerencial.ID,10))
        //log.Println("get by conta Conta : "+gerencial.Conta)
        //log.Println("get by conta Descrição : "+gerencial.Descricao)
	return gerencial
}
func gerencialGetByCodred(codred int64, exercicio int64) Gerencial {
	stmt, err := mainDB.Prepare(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitoCaixa,
                                            movimentocreditoCaixa,
                                            movimentodebitoCompetencia,
                                            movimentocreditoCompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao  
                                            FROM planodecontasgerencial where exercicio = ? and codred = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(exercicio,codred)
	checkErr(errQuery)
	var gerencial Gerencial
	for rows.Next() {
		err = rows.Scan(&gerencial.ID,
                                &gerencial.IdPai,
                                &gerencial.Exercicio,
                                &gerencial.Conta, 
                                &gerencial.Codred,
                                &gerencial.Descricao,
                                &gerencial.Analitica,
                                &gerencial.SaldoAnterior,
                                &gerencial.MovimentoDebitoCaixa,
                                &gerencial.MovimentoCreditoCaixa,
                                &gerencial.MovimentoDebitoCompetencia,
                                &gerencial.MovimentoCreditoCompetencia,
                                &gerencial.Orcamento, 
                                &gerencial.DataCriacao,
                                &gerencial.DataExclusao)
		//checkErr(err)
	}
        //log.Println("get by conta ID    : "+strconv.FormatInt(gerencial.ID,10))
        //log.Println("get by conta Conta : "+gerencial.Conta)
        //log.Println("get by conta Descrição : "+gerencial.Descricao)
	return gerencial
}

func gerencialSum(exercicio int64) {
	stmt, err := mainDB.Prepare(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitoCaixa,
                                          movimentocreditoCaixa,
                                          movimentodebitoCompetencia,
                                          movimentocreditoCompetencia,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM planodecontasgerencial where exercicio = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(exercicio)
	checkErr(errQuery)
	var gerencialArr GerencialArr
	for rows.Next() {
		var gerencial Gerencial
		err = rows.Scan(
                               &gerencial.ID,
                               &gerencial.IdPai,
                               &gerencial.Exercicio, 
                               &gerencial.Conta,
                               &gerencial.Codred,
                               &gerencial.Descricao,
                               &gerencial.Analitica,
                               &gerencial.SaldoAnterior,
                               &gerencial.MovimentoDebitoCaixa,
                               &gerencial.MovimentoCreditoCaixa,
                               &gerencial.MovimentoDebitoCompetencia,
                               &gerencial.MovimentoCreditoCompetencia,
                               &gerencial.Orcamento,
                               &gerencial.DataCriacao,
                               &gerencial.DataExclusao, 
                               )
		//checkErr(err)

		gerencialArr = append(gerencialArr, gerencial)
	}
        for _,gerencial := range gerencialArr {
            if len(strings.Trim(gerencial.Analitica," "))==0 {
                continue
            }
            parent := extractParent(gerencial.Conta) 
            //log.Println("Parent---->"+parent+" da conta "+gerencial.Conta)
            for len(parent)>0 {
                gerencialParent := gerencialGetByConta(parent) 
                //log.Println("conta parent....->"+gerencialParent.Conta)
                //log.Println("orcamento parent->"+gerencialParent.Conta+"|"+strconv.FormatFloat(gerencialParent.Orcamento,'f',2,64))
                //log.Println("orcamento conta ->"+gerencial.Conta+"|"+strconv.FormatFloat(gerencial.Orcamento,'f',2,64))
                orcamento := gerencialParent.Orcamento + gerencial.Orcamento
                var movimentoDebitoCaixa float64
                var movimentoCreditoCaixa float64 
                var movimentoDebitoCompetencia float64
                var movimentoCreditoCompetencia float64 
                movimentoDebitoCaixa = gerencialParent.MovimentoDebitoCaixa + gerencialMovimentoDebitoCaixa
                movimentoCreditoCaixa = gerencialParent.MovimentoCreditoCaixa + gerencialMovimentoCreditoCaixa      
                movimentoDebitoCompetencia = gerencialParent.MovimentoDebitoCompetencia + gerencialMovimentoDebitoCompetencia
                movimentoCreditoCompetencia = gerencialParent.MovimentoCreditoCompetencia + gerencialMovimentoCreditoCompetencia      
                gerencialUpdateOrcamentoByID(gerencialParent.ID, orcamento)
                gerencialUpdateMovimentoDebitoByID(gerencialParent.ID,movimentoDebitoCaixa,movimentoDebitoCompetencia)
                gerencialUpdateMovimentoCreditoByID(gerencialParent.ID, movimentoCreditoCreditoCaixa,movimentoCreditoCompetencia)
                parent = extractParent(parent)
            }
        }
}

func gerencialInit(exercicio int64) {
	stmt, err := mainDB.Prepare(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitoCaixa,
                                          movimentocreditoCaixa,
                                          movimentodebitoCompetencia,
                                          movimentocreditoCompetencia,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM planodecontasgerencial where exercicio = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(exercicio)
	checkErr(errQuery)
	var gerencialArr GerencialArr
	for rows.Next() {
		var gerencial Gerencial
		err = rows.Scan(
                               &gerencial.ID,
                               &gerencial.IdPai,
                               &gerencial.Exercicio, 
                               &gerencial.Conta,
                               &gerencial.Codred,
                               &gerencial.Descricao,
                               &gerencial.Analitica,
                               &gerencial.SaldoAnterior,
                               &gerencial.MovimentoDebitoCaixa,
                               &gerencial.MovimentoCreditoCaixa,
                               &gerencial.MovimentoDebitoCompetencia,
                               &gerencial.MovimentoCreditoCompetencia,
                               &gerencial.Orcamento,
                               &gerencial.DataCriacao,
                               &gerencial.DataExclusao, 
                               )
		//checkErr(err)

		gerencialArr = append(gerencialArr, gerencial)
	}
        for _,gerencial := range gerencialArr {
            if len(strings.Trim(gerencial.Analitica," "))==0 {
                gerencialUpdateOrcamentoById(exercicio, gerencial.ID, 0.00)
                gerencialUpdateMovimentoDebitoById(exercicio, gerencial.ID, 0.00,0.00) 
                gerencialUpdateMovimentoCreditoById(exercicio,gerencial.ID, 0.00,0.00) 
            } else {
                gerencialUpdateMovimentoDebitoById(exercicio, gerencial.ID, 0.00,0.00) 
                gerencialUpdateMovimentoCreditoById(exercicio,gerencial.ID, 0.00,0.00) 
            }
        }
}




func gerencialUpdateOrcamentoByID(exercicio int64, id int64, orcamento float64) {

	stmt, err := mainDB.Prepare(`UPDATE planodecontasgerencial 
                                    SET (orcamento)=
                                    (?)     
                                    WHERE exercicio = ? and id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  orcamento,
                                  exercicio,
                                  id)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
 	if rowAffected > 0 {
		//jsonB, errMarshal := json.Marshal(gerencial)
		//checkErr(errMarshal)
		//fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		//fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}
}

func gerencialUpdateMovimentoDebitoByID(exercicio int64, id int64, movimentoDebitoCaixa float64. movimentoDebitoCompetencia) {

	stmt, err := mainDB.Prepare(`UPDATE planodecontasgerencial 
                                    SET (movimentoDebitoCaixa, movimentoDebitoCompetencia)=
                                    (?,?)     
                                    WHERE exercicio = ? and id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  movimentoDebitoCaixa,
                                  movimentoDebitoCompetencia,
                                  exercicio,
                                  id)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
 	if rowAffected > 0 {
		//jsonB, errMarshal := json.Marshal(gerencial)
		//checkErr(errMarshal)
		//fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		//fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}
}

func gerencialUpdateMovimentoCreditoByID(exercicio int64, id int64, movimentoCreditoCaixa float64, movimentoCreditoCompetencia) {

	stmt, err := mainDB.Prepare(`UPDATE planodecontasgerencial 
                                    SET (movimentoCreditoCaixa, movimentoCreditoCompetencia)=
                                    (?,?)     
                                    WHERE exercicio = ? and id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  movimentoCreditoCaixa,
                                  movimentoCreditoCompetencia,
                                  exercicio,   
                                  id)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
 	if rowAffected > 0 {
		//jsonB, errMarshal := json.Marshal(gerencial)
		//checkErr(errMarshal)
		//fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		//fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}
}

func extractParent(conta string) string {
    pos:=strings.LastIndex(conta,".") 
    if pos<0 {
        return ""
    } else {
        return conta[0:pos]
    }
}




func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func gerencialMovimento(exercicio int64) {
	rows, err := mainDB.Query(`SELECT id,
                                          evento,
                                          valor,
                                          data,
                                          exercicio,
                                          mesreferencia,
                                          historico,
                                          datacriacao,
                                          dataexclusao FROM lancamento`)
	checkErr(err)
	var lancamentoArr LancamentoArr
        var lancamento Lancamento 
	for rows.Next() {
		err = rows.Scan(
                               &lancamento.ID,
                               &lancamento.Evento,
                               &lancamento.Valor, 
                               &lancamento.Data,
                               &lancamento.Exercicio,
                               &lancamento.MesReferencia,
                               &lancamento.Historico,
                               &lancamento.DataCriacao,
                               &lancamento.DataExclusao, 
                               )
		//checkErr(err)

                evento :=eventoGetByID2(lancamento.Evento)
                lancamento.NomeEvento = evento.Nome
                lancamento.DescricaoEvento = evento.Descricao 
                lancamento.TipoEvento = evento.Tipo
		lancamentoArr = append(lancamentoArr, lancamento)
	}

        var contents string = ""
        for _,lancamento = range lancamentoArr {
            






           contents+=strconv.FormatInt(lancamento.ID, 10)+"\t"
           contents+=strconv.FormatInt(lancamento.Evento, 10)+"\t"
           contents+=lancamento.NomeEvento+"\t"
           contents+=lancamento.DescricaoEvento+"\t"
           contents+=lancamento.TipoEvento+"\t"
           contents+=strconv.FormatFloat(lancamento.Valor,'f',-1,64)+"\t"
           contents+=lancamento.Data.Format("2006-01-02 15:04:05")+"\t"
           contents+=strconv.FormatInt(lancamento.Exercicio,10)+"\t"
           contents+=strconv.FormatInt(lancamento.MesReferencia,10)+"\t"
           contents+=lancamento.Historico+"\t"
           contents+=lancamento.DataCriacao+"\t"
           contents+=lancamento.DataExclusao+"\n"
         } 
} 

func temaOrcamentoPopulate(int64 exercicio) {
        temaOrcamentoDeleteByExercicio(exercicio)
	stmt, err := mainDB.Prepare(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebito,
                                          movimentocredito,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM planodecontasgerencial where exercicio = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(exercicio)
	checkErr(errQuery)
	var gerencialArr GerencialArr
	for rows.Next() {
		var gerencial Gerencial
		err = rows.Scan(
                               &gerencial.ID,
                               &gerencial.IdPai,
                               &gerencial.Exercicio, 
                               &gerencial.Conta,
                               &gerencial.Codred,
                               &gerencial.Descricao,
                               &gerencial.Analitica,
                               &gerencial.SaldoAnterior,
                               &gerencial.MovimentoDebito,
                               &gerencial.MovimentoCredito,
                               &gerencial.Orcamento,
                               &gerencial.DataCriacao,
                               &gerencial.DataExclusao, 
                               )
		//checkErr(err)

		gerencialArr = append(gerencialArr, gerencial)
	}
        for _,gerencial := range gerencialArr {
            if len(strings.Trim(gerencial.Analitica," "))==0 {
                continue
            }
            parent := extractParent(gerencial.Conta) 
            //log.Println("Parent---->"+parent+" da conta "+gerencial.Conta)
            for len(parent)>0 {
                gerencialParent := gerencialGetByConta(parent) 
                //log.Println("conta parent....->"+gerencialParent.Conta)
                //log.Println("orcamento parent->"+gerencialParent.Conta+"|"+strconv.FormatFloat(gerencialParent.Orcamento,'f',2,64))
                //log.Println("orcamento conta ->"+gerencial.Conta+"|"+strconv.FormatFloat(gerencial.Orcamento,'f',2,64))
                acmOrcamento:= gerencialParent.Orcamento + gerencial.Orcamento
                acm
                gerencialUpdateByID(gerencialParent.ID, sum)
                parent = extractParent(parent)
            }
        }
}


func temaOrcamentoDeleteByExercicio(exercicio int64) {
	stmt, err := mainDB.Prepare("DELETE FROM temaorcamento WHERE exercicio = ?")
	checkErr(err)
	result, errExec := stmt.Exec(exercicio)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func temaDespesaDeleteByExercicio(exercicio int64) {
	stmt, err := mainDB.Prepare("DELETE FROM temadespesa WHERE exercicio = ?")
	checkErr(err)
	result, errExec := stmt.Exec(exercicio)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func temaReceitaDeleteByExercicio(exercicio int64) {
	stmt, err := mainDB.Prepare("DELETE FROM temareceita WHERE exercicio = ?")
	checkErr(err)
	result, errExec := stmt.Exec(exercicio)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}



