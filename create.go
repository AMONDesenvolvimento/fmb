package main
import (
    "database/sql"
//    "fmt"
//    "time"
    _ "github.com/mattn/go-sqlite3"
)
func main() {
    db, err := sql.Open("sqlite3","./fmbtransparencia.db")
    createConfiguracao(db)
    createPerfil(db)
    createPermissao(db)
    createUsuario(db)
    createEmailUsuario(db)
    createDataUsuario(db)
    createTelefoneUsuario(db)
    createEvento(db)
    createProjeto(db)
    createPlanoDeContas(db)
    createPlanoDeContasGerencial(db)
    createOrcamento(db)
    createLancamento(db)
    createInfografico(db) 
    createMensagem(db)
    createEventom(db)
    createDestinatario(db)
    createTema(db) 
    createTemaOrcamento(db)
    createTemaDespesa(db)
    createTemaReceita(db)
    checkErr(err)
    db.Close()
}
func checkErr(err error) {
    if err != nil {
        panic(err)
    }

}
func createConfiguracao(db *sql.DB) {
    //create table if not exists

    sql_table := `
    CREATE TABLE IF NOT EXISTS configuracao (
        id  integer not null primary key,
        nome varchar(50),
        descricao text,
        exercicio int,
        mesreferencia varchar(2),
        ultimomes     int,
        regimedespesa varchar(20),
        contadespesa  text,
        todassubcontasdespesa varchar(1),
        regimereceita varchar(20),
        contareceita  text,
        todassubcontasreceita varchar(1),
        regimeorcamento varchar(20),
        contaorcamento  text,
        todassubcontasorcamento varchar(1),
        datacriacao datetime,
        dataexclusao datetime
        );
    `
   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
}
func createPerfil(db *sql.DB) {
    //create table if not exists

    sql_table := `
    CREATE TABLE IF NOT EXISTS perfil (
        id  integer not null primary key,
        nome varchar(50),
        descricao text,
        login varchar(01),
        emissor varchar(01),
        destinatario varchar(01),
        responsavel  varchar(01),
        datacriacao datetime,
        dataexclusao datetime
        );
    `
/*

    sql_table := `
    CREATE TABLE IF NOT EXISTS items(
        Id TEXT NOT NULL PRIMARY KEY,
        Name TEXT, 
        Phone TEXT, 
        InsertedDatetime DATETIME
        );
        `
*/
   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
}


func createPermissao(db *sql.DB) {
    sql_table := `
                 CREATE TABLE IF NOT EXISTS permissao (
                 id integer primary key autoincrement,
                 perfil int,
                 opcao1 varchar(50),
                 opcao2 varchar(50),
                 opcao3 varchar(50),
                 habilitado varchar(1),
                 datacriacao datetime,
                 dataexclusao datetime
                 );` 

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
} 




func createEvento(db *sql.DB) {
    sql_table := `
                 CREATE TABLE IF NOT EXISTS evento (
                 id integer primary key,
                 nome varchar(50),
                 descricao text,
                 recdes integer,
                 debito integer,
                 credito integer,
                 projeto integer,
                 gerencial integer, 
                 tipo  varchar(2),  
                 datacriacao datetime,
                 dataexclusao datetime
                 );` 

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
} 

func createProjeto(db *sql.DB) {

    sql_table := `
    CREATE TABLE IF NOT EXISTS projeto (
    id  integer primary key autoincrement,
    idpai integer,
    conta text,  
    descricao text,
    acao varchar(1),
    datainicio date,
    datafim date,
    orcamento decimal(10,2),
    despesa   decimal(10,2),
    meta      decimal(10,2),
    realizado decimal(10,2),
    unidade   text,
    status    text,
    datacriacao datetime,
    dataexclusao datetime
);
    `


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }

} 



func createPlanoDeContas(db *sql.DB) {

    sql_table := `
    CREATE TABLE IF NOT EXISTS planodecontas (
    id  integer primary key autoincrement,
    idpai integer,
    exercicio integer,
    conta text,
    codred integer,  
    descricao text,
    analitica varchar(1),
    saldoanterior decimal(10,2),
    movimentodebito decimal(10,2),
    movimentocredito decimal(10,2),
    orcamento decimal(10,2),
    datacriacao datetime,
    dataexclusao datetime
);
    `


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }

}

func createPlanoDeContasGerencial(db *sql.DB) {

    sql_table := `
    CREATE TABLE IF NOT EXISTS planodecontasgerencial (
    id  integer primary key autoincrement,
    idpai integer,
    exercicio integer,
    conta text,
    codred integer,  
    descricao text,
    analitica varchar(1),
    saldoanterior decimal(10,2),
    movimentodebitoCaixa decimal(10,2),
    movimentocreditoCaixa decimal(10,2),
    movimentodebitoCompetencia decimal(10,2),
    movimentocreditoCompetencia decimal(10,2),
    orcamento decimal(10,2),
    datacriacao datetime,
    dataexclusao datetime
);
    `


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }

} 


func createOrcamento(db *sql.DB) {

    sql_table := `
    CREATE TABLE IF NOT EXISTS orcamento (
    id  integer primary key autoincrement,
    idpai integer,
    exercicio integer,
    conta text,
    codred integer,  
    descricao text,
    analitica varchar(1),
    saldoanterior decimal(10,2),
    movimentodebitoCaixa decimal(10,2),
    movimentocreditoCaixa decimal(10,2),
    movimentodebitoCompetencia decimal(10,2),
    movimentocreditoCompetencia decimal(10,2),
    orcamento decimal(10,2),
    datacriacao datetime,
    dataexclusao datetime
);
    `


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }

} 

 
func createUsuario(db *sql.DB) {

    sql_table := `
    CREATE TABLE IF NOT EXISTS usuario (
    id  integer primary key autoincrement,
    nome varchar(50),
    reduzido varchar(20),
    perfil integer,
    cpfcnpj varchar(18),
    dia integer,
    mes integer,
    endereco text,
    telefones text,
    email text,
    datacriacao datetime,
    dataexclusao datetime
);`


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
}

func createEmailUsuario(db *sql.DB) {
    sql_table := `
    CREATE TABLE IF NOT EXISTS emailusuario (
    id  integer primary key autoincrement,
    usuario int,
    tipo varchar(20),
    email text,
    datacriacao datetime,
    dataexclusao datetime
);`


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
}

func createTelefoneUsuario(db *sql.DB) {
    sql_table := `
    CREATE TABLE IF NOT EXISTS telefoneusuario (
    id  integer primary key autoincrement,
    usuario int,
    tipo varchar(20),
    telefone text,
    datacriacao datetime,
    dataexclusao datetime
);`


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
}

func createDataUsuario(db *sql.DB) {
    sql_table := `
    CREATE TABLE IF NOT EXISTS telefoneusuario (
    id  integer primary key autoincrement,
    usuario int,
    tipo varchar(20),
    dia varchar(2),
    mes varchar(2),
    ano varchar(4), 
    datacriacao datetime,
    dataexclusao datetime
);`


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
}


func createLancamento(db *sql.DB) {

    sql_table := `
    CREATE TABLE IF NOT EXISTS lancamento (
    id  integer primary key autoincrement,
    evento integer,
    valor decimal(10,2),
    data date,
    exercicio integer,
    mesreferencia integer,
    historico text,
    datacriacao datetime,
    dataexclusao datetime
);`


   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }
}

func createInfografico(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS infografico (
                id  integer primary key autoincrement,
                descricao varchar(50),
                tema varchar(50),
                perspectivas text,
                variaveis text,
                filtros text,
                tipo varchar(50),
                visibilidade varchar(10),
                abrangencia varchar(50),
                datacriacao datetime,
                dataexclusao datetime
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}

func createTema(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS tema (
                id  integer primary key autoincrement,
                nome varchar(50),
                descricao text,
                perspectivas text,
                variaveis text,
                filtros text,
                ultimageracao text,
                datacriacao datetime,
                dataexclusao datetime
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}
func createMensagem(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS mensagem (
                id  integer primary key autoincrement,
                nome varchar(50),
                descricao text,
                evento int,
                destinatario int,
                meio varchar(20),
                modelo text,
                datacriacao datetime,
                dataexclusao datetime
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}

func createEventom(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS eventom (
                id  integer primary key autoincrement,
                nome varchar(50),
                descricao text,
                tipodisparo varchar(20),
                disparo text,
                datacriacao datetime,
                dataexclusao datetime
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}

func createDestinatario(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS destinatario (
                id  integer primary key autoincrement,
                nome varchar(50),
                descricao text,
                expressao text,
                datacriacao datetime,
                dataexclusao datetime
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}
func createTemaOrcamento(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS temaorcamento (
                id  integer primary key autoincrement,
                exercicio int,
                conta1 text,
                descricao1 text,
                conta2 text,
                descricao2 text,
                conta3 text,
                descricao3 text,
                conta4 text,
                descricao4 text,
                orcamento decimal(10,2),
                realizadocaixa decimal(10,2),
                realizadocompetencia decimal(10,2)
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}

func createTemaDespesa(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS temadespesa (
                id  integer primary key autoincrement,
                exercicio int,
                mes       int,
                desmes    varchar(3),
                tipo      varchar(2),
                conta1 text,
                descricao1 text,
                conta2 text,
                descricao2 text,
                conta3 text,
                descricao3 text,
                conta4 text,
                descricao4 text,
                regimecaixa decimal(10,2),
                regimecompetencia decimal(10,2)
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}

func createTemaReceita(db *sql.DB) {

    sql_table:=`CREATE TABLE IF NOT EXISTS temareceita (
                id  integer primary key autoincrement,
                exercicio int,
                mes       int,
                desmes    varchar(3),
                tipo      varchar(2),
                conta1 text,
                descricao1 text,
                conta2 text,
                descricao2 text,
                conta3 text,
                descricao3 text,
                conta4 text,
                descricao4 text,
                regimecaixa decimal(10,2),
                regimecompetencia decimal(10,2)
     );`

   _, err := db.Exec(sql_table)
    if err != nil {
        panic(err)
    }


}





 
