/*
	FUNÇÕES DA PÁGINA /fmb/mensageria/index.html

	Este arquivo deve implementar todas as funções disparads pelos elementos de interação da página (cliques em botões,
	submissão de formulários, etc)

	Dependências deste script: 
	-Nenhuma
*/

// Operações de exclusão
function confirmarExclusaoLancamento(id){
	var orcamento;
	$.get( "lancamento" + '/' + id, function( data ) {
  		lancamento = JSON.parse(data);
	var modal = $("#modal-confirmacao");
	modal.find(".modal-content h4").html("Exclusão");
	modal.find(".modal-content p").remove();
	var detalhes = modal.find(".modal-content");
	detalhes.append("<p>").find("p:last").html("Evento: <b>"+lancamento.evento+"</b>");
	detalhes.append("<p>").find("p:last").html("Exercício: <b>"+lancamento.exercicio+"</b>");
	detalhes.append("<p>").find("p:last").html("Mês Referência: <b>"+descricaoMes(lancamento.mesreferencia)+"</b>");
	detalhes.append("<p>").find("p:last").html("Data: <b>"+formataData(lancamento.data)+"</b>");
	detalhes.append("<p>").find("p:last").html("valor: <b>"+formataDinheiro(lancamento.valor)+"</b>");
	
	//modal.find(".modal-content p").html("Deseja excluir o item <b>"+id+"</b>?");
	modal.find("#confirmacao-sim").click(function(){ excluirLancamento(id); });
	modal.find("#confirmacao-nao").click(function(){ modal.modal("close"); });
	modal.modal('open');
	});
}

function excluirLancamento(id){
    $.ajax({
	type: 'DELETE',
	url: "lancamento" + '/' + id,
	success: function(data, textStatus, jqXHR){
            $("#modal-confirmacao").modal("close");
		    var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Sucesso");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Lançamento excluído com sucesso!");
			modal.modal('open');
            setupLancamento();
	},
	error: function(jqXHR, textStatus, errorThrown){
		$("#modal-confirmacao").modal("close");
		    var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Erro");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Erro ao excluir Lançamento!");
			modal.modal('open');
		}
    });
}







//Operações de edição
function preencherFormEdicaoLancamento(id){
	//Buscar os dados do contato via AJAX
	$('.fixed-action-btn.toolbar').closeToolbar();

	var lancamento;
	$.get( "lancamento" + '/' + id, function( data ) {
  		lancamento = JSON.parse(data);

	var modal = $("#modal-lancamento-edicao");
	modal.find("#lancamento-edicao-exerc").val(lancamento.exercicio);
	modal.find("#lancamento-edicao-id").val(lancamento.id);
	modal.find("#evento-edicao-inclusao").val(lancamento.evento + " - " + lancamento.NomeEvento);
	modal.find("#lancamento-edicao-exercicio").find("option[value = "+ lancamento.exercicio +"]").prop("selected",true);
	modal.find("#lancamento-edicao-exercicio").parent().find("input.select-dropdown").val(lancamento.exercicio).prop("disabled",true);
	modal.find("#lancamento-edicao-valor").val(formataDinheiro(lancamento.valor));
	modal.find("#lancamento-edicao-data").val(formataData(lancamento.data));
	modal.find("#lancamento-edicao-mes").find("option[value = "+ lancamento.mesreferencia +"]").prop("selected",true);
	modal.find("#lancamento-edicao-mes").parent().find("input.select-dropdown").val(descricaoMes(lancamento.mesreferencia));
	modal.find("#lancamento-edicao-historico").val(lancamento.historico);
	modal.find("label").addClass("active");
	modal.modal("open");
	});
}

function alterarLancamento() {

    var parametros = {
    	"id" : parseInt($('#lancamento-edicao-id').val()),
		"exercicio": parseInt($('#lancamento-edicao-exerc').val()),
		"evento": parseInt($('#evento-edicao-inclusao').val()),
        "mesreferencia": parseInt($('#lancamento-edicao-mes').val()), 
		"historico": $('#lancamento-edicao-historico').val(),
		"valor": processaValorDinheiroNulo($('#lancamento-edicao-valor').val()),
		"data":  stringToDate($('#lancamento-edicao-data').val(),"dd/MM/yyyy","/")
	};
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: "lancamento/" + parametros.id,
		dataType: "json",
		data: JSON.stringify(parametros),
		success: function(data, textStatus, jqXHR){
			        $("#modal-lancamento-edicao").modal("close");
				    var modal = $("#modal-informacao");
					modal.find(".modal-content h4").html("Sucesso");
					modal.find(".modal-content p").remove();
					var detalhes = modal.find(".modal-content");
					detalhes.append("<p>").find("p:last").html("Lançamento Alterado com sucesso!");
					modal.modal('open');
					$('#form-lancamento')[0].reset();
					setupLancamento();         
			},
		error: function(jqXHR, textStatus, errorThrown){
	            $("#modal-lancamento-edicao").modal("close");
			    var modal = $("#modal-informacao");
				modal.find(".modal-content h4").html("Erro");
				modal.find(".modal-content p").remove();
				var detalhes = modal.find(".modal-content");
				detalhes.append("<p>").find("p:last").html("Erro ao alterar Lançamento!");
				modal.modal('open');
		}
	    });
}




//Exibição de detalhes
function detalharLancamento(id) {
	//Buscar o lancamento via AJAX pelo ID
	var lancamento,evento;
	$.ajaxSetup({async : false});
	$.get( "lancamento" + '/' + id, function( data ) {
  		lancamento = JSON.parse(data);
  	});
		$.get( "evento" + '/' + lancamento.evento, function( data ) {
			evento = JSON.parse(data);
		});
			var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Detalhar Lançamento");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Evento: <b>"+(evento.descricao == "" ? evento.nome : evento.descricao)+"</b>");
			detalhes.append("<p>").find("p:last").html("Evento: <b>"+evento.tipo+"</b>");
			detalhes.append("<p>").find("p:last").html("Exercício: <b>"+lancamento.exercicio+"</b>");
			detalhes.append("<p>").find("p:last").html("Mês Referência: <b>"+descricaoMes(lancamento.mesreferencia)+"</b>");
			detalhes.append("<p>").find("p:last").html("Data: <b>"+formataData(lancamento.data)+"</b>");
			detalhes.append("<p>").find("p:last").html("valor: <b>"+formataDinheiro(lancamento.valor)+"</b>");
			detalhes.append("<p>").find("p:last").html("Histórico: <b>"+lancamento.historico+"</b>");
			modal.modal('open');
}

//implementação valendo
function cadastrarLancamento() { 
	if(verificaRequeridos("#form-lancamento")){
		$("#modal-lancamento").modal("close");

		var parametros = {
		"exercicio": parseInt($('#lancamento-exercicio').val()),
		"evento": parseInt($('#evento-inclusao').val()),
        "data":  stringToDate($('#lancamento-data').val(),"dd/MM/yyyy","/"), 
		"mesreferencia": parseInt($('#lancamento-mes').val()),
		"historico": $('#lancamento-historico').val(),
		"valor": processaValorDinheiroNulo($('#lancamento-valor').val())
	};
	    $.ajax({
			type: 'POST',
			contentType: 'application/json',
			url: "lancamento",
			dataType: "json",
			data: JSON.stringify(parametros),
			success: function(data, textStatus, jqXHR){
			},
			error: function(jqXHR, textStatus, errorThrown){
			}
		});
			$("#modal-confirmacao").modal("close");
		    var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Sucesso");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Lançamento Incluido com sucesso!");
			modal.modal('open');
			$('#form-lancamento')[0].reset();
			setupLancamento();
	}
	else{		
		 $("#modal-confirmacao").modal("close");
		    var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Erro");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Prencha os campos obrigatórios");
			modal.modal('open');
		}

}
