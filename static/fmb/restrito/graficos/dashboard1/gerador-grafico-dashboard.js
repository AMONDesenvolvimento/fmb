function gerarGrafico(containerHTML, fData){
    var barColor = '#0277bd';
    
    function segColor(c){ return {
        aplicacaoFinanceira:"#807DBA", 
        disponibilidade:"#6BE5DB",
        contribuicaoSocial:"#41AB5D",
        contribuicaoSindical:"#E08214"}[c]; 
    }

    var hGDim;

    var dimensoesContainerBarras = { width: window.innerWidth * 0.54, height: 320 };
    var dimensoesContainerPizza = { width: window.innerWidth * 0.21, height: dimensoesContainerBarras.height };
    var dimensoesBarras = { width: dimensoesContainerBarras.width * 0.93, height: dimensoesContainerBarras.height * 0.9 };
    var dimensoesPizza = { width: dimensoesContainerPizza.width * 0.9, height: dimensoesContainerPizza.width * 0.9 };
    var margensBarras = { top: 60, right: 20, bottom: 30, left: 20 };
    var margensPizza = { top: 175, right: 20, bottom: 0, left: 20 };

    if(window.innerWidth <= 992 && window.innerWidth > 650) {
        dimensoesContainerBarras.width = window.innerWidth * 0.91;
        dimensoesBarras.width = dimensoesContainerBarras.width * 0.95;

        dimensoesContainerPizza.width = window.innerWidth * 0.91;
        dimensoesPizza.width = dimensoesContainerPizza.width * 0.3;
        margensPizza.top = 130;
    }
    else if(window.innerWidth <= 650 && window.innerWidth > 520){
        dimensoesContainerBarras.width = window.innerWidth * 0.88;
        dimensoesBarras.width = dimensoesContainerBarras.width * 0.93;

        dimensoesContainerPizza.width = window.innerWidth * 0.88;
        dimensoesPizza.width = dimensoesContainerPizza.width * 0.9;
        margensPizza.top = 130;
    }
    else if(window.innerWidth <= 520){
        dimensoesContainerBarras.width = window.innerWidth * 0.85;
        dimensoesBarras.width = dimensoesContainerBarras.width * 0.9;

        dimensoesContainerPizza.width = window.innerWidth * 0.85;
        dimensoesPizza.width = dimensoesContainerPizza.width * 0.9;
    }

    var tooltip = d3.select("body")
        .append("div")
        .attr("id", "tooltip")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("opacity", 0);
    // compute total for each state.
    //fData.forEach(function(d){d.total = d.freq.assessorias+d.freq.custeioInstalacoes+d.freq.despesasDiretoria+d.freq.folhaPagamento+d.freq.provisionamento;});
    
    // function to handle histogram.
    function barras(fD){
        var hG={};
        hGDim = {t: 50, r: 0, b: 30, l: 0};
        hGDim.h = 300 - hGDim.t - hGDim.b;

        if(window.innerWidth > 992){
            hGDim.w = window.innerWidth * 0.4;
        }
        else if(window.innerWidth <= 992 && window.innerWidth >= 790){
            hGDim.w = window.innerWidth * 0.926;
        }
        else if(window.innerWidth <= 790 && window.innerWidth >= 660){
            hGDim.w = window.innerWidth * 0.92;
        }
        else{
            hGDim.w = window.innerWidth * 0.91;
        }
            
        //create svg for histogram.
        var hGsvg = d3.select(containerHTML)
            .append("svg")
            .attr("width", dimensoesContainerBarras.width)
            .attr("height", dimensoesContainerBarras.height)
            .append("g")
            .attr("transform", "translate(" + margensBarras.left + "," + margensBarras.top + ")");

        // create function for x-axis mapping.
        var x = d3.scale.ordinal().rangeRoundBands([0, dimensoesBarras.width], 0.1)
                .domain(fD.map(function(d) { return d[0]; }));

        // Add x-axis to the histogram svg.
        hGsvg.append("g").attr("class", "x axis")
            .attr("transform", "translate(0," + 220 + ")")
            .call(d3.svg.axis().scale(x).orient("bottom"));

        var y = d3.scale.linear().range([220, 0])
                .domain([0, d3.max(fD, function(d) { return d[1]; })]);

        var bars = hGsvg.selectAll(".bar").data(fD).enter()
                .append("g").attr("class", "bar");
        
        bars.append("rect")
            .attr("x", function(d) { return x(d[0]); })
            .attr("y", function(d) { return y(d[1]); })
            .attr("width", x.rangeBand())
            .attr("height", function(d) { return 220 - y(d[1]); })
            .attr('fill',barColor)
            .on("mouseover",mouseover)// mouseover is defined below.
            .on("mouseout",mouseout);// mouseout is defined below.
            
        //Create the frequency labels above the rectangles.
        bars.append("text").text(function(d){ return formataDinheiro(d[1], false, false)})
            .attr("x", function(d) { return x(d[0])+x.rangeBand()/2; })
            .attr("y", function(d) { return y(d[1])-5; })
            .attr("font-size", "13px")
            .attr("text-anchor", "middle");
        
        function mouseover(d){
            var st = fData.filter(function(s){ return s.mes == d[0];})[0],
                nD = d3.keys(st.freq).map(function(s){ return {type:s, freq:st.freq[s]};})
                       .sort(function(a, b) { return d3.ascending(a.type, b.type); });
               
            // call update functions of pie-chart and legend.    
            pC.update(nD);
            leg.update(nD);
        }
        
        function mouseout(d){    // utility function to be called on mouseout.
            // reset the pie-chart and legend.    
            pC.update(tF);
            leg.update(tF);
        }
        
        // create function to update the bars. This will be used by pie-chart.
        hG.update = function(nD, color){
            // update the domain of the y-axis map to reflect change in frequencies.
            y.domain([0, d3.max(nD, function(d) { return d[1]; })]);
            
            // Attach the new data to the bars.
            var bars = hGsvg.selectAll(".bar").data(nD);
            
            // transition the height and color of rectangles.
            bars.select("rect").transition().duration(500)
                .attr("y", function(d) {return y(d[1]); })
                .attr("height", function(d) { return 220 - y(d[1]); })
                .attr("fill", color);

            // transition the frequency labels location and change value.
            bars.select("text").transition().duration(500)
                .text(function(d){ return formataDinheiro(d[1], false, false)})
                .attr("y", function(d) {return y(d[1])-5; });            
        }        
        return hG;
    }
    
    // function to handle pieChart.
    function pieChart(pD){
        var pC ={}, 
        pieDim = {w:270, h: 220};
        var raio = dimensoesPizza.width / 2;

        if(window.innerWidth > 650 && window.innerWidth <= 992)
            raio = 130;
        else if(window.innerWidth <= 650)
            raio = 130;

        // create svg for pie chart.
        var piesvg = d3.select(containerHTML).append("svg")
            .attr("width", dimensoesContainerPizza.width) 
            .attr("height", dimensoesContainerPizza.height)
            .append("g")
                .attr("transform", "translate("+dimensoesContainerPizza.width/2+","+margensPizza.top+")");
        
        // create function to draw the arcs of the pie slices.
        var arc = d3.svg.arc().outerRadius(raio - 10).innerRadius(0);

        // create a function to compute the pie slice angles.
        var pie = d3.layout.pie().sort(null).value(function(d) { return d.freq; });

        // Draw the pie slices.
        piesvg.selectAll("path").data(pie(pD)).enter().append("path").attr("d", arc)
            .each(function(d) { this._current = d; })
            .style("fill", function(d) { return segColor(d.data.type); })
            .on("mouseover",mouseover)
            .on("mouseout",mouseout)
            .on("mousemove",mousemove);

        // create function to update pie-chart. This will be used by histogram.
        pC.update = function(nD){
            piesvg.selectAll("path").data(pie(nD)).transition().duration(500)
                .attrTween("d", arcTween);
        }        
        // Utility function to be called on mouseover a pie slice.
        function mouseover(d){
            hG.update(fData.map(function(v){ 
                return [v.mes,v.freq[d.data.type]];}),segColor(d.data.type));
            tooltip.html(format_description(d.data));
            return tooltip.transition()
                  .duration(50)
                  .style("opacity", 0.9);
        }

        //Utility function to be called on mouseout a pie slice.
        function mouseout(d){
            // call the update function of histogram with all data.
            hG.update(fData.map(function(v){
                return [v.mes,v.total];}), barColor);
            return tooltip.style("opacity", 0);
        }

        function mousemove(d){
            return tooltip
                .style("top", (d3.event.pageY-10)+"px")
                .style("left", (d3.event.pageX+10)+"px");
        }

        function format_description(d) {
            switch(d.type){
                case "aplicacaoFinanceira": return "Aplicação Financeira";
                case "contribuicaoSindical": return "Contribuição Sindical";
                case "contribuicaoSocial": return "Contribuição Social";
                case "disponibilidade": return "Disponibilidade em 31/12/2017";
            }
            return d.type;
        }
        // Animating the pie-slice requiring a custom function which specifies
        // how the intermediate paths should be drawn.
        function arcTween(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function(t) { return arc(i(t));    };
        }    
        return pC;
    }
    
    // function to handle legend.
    function legend(lD){
        var leg = {};

        // create table for legend.
        var legend = d3.select(containerHTML).append("table").attr('class','legend bordered');
        
        // create one row per segment.
        var tr = legend.append("tbody").selectAll("tr").data(lD).enter().append("tr");
            
        // create the first column for each segment.
        tr.append("td")
                .attr("width", '16')
          .append("svg")
                .attr("width", '16')
                .attr("height", '16')
          .append("rect")
                .attr("width", '16')
                .attr("height", '16')
          .attr("fill", function(d){ return segColor(d.type); });
            
        // create the second column for each segment.
        tr.append("td").text(function(d){
            switch(d.type){
                case "aplicacaoFinanceira": return "Aplicação Financeira";
                case "contribuicaoSindical": return "Contribuição Sindical";
                case "contribuicaoSocial": return "Contribuição Social";
                case "disponibilidade": return "Disponibilidade em 31/12/2017";
            }
            return d.type;
        });

        // create the third column for each segment.
        tr.append("td").attr("class",'legendFreq')
            .text(function(d){ return formataDinheiro(d.freq);});

        // create the fourth column for each segment.
        tr.append("td").attr("class",'legendPerc')
            .text(function(d){ return getLegend(d,lD);});

        // Utility function to be used to update the legend.
        leg.update = function(nD){
            // update the data attached to the row elements.
            var l = legend.select("tbody").selectAll("tr").data(nD);

            // update the frequencies.
            l.select(".legendFreq").text(function(d){ return formataDinheiro(d.freq); });

            // update the percentage column.
            l.select(".legendPerc").text(function(d){ return getLegend(d,nD);});        
        }
        
        function getLegend(d,aD){ // Utility function to compute percentage.
            return ((d.freq/d3.sum(aD.map(function(v){ return v.freq; }))) * 100).toFixed(2) + "%";
            //return d3.format("%")(d.freq/d3.sum(aD.map(function(v){ return v.freq; })));
        }

        return leg;
    }
    
    // calculate total frequency by segment for all state.
    var tF = ['aplicacaoFinanceira','contribuicaoSindical','contribuicaoSocial','disponibilidade'].map(function(d){ 
        return {type:d, freq: d3.sum(fData.map(function(t){ return t.freq[d];}))};
    });    
    
    // calculate total frequency by state for all segment.
    var sF = fData.map(function(d){return [d.mes,d.total];});

    var hG = barras(sF), // create the histogram.
        pC = pieChart(tF), // create the pie-chart.
        leg= legend(tF);  // create the legend.
}
