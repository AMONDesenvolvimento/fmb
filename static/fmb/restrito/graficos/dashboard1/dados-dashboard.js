function obterTitulo(){
	return "Receitas mês a mês";
}

function obterSubtitulo(){
    return "Regime de Caixa";
}

function tratamentoDados(dados){
    var original = JSON.stringify(dados);

    var tratado = original.replace(/(Aplicação Financeira)/g, 'aplicacaoFinanceira');
    tratado = tratado.replace(/(Disponibilidade em 31\/12\/2017)/g, 'disponibilidade');
    tratado = tratado.replace(/(Contribuição Social)/g, 'contribuicaoSocial');
    tratado = tratado.replace(/(Contribuição Sindical)/g, 'contribuicaoSindical');

    var adaptado = $.parseJSON(tratado);

    //Tratamento dos dados para o layout correto do JSON
    var dados = [];
    $.each(adaptado, function(i, v){
        var total = v.p2.aplicacaoFinanceira + v.p2.disponibilidade + v.p2.contribuicaoSocial + v.p2.contribuicaoSindical;
        dados.push(
            {
                mes: v.p1, 
                freq: v.p2,
                total: total
            }
        );
    });

    return dados;
}

function gerarDados(){
    var dadosGrafico;

    $.ajaxSetup({async: false});

    $.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);

            var parametrosReceita = {
                exercicio: parametros.exercicio.toString(),
                mesreferencia: parametros.mesreferencia.toString(),
                ultimomes: parametros.ultimomes.toString(),
                conta: parametros.contareceita,
                regime: parametros.regimereceita,
                arvorecompleta: parametros.todassubcontasreceita
            };

            parametrosReceita = JSON.stringify(parametrosReceita);

            $.ajax({
                url: baseURL + "orcamentoreceitamensaldashboard",
                type: "POST",
                data: parametrosReceita
            })
            .done(function(data){
                var dadosJSON = $.parseJSON(data).dados;
                dadosGrafico = dadosJSON;
            });
        });

    return tratamentoDados(dadosGrafico);
}

