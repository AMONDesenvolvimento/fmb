function obterTitulo(){
	return "Execução orçamentária";
}

function obterSubtitulo(){
	var titulo;

	$.ajaxSetup({async: false});

	$.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);
            titulo = "até " + descricaoMes(parametros.ultimomes) + "/" + parametros.exercicio;
        });

	return titulo;
}

function gerarDados(){
	var dadosGrafico;

    $.ajaxSetup({async: false});

    $.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);

            var parametrosReceita = {
                exercicio: parametros.exercicio.toString(),
                mesreferencia: parametros.mesreferencia.toString(),
                ultimomes: parametros.ultimomes.toString(),
                conta: parametros.contareceita,
                regime: parametros.regimereceita,
                arvorecompleta: parametros.todassubcontasreceita
            };

            parametrosReceita = JSON.stringify(parametrosReceita);

            $.ajax({
                url: baseURL + "weightedTree/orcamentotree",
                type: "POST",
                data: parametrosReceita
            })
            .done(function(data){
                dadosGrafico = $.parseJSON(data);
            });
        });

    return  tratamentoDados(dadosGrafico);
}

function tratamentoDados(original){

	//Calcula as somatórias de despesa e receita
	var totalReceita = {orcado: 0.0, realizado: 0.0, saldo: 0.0};
	var totalDespesa = {orcado: 0.0, realizado: 0.0, saldo: 0.0};

	$.each(original, function(i, v){
		if(v.Category == "-Receita") {
			totalReceita.orcado += parseFloat(v.Total);
			totalReceita.realizado += parseFloat(v.State);
			totalReceita.saldo += parseFloat(v.Local);
		}
		else { //Despesa
			totalDespesa.orcado += parseFloat(v.Total);
			totalDespesa.realizado += parseFloat(v.State);
			totalDespesa.saldo += parseFloat(v.Local);
		}
	});

	var output = {
					realizado : 1025972.78,
					saldo : 1017026.57, 
					orcado : 2043000.00,
					nome : "",
					children: [ 
						{
							titulo: "Receita",
							subtitulo: "",
							nome: "Receita",
							cor: "green",
							orcado: totalReceita.orcado,
							realizado: totalReceita.realizado,
							saldo: totalReceita.saldo,
							children: []
						},
						{
							titulo: "Despesa",
							subtitulo: "",
							nome: "Despesa",
							cor: "red",
							orcado: totalDespesa.orcado,
							realizado: totalDespesa.realizado,
							saldo: totalDespesa.saldo,
							children: []
						} 
					]
				 };


	$.each(original, function(i, n1){
		var posicao, cor;
		if(n1.Category == "-Receita") {
			posicao = 0;
			cor = "green";
		}
		else {
			posicao = 1;
			cor = "red";
		}

		//Adicionando o nível 2
		if(n1.Level2 != "" && !itemInArray(n1, output.children[posicao].children)) {
			output.children[posicao].children.push(
				{
					titulo: n1.Category == "-Receita" ? "RECEITA" : "DESPESA",
					subtitulo: n1.Level2,
					nome: n1.Level2,
					orcado: n1.Total,
					realizado: n1.State,
					saldo: n1.Local,
					cor: cor,
					children: []
				}
			);
		}

		//Adicionando o nível 3
		if(n1.Level3 != ""){
			$.each(output.children[posicao].children, function(a, n2){
				if(n1.Level2 == n2.nome && n1.Total != 0){
					var obj = {
						titulo: n1.Category == "-Receita" ? "RECEITA" : "DESPESA",
						subtitulo: n1.Level3,
						nome: n1.Level3,
						orcado: n1.Total,
						realizado: n1.State,
						saldo: n1.Local,
						cor: cor,
						children: []
					};

					if(!itemInArray(obj, n2.children))
						n2.children.push(obj);
				}
			});
		}

	});


	

	//Adicionando o nível 4
	var zerado = false;

	$.each(original, function(i, v){
		if(v.Level4 != ""){
			$.each(output.children[1].children, function(a, n3){
				if(n3.nome == v.Level2){

					$.each(n3.children, function(a, n4){
						if(!zerado){
							n4.orcado = n4.realizado = n4.saldo = 0.0;
							zerado = true;
						}

						if(n4.nome == v.Level3){
							var obj = {
								titulo: n4.Category == "-Receita" ? "RECEITA" : "DESPESA",
								subtitulo: v.Level4,
								nome: v.Level4,
								cor: "red",
								orcado: v.Total,
								realizado: v.State,
								saldo: v.Local
							};

							if(!itemInArray(obj, n4.children)) {
								n4.children.push(obj);
								n4.orcado += obj.orcado;
								n4.realizado += obj.realizado;
								n4.saldo += obj.saldo;
							}
						}
					});
				}
			});
		}
	});


	//Ajuste na somatória do nível 2
	$.each([0, 1], function(i, p){
		$.each(output.children[p].children, function(i, v){
			var orcado = 0, realizado = 0, saldo = 0;
			if(v.children.length > 0) {
				$.each(v.children, function(x, y){
					orcado += y.orcado;
					realizado += y.realizado;
					saldo += y.saldo;
				});
			}
			else {
				orcado = v.orcado;
				realizado = v.realizado;
				saldo = v.saldo;
			}
			v.orcado = orcado;
			v.realizado = realizado;
			v.saldo = saldo;
		});
	});

	return output;
}

function itemInArray(item, array){
	var inArray = false;
	$.each(array, function(i, v){
		if(item.Level2 == v.nome || item.nome == v.nome)
			inArray = true;
	});
	return inArray;
}

function gerarDadosTeste(){
	return {
			cor : "teal",
			nome : "Receita",
			orcado : 100.00,
			realizado : 90.32,
			saldo : 9.68, 
			children : [
				{
					cor : "red",
					nome : "Disponibilidade em 31/12/2016",
					orcado : 30.54,
					realizado : 20.33,
					saldo : 10.78
				},
				{
					cor : "red",
					nome : "Contribuição social",
					orcado : 70.66,
					realizado : 20.15,
					saldo : 50.10,
					children: [
						{
							cor: "green",
							nome: "Pará/PA",
							orcado: 42000.00,
							realizado: 17500.00,
							saldo: 24500.00
						},
						{
							cor: "green",
							nome: "Paraíba/PB",
							orcado: 24000.00,
							realizado: 10000.00,
							saldo: 14000.00
						},
						{
							cor: "green",
							nome: "Campinas/SP",
							orcado: 36000.00,
							realizado: 8100.00,
							saldo: 27900.00
						}
					]
				}
			]
	};
}
