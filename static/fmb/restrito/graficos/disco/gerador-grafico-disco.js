function gerarGrafico(containerHTML, dados){
    var margem;
    if(window.innerWidth > 992)
      margem = window.innerWidth * 0.315;
    else if(window.innerWidth >= 600)
      margem = window.innerWidth * 0.4;
    else
      margem = window.innerWidth * 0.3;

    var subtracao;
    if(window.innerWidth >= 992)
      subtracao = 140;
    else if(window.innerWidth >= 600)
      subtracao = 80;
    else
      subtracao = 10;

    var margin = {top: margem, right: margem, bottom: margem, left: margem},
        radius = Math.min(margin.top, margin.right, margin.bottom, margin.left) - subtracao;

    function filter_min_arc_size_text(d, i) { return (d.dx*d.depth*radius/3) > 14 }; 

    var hue = d3.scale.category10().range(["#807dba","#6be5db","#f98a09","#AA78DE","#41ab5d","#ff9ea4","#00ffff"]);

    var luminance = d3.scale.sqrt()
        .domain([0, 1e6])
        .clamp(true)
        .range([90, 20]);

    var svg = d3.select(containerHTML).append("svg")
        .attr("width", function(){
          var larguraSVG;
          if(window.innerWidth <= 600)
            larguraSVG = window.innerWidth * 0.87;
          else if(window.innerWidth <= 992)
            larguraSVG = window.innerWidth * 0.94;
          else
            larguraSVG = window.innerWidth * 0.79;
          return larguraSVG;
        })
        .attr("height", 2 * (radius + 60))
      .append("g")
        .attr("transform", function(){
          var x, y = margin.top * 0.8;
          if(window.innerWidth <= 600) {
            x = margin.left * 1.4;
            y = margin.top;
          }
          else if(window.innerWidth <= 992) {
            x = margin.left * 1.15;
            y = margin.top * 0.9;
          }
          else
            x = margin.left * 1.25;

          return "translate(" + x + "," + y + ")";
        });

    var partition = d3.layout.partition()
        .sort(function(a, b) { return d3.ascending(a.name, b.name); })
        .size([2 * Math.PI, radius]);

    var arc = d3.svg.arc()
        .startAngle(function(d) { return d.x; })
        .endAngle(function(d) { return d.x + d.dx - .01 / (d.depth + .5); })
        .innerRadius(function(d) { return radius / 3 * d.depth; })
        .outerRadius(function(d) { return radius / 3 * (d.depth + 1) - 1; });

    var tooltip = d3.select("body")
        .append("div")
        .attr("id", "tooltip")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("opacity", 0);

    function formatNumber(_number,scale) { 
        var r=_number.toFixed(scale).replace(/./g, function(c,i,a) { 
            return i>0 && c !== "." && ((a.length-i)%3 ===0)?','+c:c; 
        }); 
        r=r.replace(".","#"); 
        r=r.replace(",","."); 
        r=r.replace("#",","); 
        return "R$ "+r; 
    }

    function format_description(d) {
      return '<b>' + d.name + '</b></br>' + formatNumber(d.value, 2);
    }

    function computeTextRotation(d) {
    	var angle=(d.x +d.dx/2)*180/Math.PI - 90	
    	return angle;
    }

    function mouseOverArc(d) {
		  d3.select(this).attr("stroke","black");
		  tooltip.html(format_description(d));
      return tooltip.transition()
          .duration(50)
          .style("opacity", 0.9);
    }

    function mouseOutArc(){
    	d3.select(this).attr("stroke","")
    	return tooltip.style("opacity", 0);
    }

    function mouseMoveArc (d) {
              return tooltip
                .style("top", (d3.event.pageY-10)+"px")
                .style("left", (d3.event.pageX+10)+"px");
    }

    var root = dados;

    partition
        .value(function(d) { return d.size; })
        .nodes(root)
        .forEach(function(d) {
          d._children = d.children;
          d.sum = d.value;
          d.key = key(d);
          d.fill = fill(d);
        });

    // Now redefine the value function to use the previously-computed sum.
    partition
        .children(function(d, depth) { 
          return depth < 2 ? d._children : null; })
        .value(function(d) { return d.sum; });

    var center = svg.append("circle")
        .attr("r", radius / 3)
        .on("click", zoomOut);

    var partitioned_data=partition.nodes(root).slice(1)

    var path = svg.selectAll("path")
        .data(partitioned_data)
      .enter().append("path")
        .attr("d", arc)
        .style("fill", function(d) { return d.fill; })
        .each(function(d) { this._current = updateArc(d); })
        .on("click", zoomIn)
  		.on("mouseover", mouseOverArc)
        .on("mousemove", mouseMoveArc)
        .on("mouseout", mouseOutArc);

    function zoomIn(p) {
      if (p.depth > 1) p = p.parent;
      if (!p.children) return;
      zoom(p, p);
    }

    function zoomOut(p) {
      if (!p.parent) return;
      zoom(p.parent, p);
    }

    // Zoom to the specified new root.
    function zoom(root, p) {
      if (document.documentElement.__transition__) return;

      // Rescale outside angles to match the new layout.
      var enterArc,
          exitArc,
          outsideAngle = d3.scale.linear().domain([0, 2 * Math.PI]);

      function insideArc(d) {
        return p.key > d.key
            ? {depth: d.depth - 1, x: 0, dx: 0} : p.key < d.key
            ? {depth: d.depth - 1, x: 2 * Math.PI, dx: 0}
            : {depth: 0, x: 0, dx: 2 * Math.PI};
      }

      function outsideArc(d) {
        return {depth: d.depth + 1, x: outsideAngle(d.x), dx: outsideAngle(d.x + d.dx) - outsideAngle(d.x)};
      }

      center.datum(root);

      if (root === p) enterArc = outsideArc, exitArc = insideArc, outsideAngle.range([p.x, p.x + p.dx]);
  	
  	  var new_data=partition.nodes(root).slice(1)

      path = path.data(new_data, function(d) { return d.key; });
  	 	 
      if (root !== p) enterArc = insideArc, exitArc = outsideArc, outsideAngle.range([p.x, p.x + p.dx]);

      d3.transition().duration(d3.event.altKey ? 7500 : 750).each(function() {
        path.exit().transition()
            .style("fill-opacity", function(d) { return d.depth === 1 + (root === p) ? 1 : 0; })
            .attrTween("d", function(d) { return arcTween.call(this, exitArc(d)); })
            .remove();
            
        path.enter().append("path")
            .style("fill-opacity", function(d) { return d.depth === 2 - (root === p) ? 1 : 0; })
            .style("fill", function(d) { return d.fill; })
            .on("click", zoomIn)
  			 .on("mouseover", mouseOverArc)
        	 .on("mousemove", mouseMoveArc)
        	 .on("mouseout", mouseOutArc)
            .each(function(d) { this._current = enterArc(d); });
  		
        path.transition()
            .style("fill-opacity", 1)
            .attrTween("d", function(d) { return arcTween.call(this, updateArc(d)); });
      });
    }

    function key(d) {
      var k = [], p = d;
      while (p.depth) k.push(p.name), p = p.parent;
      return k.reverse().join(".");
    }

    function fill(d) {
      var p = d;
      while (p.depth > 1) p = p.parent;
      var c = d3.lab(hue(p.name));
      c.l = luminance(d.sum);
      return c;
    }

    function arcTween(b) {
      var i = d3.interpolate(this._current, b);
      this._current = i(0);
      return function(t) {
        return arc(i(t));
      };
    }

    function updateArc(d) {
      return {depth: d.depth, x: d.x, dx: d.dx};
    }

    d3.select(self.frameElement).style("height", margin.top + margin.bottom + "px");

    function legend(lD){
        var leg = {};

        // create table for legend.
        var legend = d3.select(containerHTML).append("table").attr('class','legend bordered');
        
        // create one row per segment.
        var tr = legend.append("tbody").selectAll("tr").data(lD).enter().append("tr");
            
        // create the first column for each segment.
        tr.append("td")
                .attr("width", '16')
          .append("svg")
                .attr("width", '16')
                .attr("height", '16')
          .append("rect")
                .attr("width", '16')
                .attr("height", '16')
          .attr("fill", function(d){ return hue(d.name); });
            
        // create the second column for each segment.
        tr.append("td").text(function(d){ return d.name;});

        // create the third column for each segment.
        tr.append("td").attr("class",'legendFreq')
            .text(function(d){return formataDinheiro(d.value);});

        // create the fourth column for each segment.
        tr.append("td").attr("class",'legendPerc')
            .text(function(d){ return getLegend(d,lD);});

        function getLegend(d,aD){ // Utility function to compute percentage.
            return d3.format("%")(d.value/d3.sum(aD.map(function(v){ return v.value; })));
        }

        return leg;
    }
    
    // calculate total frequency by segment for all state.
    var tF = root.children;
    
    // calculate total frequency by state for all segment.
    var leg= legend(tF);  // create the legend.
}