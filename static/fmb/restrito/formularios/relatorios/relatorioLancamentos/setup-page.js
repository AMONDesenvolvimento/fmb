function listaEventos(){
    var urlObterContatosPF = baseURL + "evento";

    $.get(urlObterContatosPF)
	    .done(function(data){
	        var eventos = JSON.parse(data);

	        var idTabela = "#tabelaevento";
	        var idFiltro = "#filtro-agenda";

	        //Configura as tabelas com filtro de dados
	        tableFilter(idTabela, idFiltro);

	        var corpoTabela = $(idTabela + " tbody");
	        corpoTabela.empty();
	        $.each(eventos, function(i, v){
	            corpoTabela.append("<tr>");

	            var ultimaLinhaAdicionada = corpoTabela.find("tr:last");
	            ultimaLinhaAdicionada.attr("id","evento-"+v.id);

	            ultimaLinhaAdicionada.append("<td>");
	            ultimaLinhaAdicionada.find("td:last").html(v.id);

	            ultimaLinhaAdicionada.append("<td>");
	            ultimaLinhaAdicionada.find("td:last").html(v.nome);

	            ultimaLinhaAdicionada.append("<td>");
	            ultimaLinhaAdicionada.find("td:last").html(v.tipo);
	        });

        modalSelector2("evento-nome","selecionar-evento-nome","evento-",2,"evento-id");
        inicializarComponentesMaterialize();
    });
}


listaEventos();

