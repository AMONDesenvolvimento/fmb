/*
	FUNÇÕES DA PÁGINA /fmb/mensageria/index.html

	Este arquivo deve implementar todas as funções disparads pelos elementos de interação da página (cliques em botões,
	submissão de formulários, etc)

	Dependências deste script: 
	-Nenhuma
*/


var baseURLEvento = baseURL + "evento";

//Operações de edição
function preencherFormEdicaoEvento(id){
	$.get(baseURLEvento+"/"+ id, function( data ) {
  		var evento = JSON.parse(data);
		var modal = $("#modal-evento-edicao");
		modal.find("#evento-edicao-id").val(evento.id);
		modal.find("#evento-edicao-nome").val(evento.nome);
		modal.find("#evento-edicao-descricao").val(evento.descricao);
		modal.find("#evento-edicao-recdes").val(evento.recdes).prop("disabled",true);;
		modal.find("#evento-edicao-credito").val(evento.credito).prop("disabled",true);;
		modal.find("#evento-edicao-debito").val(evento.debito).prop("disabled",true);;
		modal.find("#evento-edicao-projeto").val(evento.projeto).prop("disabled",true);
		modal.find("#evento-edicao-gerencial").val(evento.gerencial).prop("disabled",true);
		modal.find("#evento-edicao-tipo").parent().find("input.select-dropdown").val(evento.tipo).prop("disabled",true);
		modal.find("label").addClass("active");
		modal.modal("open");
	});
}


function alterarEvento() {
    var parametros = {
    	"id":parseInt($('#evento-edicao-id').val()),
		"nome": $('#evento-edicao-nome').val(),
		"descricao":$('#evento-edicao-descricao').val(),
        
	};


    $.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: baseURLEvento+"/"+parametros.id,
		dataType: "json",
		data: JSON.stringify(parametros),
		beforeSend: function(){
			$("#botao-salvar").hide();
			criarSpinner("#spinner-edicao-evento");
		},
		success: function(){
			$("#modal-evento-edicao").modal("close");
			ocultarSpinner("#spinner-edicao-evento");
			Materialize.toast('Evento alterado com sucesso!', 1000,"green lighten-1",function(){
				location.reload(true);
			});
		},
		error: function(){
			ocultarSpinner("#spinner-edicao-evento");
			$("#botao-salvar").show();
            Materialize.toast('Erro ao alterar Evento!', 2000,"red lighten-1");
		}
    });
}


//Exibição de detalhes
function detalharEvento(id) {
	$.get(baseURLEvento+"/"+id, function(data) {
  		var evento = JSON.parse(data);
		var modal = $("#modal-detalhamento");
		modal.find(".modal-content h4").html("Detalhes do Orçamento");
		modal.find(".modal-content p").remove();
		var detalhes = modal.find(".modal-content");
		detalhes.append("<p>").find("p:last").html("Id: <b>"+evento.id+"</b>");
		detalhes.append("<p>").find("p:last").html("Nome: <b>"+evento.nome+"</b>");
		detalhes.append("<p>").find("p:last").html("Descrição: <b>"+evento.descricao+"</b>");
		detalhes.append("<p>").find("p:last").html("RecDes: <b>"+evento.recdes+"</b>");
		detalhes.append("<p>").find("p:last").html("Crédito: <b>"+evento.credito+"</b>");
		detalhes.append("<p>").find("p:last").html("Débito: <b>"+evento.debito+"</b>");
		detalhes.append("<p>").find("p:last").html("Projeto: <b>"+evento.projeto+"</b>");
		detalhes.append("<p>").find("p:last").html("Gerencial: <b>"+evento.gerencial+"</b>");
		detalhes.append("<p>").find("p:last").html("Tipo: <b>"+evento.tipo+"</b>");
		modal.modal('open');
	});
}

//Cadastro de orçamento
function cadastrarEvento() { 
	if(verificaRequeridos("#form-evento")){
		$("#modal-agenda").modal("close");

		var parametros = {
	        "nome": $('#evento-nome').val(), 
			"descricao": $('#evento-descricao').val(),
			"recdes": parseInt($('#evento-recdes').val()),
			"credito": parseInt($('#evento-credito').val()),
			"debito": parseInt($('#evento-debito').val()),
			"projeto": parseInt($('#evento-projeto').val()),
			"gerencial": parseInt($('#evento-gerencial').val()),
			"tipo": $('#evento-tipoevento').val()
		};

	    $.ajax({
			type: 'POST',
			contentType: 'application/json',
			url: baseURLEvento,
			dataType: "json",
			data: JSON.stringify(parametros),
			success: function(){
	            Materialize.toast('Evento incluído com sucesso!', 2000, "green lighten-1", function(){
					$("#modal-confirmacao").modal("close");
					location.reload(true);
				});
			}
		});
	}
	else{
		Materialize.toast('Preencha os campos obrigatórios.', 3000, "red lighten-1");
	}
}

function confirmarExclusãoEvento(id){
	$.get(baseURLEvento+"/"+id, function(data) {
		var evento = JSON.parse(data);
			var modal = $("#modal-confirmacao");
			modal.find(".modal-content h4").html("Exclusão");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Id: <b>"+evento.id+"</b>");
			detalhes.append("<p>").find("p:last").html("Nome: <b>"+evento.nome+"</b>");
			detalhes.append("<p>").find("p:last").html("Descrição: <b>"+evento.descricao+"</b>");
			detalhes.append("<p>").find("p:last").html("Tipo: <b>"+evento.tipo+"</b>");
			modal.find("#confirmacao-sim").click(function(){ verificarExclusaoEvento(id); modal.modal('close');});
			modal.find("#confirmacao-nao").click(function(){ modal.modal("close"); });
			modal.modal('open');
	});
}

function verificarExclusaoEvento(id){
	var lanc;
	$.get(baseURLEvento+"l"+"/"+id, function(data){
		lanc = JSON.parse(data);
		if(lanc.id === ""){
			excluirEvento(id);
		}
		else {
			var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Exclusão");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Evento não pode ser excluido, pois o mesmo possui lançamentos.");
			modal.modal('open');

		}
	});
}

function excluirEvento(id){
    $.ajax({
	type: 'DELETE',
	url: baseURLEvento+ '/' + id,
	success: function(data, textStatus, jqXHR){
            $("#modal-confirmacao").modal("close");
		    var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Sucesso");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Evento excluído com sucesso!");
			modal.modal('open');
            preencheEvento();
	},
	error: function(jqXHR, textStatus, errorThrown){
		$("#modal-confirmacao").modal("close");
		    var modal = $("#modal-informacao");
			modal.find(".modal-content h4").html("Erro");
			modal.find(".modal-content p").remove();
			var detalhes = modal.find(".modal-content");
			detalhes.append("<p>").find("p:last").html("Erro ao excluir Evento!");
			modal.modal('open');
		}
    });
}