/*
	SETUP DA PÁGINA /fmb/mensageria/index.html

	Este arquivo deve fazer todo o processamento para o carregamento inicial da página.
	Neste processamento deve estar qualquer requisição AJAX necessária para carregamento de dados, execução de animações, etc.

	Dependências deste script: 
	- /fmb/dependencias/JQuery/jquery-3.1.0.min.js
	- /fmb/dependencias/JQuery/jquery-ui.min.js
	- /fmb/dependencias/Materialize/js/materialize.js
	- /fmb/dependencias/css/spinner.css
	- /fmb/dependencias/js/spinner.js
*/


//URLs temporárias
var urlObterEvento = baseURL + "evento";

function preencheEvento(){

	//Cria os spinners
	criarSpinner("#spinner-evento");


	//Preenche a tabela de Evento
	$.get(urlObterEvento)
	    .done(function(data){
			var contatos = data;
			var idTabela = "#tabela-evento";

			//Configura as tabelas com filtro de dados
			tableFilter(idTabela, "#filtro-evento");

			var corpoTabela = $(idTabela+" tbody");
			corpoTabela.empty();
			$.each(JSON.parse(contatos), function(i, v){
				corpoTabela.append("<tr>");

				var ultimaLinhaAdicionada = corpoTabela.find("tr:last");
				ultimaLinhaAdicionada.addClass("dropdown-button").addClass("hoverable").attr("data-activates", "menu-"+v.id);
	            ultimaLinhaAdicionada.attr("id","evento-"+v.id);

	            ultimaLinhaAdicionada.append("<td>");
	            ultimaLinhaAdicionada.find("td:last").html(v.id);

	            ultimaLinhaAdicionada.append("<td>");
	            ultimaLinhaAdicionada.find("td:last").html(v.nome);

	            ultimaLinhaAdicionada.append("<td>");
	            ultimaLinhaAdicionada.find("td:last").html(v.tipo);

				var ultimaCelulaAdicionada = ultimaLinhaAdicionada.find("td:last");
				ultimaCelulaAdicionada.append("<ul>").find("ul:last").addClass("dropdown-content").addClass("z-depth-3").attr("id", "menu-"+v.id);

				var listaOpcoes = ultimaCelulaAdicionada.find("ul#menu-"+v.id);
				listaOpcoes.append('<li><a href="#">Detalhar</a></li>').find("a:last").attr("onclick", "detalharEvento("+v.id+")");
				listaOpcoes.append('<li><a href="#">Editar</a></li>').find("a:last").attr("onclick", "preencherFormEdicaoEvento("+v.id+")");
				listaOpcoes.append('<li><a href="#">Excluir</a></li>').find("a:last").attr("onclick", "confirmarExclusãoEvento("+v.id+")");
				$('.dropdown-button.hoverable').dropdown({
			        constrainWidth: false,
			        hover: false
			    });

			    //Destaca o item clicado na tabela
			    corpoTabela.find("tr").click(function(){
			    	corpoTabela.find("tr").removeClass("linha-em-foco");
			    	//$(this).addClass("linha-em-foco");
			    });
			    //Remove o destaque ao sair da tabela
			    corpoTabela.on('mouseleave', function(){
			    	corpoTabela.find("tr").removeClass("linha-em-foco");
			    });
			});
		
			//Oculta o spinner e mostra a tabela
			ocultarSpinner("#spinner-evento");
			$("#painel-evento").show("fade", 500);
			
		});
	$("#form-evento")[0].reset();
}

preencheEvento();

