function enviarSMS(credenciais, numero, titulo, mensagem) {
    
    //Formatando o número destino
    numero = "55" + numero;

    $.ajax({
        type: 'POST',
        xhrFields: {
            withCredentials: false
        },
        crossDomain: true,
        contentType: 'application/json',
        url: 'https://api-rest.zenvia360.com.br/services/send-sms',
        dataType: "json",
        data: formatarParametrosSMS(numero, titulo, mensagem),
        success: function(data){
            if(data.sendSmsResponse.statusCode == "00") {
                numero = "(" + numero.substr(2, 2) + ") " + numero.substr(4, 5) + "-" + numero.substr(9, 4);
                Materialize.toast("SMS enviado para " + numero, 3000, "green");
            }
            else
                console.error("Código SMS: " + data.sendSmsResponse.statusCode);
        },
        headers: {
            "Authorization": "Basic " + credenciais
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.error(errorThrown);
            console.error(jqXHR);
        }
    });
}


function formatarParametrosSMS(numero, titulo, mensagem) {
    return JSON.stringify({
        sendSmsRequest: { 
            from: titulo, 
            to: numero,
            msg: mensagem
        }
    });
}