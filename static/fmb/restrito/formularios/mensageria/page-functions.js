/*
    FUNÇÕES DA PÁGINA /fmb/mensageria/index.html

    Este arquivo deve implementar todas as funções disparads pelos elementos de interação da página

    Dependências deste script:
    -Nenhuma
*/


// Operações de exclusão
function confirmarExclusaoContato(id, nome){
    var modal = $("#modal-confirmacao");
    modal.find(".modal-content h4").html("Exclusão");
    modal.find(".modal-content p").html("Deseja excluir o contato <b>"+nome+"</b>?");
    modal.find("#confirmacao-sim").off("click")
                                  .click(function(){ 
                                        excluirContato(id); 
                                  });
    modal.modal('open');
}

function excluirContato(id){
    $.ajax({
        type: "DELETE",
        url: baseURL + "mensageriaPF/" + id,
        success: function(){
            $("#modal-confirmacao").modal("close");
            Materialize.toast("Contato excluído com sucesso!", 1500, "green lighten-1", function(){
                location.href = "?tab=1";
            });
        },
        error: function(jqXHR, textStatus, errorThrown){
            Materialize.toast("Erro ao excluir contato!", 3000, "red lighten-1");
        }
    });
}


//Operações de edição contato PF
function preencherFormEdicaoContato(id){
    $.get(baseURL + "mensageriaPF/" + id)
        .done(function(data){
            var contato = JSON.parse(data);

            var modal = $("#modal-agenda-pf");
            modal.find("#agenda-nome").val(contato.nome);
            modal.find("#agenda-apelido").val(contato.apelido);
            modal.find("#agenda-data-nascimento").val(contato.datanascimento);
            modal.find("#agenda-celular").val(contato.celular);
            modal.find("#agenda-fixo").val(contato.fixo);
            modal.find("#agenda-email").val(contato.email);
            modal.find("#agenda-email-2").val(contato.email2);
            modal.find("#agenda-cep").val(contato.cep);
            modal.find("#agenda-uf").val(contato.uf);
            modal.find("#agenda-cidade").val(contato.cidade);
            modal.find("#agenda-bairro").val(contato.bairro);
            modal.find("#agenda-logradouro").val(contato.logradouro);
            modal.find("#agenda-numero").val(contato.numero);
            modal.find("#agenda-complemento").val(contato.complemento);
            modal.find("#agenda-canal-sms").val(contato.canalSMS);
            modal.find("#agenda-canal-email").val(contato.canalEmail);
            modal.find("#agenda-observacao").val(contato.observacao);
            modal.find("#agenda-foto").val(contato.foto);
            modal.find("#agenda-pj").val(contato.pj);
            modal.find("#agenda-cargo").val(contato.cargo);

            modal.find("label").addClass("active");

            modal.find("#agenda-pf-form-acao").html("Salvar alterações")
                                              .off('click')
                                              .click(function(){
                                                   editarContatoPF(contato.id);
                                              });
            modal.modal("open");
        });
}

function editarContatoPF(id) {
    var parametros = {
        "id": id,
        "nome": $('#agenda-nome').val(),
        "apelido": $('#agenda-apelido').val(),
        "dataNascimento": $('#agenda-data-nascimento').val(),
        "celular": $('#agenda-celular').val(),
        "fixo": $('#agenda-fixo').val(),
        "email": $('#agenda-email').val(),
        "email2": $('#agenda-email-2').val(),
        "cep": $('#agenda-cep').val(),
        "uf": $('#agenda-uf').val(),
        "cidade": $('#agenda-cidade').val(),
        "bairro": $('#agenda-bairro').val(),
        "logradouro": $('#agenda-logradouro').val(),
        "numero": $('#agenda-numero').val(),
        "complemento": $('#agenda-complemento').val(),
        "canalSMS": $('#agenda-canal-sms').val(),
        "canalEmail": $('#agenda-canalS-email').val(),
        "observacao": $('#agenda-observacao').val(),
        "foto": $('#agenda-foto').val(),
        "vinculo-pj": $('#agenda-vinculo-pj').val(),
        "pj": $('#agenda-vinculo-pj-razao-social').val(),
        "cargo": $('#agenda-cargo').val(),
    };

    $.ajax({
        type: 'PUT',
        contentType: 'application/json',
        url: baseURL + "mensageriaPF/" + parametros.id,
        dataType: "json",
        data: JSON.stringify(parametros),
        beforeSend: function(){
            $("#agenda-pf-form-acao").hide();
            criarSpinner("#spinner-edicao-agenda-pf");
        },
        success: function(){
            $("#modal-orcamento-edicao").modal("close");
            ocultarSpinner("#spinner-edicao-agenda-pf");
            Materialize.toast('Contato alterado com sucesso!', 1000, "green lighten-1",function(){
                location.href = "?tab=1";
            });
        },
        error: function(a, b, c){
            console.error(a);
            ocultarSpinner("#spinner-edicao-agenda-pf");
            $("#agenda-pf-form-acao").show();
            Materialize.toast('Erro ao alterar contato!', 2000, "red lighten-1");
        }
    });
}


//Exibição de detalhes
function detalharContato(id) {
    var contato;

    $.get(baseURL + "mensageriaPF/" + id)
        .done(function(data){
            contato = JSON.parse(data);

            var modal = $("#modal-informacao");
            modal.find(".modal-content h4").html("Detalhes do contato");
            modal.find(".modal-content p").remove();
            var detalhes = modal.find(".modal-content");
            detalhes.append("<p>").find("p:last").html("Nome: <b>"+contato.nome+"</b>");
            detalhes.append("<p>").find("p:last").html("Aniversário: <b>"+aplicarMascaraData(contato.datanascimento, true)+"</b>");
            detalhes.append("<p>").find("p:last").html("Celular: <b>"+aplicarMascaraCelular(contato.celular)+"</b>");
            detalhes.append("<p>").find("p:last").html("Fixo: <b>"+aplicarMascaraFixo(contato.fixo)+"</b>");
            detalhes.append("<p>").find("p:last").html("Email: <b>"+contato.email + espacamento + contato.email2+"</b>");
            var endereco = contato.logradouro + ", " + contato.numero;
            if(contato.complemento.length > 1)
                endereco += " (" + contato.complemento + ")";
            endereco += " - " + contato.bairro + " - " + contato.cidade + " - " + contato.uf + " - " + aplicarMascaraCEP(contato.cep);;
            detalhes.append("<p>").find("p:last").html("Endereço: <b>"+endereco+"</b>");

            modal.modal('open');
        });
}


//Cadastro de dados

function cadastrarContatoPF() { 
    if(verificaRequeridos("#form-agenda-pf")){
        $("#modal-agenda-pf").modal("close");

        var parametros = {
            //Dados pessoais
            "cpf": $('#agenda-cpf').val(),
            "nome": $('#agenda-nome').val(),
            "alias": $('#agenda-apelido').val(),
            "sexo": $('#agenda-sexo').val(), 
            "dataNascimento": $('#agenda-data-nascimento').val(),
            //Dados de contato
            "celular": $('#agenda-celular').val(),
            "fixo": $('#agenda-fixo').val(),
            "fixo2": $('#agenda-fixo-2').val(),
            "email": $('#agenda-email').val(),
            "email2": $('#agenda-email-2').val(),
            "canalsms": $('#agenda-canal-sms').prop("checked") ? "s" : "n",
            "canalemail": $('#agenda-canal-email').prop("checked") ? "s" : "n",
            //Dados de endereço
            "cep": $('#agenda-cep').val(),
            "uf": $('#agenda-uf').val(),
            "cidade": $('#agenda-cidade').val(),
            "bairro": $('#agenda-bairro').val(),
            "logradouro": $('#agenda-logradouro').val(),
            "numero": $('#agenda-numero').val(),
            "complemento": $('#agenda-complemento').val(),
            //Outros dados
            "idpj" : parseInt($("#agenda-pj-vinculada").val()),
            "idcargo" : parseInt($("#agenda-cargo").val()),
            "idfuncao" : parseInt($("#agenda-funcao").val())
        };

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: baseURL + "mensageriaPF",
            dataType: "json",
            data: JSON.stringify(parametros),
            success: function(){
                Materialize.toast('Contato cadastrado com sucesso!', 1500, "green darken-1", function(){
                    location.href = "#";
                });
            },
            error: function(a, b, c){
                console.error(a);
                console.error(c);
            }
        });
    }
    else{
        Materialize.toast('Preencha os campos obrigatórios.', 3000, "red lighten-1");
    }
}






// OPERAÇÕES PARA PJ ==========================================================================================

//Exibição de detalhes
function detalharContatoPJ(id) {
    var contato;

    $.get(baseURL + "mensageriaPJ/" + id)
        .done(function(data){
            contato = JSON.parse(data);

            var modal = $("#modal-informacao");
            modal.find(".modal-content h4").html("Detalhes do contato");
            modal.find(".modal-content p").remove();
            var detalhes = modal.find(".modal-content");
            detalhes.append("<p>").find("p:last").html("Nome: <b>"+contato.razaosocial+"</b>");
            detalhes.append("<p>").find("p:last").html("CNPJ: <b>"+aplicarMascaraCNPJ(contato.cnpj)+"</b>");
            detalhes.append("<p>").find("p:last").html("Email: <b>"+contato.email + espacamento + contato.email2+"</b>");
            detalhes.append("<p>").find("p:last").html("Celular: <b>"+aplicarMascaraCelular(contato.celular)+"</b>");
            detalhes.append("<p>").find("p:last").html("Fixo: <b>"+aplicarMascaraFixo(contato.fixo) + espacamento + aplicarMascaraFixo(contato.fixo2)+"</b>");
            
            var endereco = contato.logradouro + ", " + contato.numero;
            if(contato.complemento.length > 1)
                endereco += " (" + contato.complemento + ")";
            endereco += " - " + contato.bairro + " - " + contato.cidade + " - " + contato.uf + " - " + aplicarMascaraCEP(contato.cep);;
            detalhes.append("<p>").find("p:last").html("Endereço: <b>"+endereco+"</b>");
            detalhes.append("<p>").find("p:last").html("Observação: <b>"+contato.observacao+"</b>");

            modal.modal('open');
        });
}

success: 
//Operações de edição
function preencherFormEdicaoContatoPJ(id){
    $.get(baseURL + "mensageriaPJ/" + id)
        .done(function(data){
            var contato = JSON.parse(data);

            var modal = $("#modal-agenda-pj");
            modal.find("#agenda-pj-cnpj").val(contato.cnpj);
            modal.find("#agenda-pj-razao").val(contato.razaosocial);
            modal.find("#agenda-pj-alias").val(contato.alias);
            modal.find("#agenda-pj-celular").val(contato.celular);
            modal.find("#agenda-pj-fixo").val(contato.fixo);
            modal.find("#agenda-pj-fixo-2").val(contato.fixo2);
            modal.find("#agenda-pj-email").val(contato.email);
            modal.find("#agenda-pj-email-2").val(contato.email2);
            modal.find("#agenda-pj-site").val(contato.paginaweb);
            modal.find("#agenda-pj-cep").val(contato.cep);
            modal.find("#agenda-pj-uf").val(contato.uf);
            modal.find("#agenda-pj-cidade").val(contato.cidade);
            modal.find("#agenda-pj-bairro").val(contato.bairro);
            modal.find("#agenda-pj-logradouro").val(contato.logradouro);
            modal.find("#agenda-pj-numero").val(contato.numero);
            modal.find("#agenda-pj-complemento").val(contato.complemento);
            modal.find("#agenda-pj-canal-sms").val(contato.canalSMS);
            modal.find("#agenda-pj-canal-email").val(contato.canalEmail);
            modal.find("#agenda-pj-observacao").val(contato.observacao);

            modal.find("label").addClass("active");

            modal.find("#agenda-pj-form-acao").html("Salvar alterações")
                                              .off('click')
                                              .click(function(){
                                                    editarContatoPJ(contato.id);
                                              });
            modal.modal("open");
        });
}

function editarContatoPJ(id) {
    var parametros = {
        "id": id,
        "cnpj": $('#agenda-pj-cnpj').val(),
        "razaosocial": $('#agenda-pj-razao').val(),
        "alias": $('#agenda-pj-alias').val(),
        "celular": $('#agenda-pj-celular').val(),
        "fixo": $('#agenda-pj-fixo').val(),
        "fixo2": $('#agenda-pj-fixo').val(),
        "email": $('#agenda-pj-email').val(),
        "email2": $('#agenda-pj-email-2').val(),
        "paginaweb": $('#agenda-pj-site').val(),
        "cep": $('#agenda-pj-cep').val(),
        "uf": $('#agenda-pj-uf').val(),
        "cidade": $('#agenda-pj-cidade').val(),
        "bairro": $('#agenda-pj-bairro').val(),
        "logradouro": $('#agenda-pj-logradouro').val(),
        "numero": $('#agenda-pj-numero').val(),
        "complemento": $('#agenda-pj-complemento').val(),
        "canalSMS": $('#agenda-pj-canal-sms').val(),
        "canalEmail": $('#agenda-pj-canal-email').val(),
        "observacao": $('#agenda-pj-observacao').val()
    };

    $.ajax({
        type: 'PUT',
        contentType: 'application/json',
        url: baseURL + "mensageriaPJ/" + parametros.id,
        dataType: "json",
        data: JSON.stringify(parametros),
        beforeSend: function(){
            $("#agenda-pj-form-acao").hide();
            criarSpinner("#spinner-edicao-agenda-pj");
        },
        success: function(){
            $("#modal-orcamento-edicao").modal("close");
            ocultarSpinner("#spinner-edicao-agenda-pf");
            Materialize.toast('Contato alterado com sucesso!', 1000, "green lighten-1", function(){
                location.href = "?tab=2";
            });
        },
        error: function(a, b, c){
            console.error(c);
            ocultarSpinner("#spinner-edicao-agenda-pj");
            $("#agenda-pj-form-acao").show();
            Materialize.toast('Erro ao alterar contato PJ!', 2000, "red lighten-1");
        }
    });
}


// Operações de exclusão
function confirmarExclusaoContatoPJ(id, razao){
    var modal = $("#modal-confirmacao");
    modal.find(".modal-content h4").html("Exclusão");
    modal.find(".modal-content p").html("Deseja excluir o contato <b>"+razao+"</b>?");
    modal.find("#confirmacao-sim").off("click")
                                  .click(function(){ excluirContatoPJ(id); });
    modal.modal('open');
}

function excluirContatoPJ(id){
    $.ajax({
        type: "DELETE",
        url: baseURL + "mensageriaPJ/" + id,
        success: function(){
            $("#modal-confirmacao").modal("close");
            Materialize.toast("Contato excluído com sucesso!", 1500, "green lighten-1", function(){
                location.href = "?tab=2";
            });
        },
        error: function(jqXHR, textStatus, errorThrown){
            Materialize.toast("Erro ao excluir contato!", 3000, "red lighten-1");
        }
    });
}


//Cadastro de dados

function cadastrarContatoPJ() { 
    if(verificaRequeridos("#form-agenda-pj")){
        $("#modal-agenda-pj").modal("close");

        var parametros = {
            "cnpj": $('#agenda-pj-cnpj').val(),
            "razaosocial": $('#agenda-pj-razao').val(),
            "alias": $('#agenda-pj-alias').val(),
            "celular": $('#agenda-pj-celular').val(),
            "fixo": $('#agenda-pj-fixo').val(),
            "fixo2": $('#agenda-pj-fixo').val(),
            "email": $('#agenda-pj-email').val(),
            "email2": $('#agenda-pj-email-2').val(),
            "paginaweb": $('#agenda-pj-site').val(),
            //Endereço
            "cep": $('#agenda-pj-cep').val(),
            "uf": $('#agenda-pj-uf').val(),
            "cidade": $('#agenda-pj-cidade').val(),
            "bairro": $('#agenda-pj-bairro').val(),
            "logradouro": $('#agenda-pj-logradouro').val(),
            "numero": $('#agenda-pj-numero').val(),
            "complemento": $('#agenda-pj-complemento').val(),
            //Outros
            "observacao": $('#agenda-pj-observacao').val(),
            "canalSMS": $('#agenda-pj-canal-sms').val(),
            "canalEmail": $('#agenda-pj-canal-email').val()
        };

        console.log(parametros);

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: baseURL + "mensageriaPJ",
            dataType: "json",
            data: JSON.stringify(parametros),
            success: function(id){
                Materialize.toast("Contato cadastrado com sucesso!", 1500, "green lighten-1", function(){
                    location.href = "?tab=2";
                });
            },
            error: function(a, b, c){
                console.error(a);
                console.error(c);
            }
        });
    }
    else{
        Materialize.toast('Preencha os campos obrigatórios.', 3000, "red lighten-1");
    }
}







// OPERAÇÕES PARA MENSAGEM SMS ====================================================================

function cadastrarMensagem() {
    $(".modal").modal("close");

    //Atualizar a listagem aqui

    Materialize.toast('Mensagem cadastrada com sucesso!', 1500, "green darken-1");
}


//Envio de mensagem
function abrirFormConfiguracaoMensagem(idMensagem) {
    alert("Abrir configurações de envio.");
}

function enviarMensagemManual(id) {
    alert("Abrir diálogo para seleção de destinatários.");
}

function enviarMensagem(){
    $("#mensagem-botao-enviar")
        .attr("disabled", true)
        .css("opacity", 0.4)
        .html("Enviando...");

    var titulo = "FMB - Transparência";
    var mensagem = $("#mensagem-i-texto").val();

    //Define o(s) destinatário(s)
    var destinatarios = [];
    var marcados = $(idTabela + " input[type=checkbox]:checked");
    if(marcados.length > 0) {
        $.each(marcados, function(i, v){
            if(v.id.length == 11)
                destinatarios.push(v.id);
            else
                console.error("O número " + v.id + " não contém a quantidade correta de dígitos.");
        });
    }
    else{
        var modal = $("#modal-informacao");
        modal.find(".modal-content h4").html("Atenção");
        modal.find(".modal-content p").remove();
        var detalhes = modal.find(".modal-content");
        detalhes.append("<p>").find("p:last").html("Selecione pelo menos um destinatário");
        modal.modal('open');
        return;
    }

    //Aciona o serviço de envio da mensagem
    jQuery.support.cors = true;
    $.ajax({async: false});

    $.each(destinatarios, function(i, numero){
        $.get(baseURL + "mensageriaGetCredenciais")
            .done(function(data){
                data = JSON.parse(data);
                var conta = data[0];
                var codigo = data[1];
                var credenciais = btoa(conta + ":" + codigo);
                enviarSMS(credenciais, numero, titulo, mensagem);
            })
            .fail(function(){
                console.error("Erro ao enviar SMS para um ou mais destinatários!");
            });
    });

    setTimeout(function(){
        $("#mensagem-botao-enviar")
            .attr("disabled", false)
            .css("opacity", 1)
            .html("Enviar");
    }, 4000);
}


/*function obterMensagensRecebidas(){
    var sucesso = function(data){
        console.log("SUCESSO");
        console.log(data);
    };
    var falha = function(data){
        console.error("ERRO");
        console.data(data);
    };

    var msgs = listUnreadMessages(sucesso, falha);
    console.log(msgs);
}*/


function confirmarExclusaoMensagem(id, titulo){
    var modal = $("#modal-confirmacao");
    modal.find(".modal-content h4").html("Exclusão");
    modal.find(".modal-content p").html("Deseja excluir a mensagem <b>"+titulo+"</b>?");
    modal.find("#confirmacao-sim").click(function(){ excluirMensagem(id); });
    modal.modal('open');
}

function excluirMensagem(id){
    $("#modal-confirmacao").modal("close");

    //Atualizar a listagem aqui

    var modal = $("#modal-informacao");
    modal.find(".modal-content h4").html("Sucesso");
    modal.find(".modal-content p").remove();
    var detalhes = modal.find(".modal-content");
    detalhes.append("<p>").find("p:last").html("Mensagem excluída com sucesso!");
    modal.modal('open');
}

function preencherFormEdicaoMensagem(id){
    var mensagem = {id: 1, disparo: "automático", titulo: "Título mensagem", texto: "Texto da mensagem...", regra: "SELECT * FROM agenda"};

    var modal = $("#modal-mensagem");
    modal.find("#mensagem-titulo").val(mensagem.titulo);
    modal.find("#mensagem-texto").val(mensagem.texto);
    modal.find("#mensagem-regra").val(mensagem.regra);
    modal.find("#mensagem-disparo").prop("checked", mensagem.disparo === "automático");

    modal.find("label").addClass("active");

    modal.modal("open");
}

function detalharMensagem(id){
    //Buscar a mensagem via AJAX pelo ID
    var mensagem = {id: id, 
                    titulo: "Chamada para reunião", 
                    disparo: "manual", 
                    conteudo: "Sr(a) #nome, a FMB fará uma reunião de alinhamento na data #dataReuniao e gostaria de contar com sua participação. Responda SIM para confirmar sua participação e NAO para informar que não poderá comparecer.", 
                    regra: "SELECT nome, salario FROM CLIENTE WHERE  salario > (SELECT avg(salario) FROM CLIENTE WHERE SEXO=’MASCULINO’)"}

    var modal = $("#modal-informacao");
    modal.find(".modal-content h4").html("Detalhes da mensagem");
    modal.find(".modal-content p").remove();

    var detalhes = modal.find(".modal-content");
    detalhes.append("<p>").find("p:last").html("Identificador: <b>"+mensagem.id+"</b>");
    detalhes.append("<p>").find("p:last").html("Disparo: <b>"+mensagem.disparo+"</b>");
    detalhes.append("<p>").find("p:last").html("Título: <b>"+mensagem.titulo+"</b>");
    detalhes.append("<p>").find("p:last").html("Texto: <b>"+mensagem.conteudo+"</b>");
    detalhes.append("<p>").find("p:last").html("<span class='btn light-green darken-1'>Ver regra de envio</span><p style='display: none' id='texto-regra'><b>"+mensagem.regra+"</b></p>")
        .find("span").click(function(){ $("#texto-regra").show("fade", 1000); });

    modal.modal('open');
}