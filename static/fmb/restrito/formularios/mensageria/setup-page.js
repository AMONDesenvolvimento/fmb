/*
	SETUP DA PÁGINA /fmb/mensageria/index.html

	Este arquivo deve fazer o carregamento inicial da página.
	Neste processamento deve estar qualquer requisição AJAX necessária para carregamento de dados, execução de animações, etc.

	Dependências deste script: 
	- /fmb/dependencias/JQuery/jquery-3.1.0.min.js
	- /fmb/dependencias/JQuery/jquery-ui.min.js
	- /fmb/dependencias/Materialize/js/materialize.js
	- /fmb/dependencias/css/spinner.css
	- /fmb/dependencias/js/spinner.js
*/


//TELA DE CONTATOS ========================================================

criarSpinner("#spinner-agenda-pf");
criarSpinner("#spinner-agenda-pj");
criarSpinner("#spinner-mensagens");


//Preenche a tabela de contatos PF
var urlObterContatosPF = baseURL + "mensageriaPF";

$.get(urlObterContatosPF)
    .done(function(data){
    	var contatosPF = JSON.parse(data);

		var idTabela = "#tabela-agenda";
		var idFiltro = "#filtro-agenda";

		//Configura as tabelas com filtro de dados
		tableFilter(idTabela, idFiltro);

		var corpoTabela = $(idTabela + " tbody");
		corpoTabela.empty();
		$.each(contatosPF, function(i, v){
			corpoTabela.append("<tr>");

			var ultimaLinhaAdicionada = corpoTabela.find("tr:last");
			ultimaLinhaAdicionada.addClass("dropdown-button").addClass("hoverable").attr("data-activates", "menu-" + i);
			ultimaLinhaAdicionada.append("<td>");
			ultimaLinhaAdicionada.find("td:last").html(v.nome);

			ultimaLinhaAdicionada.append("<td>");
			ultimaLinhaAdicionada.find("td:last").addClass("hide-on-med-and-down").html(aplicarMascaraCelular(v.celular));

			ultimaLinhaAdicionada.append("<td>");
			ultimaLinhaAdicionada.find("td:last").addClass("hide-on-small-only").html(v.email);

			var ultimaCelulaAdicionada = ultimaLinhaAdicionada.find("td:last");
			ultimaCelulaAdicionada.append("<ul>").find("ul:last").addClass("dropdown-content").addClass("z-depth-3").attr("id", "menu-"+i);
			var listaOpcoes = ultimaCelulaAdicionada.find("ul#menu-"+i);
			listaOpcoes.append('<li><a href="#">Detalhar</a></li>').find("a:last").attr("onclick", "detalharContato("+v.id+")");
			listaOpcoes.append('<li><a href="#">Editar</a></li>').find("a:last").attr("onclick", "preencherFormEdicaoContato("+v.id+")");
			listaOpcoes.append('<li><a href="#">Excluir</a></li>').find("a:last").attr("onclick", "confirmarExclusaoContato("+v.id+",'"+v.nome+"')");
		});

		//Oculta o spinner e mostra a tabela
		ocultarSpinner("#spinner-agenda-pf");
		$("#painel-agenda-pf").show("fade", 500);

		preencherListaContatosParaSelecao(contatosPF);

		inicializarComponentesMaterialize();
	});


//Preenche a tabela de contatos PJ
var urlObterContatosPJ = baseURL + "mensageriaPJ";

$.get(urlObterContatosPJ)
    .done(function(data){
    	var contatosPJ = JSON.parse(data);

		var idTabela = "#tabela-agenda-pj";
		var idFiltro = "#filtro-agenda-pj";

		//Configura as tabelas com filtro de dados
		tableFilter(idTabela, idFiltro);

		var corpoTabela = $(idTabela + " tbody");
		corpoTabela.empty();
		$.each(contatosPJ, function(i, v){
			corpoTabela.append("<tr>");

			var ultimaLinhaAdicionada = corpoTabela.find("tr:last");
			ultimaLinhaAdicionada.addClass("dropdown-button").addClass("hoverable").attr("data-activates", "menu-pj-" + i);
			ultimaLinhaAdicionada.append("<td>");
			ultimaLinhaAdicionada.find("td:last").html(v.razaosocial);

			ultimaLinhaAdicionada.append("<td>");
			ultimaLinhaAdicionada.find("td:last").addClass("hide-on-med-and-down").html(aplicarMascaraCelular(v.celular));

			ultimaLinhaAdicionada.append("<td>");
			ultimaLinhaAdicionada.find("td:last").addClass("hide-on-small-only").html(v.email);

			var ultimaCelulaAdicionada = ultimaLinhaAdicionada.find("td:last");
			ultimaCelulaAdicionada.append("<ul>").find("ul:last").addClass("dropdown-content").addClass("z-depth-3").attr("id", "menu-pj-"+i);
			var listaOpcoes = ultimaCelulaAdicionada.find("ul#menu-pj-"+i);
			listaOpcoes.append('<li><a href="#">Detalhar</a></li>').find("a:last").attr("onclick", "detalharContatoPJ("+v.id+")");
			listaOpcoes.append('<li><a href="#">Editar</a></li>').find("a:last").attr("onclick", "preencherFormEdicaoContatoPJ("+v.id+")");
			listaOpcoes.append('<li><a href="#">Excluir</a></li>').find("a:last").attr("onclick", "confirmarExclusaoContatoPJ("+v.id+",'"+v.razaosocial+"')");
		});

		//Oculta o spinner e mostra a tabela
		ocultarSpinner("#spinner-agenda-pj");
		$("#painel-agenda-pj").show("fade", 500);
		inicializarComponentesMaterialize();
	});



//Preenche o combo de PJs no form de cadastro de PF (ao marcar o checkbox de vículo)
$("#agenda-vinculo-pj").change(function(){
	var container = $("#agenda-pj-vinculada-container");

	if($(this).prop("checked") == true) {
		var urlObterEmpresas = "https://jsonplaceholder.typicode.com/posts";

		$.get(urlObterEmpresas)
			.done(function(data){
				data = [
					{id: 1, descricao: "AMON Consultoria em TI Ltda."},
					{id: 2, descricao: "Empresa X"},
					{id: 3, descricao: "Empresa Y"}
				];

				var comboPJ = $("#agenda-pj-vinculada");
				comboPJ.find("option:not(:nth-child(1))").remove();
				$.each(data, function(i, v){
					var option = "<option value='" + v.id + "'>" + v.descricao + "</option>";
					comboPJ.append(option);	
				});
				comboPJ.material_select();
			});

		container.show("fade", 600);
	}
	else {
		container.hide("fade", 600);
		$("#agenda-cargo-funcao-container").hide("fade", 600);
	}
});


//AO selecionar a PJ viculada, dispara o seguinte:
$("#agenda-pj-vinculada").change(function(){
	var selecionado = $(this).val();
	var container = $("#agenda-cargo-funcao-container");

	if(selecionado != "") {
		var urlObterCargos = "https://jsonplaceholder.typicode.com/posts";
		var urlObterFuncoes = "https://jsonplaceholder.typicode.com/posts";

		var comboCargos = $("#agenda-cargo");
		var comboFuncoes = $("#agenda-funcao");

		$.get(urlObterCargos)
			.done(function(data){
				data = [
					{id: 1, descricao: "Analista de Sistemas Junior"},
					{id: 2, descricao: "Analista de Sistemas Pleno"},
					{id: 3, descricao: "Analista de Sistemas Sênior"}
				];
				
				comboCargos.find("option:not(:nth-child(1))").remove();
				$.each(data, function(i, v){
					var option = "<option value='" + v.id + "'>" + v.descricao + "</option>";
					comboCargos.append(option);	
				});
				comboCargos.material_select();
			});

		$.get(urlObterFuncoes)
			.done(function(data){
				data = [
					{id: 1, descricao: "Diretor"}
				];

				comboFuncoes.find("option:not(:nth-child(1))").remove();
				$.each(data, function(i, v){
					var option = "<option value='" + v.id + "'>" + v.descricao + "</option>";
					comboFuncoes.append(option);	
				});
				comboFuncoes.material_select();
			});

		container.show("fade", 600);
	}
	else {
		container.hide("fade", 600);
	}
});



//TELA DE MENSAGENS ========================================================

//Preenche a tabela de mensagens 

//URL temporária
var urlObterMensagens = "https://jsonplaceholder.typicode.com/posts";

$.get(urlObterMensagens)
    .done(function(data){
		var mensagens = [
			{id: 4, titulo: "Feliz aniversário!", disparo: "manual", conteudo: "Sr(a) #nome, a FMB deseja um feliz aniversário!"},
			{id: 5, titulo: "Boleto a pagar", disparo: "automatico", conteudo: "Sr(a) #nome, consta em nosso sistema que o boleto #identificadorBoleto ainda não foi pago. Favor realizar o pagamento até a data #dataVencimento. Caso já o tenha pago, desconsidere esta mensagem. \nAtt. \nFMB"},
			{id: 6, titulo: "Boleto pago com sucesso", disparo: "automatico", conteudo: "Sr(a) #nome, seu boleto #identificadorBoleto foi pago com sucesso! A FMB agradece sua colaboração."},
			{id: 10, titulo: "Chamada para reunião", disparo: "manual", conteudo: "Sr(a) #nome, a FMB fará uma reunião de alinhamento na data #dataReuniao e gostaria de contar com sua participação. Responda SIM para confirmar sua participação e NAO para informar que não poderá comparecer."}
		];

		//Configura as tabelas com filtro de dados
		tableFilter("#tabela-mensagens", "#filtro-mensagens");

		$("#tabela-mensagens tbody").empty();
		$.each(mensagens, function(i, v){
			$("#tabela-mensagens tbody").append("<tr>");

			$("#tabela-mensagens tbody tr:last").append("<td>");
			$("#tabela-mensagens tbody tr td:last").html(v.titulo);

			$("#tabela-mensagens tbody tr:last").append("<td>");
			var quantCaracteres = 60;
			var inicioMsg = v.conteudo.length >= quantCaracteres ? v.conteudo.substr(0, quantCaracteres) + "..." : v.conteudo;
			$("#tabela-mensagens tbody tr td:last").html(inicioMsg);

			$("#tabela-mensagens tbody tr:last").append("<td>");
			var ultimaCelula = $("#tabela-mensagens tbody tr td:last");
			ultimaCelula.addClass("action-col");
			ultimaCelula.append('<a href="#"><i class="fa fa-eye"></i></a>').find("a:last").attr("onclick", "detalharMensagem("+v.id+")");
			ultimaCelula.append('<a href="#"><i class="fa fa-pencil"></i></a>').find("a:last").attr("onclick", "preencherFormEdicaoMensagem("+v.id+")");
			ultimaCelula.append('<a href="#"><i class="fa fa-trash"></i></a>').find("a:last").attr("onclick", "confirmarExclusaoMensagem("+v.id+",'"+v.titulo+"')");
			if(v.disparo === "manual") 
				ultimaCelula.append('<a href="#"><i class="fa fa-paper-plane-o"></i></a>').find("a:last").attr("onclick", "enviarMensagemManual("+v.id+")");

			//Adiciona o tooltips nas ícones de ação da tabela
			var tooltips = [
					{posicao: 1, texto: "Detalhes"},
					{posicao: 2, texto: "Editar"},
					{posicao: 3, texto: "Excluir"},
					{posicao: 4, texto: "Enviar"}
				];

			$.each(tooltips, function(i, v){
				$("#tabela-mensagens tbody tr:last td.action-col a:nth-child("+v.posicao+")").addClass("tooltipped").attr("data-tooltip", v.texto);
			});
		});

		//Simula o tempo de requisição dos dados
		setTimeout(function(){
			//Oculta o spinner e mostra a tabela
			ocultarSpinner("#spinner-mensagens");
			$("#painel-mensagens").show("fade", 500);
		}, 500);
	});



//TABELA DE CONTATOS PARA ENVIO DE SMS INDIVIDUAL ========================================================

//Preenche a tabela de contatos para seleção de destinatário 

function preencherListaContatosParaSelecao(contatosPF){    	
		var idTabela = "#tabela-contatos-selecao";
		var idInputFilter = "#filtro-contatos-selecao";

		//Configura as tabelas com filtro de dados
		tableFilter(idTabela, idInputFilter);

		var corpoTabela = $(idTabela + " tbody");
		corpoTabela.empty();
		$.each(contatosPF, function(i, v){
			corpoTabela.append("<tr>");

			var ultimaLinhaAdicionada = corpoTabela.find("tr:last");

			ultimaLinhaAdicionada.append("<td>");
			var checkbox = "<input type='checkbox' class='filled-in' id='"+v.celular+"' value='"+v.celular+"' /><label for='"+v.celular+"'></label>";

			ultimaLinhaAdicionada.find("td:last").html(checkbox);

			ultimaLinhaAdicionada.append("<td>");
			ultimaLinhaAdicionada.find("td:last").html(v.nome);

			ultimaLinhaAdicionada.append("<td>");
			var celular = "(" + v.celular.substr(0, 2) + ") " + v.celular.substr(2, 5) + "-" + v.celular.substr(7, 4);
			ultimaLinhaAdicionada.find("td:last").addClass("hide-on-small-only").html(celular);
		});
}


var idTabela = "#tabela-contatos-selecao";

//Trata o evento de marcar/desmarcar todos
$("#destinatarios-selecionar-todos").click(function(){
	var icone = $(this).find("i");

	if(icone.hasClass("fa-check-square-o")) {
		$(idTabela + " input[type=checkbox]").prop("checked", true);
		icone.removeClass("fa-check-square-o").addClass("fa-square-o");
	}
	else {
		$(idTabela + " input[type=checkbox]").prop("checked", false);
		icone.removeClass("fa-square-o").addClass("fa-check-square-o");
	}
});



//Aplica os textos aos botões de cadastro / edição
$("#agenda-botao-cadastro-pf").click(function(){
    $("#modal-agenda-pf").find("#agenda-pf-form-acao")
                         .html("Cadastrar")
                         .click(cadastrarContatoPF);
});

$("#agenda-botao-cadastro-pj").click(function(){
    $("#modal-agenda-pj").find("#agenda-pj-form-acao")
                         .html("Cadastrar")
                         .click(cadastrarContatoPJ);
});



inicializarComponentesMaterialize();