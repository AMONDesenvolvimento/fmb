/*
	SETUP DA PÁGINA /fmb/mensageria/index.html

	Este arquivo deve fazer todo o processamento para o carregamento inicial da página.
	Neste processamento deve estar qualquer requisição AJAX necessária para carregamento de dados, execução de animações, etc.

	Dependências deste script: 
	- /fmb/dependencias/JQuery/jquery-3.1.0.min.js
	- /fmb/dependencias/JQuery/jquery-ui.min.js
	- /fmb/dependencias/Materialize/js/materialize.js
	- /fmb/dependencias/css/spinner.css
	- /fmb/dependencias/js/spinner.js
*/


//URLs temporárias
var urlObterOrcamentos = baseURL + "orcamento/";
function setup(){
	var ano = parseInt($('#filtro-orcamento-exercicio').val());
	//Cria os spinners
	criarSpinner("#spinner-orcamento");
	//Preenche a tabela de Orcamento
	$.get(urlObterOrcamentos + ano)
	    .done(function(data){
			var contatos = data;
			var idTabela = "#tabela-orcamento";

			//Configura as tabelas com filtro de dados
			tableFilter(idTabela, "#filtro-orcamento");

			var corpoTabela = $(idTabela+" tbody");
			corpoTabela.empty();
			$.each(JSON.parse(contatos), function(i, v){
				corpoTabela.append("<tr>");

				var ultimaLinhaAdicionada = corpoTabela.find("tr:last");
				ultimaLinhaAdicionada.addClass("dropdown-button").addClass("hoverable").attr("data-activates", "menu-"+v.id);
				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").html((v.codred === 0) ? "" : v.codred);

				var margin = 0;
				for(var i = 0; i<v.conta.split(".").length;i++){
					margin+= 24;
				}

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").css("padding-left",margin + "px").html(v.descricao);

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").addClass("hide-on-small-only").html(formataDinheiro(v.orcamento));

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").addClass("hide-on-small-only").html(v.exercicio);

				var ultimaCelulaAdicionada = ultimaLinhaAdicionada.find("td:last");
				ultimaCelulaAdicionada.append("<ul>").find("ul:last").addClass("dropdown-content").addClass("z-depth-3").attr("id", "menu-"+v.id);

				var listaOpcoes = ultimaCelulaAdicionada.find("ul#menu-"+v.id);
				listaOpcoes.append('<li><a href="#">Detalhar</a></li>').find("a:last").attr("onclick", "detalharOrcamento("+v.id+")");
				listaOpcoes.append('<li><a href="#">Editar</a></li>').find("a:last").attr("onclick", "preencherFormEdicaoContato("+v.id+")");
				listaOpcoes.append('<li><a href="#">Excluir</a></li>').find("a:last").attr("onclick", "confirmarExclusaoContato("+v.id+",'"+v.nome+"')");

				$('.dropdown-button.hoverable').dropdown({
			        constrainWidth: false,
			        hover: false
			    });

			    //Destaca o item clicado na tabela
			    corpoTabela.find("tr").click(function(){
			    	corpoTabela.find("tr").removeClass("linha-em-foco");
			    	//$(this).addClass("linha-em-foco");
			    });
			    //Remove o destaque ao sair da tabela
			    corpoTabela.on('mouseleave', function(){
			    	corpoTabela.find("tr").removeClass("linha-em-foco");
			    });
			});
		
			//Oculta o spinner e mostra a tabela
			ocultarSpinner("#spinner-orcamento");
			$("#painel-orcamento").show("fade", 500);
			
		});
	$("#form-orcamento")[0].reset();
}

setup();
