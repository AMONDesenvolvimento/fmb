/*
	FUNÇÕES DA PÁGINA /fmb/mensageria/index.html

	Este arquivo deve implementar todas as funções disparads pelos elementos de interação da página (cliques em botões,
	submissão de formulários, etc)

	Dependências deste script: 
	-Nenhuma
*/


var baseURL = baseURL + "orcamento/";
var ano;

// Operações de exclusão
function confirmarExclusaoContato(id){
	ano = parseInt($('#filtro-orcamento-exercicio').val());
	$.get(baseURL + ano + '/' + id, function( data ) {
  		var orcamento = JSON.parse(data);
		var modal = $("#modal-confirmacao");
		modal.find(".modal-content h4").html("Exclusão");
		modal.find(".modal-content p").remove();
		var detalhes = modal.find(".modal-content");
		detalhes.append("<p>").find("p:last").html("<b>Deseja realmente excluir o item a seguir?</b>");
		detalhes.append("<p>").find("p:last").html("Exercício: <b>"+orcamento.exercicio+"</b>");
		detalhes.append("<p>").find("p:last").html("Conta: <b>"+orcamento.conta+"</b>");
		detalhes.append("<p>").find("p:last").html("Código Reduzido: <b>"+orcamento.codred+"</b>");
		detalhes.append("<p>").find("p:last").html("Descrição: <b>"+orcamento.descricao+"</b>");
		detalhes.append("<p>").find("p:last").html("Orçamento: <b>"+formataDinheiro(orcamento.orcamento)+"</b>");
		
		modal.find("#confirmacao-sim").off("click").click(function(){ excluirOrcamento(id); });
		modal.find("#confirmacao-nao").click(function(){ modal.modal("close"); });
		modal.modal('open');
	});
}

function excluirOrcamento(id){
    $.ajax({
		type: 'DELETE',
		url: baseURL + id,
		success: function(data, textStatus, jqXHR){
	            $("#modal-confirmacao").modal("close");
				Materialize.toast('Orçamento excluído com sucesso!', 1500,"green lighten-1",function(){
					location.reload(true);
				});
		},
		error: function(jqXHR, textStatus, errorThrown){
			Materialize.toast('Erro ao excluir orçamento!', 3000,"red lighten-1");
		}
    });
}

//Operações de edição
function preencherFormEdicaoContato(id){
	ano = parseInt($('#filtro-orcamento-exercicio').val());
	$.get(baseURL + ano + '/' + id, function( data ) {
  		var orcamento = JSON.parse(data);
		var modal = $("#modal-orcamento-edicao");
		modal.find("#orcamento-edicao-exerc").val(orcamento.exercicio);
		modal.find("#orcamento-edicao-exercicio").find("option[value = "+ orcamento.exercicio +"]").prop("selected",true);
		modal.find("#orcamento-edicao-exercicio").parent().find("input.select-dropdown").val(orcamento.exercicio).prop("disabled",true);
		modal.find("#orcamento-edicao-conta").val(orcamento.conta);
		modal.find("#orcamento-edicao-descricao").val(orcamento.descricao);
		modal.find("#orcamento-edicao-codred").val(orcamento.codred).prop("disabled",true);
		modal.find("#orcamento-edicao-orcamento").val(orcamento.orcamento);
		modal.find("#orcamento-edicao-id").val(orcamento.id);
		modal.find("label").addClass("active");
		modal.modal("open");
	});
}


function alterarOrcamento() {
    var parametros = {
		"exercicio": parseInt($('#orcamento-edicao-exerc').val()),
		"codred": parseInt($('#orcamento-edicao-codred').val()),
        "conta": $('#orcamento-edicao-conta').val(), 
		"descricao": $('#orcamento-edicao-descricao').val(),
		"orcamento": processaValorDinheiroNulo($('#orcamento-edicao-orcamento').val())
	};

    $.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: baseURL + parametros.exercicio + '/' + parametros.codred,
		dataType: "json",
		data: JSON.stringify(parametros),
		beforeSend: function(){
			$("#botao-salvar").hide();
			criarSpinner("#spinner-edicao-orcamento");
		},
		success: function(){
			$("#modal-orcamento-edicao").modal("close");
			ocultarSpinner("#spinner-edicao-orcamento");
			Materialize.toast('Orçamento alterado com sucesso!', 1000,"green lighten-1",function(){
				location.reload(true);
			});
		},
		error: function(){
			ocultarSpinner("#spinner-edicao-orcamento");
			$("#botao-salvar").show();
            Materialize.toast('Erro ao alterar orçamento!', 2000,"red lighten-1");
		}
    });
}


//Exibição de detalhes
function detalharOrcamento(id) {
	ano = parseInt($('#filtro-orcamento-exercicio').val());
	$.get(baseURL + ano + '/' + id, function(data) {
  		var orcamento = JSON.parse(data);
		var modal = $("#modal-detalhamento");
		modal.find(".modal-content h4").html("Detalhes do Orçamento");
		modal.find(".modal-content p").remove();
		var detalhes = modal.find(".modal-content");
		detalhes.append("<p>").find("p:last").html("Exercício: <b>"+orcamento.exercicio+"</b>");
		detalhes.append("<p>").find("p:last").html("Código Reduzido: <b>"+orcamento.codred+"</b>");
		detalhes.append("<p>").find("p:last").html("Conta: <b>"+orcamento.conta+"</b>");
		detalhes.append("<p>").find("p:last").html("Descrição: <b>"+orcamento.descricao+"</b>");
		detalhes.append("<p>").find("p:last").html("Orçamento: <b>"+formataDinheiro(orcamento.orcamento)+"</b>");
		modal.modal('open');
	});
}

//Cadastro de orçamento
function cadastrarOrcamento() { 
	if(verificaRequeridos("#form-orcamento")){
		$("#modal-agenda").modal("close");

		var parametros = {
			"exercicio": parseInt($('#orcamento-exercicio').val()),
	        "conta": $('#orcamento-conta').val(), 
			"descricao": $('#orcamento-descricao').val(),
			"analitica": $('#orcamento-analitica').prop("checked") ? "X" : "",
			"saldoanterior": processaValorDinheiroNulo($('#orcamento-saldo-anterior').val()),
			"movimentodebitocaixa": processaValorDinheiroNulo($('#orcamento-mov-deb-caixa').val()),
			"movimentocreditocaixa": processaValorDinheiroNulo($('#orcamento-mov-cred-caixa').val()),
			"movimentodebitocompetencia": processaValorDinheiroNulo($('#orcamento-mov-deb-competencia').val()),
			"movimentocreditocompetencia": processaValorDinheiroNulo($('#orcamento-mov-cred-competencia').val()),
			"orcamento": processaValorDinheiroNulo($('#orcamento-orcamento').val())
		};

	    $.ajax({
			type: 'POST',
			contentType: 'application/json',
			url: baseURL,
			dataType: "json",
			data: JSON.stringify(parametros),
			success: function(){
	            Materialize.toast('Orçamento incluído com sucesso!', 2000, "green lighten-1", function(){
					$("#modal-confirmacao").modal("close");
					location.reload(true);
				});
			}
		});
	}
	else{
		Materialize.toast('Preencha os campos obrigatórios.', 3000, "red lighten-1");
	}
}

//Carga Ano Seguinte
function geraOrcamento(){
	var modal = $("#modal-confirmacao");
	modal.find(".modal-content h4").html("Geração do Orçamento");
	modal.find(".modal-content p").html("Deseja gerar o Orçamento do ano seguinte?");

	modal.find("#confirmacao-sim").off("click").click(function(){obterOrcamentoAnoAtual(new Date().getYear() + 1900); modal.modal("close"); });
	modal.find("#confirmacao-nao").click(function(){modal.modal("close"); });
	modal.modal('open');
}

function obterOrcamentoAnoAtual(anoAtual){
	$.ajax({async: false});

	$.get(baseURL)
	.done(function(data){
		data = JSON.parse(data);
		data.forEach(function(v , i){
			if(v.exercicio == anoAtual){
				v.exercicio = anoAtual + 1;
				setTimeout(function(){
					cadastrarOrcamentoAnoSeguinte(v); 
				}, 1000);
			}
		});
	Materialize.toast('Orçamentos incluídos com sucesso!', 3000,"green lighten-1");
	});
}

function cadastrarOrcamentoAnoSeguinte(orcamento) { 

		var parametros = {
			"exercicio": parseInt(orcamento.exercicio),
	        "conta": orcamento.conta,
	        "codred": orcamento.codred,
			"descricao": orcamento.descricao,
			"analitica": orcamento.analitica,
			"saldoanterior": orcamento.orcamento,
			"movimentodebitocaixa": orcamento.movimentodebitocaixa,
			"movimentocreditocaixa": orcamento.movimentocreditocaixa,
			"movimentodebitocompetencia": orcamento.movimentodebitocompetencia,
			"movimentocreditocompetencia": orcamento.movimentocreditocompetencia
		};
		$.ajax({async: false});

	    $.ajax({
			type: 'PUT',
			contentType: 'application/json',
			url: baseURL,
			dataType: "json",
			data: JSON.stringify(parametros),
			error: function(){
	            setTimeout(function(){
	            	cadastrarOrcamentoAnoSeguinte(orcamento);
	            }, 2000);
			}
		});
}
