var perfil = "admin";
var url = "https://jsonplaceholder.typicode.com/users";

$.get(url)
    .done(function(data){
		var opcoes = [
			{id: 1, link: "/fmb/restrito", texto: "Home", icone: "fa fa-home"},
			{id: 2, link: "", texto: "Gráficos", icone: "fa fa-bar-chart", submenu: [
				{id: 1, link: "/fmb/restrito/graficos/dashboard1", texto: "Receitas mês a mês", icone: "fa fa-pie-chart", tipo: "Dashboard"},
				{id: 2, link: "/fmb/restrito/graficos/dashboard2", texto: "Desp. fixas e variáveis mês a mês", icone: "fa fa-pie-chart", tipo: "Dashboard"},
				{id: 3, link: "/fmb/restrito/graficos/dashboard3", texto: "Despesas mês a mês", icone: "fa fa-pie-chart", tipo: "Dashboard"},
				{id: 4, link: "/fmb/restrito/graficos/stackbar", texto: "Desp. fixas e variáveis mês a mês", icone: "fa fa-bar-chart", tipo: "Barras"},
				{id: 5, link: "/fmb/restrito/graficos/bolhas", texto: "Despesas totais por tipo", icone: "fa fa-bullseye", tipo: "Bolhas"},
				{id: 6, link: "/fmb/restrito/graficos/bolhas2", texto: "Receitas totais por tipo", icone: "fa fa-bullseye", tipo: "Bolhas"},
				{id: 7, link: "/fmb/restrito/graficos/disco", texto: "Despesas totais por tipo", icone: "fa fa-bullseye", tipo: "Disco"},
				{id: 8, link: "/fmb/restrito/graficos/arvore1", texto: "Execução orçamentária", icone: "fa fa-tree", tipo: "Árvore ponderada"},
				{id: 9, link: "/fmb/restrito/graficos/arvore2", texto: "Despesas de assessoria", icone: "fa fa-tree", tipo: "Árvore ponderada"},
				{id: 10, link: "/fmb/restrito/graficos/arvore3", texto: "Despesas de diretoria", icone: "fa fa-tree", tipo: "Árvore ponderada"}
			]},
			{id: 3, link: "/fmb/restrito/formularios/orcamento", texto: "Orçamento", icone: "fa fa-file-text-o"},
			{id: 4, link: "/Lancamento.html", texto: "Lançamento", icone: "fa fa-file-text-o"},
			{id: 5, link: "/fmb/restrito/formularios/mensageria", texto: "Mensageria", icone: "fa fa-envelope"},
			{id: 6, link: "/fmb/restrito/formularios/evento", texto: "Evento", icone: "fa fa-file-text-o"},
			{id: 7, link: "", texto: "Relatórios", icone: "fa fa-bar-chart", submenu: [
				{id: 1, link: "/fmb/restrito/formularios/relatorios/relatorioLancamentos", texto: "Relatório de Lançamentos", icone: "fa fa-pie-chart"},
				{id: 2, link: "/fmb/restrito/formularios/relatorios/relatorioDiretoria", texto: "Despesas Diretoria", icone: "fa fa-pie-chart"},
				{id: 3, link: "/fmb/restrito/formularios/relatorios/relatorioDespesas", texto: "Despesas Totais", icone: "fa fa-pie-chart"},
				{id: 4, link: "/fmb/restrito/formularios/relatorios/relatorioContribuicao", texto: "Relatorio Contribuição", icone: "fa fa-pie-chart"}
			]},
			{id: 8, link: "", texto: "Configurações", icone: "fa fa-cogs", submenu: [
				{id: 1, link: "/ConfiguracaoCRUD.html", texto: "Configuração Gráficos", icone: "fa fa-cogs"}
			]},
			{id: 9, link: "/fmb/formularios/login", texto: "Sair", icone: "fa fa-sign-out"}
		];

		var containerLateralMenu = ".lateral-menu";
		var containerDrawerMenu = ".drawer-menu";

		$.each(opcoes, function(i, v) {
			
			if(v.submenu !== undefined){
				//Insere no menu lateral
				$(containerLateralMenu).append("<ul class='collapsible' data-collapsible='accordion'></ul>");
				$(containerLateralMenu+" ul.collapsible:last").append("<li>");
				$(containerLateralMenu+" ul.collapsible:last li:last")
															  .append("<div class='collapsible-header'>"+v.texto+" <i class='material-icons right'>arrow_drop_down</i></div>")
															  .append("<div class='collapsible-body'></div>");
		  	    $(containerLateralMenu+" ul.collapsible:last li:last div.collapsible-body:last").append("<div class='collection'></div>");

				$.each(v.submenu, function(a, b) {
					$(containerLateralMenu+" > ul.collapsible:last li:last > div.collapsible-body > div.collection").append("<a class='collection-item' href='"+b.link+"'>"+b.texto+"</a>");

					if(b.tipo != undefined)
						$(containerLateralMenu+" > ul.collapsible:last li:last > div.collapsible-body > div.collection a.collection-item:last").addClass("tooltipped").attr("data-tooltip", "<i class='"+b.icone+"'></i> " + b.tipo).attr("data-position", "right");
				});

				//Insere no drawer menu
				var submenuContainer = $(containerDrawerMenu).append("<li>").find("li:last").append("<ul class='collapsible collapsible-accordion'><li></li></ul>").find("li:last");
				submenuContainer.append("<a class='collapsible-header'>"+v.texto+"<i class='material-icons right'>arrow_drop_down</i></a>");
				var opcoesContainer = submenuContainer.append("<div class='collapsible-body'></div>").find("div:last");

				$.each(v.submenu, function(a, b) {
					opcoesContainer.append("<li><a href='"+b.link+"'>"+b.texto+"</a></li>");
				});
			}
			else{
				//Insere no menu lateral
				$(containerLateralMenu).append("<a class='collection-item' href='" + v.link + "'><i class='" + v.icone + "'></i>" + v.texto + "</a>");

				//Insere no drawer menu
				$(containerDrawerMenu).append("<li>").find("li:last").append("<a href='"+v.link+"'>"+v.texto+"</a>");			
			}
		});

		$(containerDrawerMenu).append("<li class='divider'></li>").append("<li><a class='close-drawer-menu'>Fechar</a></li>");

		$(containerLateralMenu).append("<div class='offset-lateral-menu'></div>");

		inicializarComponentesMaterialize();
	});
