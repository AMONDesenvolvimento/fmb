/*
	SETUP DA PÁGINA /fmb/mensageria/index.html

	Este arquivo deve fazer todo o processamento para o carregamento inicial da página.
	Neste processamento deve estar qualquer requisição AJAX necessária para carregamento de dados, execução de animações, etc.

	Dependências deste script: 
	- /fmb/dependencias/JQuery/jquery-3.1.0.min.js
	- /fmb/dependencias/JQuery/jquery-ui.min.js
	- /fmb/dependencias/Materialize/js/materialize.js
	- /fmb/dependencias/css/spinner.css
	- /fmb/dependencias/js/spinner.js
*/

//URLs temporárias
var urlObterLancamentos = "lancamento";


function setupLancamento(){

	//Cria os spinners
	criarSpinner("#spinner-lancamento");

	//Preenche a tabela de Orcamento
	$.get(urlObterLancamentos)
	    .done(function(data){
			var contatos = data;
			var idTabela = "#tabela-lancamento";
			var idColuna = 2;
			//Configura as tabelas com filtro de dados
			tableFilter2("#tabela-lancamento", "#filtro-lancamento", "#pesquisa-lancamento", idColuna);

			var corpoTabela = $(idTabela+" tbody");
			corpoTabela.empty();
			$.each(JSON.parse(contatos), function(i, v){
				corpoTabela.append("<tr>");

				var ultimaLinhaAdicionada = corpoTabela.find("tr:last");
				ultimaLinhaAdicionada.addClass("dropdown-button").addClass("hoverable").attr("data-activates", "menu-"+v.id);
				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").html(v.evento).addClass(`hide-on-small-only`);

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").html(v.NomeEvento);

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").addClass("hide-on-small-only").html(formataDinheiro(v.valor)).addClass(`hide-on-small-only`);

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").addClass("hide-on-small-only").html(formataData(v.data)).addClass(`hide-on-small-only`);

				var ultimaCelulaAdicionada = ultimaLinhaAdicionada.find("td:last");
				ultimaCelulaAdicionada.append("<ul>").find("ul:last").addClass("dropdown-content").addClass("z-depth-3").attr("id", "menu-"+v.id);

				var listaOpcoes = ultimaCelulaAdicionada.find("ul#menu-"+v.id);
				listaOpcoes.append('<li><a href="#">Detalhar</a></li>').find("a:last").attr("onclick", "detalharLancamento("+v.id+")");
				listaOpcoes.append('<li><a href="#">Editar</a></li>').find("a:last").attr("onclick", "preencherFormEdicaoLancamento("+v.id+")");
				listaOpcoes.append('<li><a href="#">Excluir</a></li>').find("a:last").attr("onclick", "confirmarExclusaoLancamento("+v.id+",'"+v.nome+"')");

				$('.dropdown-button.hoverable').dropdown({
					constrainWidth: false,
					hover: false
				});

				//Destaca o item clicado na tabela
				corpoTabela.find("tr").click(function(){
					corpoTabela.find("tr").removeClass("linha-em-foco");
					//$(this).addClass("linha-em-foco");
				});

				//Remove o destaque ao sair da tabela
				corpoTabela.on('mouseleave', function(){
					corpoTabela.find("tr").removeClass("linha-em-foco");
				});
			});
		
			//Oculta o spinner e mostra a tabela
			ocultarSpinner("#spinner-lancamento");
			$("#painel-lancamento").show("fade", 500);
		});

}



var urlObterEventos = "evento";

function setupEvento(){

	//Preenche a tabela de Orcamento
	$.get(urlObterEventos)
	    .done(function(data){
			var evento = data;
			var idTabela = "#tab-eventos";

			//Configura as tabelas com filtro de dados
			tableFilter(idTabela, "#filtro-inclusao");

			var corpoTabela = $(idTabela+" tbody");
			corpoTabela.empty();

			$.each(JSON.parse(evento), function(i, v){
				corpoTabela.append("<tr id='evento-"+v.id+"'></tr>");

				var ultimaLinhaAdicionada = corpoTabela.find("tr:last");
				ultimaLinhaAdicionada.css("cursor", "pointer");
				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").html(v.id);

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").html(v.nome);

				ultimaLinhaAdicionada.append("<td>");
				ultimaLinhaAdicionada.find("td:last").html(v.tipo);

				var ultimaCelulaAdicionada = ultimaLinhaAdicionada.find("td:last");
				ultimaCelulaAdicionada.append("<ul>").find("ul:last").addClass("dropdown-content").addClass("z-depth-3").attr("id", "menu-"+v.id);

				$('.dropdown-button.hoverable').dropdown({
			        constrainWidth: false,
			        hover: false
			    });

			    $("#evento-inclusao").css("cursor", "pointer");
			    $(".oculto").hide();

			    $("#evento-inclusao").click(function(){
			    	$("#filtro-evento").show("slide", 600);
			    });

			    $("#filtro-evento tr").click(function(){
			    	var id = $(this).find("td:nth-child(1)").html();
			    	var descricao = $(this).find("td:nth-child(2)").html();
			    	$("#filtro-evento").hide("slide", 600);
			    	$("#form-lancamento label[for=evento-inclusao]").addClass("active");
			    	$("#evento-inclusao").val(id + " - " + descricao);
			    	$(".oculto").show();
			    });
			});
		});

}

setupLancamento();
setupEvento();