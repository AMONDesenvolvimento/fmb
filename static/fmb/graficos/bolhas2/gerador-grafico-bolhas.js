function gerarGrafico(dados, dadosLegenda, containerHTML){

    $(containerHTML)
        .css("padding", (window.innerWidth * 0.09) + "px")
        .append("<svg>");
    
    $("svg").attr("width", window.innerWidth * ((window.innerWidth >= 993) ? 0.73 : 0.92))
            .attr("height", window.innerWidth >= 993 ? window.innerWidth * 0.72 : window.innerWidth)
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("style", function(){
                var css = "margin-top: " + (window.innerWidth * -0.02) + "px"; 
                if(window.innerWidth <= 992)
                    css += ";margin-left:-30px";
                return css;
            });

    var svg = d3.select(containerHTML+" > svg"),
        width = +svg.attr("width"),
        height = +svg.attr("height");

    var color = d3.scaleOrdinal(d3.schemeCategory10).range(["#6be5db","#41AB5D","#E08214","#807DBA"]);

    var pack = d3.pack()
        .size([width, height])
        .padding(1.5);

    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("color", "white")
        .style("padding", "8px")
        .style("background-color", "rgba(0, 0, 0, 0.75)")
        .style("border-radius", "6px")
        .style("font", "12px sans-serif");

    var root = d3.hierarchy({children: dados})
        .sum(function(d) { return d.value; })
        .each(function(d) {
          if (id = d.data.id) {
            var id, i = id.lastIndexOf(".");
            d.id = id;
            d.package = id.slice(0, i);
            d.class = id.slice(i + 1);
          }
        });

    var node = svg.selectAll(".node")
      .data(pack(root).leaves())
      .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { 
            return "translate(" + d.x + "," + d.y + ")"; 
        });

    node.append("circle")
        .attr("id", function(d) { return d.id; })
        .attr("r", function(d) {return d.r; })
        .style("fill", function(d) { return color(d.package); })
        .on("mouseover", function(d) {
              tooltip.html(d.class + "<hr>" + formataDinheiro(d.value));
              tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() { return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
        .on("mouseout", function(){ return tooltip.style("visibility", "hidden"); });

var lista = [];

    node.append("text")
      .selectAll("tspan")
      .data(function(d) {
        if(d.r < 15){
            $.each(d.class.split(" "), function(i,v){
                lista.push(v); 
            });
        }
        return (d.value != 0) ? d.class.split(/(?=[A-Z][^A-Z])/g) : ""; })
      .enter().append("tspan")
        .attr("x", 0)
        .attr("y", function(d, i, nodes) {return 13 + (i - nodes.length / 2 - 0.5) * 10; })
        .attr("font-size", function(){
            if(window.innerWidth >= 993)
                return "11.5px";
            return "9px";
        })
        .text(function(d) {
            return lista.indexOf(d.trim())== -1 ?d:"";});

    function legend(lD){
        var leg = {};

        // create table for legend.
        var legend = d3.select(containerHTML).append("table")
            .attr('class','legend bordered')
            .style("margin", function(){
                return (window.innerWidth <= 992) ? "0 0 0 -40px" : "35px 0 0 0";
            })
            .style("width", function(){
                return (window.innerWidth <= 992) ? "85vw" : "61vw";
            });
        
        // create one row per segment.
        var tr = legend.append("tbody").selectAll("tr").data(lD).enter().append("tr");
            
        // create the first column for each segment.
        tr.append("td")
                .attr("width", '12')
                .style("padding", "5px")
          .append("svg")
                .attr("width", '12')
                .attr("height", '12')
          .append("rect")
                .attr("width", '16')
                .attr("height", '16')
          .attr("fill", function(d){
            switch(d.id){
                case "Aplicação Financeira" : return "#807dba";
                case "Contribuição Sindical" : return "#E08214";
                case "Contribuição Social" : return "#41AB5D";
                case "Disponibilidade em 31/12/2016" : return "#6BE5DB";
            }
            });

        var defineFonte = function(){
                            return (window.innerWidth <= 992) ? "10px" : "13px";
                      };
            
        // create the second column for each segment.
        tr.append("td").text(function(d){return d.id;})
            .style("font-size", defineFonte);

        // create the third column for each segment.
        tr.append("td").attr("class",'legendFreq')
            .attr("width", "15%")
            .text(function(d){return formataDinheiro(d.valor);})
            .style("font-size", defineFonte);

        // create the fourth column for each segment.
        tr.append("td").attr("class",'legendPerc')
            .attr("width", "10%")
            .text(function(d){ return getLegend(d,lD);})
            .style("font-size", defineFonte);

        function getLegend(d,aD){ // Utility function to compute percentage.
            return ((d.valor/d3.sum(aD.map(function(v){ return v.valor; }))) * 100).toFixed(2) + "%";
        }

        return leg;
    }
    
    var leg = legend(dadosLegenda);
}