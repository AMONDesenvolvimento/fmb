var dadosGrafico;

function obterTitulo(){
    return "Despesas totais por tipo";
}

function obterSubtitulo(){
    return "Regime de Caixa";
}

function gerarDados(){

    $.ajaxSetup({async: false});

    $.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);

            var parametrosDespesa = {
                exercicio: parametros.exercicio.toString(),
                mesreferencia: parametros.mesreferencia.toString(),
                ultimomes: parametros.ultimomes.toString(),
                conta: parametros.contadespesa,
                regime: parametros.regimedespesa,
                arvorecompleta: parametros.todassubcontasdespesa
            };

            parametrosDespesa = JSON.stringify(parametrosDespesa);

            $.ajax({
                url: baseURL + "orcamentodespesabubble",
                type: "POST",
                data: parametrosDespesa
            })
            .done(function(data){
                var dadosJSON = $.parseJSON(data);
                dadosGrafico = dadosJSON.children;
            });
        });

    return tratamentoDados(dadosGrafico);
}


function tratamentoDados(original){
    var dados = [];

    //Processa apenas até 3 níveis de agrupamento
    $.each(original, function(i, n1){
        var obj = {};

        if(n1.children){
          $.each(n1.children, function(a, n2){
              obj.id = n1.name.trim() + ".";

              if(n2.children){
                  $.each(n2.children, function(x, n3){
                      obj.id = n1.name.trim() + "." + n2.name.trim() + ".";

                      obj.id += n3.name.trim();
                      obj.value = n3.size;
                      dados.push(obj);
                      obj = {};
                  });
              }
              else {
                  obj.id += n2.name.trim();
                  obj.value = n2.size;
                  dados.push(obj);
                  obj = {};
              }
          });
        }
        else{
            obj.id = n1.name.trim();
            obj.value = n1.size;
            dados.push(obj);
            obj = {};
        }
    });

    return dados;
}

function gerarLegenda(dados){
  var legenda = [{id: "Assessorias", valor : 0}, 
                 {id: "Custeio de Instalações", valor : 0},
                 {id: "Despesas Diretoria", valor : 0},
                 {id: "Folha de Pagamento", valor : 0},
                 {id: "Provisionamento", valor : 0}];
    $.each(dados, function(i,v){
      var grupo = dados[i].id.split(".")[0];
        adicionaSomatoria(legenda, grupo, dados[i].value);
    });

    return legenda;
}

function adicionaSomatoria(array, index, value){
  $.each(array, function(i,v){
    if(v.id == index)
      v.valor += value;
  });
}