function gerarGrafico(containerHTML, dados){

    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("margin", "23px 0 0 -50px")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("color", "white")
        .style("padding", "12px")
        .style("background-color", "rgba(0, 0, 0, 0.75)")
        .style("border-radius", "6px")
        .style("font", "12px sans-serif")
        .attr("id", "tooltip");

    var margin = {top: 20, right: 120, bottom: 20, left: 120},
        width = 1000 - margin.right - margin.left,
        height = 1200 - margin.top - margin.bottom;
        
    var i = 0,
        duration = 750,
        root;

    var tree = d3.layout.tree()
        .size([height, width]);

    var edge_weight = d3.scale.linear()
    					.domain([0, 100])
                        .range([0, 100]);

    var diagonal = d3.svg.diagonal()
        .projection(function(d) { return [d.y, d.x]; });

    var svg = d3.select(containerHTML).append("svg")
        .attr("width", width + margin.right + margin.left)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    edge_weight.domain([0, dados.orcado]);
    root = dados;
    root.x0 = height / 2;
    root.y0 = 0;

    //Recolhe todos os galhos
    //root.children.forEach(collapse);
    update(root);

    d3.select(self.frameElement).style("height", "800px");

    function update(source) {
      
      var nodes = tree.nodes(root).reverse(),
          links = tree.links(nodes);

      nodes.forEach(function(d) { d.y = d.depth * 180; });

      var node = svg.selectAll("g.node")
          .data(nodes, function(d) { return d.id || (d.id = ++i); });

      var nodeEnter = node.enter().append("g")
          .attr("class", "node")
          .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
          .on("click", click);

      var tooltipHTMLBase = "<h5>#titulo</h5><h6>#subtitulo</h6>";
      tooltipHTMLBase += "<table id='tooltip-table'><tr><td>Orçamento</td><td>#col1</td></tr><tr><td>Realizado</td><td>#col2</td></tr><tr><td>Saldo</td><td>#col3</td></tr></table>";

      nodeEnter.append("circle")
          .attr("r", 1e-6)
          .style("fill", "grey")
          .on("mouseover", function(d) {
              if(d.titulo != undefined) {
                  tooltip.html(function(){
                      var titulo = d.titulo;
                      var texto = tooltipHTMLBase.replace("#titulo", titulo);
                      texto = texto.replace("#subtitulo", d.subtitulo);
                      texto = texto.replace("#col1", formataDinheiro(d.orcado));
                      texto = texto.replace("#col2", formataDinheiro(d.realizado));
                      texto = texto.replace("#col3", formataDinheiro(d.saldo));
                      return texto;
                  });
                  tooltip.style("visibility", "visible");
              }
          })
          .on("mousemove", function() { return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
          .on("mouseout", function(){ return tooltip.style("visibility", "hidden"); });

      nodeEnter.append("text")
          .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
          .attr("dy", ".35em")
          .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
          .text(function(d) { return d.nome; })
          .style("fill-opacity", 1e-6)
          .on("mouseover", function(d) {
                  tooltip.html(function(){
                    var titulo = d.titulo;
                    var texto = tooltipHTMLBase.replace("#titulo", titulo);
                    texto = texto.replace("#subtitulo", d.subtitulo);
                    texto = texto.replace("#col1", formataDinheiro(d.orcado));
                    texto = texto.replace("#col2", formataDinheiro(d.realizado));
                    texto = texto.replace("#col3", formataDinheiro(d.saldo));
                    return texto;
                  });
                  tooltip.style("visibility", "visible");
          })
          .on("mousemove", function() { return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
          .on("mouseout", function(){ return tooltip.style("visibility", "hidden"); });

      var nodeUpdate = node.transition()
          .duration(duration)
          .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

      nodeUpdate.select("circle")
          .attr("r", function(d){ return edge_weight(d.orcado/2);})
          .style("fill", function(d) { 
              return d._children ? "#fff" : "#ddd"; 
          });

      nodeUpdate.select("text")
          .style("fill-opacity", 1);

      var nodeExit = node.exit().transition()
          .duration(duration)
          .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
          .remove();

      nodeExit.select("circle")
          .attr("r", 1e-6);

      nodeExit.select("text")
          .style("fill-opacity", 1e-6);

      var link = svg.selectAll("path.link")
          .data(links, function(d) { return d.target.id; });

      link.enter().insert("path", "g")
          .attr("class", "link")
          .attr("stroke-width", 1)
          .attr("d", function(d) {
            var o = {x: source.x0, y: source.y0};
            return diagonal({source: o, target: o});
          })
          .attr("stroke", function(d){ 
          	return d.target.cor;});

      link.transition()
          .duration(duration)
          .attr("d", function(d){
          var source = {x: d.source.x - edge_weight(calculateLinkSourcePosition(d)), y: d.source.y};
          var target = {x: d.target.x, y: d.target.y};
            return diagonal({source: source, target: target});
          })
          .attr("stroke-width", function(d){
          	return edge_weight(d.target.orcado) + 1;
          });

      link.exit().transition()
          .duration(duration)
          .attr("d", function(d) {
            var o = {x: source.x, y: source.y};
            return diagonal({source: o, target: o});
          })
          .remove();

      nodes.forEach(function(d) {
        d.x0 = d.x;
        d.y0 = d.y;
      });

      //var SVGwidth = $("svg").attr("width");
      var fator;
      if(window.innerWidth > 992){
          fator = 1.23;
          $(".brand-logo").css("margin-left", "0px");
      }
      else {
          fator = 1.5;
          $(".brand-logo").css("margin-left", "50px");
      }
      
      //$(".nav-extended").css("width", "100vw");
    }

    function calculateLinkSourcePosition(link) {
    	targetID = link.target.id;
    	var childrenNumber = link.source.children.length;
    	var widthAbove = 0;
    	for (var i = 0; i < childrenNumber; i++)
    	{
    		if (link.source.children[i].id == targetID)
    		{
    			widthAbove = widthAbove + link.source.children[i].orcado/2;
    			break;
    		}
        else {
    			widthAbove = widthAbove + link.source.children[i].orcado
    		}
    	}
    	return link.source.orcado/2 - widthAbove;
    }

    function click(d) {
      if (d.children) {
        d._children = d.children;
        d.children = null;
      } else {
        d.children = d._children;
        d._children = null;
      }
      update(d);
    }

    function collapse(d) {
      if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
      }
    }

    function collapseAll() {
    	root.children.forEach(collapse);
    	update(root);
    }

    function expand(d) {
    	if (d._children) {
    		d.children = d._children;
    		d._children = null;
    	}
    	if (d.children) {
    		d.children.forEach(expand);
    	}
    }

    function expandAll() {
    	root.children.forEach(expand);
    	update(root);
    }
}