function obterTitulo(){
	return "Despesas mês a mês";
}

function obterSubtitulo(){
    return "Regime de Caixa";
}

function tratamentoDados(original){
    original = JSON.stringify(original);

    var tratado = original.replace(/(Folha de Pagamento )/g, 'folhaPagamento');
    tratado = tratado.replace(/(Custeio de Instalações)/g, 'custeioInstalacoes');
    tratado = tratado.replace(/(Assessorias)/g, 'assessorias');
    tratado = tratado.replace(/(Despesas Diretoria)/g, 'despesasDiretoria');
    tratado = tratado.replace(/(Provisionamento )/g, 'provisionamento');

    var adaptado = $.parseJSON(tratado);

    //Tratamento dos dados para o layout correto do JSON
    var dados = [];
    $.each(adaptado, function(i, v){
        var total = v.p2.assessorias + v.p2.custeioInstalacoes + v.p2.despesasDiretoria + v.p2.provisionamento + v.p2.folhaPagamento;

        dados.push(
            {
                mes: v.p1, 
                freq: v.p2,
                total: total
            }
        );
    });

    return dados;
}


function gerarDados(){
    var dadosGrafico;

    $.ajaxSetup({async: false});

    $.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);

            var parametrosDespesa = {
                exercicio: parametros.exercicio.toString(),
                mesreferencia: parametros.mesreferencia.toString(),
                ultimomes: parametros.ultimomes.toString(),
                conta: parametros.contadespesa,
                regime: parametros.regimedespesa,
                arvorecompleta: parametros.todassubcontasdespesa
            };

            parametrosDespesa = JSON.stringify(parametrosDespesa);

            $.ajax({
                url: baseURL + "orcamentodespesamensaldashboard",
                type: "POST",
                data: parametrosDespesa
            })
            .done(function(data){
                var dadosJSON = $.parseJSON(data).dados;
                dadosGrafico = dadosJSON;
            });
        });

    return tratamentoDados(dadosGrafico);
}