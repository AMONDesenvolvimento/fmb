function obterTitulo(){
	return "Despesas fixas e variáveis mês a mês";
}

function obterSubtitulo(){
	return "Regime de Caixa";
}

function tratamentoDados(dados){
	var tratado = [];
	for(var i=1; i<dados[0].length; i++){
		tratado.push(
			{
				id: dados[0][i].charAt(0).toUpperCase() + dados[0][i].slice(1),
				fixo: parseFloat(dados[2][i]),
				variavel: parseFloat(dados[1][i]),
				total: parseFloat(dados[1][i]) + parseFloat(dados[2][i])
			}
		);
	}
	return tratado;
}

function gerarDados(){

	var dadosGrafico;

	$.ajaxSetup({ async: false});

	$.ajax(baseURL + "configuracao/1")
		.done(function(data){
			var parametros = $.parseJSON(data);

			var parametrosInit = {
					exercicio : parametros.exercicio.toString(),
					mesreferencia : parametros.mesreferencia.toString(),
					ultimomes : parametros.ultimomes.toString(),
					conta : parametros.contadespesa,
					regime : parametros.regimedespesa,
					arvorecompleta : parametros.todassubcontasdespesa
			};

			parametrosInit = JSON.stringify(parametrosInit);

			$.ajax({
				type : "POST",
				url : baseURL + "orcamentotipodespesamensaldashboard",
				data : parametrosInit
			})
			.done(function(data){
				var temp = $.parseJSON(data);

				var dados = [["x"],["Variavel"],["Fixo"]];

				$.each(temp, function(i, v){
					dados[0].push(v.p1);
					dados[1].push(v.p2.Var);
					dados[2].push(v.p2.Fixo);
				});
				dadosGrafico = tratamentoDados(dados);
			});
		});
		
	return dadosGrafico;
}