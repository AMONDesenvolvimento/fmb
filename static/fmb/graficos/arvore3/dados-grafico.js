function obterTitulo(){
	return "Despesas com Diretoria";
}

function obterSubtitulo(){
	var titulo;

	$.ajaxSetup({async: false});

	$.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);
            titulo = "até " + descricaoMes(parametros.ultimomes) + "/" + parametros.exercicio;
        });

	return titulo;
}

function gerarDados(){
	var dadosGrafico;

    $.ajaxSetup({async: false});

    $.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);

            var parametrosReceita = {
                exercicio: parametros.exercicio.toString(),
                mesreferencia: parametros.mesreferencia.toString(),
                ultimomes: parametros.ultimomes.toString(),
                conta: parametros.contareceita,
                regime: parametros.regimereceita,
                arvorecompleta: parametros.todassubcontasreceita
            };

            parametrosReceita = JSON.stringify(parametrosReceita);

            $.ajax({
                url: baseURL + "weightedTree_Despesas_Diretoria/orcamentotree",
                type: "POST",
                data: parametrosReceita
            })
            .done(function(data){
                dadosGrafico = $.parseJSON(data);
            });
        });

    return tratamentoDados(dadosGrafico);
}

function tratamentoDados(original){
	var totalAssessorias = {realizado: 0.0};

	$.each(original, function(i, v){
		totalAssessorias.realizado += parseFloat(v.Total);
	});

	var output = {
					titulo: "Diretoria",
					subtitulo: "",
					nome: "Diretoria",
					cor: "red",
					realizado: totalAssessorias.realizado,
					children: []
				 };

	$.each(original, function(i, n1){
		var posicao = 0, 
		    cor = "red";

		if(n1.Level3 != "" && !itemInArray(n1, output.children, 3)) {
			output.children.push(
				{
					titulo: n1.Level1,
					subtitulo: n1.Level3,
					nome: n1.Level3,
					realizado: 0.0,
					cor: cor,
					children: []
				}
			);
		}

		if(n1.Level2 != ""){
			$.each(output.children, function(a, pessoa){
				if(!itemInArray(n1, pessoa.children, 2) && n1.Level3 == pessoa.nome){
					var obj = {
						titulo: "Diretoria - " + pessoa.nome,
						subtitulo: n1.Level2,
						nome: n1.Level2,
						realizado: n1.Total,
						cor: cor
					};

					pessoa.children.push(obj);
					pessoa.realizado += obj.realizado;
				}
			});
		}

	});

	return output;
}

function itemInArray(item, array, level){
	var inArray = false;
	$.each(array, function(i, v){
		switch(level) {
			case 2:
				if(item.Level2 == v.nome || item.nome == v.nome)
					inArray = true;
				break;
			case 3: 
				if(item.Level3 == v.nome || item.nome == v.nome)
					inArray = true;
				break;
		}
	});
	return inArray;
}

function gerarDadosTeste(){
	return {
			cor : "teal",
			nome : "Receita",
			orcado : 100.00,
			realizado : 90.32,
			saldo : 9.68, 
			children : [
				{
					cor : "red",
					nome : "Disponibilidade em 31/12/2016",
					orcado : 30.54,
					realizado : 20.33,
					saldo : 10.78
				},
				{
					cor : "red",
					nome : "Contribuição social",
					orcado : 70.66,
					realizado : 20.15,
					saldo : 50.10,
					children: [
						{
							cor: "green",
							nome: "Pará/PA",
							orcado: 42000.00,
							realizado: 17500.00,
							saldo: 24500.00
						},
						{
							cor: "green",
							nome: "Paraíba/PB",
							orcado: 24000.00,
							realizado: 10000.00,
							saldo: 14000.00
						},
						{
							cor: "green",
							nome: "Campinas/SP",
							orcado: 36000.00,
							realizado: 8100.00,
							saldo: 27900.00
						}
					]
				}
			]
	};
}
