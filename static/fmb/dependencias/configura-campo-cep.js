$(function(){

	$("input.mascara-cep").keyup(function(){
		var completo = $(this).val().replace(/\d{5}-\d{3}/g, "") === "" && $(this).val().length == 9;

		if(completo) {
			var inputId = "#"+$(this).attr("id");
			var cep = $(inputId).val();

			$(inputId).addClass("tooltiped")
			          .attr("data-tooltip", "Aguarde o preenchimento automático")
				      .tooltip({delay: 0})
				      .mouseover();

			var urlApi = "https://viacep.com.br/ws/"+cep+"/json/";
			$.get(urlApi, function() { }, "json")
				.done(function(dados){
					//Simula o tempo de requisição
				    setTimeout(function(){
				    	//Define os campos autocompletados a partir do CEP
					    $("input[id$=-uf]").val(dados.uf);
					    $("input[id$=-cidade]").val(dados.localidade);
					    $("input[id$=-bairro]").val(dados.bairro);
					    $("input[id$=-logradouro]").val(dados.logradouro);

					    $(".autocompletado > label").addClass("active");
					    $(".autocompletado > input").removeAttr("disabled");

				    	$(inputId).tooltip().mouseout();
				    }, 1500);
				});
		}
	});

});