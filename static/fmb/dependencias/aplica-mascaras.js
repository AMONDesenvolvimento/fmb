$(function(){

    $("input.mascara-dinheiro").maskMoney({prefix:'R$ ', howSymbol:true, thousands:'.', decimal:',', symbolStay: true});

    $(".mascara-celular").inputmask("(9{2}) 9{5}-9{4}");

    $(".mascara-telefone").inputmask("(9{2}) 9{4}-9{4}");

    $(".mascara-cep").inputmask("9{5}-9{3}");

    $(".mascara-cpf").inputmask("9{3}.9{3}.9{3}-9{2}");

    $(".mascara-cnpj").inputmask("9{2}.9{3}.9{3}/9{4}-9{2}");

    $("input.mascara-email").inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
          return pastedValue.toLowerCase();
        },
        definitions: {
          '*': {
            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
            cardinality: 1,
            casing: "lower"
          }
        }
    });
});


function aplicarMascaraCelular(valor) {
    if(valor === undefined)
        return "";
    if(valor.length != 11)
        return valor;
    
    var ddd = valor.substr(0, 2);
    var parte1 = valor.substr(2, 5);
    var parte2 = valor.substr(7, 4);
    return "(" + ddd + ") " + parte1 + "-" + parte2;
}

function aplicarMascaraFixo(valor) {
    if(valor === undefined)
        return "";
    if(valor.length != 10)
        return valor;
    
    var ddd = valor.substr(0, 2);
    var parte1 = valor.substr(2, 4);
    var parte2 = valor.substr(6, 4);
    return "(" + ddd + ") " + parte1 + "-" + parte2;
}

function aplicarMascaraCEP(valor) {
    if(valor === undefined)
        return "";
    if(valor.length != 8)
        return valor;

    return valor.substr(0, 5) + "-" + valor.substr(5, 3);
}

function aplicarMascaraData(valor, semAno = false){
    if(valor === undefined || valor === "")
        return "";

    var ano = valor.substr(0, 4);
    var mes = valor.substr(5, 2);
    var dia = valor.substr(8, 2);
    var retorno = dia + "/" + mes;

    if(!semAno){
        retorno += "/" + ano;
    }

    return retorno;
}

function aplicarMascaraHora(valor, comSegundos = true){
    if(valor === undefined || valor === "")
        return "";

    var hora = valor.substr(11, 2);
    var minuto = valor.substr(14, 2);
    var segundo = valor.substr(17, 2);
    var retorno = hora + ":" + minuto;

    if(comSegundos){
        comSegundos += ":" + segundo;
    }

    return retorno;
}