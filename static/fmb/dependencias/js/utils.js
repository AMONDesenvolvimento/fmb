function identificarBrowser(){

    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    if(isFirefox) return ("Firefox");
    if(isIE) return("IE");
    if(isEdge) return("Edge");
    if(isChrome) return("Chrome");
    if(isSafari) return("Safari");
    if(isOpera) return("Opera");
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function formataDinheiro(valor, comCifrao = true, comCentavos = true) { 
    var r=valor.toFixed(2).replace(/./g, function(c,i,a) { 
        return i>0 && c !== "." && ((a.length-i)%3 ===0)?','+c:c; 
    }) 
    r=r.replace(".","#"); 
    r=r.replace(",","."); 
    r=r.replace("#",","); 

    if(!comCentavos)
        r = r.slice(0, r.indexOf(","));

    return comCifrao ? "R$ " + r : r; 
}

function desformataDinheiro(_number) {
    var resultado = _number.replace(/(\R\$\ )|\./g,"");
    resultado = resultado.replace(",",".");
    return resultado;
}

function formataData(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

/*function formataData_mesAno(dateObject) {
    var d = new Date(dateObject);
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (month < 10) {
        month = "0" + month;
    }
    var date = month + "/" + year;
    return date;
};
*/
function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase=_format.toLowerCase();
    var formatItems=formatLowerCase.split(_delimiter);
    var dateItems=_date.split(_delimiter);
    var monthIndex=formatItems.indexOf("mm");
    var dayIndex=formatItems.indexOf("dd");
    var yearIndex=formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    month-=1;
    var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
    return formatedDate;
}

//verifica campos requeridos do form estão vazios e retorna bol
function verificaRequeridos(idform){
    var requeridos = $(idform + " :input[required]");
    var a = true;
    $.each(requeridos, function(i, v){
        if(v.value === ""){
            a = false;
            return;
        }
    });
    return a;
}

function processaValorDinheiroNulo(valor){
    return (valor === "") ? 0 : parseFloat(desformataDinheiro(valor));
}

function descricaoMes(mesReferencia){
 var mes = "";

    switch (mesReferencia) {
        case  1:
            mes = "Janeiro";
            break;
        case 2:
             mes = "Fevereiro"
            break;
        case 3:
            mes = "Março"
            break;
        case 4:
             mes =  "Abril"
            break;
        case 5:
            mes =  "Maio"
            break;
        case 6:
             mes = "Junho"
            break;
        case 7:
             mes =  "Julho"
            break;
        case 8:
             mes =  "Agosto"
            break;
        case 9:
             mes = "Setembro"
            break;
        case 10:
             mes =  "Outubro"
            break;
        case 11:
             mes = "Novembro"
            break;
        case 12:
             mes =  "Dezembro"
            break;
        default:
             mes =  "***"
    }
    return mes;
}

var espacamento = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";