function criarSpinner(containerHTML) {
	var dimensao = "65px";

	$(containerHTML).append('<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="circle" fill="none" stroke-width="3" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg>');
}

function ocultarSpinner(containerHTML){
	$(containerHTML).hide();
	$(containerHTML).empty();
}