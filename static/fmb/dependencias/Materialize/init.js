﻿function inicializarComponentesMaterialize() {
    $("body").show("fade", "slow");

    $('.button-collapse').sideNav();

    $('select').material_select();

    $('.tooltipped').tooltip({ 
        delay: 50
    });

    $('.dropdown-button').dropdown({
        hover: false,
        constrainWidth: false
    });

    $('.dropdown-button.hoverable').dropdown({
        constrainWidth: false
    });

    $('.buton-collapse').sideNav();

    $('.collapsible').collapsible();

    $('ul.tabs').tabs();

    $('.modal').modal({
        dismissible: false, //Pode ser fechado com um clique na parte externa
        opacity: .5
    });

    $('.datepicker').pickadate({
        selectMonths: true, // Mostra um combo contendo os meses do ano
        selectYears: 154, // Mostra um combo contendo um intervalo de 5 anos
    	clear: 'Limpar',
        format: 'dd/mm/yyyy',
        onOpen: function(){ },
        onSet: function(){ 
   		$("label[for*=data-]").addClass("active");
    	},
    	onClose: function(){ }
    });

    $('.datepicker-format').pickadate({
        selectMonths: true, // Mostra um combo contendo os meses do ano
        selectYears: 154, // Mostra um combo contendo um intervalo de 5 anos
        clear: 'Limpar',
        format: 'yyyy-mm-dd',
        onOpen: function(){ },
        onSet: function(){ 
            $("label[for*=data-]").addClass("active");
        },
        onClose: function(){ }
    });

    $('.datepicker-without-year').pickadate({
        selectMonths: true, 
        format: 'dd/mm',
        onOpen: function(){ 
            $(".picker__year, .picker__table thead").hide();
        },
        onSet: function(){ 
            $(".picker__year, .picker__table thead").hide();
            $("label[for*=data-]").addClass("active");
        },
        onClose: function(){ }
        
    });

    $('.datepicker-only-year').pickadate({
        selectMonths: false, 
        selectYears: 10,
        format: 'yyyy'
    });

  $('.datepicker-only-month').pickadate({
        selectYears: false,
        format: 'mm'
    });

    $('.timepicker').pickatime({
        default: 'now',
        twelvehour: false, //formato 12 ou 24 horas
        donetext: 'OK',
        autoclose: true
    });

    $('.slider').slider({
        indicators: true,
        interval: 3000
    });

    $('.close-drawer-menu').click(function(){
        $('.button-collapse').sideNav('hide');
    });

    $(".tab a").click(function(){
        if(!$(this).hasClass("active")) {
            $(".tab-content").hide();
            var tabSelector = $(this).attr("href");
            $(tabSelector).show("slide", 400);
        }
    });

    return true;
}
