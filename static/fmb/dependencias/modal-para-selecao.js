function modalSelector(disparador, idModal, item){
	$("#"+idModal+" table td").css("cursor", "pointer");
	$("#"+disparador+",label[for="+disparador+"]").css("cursor", "pointer");

	//Aciona o modal a partir do disparador
	$("#"+disparador).click(function(){
    	$("#"+idModal).modal("open");
	});
	//Fecha o modal e define o valor a partir da linha selecionada na tabela
	$("#"+idModal+" tr[id^="+item+"]").click(function(){
		var id = idModal.replace("selecionar-", "");
		$("#"+id).val($(this).find("td:first").html());
		$("label[for="+id+"]").addClass("active");
		$("#"+idModal).modal("close");
	});
}

function modalSelector2(disparador, idModal, item, targetColumn,targetInput){
	$("#"+idModal+" table td").css("cursor", "pointer");
	$("#"+disparador+",label[for="+disparador+"]").css("cursor","pointer");

	//Aciona o modal a partir do disparador
	$("#"+disparador).click(function(){
    	$("#"+idModal).modal("open");
	});
	//Fecha o modal e define o valor a partir da linha selecionada na tabela
	$("#"+idModal+" tr[id^="+item+"]").click(function(){
		var id = idModal.replace("selecionar-", "");
		$("#"+targetInput).val($(this).find("td:first").html());
		$("#"+id).val($(this).find("td:nth-child("+targetColumn+")").html());
		$("label[for="+id+"]").addClass("active");
		$("#"+idModal).modal("close");
	});
}