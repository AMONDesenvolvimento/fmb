function tableFilter(idTabela, idInputFilter){

  $(idInputFilter).keyup(function(){
      //Guarda o valor digitado na pesquisa
      var search = $(idInputFilter).val();
      
      //Esconde todas as linhas 
      $(idTabela+" tbody tr").hide();
      
      //Exibe apenas as linhas que correspondem à pesquisa
      $.each($(idTabela+" tbody").children("tr"), function(){
          var quantCols = $(idTabela+" tbody tr:first").children("td").length;
          for(var i=1; i<=quantCols; i++){
              if($(this).find("td:nth-child("+i+")").html().search(search) >= 0)
                  $(this).show(70);
          }
      });
  });
}


//////////////////////////////////////////////


function tableFilter2(idTabela, idInputFilter, idDisparador, idColuna){

    if(idDisparador != undefined) {
        $(idDisparador).addClass("tooltipped")
                       .attr("data-tooltip", "Digite e clique para pesquisar")
                       .attr("data-position", "right")
                       .css("font-size", "1.4em")
                       .parent()
                       .css("padding-top", "20px");

        var span = $("<span></span>").css("font-size", "10px")
                       .css("padding-bottom", "20px")
                         .html("Para listar todos, faça uma pesquisa sem filtro.");

              $(idInputFilter).css("margin-bottom", "0px")
                              .parent(".input-field")
                              .after(span);


          $(idDisparador).click(function(){
            disparaBusca(idInputFilter, idTabela, idColuna);
        });
    }

    $(idInputFilter).keyup(function(e){
        var tecla = e.which || e.keyCode;
        if(tecla == 13) {
            disparaBusca(idInputFilter, idTabela, idColuna);
        }
    });
}



function disparaBusca(idInputFilter, idTabela, idColuna){
    //Guarda o valor digitado na pesquisa
    var search = $(idInputFilter).val().toLowerCase();

    console.log(search);
    
    //Esconde todas as linhas 
    $(idTabela+" tbody tr").hide();
    
    //Exibe apenas as linhas que correspondem à pesquisa
    $.each($(idTabela+" tbody").children("tr"), function(){
      var valorColuna = $(this).find("td:nth-child("+idColuna+")").html().toLowerCase();
      if(valorColuna.search(search) >= 0)
        $(this).show(70);
    });
}