function obterTitulo(){
	return "Despesas fixas e variáveis mês a mês";
}

function obterSubtitulo(){
    return "Regime de Caixa";
}

function tratamentoDados(original){
    var tratado = original.replace(/(Fixo)/g, 'fixo');
    tratado = tratado.replace(/(Var)/g, 'variavel');

    var adaptado = $.parseJSON(tratado);

    //Tratamento dos dados para o layout correto do JSON
    var dados = [];
    $.each(adaptado, function(i, v){
        var total = v.p2.fixo + v.p2.variavel;

        dados.push(
            {
                mes: v.p1, 
                freq: v.p2,
                total: total
            }
        );
    });

    return dados;
}


function gerarDados(){
    var dadosGrafico;

    $.ajaxSetup({async: false});

    $.ajax(baseURL + "configuracao/1")
        .done(function(data){
            var parametros = $.parseJSON(data);

            var parametrosDespesa = {
                exercicio: parametros.exercicio.toString(),
                mesreferencia: parametros.mesreferencia.toString(),
                ultimomes: parametros.ultimomes.toString(),
                conta: parametros.contadespesa,
                regime: parametros.regimedespesa,
                arvorecompleta: parametros.todassubcontasdespesa
            };
            parametrosDespesa = JSON.stringify(parametrosDespesa);

            $.ajax({
                url: baseURL + "orcamentotipodespesamensaldashboard",
                type: "POST",
                data: parametrosDespesa
            })
            .done(function(data){
                dadosGrafico = data;
            });
        });
        
    return tratamentoDados(dadosGrafico);
}