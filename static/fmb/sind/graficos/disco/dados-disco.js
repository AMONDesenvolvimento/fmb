function obterTitulo(){
    return "Despesas totais por tipo";
}

function obterSubtitulo(){
    return "Regime de Caixa";
}


function obterParametros(){
    $.ajaxSetup({async: false});

    var parametros;

    $.ajax({
        method:"GET",
        dataType:"json",
        url : baseURL + "configuracao/1",
        success : function(data){
            parametros = {
                    exercicio : data.exercicio.toString(),
                    mesreferencia : data.mesreferencia.toString(),
                    ultimomes : data.ultimomes.toString(),
                    regime : data.regimedespesa,
                    conta : data.contadespesa,
                    arvorecompleta : "N",
            };
        },
        error : function(a,b,c){
            console.error(c);
        }
    });

    return parametros;
}

function gerarDados(){
    $.ajaxSetup({async: false});

    var parametros = obterParametros();
    var dados;

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: baseURL + "orcamentodespesapartition",
        dataType: "json",
        data: JSON.stringify(parametros),
        success: function(data, textStatus, jqXHR){
            //gerarGrafico('#grafico1', data);
            dados = data;
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert('Erro na geração dos dados: ' + errorThrown);
        }
    });

    return dados;
}