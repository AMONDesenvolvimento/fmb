$("head > title").html("FMB Transparência");

$(".nav-extended a.brand-logo").attr("href", "/fmb")
                               .append("<img src='/fmb/dependencias/Img/fmb.png' />");

$(".nav-extended .drawer-menu .userView .drawer-logo").attr("src", "/fmb/dependencias/Img/fmb.png");                       

$(".nav-extended .titulo-aplicacao").html("Portal de Transparência");    



var links = [
	{link: "http://portalfmb.org.br", texto: "Portal FMB"},
	{link: "https://portal.cfm.org.br", texto: "Portal CFM"},
	{link: "http://portalfmb.org.br/arquivo/", texto: "Arquivo"},
	{link: "http://portalfmb.org.br/fale-conosco/", texto: "Fale conosco"},
	{link: "http://portalfmb.org.br/institucional/", texto: "Institucional"}
];

$(".page-footer > .container img").attr("src", "/fmb/dependencias/Img/fmb.png");

$.each(links, function(i, v){
	$(".page-footer > .container ul").append("<li><a class='grey-text text-lighten-3' href='"+v.link+"'>"+v.texto+"</a></li>");
});

$(".page-footer > .footer-copyright > .container").html("Copyright © 2017 | Federação Médica Brasileira - Todos os direitos reservados");
