﻿$(function () {
    $("body").show("fade", "slow");

    $('.button-collapse').sideNav();

    $('select').material_select();

    $('.tooltipped').tooltip({ delay: 50 });

    $('.dropdown-button').dropdown();

    $('.buton-collapse').sideNav();

    $('.collapsible').collapsible();

    $('ul.tabs').tabs();

    $('.modal').modal({
        dismissible: true, //Pode ser fechado com um clique na parte externa
        opacity: .5
    });

    $('.datepicker').pickadate({
        selectMonths: true, // Mostra um combo contendo os meses do ano
        selectYears: 5 // Mostra um combo contendo um intervalo de 15 anos (7 para mais e 7 para menos)
    });

    $('.timepicker').pickatime({
        default: 'now',
        twelvehour: false, //formato 12 ou 24 horas
        donetext: 'OK',
        autoclose: true
    });

    $('.slider').slider({
        indicators: true,
        interval: 3000
    });

    $('.close-drawer-menu').click(function(){
        $('.button-collapse').sideNav('hide');
    });

    $(".tab a").click(function(){
        $(".tab-content").hide();
        var tabSelector = $(this).attr("href");
        $(tabSelector).show("slide", 400);
    });

    //Ajuste na margem esquerda do nome do sistema, dentro da navbar
    /*var browzers = ["IE", "Chrome", "Opera", "Edge"];
    var browser = identificarBrowser(); //Método implementado em dependencias/JS/utils.js
    browzers.forEach(function(value, index){
        if(browser == value && window.innerWidth > 990)
            $("nav .brand-logo").css("margin-left", "60px");
    });*/
});