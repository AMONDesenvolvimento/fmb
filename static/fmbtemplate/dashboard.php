<!DOCTYPE html>
<html>

<?php include("components/head.php"); ?>

<body>

    <?php include("components/navbar.php"); ?>


    <div class="row main-page">

        <?php include("components/menuLateral.php"); ?>
    

        <!-- Conteúdo principal -->
        <div class="col s12 m12 l10 page-content">

            <!-- Tabs -->
            <ul class="tabs green darken-2">
                <li class="tab"><a href="#tab-1" class="active">Gráfico 1</a></li>
                <li class="tab"><a href="#tab-2">Gráfico 2</a></li>
            </ul>

            <!-- Conteúdo Tab 1 -->
            <div id="tab-1" class="col s12 tab-content">
                <div class="hide-on-small-only" style="padding-top:3em"></div>
                <h4 class="center-align">Título</h4>
                <h5 class="center-align">Subtítulo</h5>

                <div id="grafico1"></div>
            </div>

            <!-- Conteúdo Tab 2 -->
            <div id="tab-2" class="col s12 tab-content">
                <div class="hide-on-small-only" style="padding-top:3em"></div>
                <h4 class="center-align">Título</h4>
                <h5 class="center-align">Subtítulo</h5>

                <div id="grafico2"></div>
            </div>

        </div>

    </div>


    <?php include("components/footer.php"); ?>


    <script src="dependencias/d3.v3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="template/estilo-grafico-dashboard.css" />
    <script src="template/exemplo-grafico-dashboard.js"></script>
    <script src="template/dados-dashboard.js"></script>
    <script type="text/javascript">
        var dados = gerarDadosDashboard();
        var dados2 = gerarDadosDashboard();

        gerarGrafico('#grafico1', dados);
        gerarGrafico('#grafico2', dados2);

        window.addEventListener("resize", Reload);

        function Reload(){
            $("#grafico1").empty();
            $("#grafico2").empty();
            gerarGrafico('#grafico1', dados);
            gerarGrafico('#grafico2', dados2);
        }
    </script>

    <style type="text/css">
        div[id^=grafico] {
            padding-bottom:50px;
        }
    </style>
    
</body>
</html>