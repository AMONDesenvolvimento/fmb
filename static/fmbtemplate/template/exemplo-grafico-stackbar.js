function gerarGrafico(containerHTML, dados) {

	$(containerHTML)
		.append("<svg></svg>");
	    
	var margin = {top: 60, right: 150, bottom: 60, left: 150};
	var deslocamentoLegendaLarge = 0,
	    deslocamentoLegendaSmall = 0;

	$(containerHTML+" > svg").attr("width", 
				function(){ 
					if(window.innerWidth >= 993){
						deslocamentoLegendaLarge = 60;
						return window.innerWidth * 0.81;
					}
					else {
						deslocamentoLegendaSmall = -60;
						margin.left = margin.right = window.innerWidth * 0.13;
						return window.innerWidth;
					}
				})
            .attr("height", 
        		function(){
        			if(window.innerHeight <= 250)
        				return window.innerHeight * 3;
        			if(window.innerHeight > 250 && window.innerHeight <= 400)
        				return window.innerHeight * 1.7;
        			else if(window.innerHeight > 400)
        				return window.innerHeight;
            	})
            .attr("text-anchor", "middle")
            .attr("font-size", window.innerWidth < 400 ? "10px" : "12px")
            .attr("font-family", "sans-serif");


	var svg = d3.select(containerHTML+" > svg");
	
	var width = +svg.attr("width") - margin.left - margin.right,
	    height = +svg.attr("height") - margin.top - margin.bottom;
	
	var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	var x = d3.scaleBand()
	    .rangeRound([0, width])
	    .paddingInner(0.05)
	    .align(0.1);

	var y = d3.scaleLinear()
	    .rangeRound([height, 0]);

	//Cores automáticas
    //var z = d3.scaleOrdinal(d3.schemeCategory10);

	var z = d3.scaleOrdinal()
	    .range(["#98abc5", "#8a89a6", "#705494"]);

	//Lê os atributos do objeto, guardando seus nomes em um array
	var columns = [];
	Object.keys(dados[0]).forEach(function(id) {
		if(id != "id" && id != "total")
			columns.push(id);
	});

	//Calcula o total por coluna
	var total;
	$.each(dados, function(i, v){
		total = 0;
		Object.keys(dados[i]).forEach(function(id) {
			if(id != "id" && id != "total") //Define a regra de quais coluna devem ser excluídas do cálculo, no caso, id porque não tem valor numérico
				total += v[id];
		});

		dados[i].total = total;
	});
	dados.columns = columns;


	var data = dados;
    var keys = data.columns;

    x.domain(data.map(function(d) { return d.id; }));
    //Define o valor máximo do eixo Y
    y.domain([0, d3.max(data, function(d) { return d.total; })]).nice();
    z.domain(keys);

    g.append("g")
	    .selectAll("g")
	    .data(d3.stack().keys(keys)(data))
	    .enter().append("g")
	      .attr("fill", function(d) { return z(d.key); })
	    .selectAll("rect")
	    .data(function(d) { return d; })
	    .enter().append("rect")
	      .attr("x", function(d) { return x(d.data.id); })
	      .attr("y", function(d) { return y(d[1]); })
	      .attr("height", function(d) { return y(d[0]) - y(d[1]); })
	      .attr("width", x.bandwidth());

    g.append("g")
	      .attr("class", "axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(d3.axisBottom(x));

    g.append("g")
	      .attr("class", "axis")
	      .call(d3.axisLeft(y))
	    .append("text")
	      .attr("x", - 25)
	      .attr("y", y(y.ticks().pop()) - 30)
	      .attr("dy", "0.32em")
	      .attr("fill", "#000")
	      .attr("font-weight", "bold")
	      .attr("text-anchor", "start")
	      .attr("font-size", "1.6em")
	      .attr("font-family", "Roboto")
	      .text("Eixo Y");

    var legend = g.append("g")
	      .attr("text-anchor", "end")
	      .attr("transform", "translate("+deslocamentoLegendaLarge+"," + deslocamentoLegendaSmall + ")")
	    .selectAll("g")
	    .data(keys)
	    .enter()
	      .append("g")
	      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

    legend.append("rect")
	      .attr("x", width)
	      .attr("width", 19)
	      .attr("height", 19)
	      .attr("fill", z);

    legend.append("text")
	      .attr("x", width - 5)
	      .attr("y", 9.5)
	      .attr("font-size", "1.1em")
	      .attr("font-family", "Roboto")
	      .attr("dy", "0.32em")
	      .text(function(d) { return d; });
}