function gerarDadosBolhas(){
	
	var dados = [
          { id : "despesa.diretoria.Diárias", value : 3254},
          { id : "despesa.diretoria.Passagens", value : 545.4},
          { id : "despesa.diretoria.Hospesagem", value : 353.45},
          { id : "despesa.assessoria.Tróia", value : 3434.5},
          { id : "despesa.assessoria.Jurídica", value : 556.54},
          { id : "despesa.provisionamento.Item1", value : 44.54},
          { id : "despesa.provisionamento.Item2", value : 333.54}
        ];

	return dados;
}



function processarJSON(){
    var configuracaoRootURL = "/configuracao";
    $.ajax({
	type: "GET",
	dataType: "json",
	url: configuracaoRootURL+"/1",
	success: function(data){
	    localStorage.setItem("exercicio",data.exercicio.toString());
	    localStorage.setItem("mesreferencia",data.mesreferencia);
	    localStorage.setItem("ultimomes",data.ultimomes.toString());
	    localStorage.setItem("regimedespesa",data.regimedespesa);
	    localStorage.setItem("contadespesa",data.contadespesa);
	    localStorage.setItem("todassubcontasdespesa",data.todassubcontasdespesa);
	    localStorage.setItem("regimereceita",data.regimereceita);
	    localStorage.setItem("contareceita",data.contareceita);
	    localStorage.setItem("todassubcontasreceita",data.todassubcontasreceita);
	    localStorage.setItem("regimeorcamento",data.regimeorcamento);
	    localStorage.setItem("contaorcamento",data.contaorcamento);
	    localStorage.setItem("todassubcontasorcamento",data.todassubcontasorcamento);
	}
    });

    function parmsReceitaToJSON() {
	return JSON.stringify({
	    "exercicio": localStorage.getItem("exercicio"),
	    "mesreferencia": localStorage.getItem("mesreferencia"),
	    "ultimomes": localStorage.getItem("ultimomes"),
	    "conta": localStorage.getItem("contareceita"),
	    "regime":localStorage.getItem("regimereceita"),
	    "arvorecompleta": localStorage.getItem("todassubcontasreceita")
    	});
    }

    var dadosProcessados = [];
    $.post("/orcamentoreceitabubble", parmsReceitaToJSON())
       .done(function(result) {
	    result = $.parseJSON(result);

            $.each(result.children, function(i, v){
		if(v.children === undefined)
			dadosProcessados.push({id: result.name + "." + v.name, value: v.size});
		else {
			$.each(v.children, function(a, b){
				dadosProcessados.push({id: result.name + "." + v.name + "." + b.name, value: b.size});
			});
		}
	    });
            return dadosProcessados;
       });
}






