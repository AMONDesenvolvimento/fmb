function gerarDadosStackbar() {
	var dados = [
		{id: "Jan", "Grupo 1": 234, "Grupo 2": 235, "Grupo 3": 123},
		{id: "Fev", "Grupo 1": 444, "Grupo 2": 444, "Grupo 3": 123},
		{id: "Mar", "Grupo 1": 368, "Grupo 2": 335, "Grupo 3": 123},
		{id: "Abr", "Grupo 1": 200, "Grupo 2": 160, "Grupo 3": 123},
		{id: "Mai", "Grupo 1": 322, "Grupo 2": 333, "Grupo 3": 123},
		{id: "Jun", "Grupo 1": 433, "Grupo 2": 334, "Grupo 3": 123},
		{id: "Jul", "Grupo 1": 112, "Grupo 2": 55, "Grupo 3": 123},
		{id: "Ago", "Grupo 1": 43, "Grupo 2": 325, "Grupo 3": 123},
		{id: "Set", "Grupo 1": 543, "Grupo 2": 22, "Grupo 3": 123},
		{id: "Out", "Grupo 1": 222, "Grupo 2": 343, "Grupo 3": 123},
		{id: "Nov", "Grupo 1": 334, "Grupo 2": 160, "Grupo 3": 123},
		{id: "Dez", "Grupo 1": 350, "Grupo 2": 330, "Grupo 3": 353}
	];

	return dados;
}