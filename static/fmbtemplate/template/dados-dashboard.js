function ran(){
	//Gera um valor de 1 a 9999
	return Math.floor((Math.random() * 9999) + 1);
}

function gerarDadosDashboard(){
	var meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

	var dados = [];

	meses.forEach(function(i){
		dados.push({State: i.substring(0,3), freq: {
			assessorias: ran(), 
			custeioInstalacoes: ran(), 
			despesasDiretoria: ran(), 
			folhaPagamento: ran(), 
			provisionamento: ran()
		}});
	});

    return dados;
}
