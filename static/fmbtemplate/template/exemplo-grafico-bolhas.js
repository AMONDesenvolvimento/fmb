function gerarGrafico(dados, containerHTML){
    showLoadText(containerHTML);

    $(containerHTML)
        .css("padding", (window.innerWidth * 0.03) + "px")
        .append("<svg></svg>");

    var medidaLado = window.innerWidth * ((window.innerWidth >= 993) ? 0.6 : 0.94);
    $(containerHTML+" > svg")
            .attr("width", medidaLado)
            .attr("height", medidaLado)
            .attr("font-size", window.innerWidth >= 993 ? "12px" : "10px")
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
	    .css("margin-left", function(){
		if(window.innerWidth >= 993)
			return (window.innerWidth * 0.1)+"px";
	    });

    var svg = d3.select(containerHTML+" > svg"),
        width = +svg.attr("width"),
        height = +svg.attr("height");

    var format = d3.format(",d");

    //SOBRESCREVER A FUNÇÃO DE CORES USANDO SWITCH-CASE E CORES PERSONALIZADAS
    var color = d3.scaleOrdinal(d3.schemeCategory10);

    var pack = d3.pack()
        .size([width, height])
        .padding(1.5);

    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("color", "white")
        .style("padding", "8px")
        .style("background-color", "rgba(0, 0, 0, 0.75)")
        .style("border-radius", "6px")
        .style("font", "12px sans-serif");

    var root = d3.hierarchy({children: dados})
        .sum(function(d) { return d.value; })
        .each(function(d) {
          if (id = d.data.id) {
            var id, i = id.lastIndexOf(".");
	    d.id = id;
	    if(id.split('.').length === 2)
		d.package = id;
            else
            	d.package = id.slice(0, i);

            d.class = id.slice(i + 1);
          }
        });

    var node = svg.selectAll(".node")
      .data(pack(root).leaves())
      .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { 
            return "translate(" + d.x + "," + d.y + ")"; 
        });

    node.append("circle")
        .attr("id", function(d) { return d.id; })
        .attr("r", function(d) { return d.r; })
        .style("fill", function(d) { return color(d.package); })
        .on("mouseover", function(d) {
              tooltip.text("R$ " + formatNumber(d.value,2));
              tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() { return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
        .on("mouseout", function(){ return tooltip.style("visibility", "hidden"); });

    node.append("text")
      .selectAll("tspan")
      .data(function(d) { return d.class.split(/(?=[A-Z][^A-Z])/g); })
      .enter().append("tspan")
        .attr("x", 0)
        .attr("y", function(d, i, nodes) { return 13 + (i - nodes.length / 2 - 0.5) * 10; })
        .text(function(d) { return d; });

     hideLoadText(containerHTML);
}


function formatNumber(valor, quantCasasDecimais) {
    var r = valor.toFixed(quantCasasDecimais).replace(/./g, function(c,i,a) {
        return i>0 && c !== "." && ((a.length-i)%3 ===0)?','+c:c;
    })
    r=r.replace(".","#");
    r=r.replace(",",".");
    r=r.replace("#",",");
    return r;
}

function showLoadText(containerHTML){
	$(containerHTML).append("<div id='load-text'>Carregando gráfico...</div>");
}

function hideLoadText(containerHTML){
	$(containerHTML+" > #load-text").remove();
}

