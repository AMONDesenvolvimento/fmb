function gerarGrafico(containerHTML, dados){

    var data = dados;

    m = data.length;
    n = data[0].length;

    var margin = {top: 20, right: 30, bottom: 50, left: 40},
        height = window.innerHeight - margin.top - margin.bottom - 30,
        width = window.innerWidth * 0.69;
    margin.left = window.innerWidth * 0.08;

    //Definição responsiva da largura do gráfico
    if(window.innerWidth >= 1200){
        width = window.innerWidth * 0.65;
        margin.left = ((window.innerWidth - width) / 2) * 0.65;
    }
    else if(window.innerWidth > 992 && window.innerWidth < 1200) {
        //mantém as dimensões padrão
    }
    else if(window.innerWidth <= 992 && window.innerWidth > 500) 
        width = window.innerWidth * 0.89;
    else if (window.innerWidth <= 500) {
        width = window.innerWidth * 0.83;
        margin.left = window.innerWidth * 0.12;
    }

    //Detecta o valor máximo dentre os dados
    var maxs = [];
    dados.forEach(function(v){
        maxs.push(d3.max(v, function(m){ return m; }));
    });
    var max = d3.max(maxs, function(m){ return m; });

    var y = d3.scale.linear()
        .domain([0, max])
        .range([height, 0]);

    var x0 = d3.scale.ordinal()
        .domain(d3.range(n))
        .rangeBands([0, width], .2);

    var x1 = d3.scale.ordinal()
        .domain(d3.range(m))
        .rangeBands([0, x0.rangeBand()]);

    //var setColor = d3.scale.category20();
    function setColor(id){
        switch(id) {
            case 0: return "#64b5f6";
            case 1: return "#1e88e5";
            default: return "#000";
        }
    }

    var xAxis = d3.svg.axis()
        .scale(x0)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var svg = d3.select(containerHTML).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("svg:g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g").selectAll("g")
        .data(data)
      .enter().append("g")
        .style("fill", function(d, i) { return setColor(i); })
        .attr("transform", function(d, i) { return "translate(" + x1(i) + ",0)"; })
      .selectAll("rect")
        .data(function(d) { return d; })
      .enter().append("rect")
        .attr("width", x1.rangeBand())
        .attr("height", y)
        .attr("x", function(d, i) { return x0(i); })
        .attr("y", function(d) { return height - y(d); 
     });
}
