﻿<!DOCTYPE html>
<html>

<?php include("components/head.php"); ?>

<body>

    <?php include("components/navbar.php"); ?>


    <div class="row main-page">


        <?php include("components/menuLateral.php"); ?>
    

        <!-- Conteúdo principal -->
        <div class="col s12 m12 l10 page-content">
            <div style="padding-top: 5em"></div>
            <h4 class="center-align">Bem-vindo ao Portal de Transparência</h4>
            <h5 class="center-align">Federação Médica Brasileira</h5>
        </div>

    </div>


    <?php include("components/footer.php"); ?>

</body>
</html>