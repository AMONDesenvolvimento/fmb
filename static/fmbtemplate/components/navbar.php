<!-- Navbar e Drawer Menu -->
<nav class="nav-extended green">
    <div class="nav-wrapper">
      <a href="/material-template-2" class="brand-logo">
          <img src="dependencias/Img/fmb.png" />
      </a>
      <span class="titulo-aplicacao hide-on-med-and-down right">Portal de Transparência</span>
      <a href="#" data-activates="drawer-menu" class="button-collapse show-on-small"><i class="material-icons">menu</i></a>

      <!-- Drawer menu -->
      <ul class="side-nav" id="drawer-menu">
        <li>
            <div class="userView">
                <img src="dependencias/Img/fmb.png" class="drawer-logo" />
            </div>
        </li>

        <!-- Opções Drawer menu -->
        <li><a href="home.php">Home</a></li>
        
        <li>
            <ul class="collapsible collapsible-accordion">
              <li>
                <a class="collapsible-header">Gráficos<i class="material-icons right">arrow_drop_down</i></a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
                    <li><a href="bolhas.php">Bolhas</a></li>
                    <li><a href="arvore.php">Árvore</a></li>
                    <li><a href="stackbar.php">Barras empilhadas</a></li>
                    <li><a href="disco.php">Disco</a></li>
                  </ul>
                </div>
              </li>

              <li>
                <a class="collapsible-header">Exemplo<i class="material-icons right">arrow_drop_down</i></a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="#!">Exemplo</a></li>
                    <li><a href="#!">Exemplo</a></li>
                  </ul>
                </div>
              </li>

              <li>
                <a class="collapsible-header">Formulários<i class="material-icons right">arrow_drop_down</i></a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="lancamento.php">Lançamento</a></li>
                  </ul>
                </div>
              </li>
            </ul>
        </li>

        <li class="divider"></li>
        <li><a href="#" class="close-drawer-menu">Fechar</a></li>
      </ul>
    </div>
</nav>