<!-- Menu lateral fixo -->
<div class="col l2 hide-on-med-and-down collection lateral-menu">
    <a href="home.php" class="collection-item"><i class="fa fa-user"></i>Home</a>

    <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header"><i class="fa fa-bar-chart"></i>Gráficos <i class="material-icons right">arrow_drop_down</i></div>
          <div class="collapsible-body">
            <div class="collection">
                <a href="dashboard.php" class="collection-item"><i class="fa fa-pie-chart"></i>Dashboard</a>
                <a href="bolhas.php" class="collection-item"><i class="fa fa-bullseye"></i>Bolhas</a>
                <a href="arvore.php" class="collection-item"><i class="fa fa-tree"></i>Árvore</a>
                <a href="stackbar.php" class="collection-item"><i class="fa fa-bar-chart"></i>Barras empilhadas</a>
                <a href="disco.php" class="collection-item"><i class="fa fa-bullseye"></i>Disco</a>
            </div>
          </div>
        </li>
        <li>
          <div class="collapsible-header"><i class="fa fa-circle"></i>Exemplo <i class="material-icons right">arrow_drop_down</i></div>
          <div class="collapsible-body">
            <div class="collection">
                <a href="#" class="collection-item"><i class="fa fa-circle"></i>Exemplo</a>
                <a href="#" class="collection-item"><i class="fa fa-circle"></i>Exemplo</a>
            </div>
          </div>
        </li>

        <li>
          <div class="collapsible-header"><i class="fa fa-file-text-o"></i>Formulários<i class="material-icons right">arrow_drop_down</i></div>
          <div class="collapsible-body">
            <div class="collection">
                <a href="lancamento.php" class="collection-item"><i class="fa fa-arrow-circle-o-up"></i>Lançamento</a>
            </div>
          </div>
        </li>

    </ul>

    <div class="offset-lateral-menu"></div>
</div>