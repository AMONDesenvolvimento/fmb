<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <title>Template Layout</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link href="dependencias/Materialize/css/materialize.css" type="text/css" rel="stylesheet" />
    <link href="dependencias/FontAwesome-4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
    <link href="dependencias/Materialize/ClockPicker/materialize.clockpicker.css" type="text/css" rel="stylesheet" />
    <link href="dependencias/Materialize/fixes.css" type="text/css" rel="stylesheet" />

    <script src="dependencias/JQuery/jquery-3.1.0.min.js"></script>
    <script src="dependencias/JQuery/jquery-ui.min.js"></script>
    <script src="dependencias/Materialize/js/materialize.js"></script>
    <script src="dependencias/Materialize/ClockPicker/materialize.clockpicker.js"></script>
    <script src="dependencias/js/utils.js"></script>
    <script src="dependencias/Materialize/init.js"></script>
</head>