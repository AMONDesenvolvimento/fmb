<div class="row">
    <div class="col s12 m10 offset-m1">
      <div class="card">

        <!-- Formulário principal -->
        <form action="#" method="post" class="form-responsive-button">

            <div class="card-content">
                <span class="card-title">Formulário</span>

                <div class="row">
                    <div class="col l4 m6 s12">
                        <div class="input-field">
                            <input type="text" id="nome" />
                            <label for="nome">Input text</label>
                        </div>
                    </div>

                    <div class="col l4 m6 s12">
                        <div class="input-field">
                            <input type="text" id="sobrenome" />
                            <label for="sobrenome">Input text 2</label>
                        </div>
                    </div>

                    <div class="col l4 m12">
                        <a href="#form-modal" class="btn waves-effect waves-light right">Form modal rodapé
                            <i class="material-icons left">note_add</i>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col m6 s12">
                        <div class="input-field">
                            <textarea id="descricao" cols="30" rows="10" class="materialize-textarea"></textarea>
                            <label for="descricao">Descrição</label>
                        </div>
                    </div>

                    <div class="col m6 s12">
                        <blockquote class="warning">
                            Tamanho máximo: 2Mb
                        </blockquote>
                        <div class="input-field file-field">
                            <div class="btn">
                                <span>Anexar</span>
                                <input type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Selecione o anexo">
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col l3 m5 s12">
                        <select>
                            <option value="">Selecione</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>

                    <div class="col l8 m7 s12">
                        <div class="col l6 m6 s12">
                            <input type="date" class="datepicker" id="datepicker" placeholder="Data">
                        </div>
                        <div class="col l6 m6 s12">
                            <input id="timepicker" class="timepicker" type="time" placeholder="Hora">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col l4 s12">
                        <div class="col s12" style="padding-bottom:18px; padding-top:20px">
                            <label class="label-field">Radio button</label>
                        </div>
                        <div class="col s6 m12">
                            <input name="group1" type="radio" id="radio1" checked="checked" />
                            <label for="radio1">Opção 1</label>
                        </div>
                        <div class="col s6 m12">
                            <input name="group1" type="radio" id="radio2" />
                            <label for="radio2">Opção 2</label>
                        </div>
                        <div class="col s6 m12">
                            <input name="group1" type="radio" id="radio3" />
                            <label for="radio3">Opção 3</label>
                        </div>
                        <div class="col s6 m12">
                            <input name="group1" type="radio" id="radio4" />
                            <label for="radio4">Opção 4</label>
                        </div>
                    </div>

                    <div class="col l3 s12">
                        <div class="col s12" style="padding-bottom:18px; padding-top:20px">
                            <label class="label-field">Checkboxes</label>
                        </div>
                        <div class="col s6 m12">
                            <input type="checkbox" class="filled-in" id="check-estudo" />
                            <label for="check-estudo">Estudo</label>
                        </div>
                        <div class="col s6 m12">
                            <input type="checkbox" class="filled-in" id="check-trabalho" checked="checked" />
                            <label for="check-trabalho">Trabalho</label>
                        </div>
                        <div class="col s6 m12">
                            <input type="checkbox" class="filled-in" id="check-nenhum" />
                            <label for="check-nenhum">Nenhum</label>
                        </div>
                    </div>

                    <div class="col l5 s12">
                        <div class="switch col s12" style="padding-top:20px">
                            <label class="label-field">Switch buttons</label>
                            <p>
                                <label class="switch-label">Receber notificações</label>
                                <label>
                                    <input type="checkbox">
                                    <span class="lever"></span>
                                </label>
                            </p>
                            <p>
                                <label class="switch-label">Ficar visível</label>
                                <label>
                                    <input type="checkbox">
                                    <span class="lever"></span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                      <a class="btn waves-effect waves-light right">Ação
                        <i class="material-icons left">room</i>
                      </a>
                    </div>
                </div>

            </div>

            <div class="card-action">
              <div class="row">
                  
                  <div class="col s12 m3 offset-m6 hide-on-small-only">
                    <button class="btn blue-grey lighten-5 black-text waves-effect waves-light right" type="reset">
                      <span class="button-text">Limpar</span>
                    </button>
                  </div>

                  <div class="col s12 m3">
                    <button class="btn waves-effect waves-light right col s12" type="submit">
                      <span class="button-text">Enviar</span>
                      <i class="material-icons right">send</i>
                    </button>
                  </div>

              </div>
            </div>

        </form>



        <!-- Formulário modal -->
        <div id="form-modal" class="modal bottom-sheet" style="height:80%">
            <div class="modal-content">

              <form>
                <h4>Formulário Modal</h4>
              
                <div class="row">
                    <div class="col s12">
                        <h5>Input text</h5>
                    </div>

                    <div class="col l3 m6 s12">
                        <div class="input-field">
                            <input type="text" id="nome" />
                            <label for="nome">Nome</label>
                        </div>
                    </div>

                    <div class="col l3 m6 s12">
                        <div class="input-field">
                            <input type="text" id="sobrenome" />
                            <label for="sobrenome">Sobrenome</label>
                        </div>
                    </div>
                </div>
              </form>

            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col s12">
                      <button class="btn waves-effect waves-light right col s12 m3 l2" type="submit" name="action">Concluir
                        <i class="material-icons right">send</i>
                      </button>
                    </div>
                </div>

                <a href="#!" class="modal-action modal-close waves-effect btn-flat">Fechar</a>
            </div>

        </div>

      </div>
    </div>
</div>





  <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
  </div>

