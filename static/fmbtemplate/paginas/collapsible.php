<div class="row">
    <div class="col s12 l10 offset-l1">

		<ul class="collapsible popout" data-collapsible="accordion">
			<li>
		      <div class="collapsible-header">Grupo 1 <span class="badge">12</span></div>
		      <div class="collapsible-body">
		      	<div class="row">
			      	<div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-1-check-1" />
	                    <label for="grupo-1-check-1">Item 1</label>
	                </div>

			      	<div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-1-check-2" />
	                    <label for="grupo-1-check-2">Item 2</label>
	                </div>

	                <div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-1-check-3" />
	                    <label for="grupo-1-check-3">Item 3</label>
	                </div>
                </div>
		      </div>
		    </li>
		    <li>
		      <div class="collapsible-header">Grupo 2 <span class="badge">21</span></div>
		      <div class="collapsible-body">
		      	<div class="row">
			      	<div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-2-check-1" />
	                    <label for="grupo-2-check-1">Item 1</label>
	                </div>

			      	<div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-2-check-2" />
	                    <label for="grupo-2-check-2">Item 2</label>
	                </div>

	                <div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-2-check-3" />
	                    <label for="grupo-2-check-3">Item 3</label>
	                </div>
                </div>
		      </div>
		    </li>
		    <li>
		      <div class="collapsible-header">Grupo 3 <span class="badge">15</span></div>
		      <div class="collapsible-body">
		      	<div class="row">
			      	<div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-3-check-1" />
	                    <label for="grupo-3-check-1">Item 1</label>
	                </div>

			      	<div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-3-check-2" />
	                    <label for="grupo-3-check-2">Item 2</label>
	                </div>

	                <div class="col s12 m6 l4">
	                    <input type="checkbox" class="filled-in" id="grupo-3-check-3" />
	                    <label for="grupo-3-check-3">Item 3</label>
	                </div>
                </div>
		      </div>
		    </li>
		</ul>

	</div>
</div>