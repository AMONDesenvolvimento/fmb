<div class="row">
	<div class="col s12 m5 l5 offset-l1">
		<ul class="collection with-header z-depth-1 check-list" id="lista-disponiveis">
		  <li class="collection-header">Items disponíveis</li>

	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="disponivel1"/>
		      <label for="disponivel1">Item 1</label>
	      </li>
	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="disponivel2"/>
		      <label for="disponivel2">Item 2</label>
	      </li>
	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="disponivel3"/>
		      <label for="disponivel3">Item 3</label>
	      </li>
	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="disponivel4"/>
		      <label for="disponivel4">Item 4</label>
	      </li>
	    </ul>
	</div>

	<div class="col s12 m2 l1 center-align">
		<div class="col s12">
			<a class="btn-floating waves-effect red alternate-button"><i class="fa fa-arrows-h"></i></a>
		</div>
	</div>

	<div class="col s12 m5 l4">
		<ul class="collection with-header z-depth-1 check-list" id="lista-incluidos">
		  <li class="collection-header">Items incluídos</li>

	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="incluido1"/>
		      <label for="incluido1">Item 1</label>
	      </li>
	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="incluido2"/>
		      <label for="incluido2">Item 2</label>
	      </li>
	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="incluido3"/>
		      <label for="incluido3">Item 3</label>
	      </li>
	      <li class="collection-item">
		      <input type="checkbox" class="filled-in" id="incluido4"/>
		      <label for="incluido4">Item 4</label>
	      </li>
	    </ul>
	</div>

</div>


<script type="text/javascript">
	// Ao selecionar algum item, desmarca todos os itens da outra lista
	$('#lista-incluidos input[type=checkbox]').click(function(){
		$('#lista-disponiveis input[type=checkbox]').prop('checked', false);
	});
	$('#lista-disponiveis input[type=checkbox]').click(function(){
		$('#lista-incluidos input[type=checkbox]').prop('checked', false);
	});
</script>