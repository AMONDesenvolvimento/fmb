<div class="row">
    <div class="col s12 m5 offset-m1">
      <div class="card">
        <div class="card-content">
          <span class="card-title">Tooltip</span>

          <a class="btn tooltipped" data-position="right" data-delay="50" data-tooltip="Isto é um botão"><i class="fa fa-question"></i></a>

          <div class="card tooltipped pink darken-2 white-text" data-position="left" data-delay="50" data-tooltip="Isto é um CARD">
          		<div class="card-content">
	              <span class="card-title">Card Title</span>
	              <p>Conteúdo ...</p>
	            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col s12 m5">
      <div class="card">
        <div class="card-content">
          <span class="card-title">Badge</span>

          	<div class="collection">
			    <a href="#!" class="collection-item">Item 1 <span class="new badge" data-badge-caption="itens">4</span></a>
			    <a href="#!" class="collection-item">Item 2 <span class="new badge red" data-badge-caption="itens">50</span></a>
			</div>

			<ul class="collection">
		      <li class="collection-item">Item 1 <span class="new badge orange" data-badge-caption="itens">20</span></li>
		      <li class="collection-item">Item 2</li>
		    </ul>
          
        </div>
      </div>
    </div>
</div>