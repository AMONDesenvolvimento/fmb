<div class="row">

        <div class="col l2 s12" style="padding: 2rem 0 0 2rem">
            <p><a class="btn waves-effect waves-light" href="#modal-default">Modal padrão</a></p>
            <p><a class="btn waves-effect waves-light modal-trigger" href="#modal-fixed-footer">Modal com rodapé fixo</a></p>
            <p><a class="btn waves-effect waves-light" onclick="exibirModal()">Modal slide rodapé</a></p>
            <p><a class="btn waves-effect waves-light" onclick="exibirToast('personalizada')">Toast!</a></p>
        </div>

          <div id="modal-default" class="modal">
            <div class="modal-content">
              <span class="modal-title">Título</span>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="modal-footer">
              <a href="#!" class="modal-action modal-close waves-effect btn-flat">Fechar</a>
            </div>
          </div>


          <div id="modal-fixed-footer" class="modal modal-fixed-footer">
            <div class="modal-content">
                <span class="modal-title">Título</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                <div class="row">
                    <div class="col l5 m6 s12">
                        <div class="input-field">
                            <input type="text" id="nome" />
                            <label for="nome">Input text</label>
                        </div>
                    </div>
                    <div class="col l3 m3 s12">
                        <button class="btn waves-effect waves-light right col s12" type="submit">
                          <span class="button-text">OK</span>
                          <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>

            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect btn-flat">Fechar</a>
            </div>
          </div>


          <div id="modal-bottom-sheet" class="modal bottom-sheet" style="height:78%">
            <div class="modal-content">

              <div class="row">
                  <div class="col l11 m11 s9">
                    <span class="modal-title">Título</span>
                  </div>
                  <div class="col l1 m1 s3">
                    <a href="#!" class="modal-action modal-close waves-effect btn-flat modal-bottom-sheet-close-icon">x</a>
                  </div>
              </div>
              
              <div class="row">
                  <div class="col l4 m5 s12">
                      <ul class="collection with-header z-depth-1" id="lista">
                        <li class="collection-header">Lista</li>
                        <li class="collection-item">Item</li>
                        <li class="collection-item">Item</li>
                        <li class="collection-item">Item</li>
                        <li class="collection-item">Item</li>
                      </ul>
                  </div>
                  <div class="col l4 m5 s12">
                      <div class="card">
                          <div class="card-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                          </div>
                      </div>
                  </div>
              </div>
            </div>

          </div>

</div>

<script type="text/javascript">
  function exibirModal(){
      $('#modal-bottom-sheet').modal('open');
      Materialize.showStaggeredList('#lista');
  }

  function exibirToast(msg){
      Materialize.toast('Mensagem ' + msg, 4000);
      Materialize.toast('Mensagem ' + msg, 4000, 'toast-warning');
      Materialize.toast('Mensagem ' + msg, 4000, 'toast-error');
      Materialize.toast('Mensagem ' + msg, 4000, 'toast-info');
      Materialize.toast('Mensagem ' + msg, 4000, 'toast-success');
  }
</script>