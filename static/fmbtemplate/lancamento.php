<!DOCTYPE html>
<html>

<?php include("components/head.php"); ?>

<body>

    <?php include("components/navbar.php"); ?>


    <div class="row main-page">

        <?php include("components/menuLateral.php"); ?>


        <!-- Conteúdo principal -->
        <div class="col s12 m12 l10 page-content">

            <!-- Tabs -->
            <ul class="tabs green darken-2">
                <li class="tab"><a href="#tab-1">Inclusão</a></li>
                <li class="tab"><a href="#tab-2">Relatório</a></li>
            </ul>

            <!-- Conteúdo Tab INCLUSÃO -->
            <div id="tab-1" class="col s12 tab-content">
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
    
                                    <!-- FORMULÁRIO DE INCLUSÃO -->
                                    <div id="inclusao" class="col s12">

                                        <form action="#" methos="post" id="form-inclusao">
                                              
                                            <div class="card-title">Inclusão de Lançamento</div>

                                            <div class="row">
                                                <div class="col s12 m3 l2">
                                                    <div class="input-field">
                                                        <input type="text" id="id-inclusao" />
                                                        <label for="id-inclusao">Identificação</label>
                                                    </div>
                                                </div>
                          
                                                <div class="col s12 m6 l4">
                                                    <div class="input-field">
                                                        <input type="text" id="evento-inclusao" readonly />
                                                        <label for="evento-inclusao">Evento</label>
                                                    </div>
                                                </div>

                                                <div class="col s12 m6 l3">
                                                    <div class="input-field">
                                                        <input type="text" id="valor-inclusao" />
                                                        <label for="valor-inclusao">Valor</label>
                                                    </div>
                                                </div>

                                                <div class="col s12 m3 l3">
                                                    <div class="input-field">
                                                        <input type="date" class="datepicker" id="data-inclusao">
                                                        <label for="data-inclusao">Data</label>
                                                    </div>
                                                </div>

                                                <div class="col s12 m6 l2 form-select-wraper">
                                                    <label for="exercicio-inclusao">Exercício</label>
                                                    <select id="exercicio-inclusao">
                                                        <option value=""></option>
                                                        <option value="2017">2017</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2014">2014</option>
                                                    </select>
                                                </div>
                            
                                                <div class="col s12 m6 l2 form-select-wraper">
                                                    <label for="mes-inclusao">Mês referência</label>
                                                    <select id="mes-inclusao">
                                                        <option value=""></option>
                                                        <option value="1">Janeiro</option>
                                                        <option value="2">Fevereiro</option>
                                                        <option value="3">Março</option>
                                                        <option value="4">Abril</option>
                                                        <option value="5">Maio</option>
                                                        <option value="6">Junho</option>
                                                        <option value="7">Julho</option>
                                                        <option value="8">Agosto</option>
                                                        <option value="9">Setembro</option>
                                                        <option value="10">Outubro</option>
                                                        <option value="11">Novembro</option>
                                                        <option value="12">Dezembro</option>
                                                    </select>
                                                </div>

                                                <div class="col s12 m6 l3 form-select-wraper">
                                                    <label for="tipo-inclusao">Tipo</label>
                                                    <select id="tipo-inclusao">
                                                        <option value=""></option>
                                                        <option value="2017">Receita</option>
                                                        <option value="2017">Despesa</option>
                                                        <option value="2017">Receita fixa</option>
                                                        <option value="2016">Receita variável</option>
                                                        <option value="2015">Despesa fixa</option>
                                                        <option value="2014">Despesa variável</option>
                                                    </select>
                                                </div>

                                                <div class="col s12 m6 l5">
                                                    <div class="input-field">
                                                        <input type="text" id="historico-inclusao" />
                                                        <label for="historico-inclusao">Histórico</label>
                                                    </div>
                                                </div>
                                                
                                          
                                                <div class="col s12 right">

                                                  <button type="submit" class="btn waves-effect waves-light light-green darken-3 right col s12 m3 l2">
                                                      <span class="button-text">Incluir</span>
                                                  </button>

                                                  <button type="reset" class="btn waves-effect waves-light grey lighten-4 right col s12 m2 l2 hide-on-small-only">
                                                      <span class="button-text black-text">Limpar</span>
                                                  </button>

                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- FIM  =  FORMULÁRIO DE INCLUSÃO -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Conteúdo Tab RELATÓRIO -->
            <div id="tab-2" class="col s12 tab-content">
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">

                                    <!-- FORMULÁRIO DE RELATÓRIO -->
                                    <div class="col s12">
                                        
                                        <form action="#" methos="post" id="form-relatorio">
                                              
                                            <div class="card-title">Relatórios de Lançamentos</div>

                                            <div class="row">
                                                <div class="col s12 m3 l2">
                                                    <div class="input-field">
                                                        <input type="text" id="id-relatorio" />
                                                        <label for="id-relatorio">Identificação</label>
                                                    </div>
                                                </div>
                          
                                                <div class="col s12 m6 l4">
                                                    <div class="input-field">
                                                        <input type="text" id="evento-relatorio" readonly />
                                                        <label for="evento-relatorio">Evento</label>
                                                    </div>
                                                </div>

                                                <div class="col s12 m6 l3">
                                                    <div class="input-field">
                                                        <input type="text" id="valor-relatorio" />
                                                        <label for="valor-relatorio">Valor</label>
                                                    </div>
                                                </div>

                                                <div class="col s12 m3 l3">
                                                    <div class="input-field">
                                                        <input type="date" class="datepicker" id="data-relatorio">
                                                        <label for="data-relatorio">Data</label>
                                                    </div>
                                                </div>

                                                <div class="col s12 m6 l2 form-select-wraper">
                                                    <label for="exercicio-relatorio">Exercício</label>
                                                    <select id="exercicio-relatorio">
                                                        <option value=""></option>
                                                        <option value="2017">2017</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2014">2014</option>
                                                    </select>
                                                </div>
                            
                                                <div class="col s12 m6 l2 form-select-wraper">
                                                    <label for="mes-relatorio">Mês referência</label>
                                                    <select id="mes-relatorio">
                                                        <option value=""></option>
                                                        <option value="1">Janeiro</option>
                                                        <option value="2">Fevereiro</option>
                                                        <option value="3">Março</option>
                                                        <option value="4">Abril</option>
                                                        <option value="5">Maio</option>
                                                        <option value="6">Junho</option>
                                                        <option value="7">Julho</option>
                                                        <option value="8">Agosto</option>
                                                        <option value="9">Setembro</option>
                                                        <option value="10">Outubro</option>
                                                        <option value="11">Novembro</option>
                                                        <option value="12">Dezembro</option>
                                                    </select>
                                                </div>

                                                <div class="col s12 m6 l3 form-select-wraper">
                                                    <label for="tipo-relatorio">Tipo</label>
                                                    <select id="tipo-relatorio">
                                                        <option value=""></option>
                                                        <option value="2017">Receita</option>
                                                        <option value="2017">Despesa</option>
                                                        <option value="2017">Receita fixa</option>
                                                        <option value="2016">Receita variável</option>
                                                        <option value="2015">Despesa fixa</option>
                                                        <option value="2014">Despesa variável</option>
                                                    </select>
                                                </div>

                                                <div class="col s12 m6 l5">
                                                    <div class="input-field">
                                                        <input type="text" id="historico-relatorio" />
                                                        <label for="historico-relatorio">Histórico</label>
                                                    </div>
                                                </div>
                                                
                                          
                                                <div class="col s12 right">

                                                  <button type="submit" class="btn waves-effect waves-light light-green darken-3 right col s12 m3 l2">
                                                      <span class="button-text">Exportar</span>
                                                  </button>

                                                  <button type="reset" class="btn waves-effect waves-light grey lighten-4 right col s12 m2 l2 hide-on-small-only">
                                                      <span class="button-text black-text">Limpar</span>
                                                  </button>

                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- FIM  =  FORMULÁRIO DE RELATÓRIO -->

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Modal evento inclusão -->
    <div id="selecionar-evento-inclusao" class="modal bottom-sheet">
      <div class="modal-content">
        <h5>Selecione o evento</h5>
        
        <div class="row">
          <div class="col s12 m12 l6">

            <div class="col s12 m6 l7">
                <div class="input-field">
                    <input type="text" id="filtro-inclusao" />
                    <label for="filtro-inclusao">Filtrar</label>
                </div>
            </div>

            <table class="bordered highlight card" id="eventos-inclusao">
              <thead>
                <tr>
                  <th>Descrição</th>
                  <th>Tipo</th>
                </tr>
              </thead>
              <tbody>
                <tr id="evento-1">
                  <td>
                    <a href="#">Evento 1</a>
                  </td>
                  <td>RF</td>
                </tr>
                <tr id="evento-2">
                  <td>
                    <a href="#">Evento 2</a>
                  </td>
                  <td>RF</td>
                </tr>
                <tr id="evento-3">
                  <td>
                    <a href="#">Evento 3</a>
                  </td>
                  <td>RF</td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>

      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
      </div>
    </div>

    <!-- Modal evento relatório -->
    <div id="selecionar-evento-relatorio" class="modal bottom-sheet">
      <div class="modal-content">
        <h5>Selecione o evento</h5>
        
        <div class="row">
          <div class="col s12 m12 l6">

            <div class="col s12 m6 l7">
                <div class="input-field">
                    <input type="text" id="filtro-relatorio" />
                    <label for="filtro-relatorio">Filtrar</label>
                </div>
            </div>

            <table class="bordered highlight card" id="eventos-relatorio">
              <thead>
                <tr>
                  <th>Descrição</th>
                  <th>Tipo</th>
                </tr>
              </thead>
              <tbody>
                <tr id="evento-1">
                  <td>
                    <a href="#">Evento 1</a>
                  </td>
                  <td>RF</td>
                </tr>
                <tr id="evento-2">
                  <td>
                    <a href="#">Evento 2</a>
                  </td>
                  <td>RF</td>
                </tr>
                <tr id="evento-3">
                  <td>
                    <a href="#">Evento 3</a>
                  </td>
                  <td>RF</td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
      </div>
    </div>



    <?php include("components/footer.php"); ?>


    <style type="text/css">
      #evento-inclusao, label[for="evento-inclusao"],
      #evento-relatorio, label[for="evento-relatorio"] {
        cursor: pointer;
      }
    </style>

    <script type="text/javascript">
      //Implementado em materialize.js (código personalizado)
      modalSelector("evento-inclusao", "selecionar-evento-inclusao", "evento-");
      modalSelector("evento-relatorio", "selecionar-evento-relatorio", "evento-");
      
      var idTabela = "eventos-inclusao";
      var idInputFilter = "filtro-inclusao";
      var searchColumn = "Descrição";
      tableFilter(idTabela, idInputFilter, searchColumn);

      var idTabela2 = "eventos-relatorio";
      var idInputFilter2 = "filtro-relatorio";
      var searchColumn2 = "Descrição";
      tableFilter(idTabela2, idInputFilter2, searchColumn2);

    </script>

</body>
</html>