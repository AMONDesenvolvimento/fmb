var n = 10, // número de itens no eixo X
    m = 2; // número de barras por agrupamento

//Gera dados aleatórios]
var data = d3.range(m).map(function() { 
    return d3.range(n).map(Math.random);
});

var margin = {top: 20, right: 30, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

//Definição responsiva da largura do gráfico
if(window.innerWidth <= 992) {
  width = window.innerWidth - margin.left - margin.right;
}
else {
  width = window.innerWidth - (window.innerWidth * 30 / 100) - margin.left - margin.right;
  margin.left = width * 15 / 100;
}

//Definição responsiva da altura do gráfico
height = width / 2;

var y = d3.scale.linear()
    .domain([0, 1])
    .range([height, 0]);

var x0 = d3.scale.ordinal()
    .domain(d3.range(n))
    .rangeBands([0, width], .2);

var x1 = d3.scale.ordinal()
    .domain(d3.range(m))
    .rangeBands([0, x0.rangeBand()]);

//var z = d3.scale.category20();
function setColor(id){
    switch(id) {
        case 0: return "#64b5f6";
        case 1: return "#1e88e5";
        default: return "#000";
    }
}

var xAxis = d3.svg.axis()
    .scale(x0)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");


function gerarGrafico(containerHTML){
    var svg = d3.select(containerHTML).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("svg:g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g").selectAll("g")
        .data(data)
      .enter().append("g")
        .style("fill", function(d, i) { return setColor(i); })
        .attr("transform", function(d, i) { return "translate(" + x1(i) + ",0)"; })
      .selectAll("rect")
        .data(function(d) { return d; })
      .enter().append("rect")
        .attr("width", x1.rangeBand())
        .attr("height", y)
        .attr("x", function(d, i) { return x0(i); })
        .attr("y", function(d) { return height - y(d); });
}
