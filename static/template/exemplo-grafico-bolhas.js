function gerarGrafico(dados, containerHTML){

    $(containerHTML).append("<svg></svg>");
    
    if(window.innerWidth >= 993){
        $("svg").attr("width", window.innerWidth - 200)
                .attr("height", window.innerWidth)
                .attr("text-anchor", "middle")
                .attr("font-size", "12px")
                .attr("font-family", "sans-serif");
    }
    else if(window.innerWidth <= 992){
        $("svg").attr("width", window.innerWidth - 30)
                .attr("height", window.innerWidth)
                .attr("text-anchor", "middle")
                .attr("font-size", "10px")
                .attr("font-family", "sans-serif");
    }
    else {
        $("svg").attr("width", window.innerWidth - 500)
                .attr("height", window.innerWidth - 500)
                .attr("text-anchor", "middle")
                .attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .css("margin-left", "10vw");
    }

    var svg = d3.select(containerHTML+" > svg"),
        width = +svg.attr("width"),
        height = +svg.attr("height");

    var format = d3.format(",d");

    //SOBRESCREVER A FUNÇÃO DE CORES USANDO SWITCH-CASE E CORES PERSONALIZADAS
    var color = d3.scaleOrdinal(d3.schemeCategory10);

    var pack = d3.pack()
        .size([width, height])
        .padding(1.5);

    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("color", "white")
        .style("padding", "8px")
        .style("background-color", "rgba(0, 0, 0, 0.75)")
        .style("border-radius", "6px")
        .style("font", "12px sans-serif");

    var root = d3.hierarchy({children: dados})
        .sum(function(d) { return d.value; })
        .each(function(d) {
          if (id = d.data.id) {
            var id, i = id.lastIndexOf(".");
            d.id = id;
            d.package = id.slice(0, i);
            d.class = id.slice(i + 1);
          }
        });

    var node = svg.selectAll(".node")
      .data(pack(root).leaves())
      .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { 
            //recuoSuperior = (window.innerWidth >= 993) ? 200 : (window.innerWidth >= 700) ? 100 : 50;
            //return "translate(" + d.x + "," + (d.y - recuoSuperior) + ")"; 
            return "translate(" + d.x + "," + d.y + ")"; 
        });

    node.append("circle")
        .attr("id", function(d) { return d.id; })
        .attr("r", function(d) { return d.r; })
        .style("fill", function(d) { return color(d.package); })
        .on("mouseover", function(d) {
              tooltip.text("R$ " + formatNumber(d.value,2));
              tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() { return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
        .on("mouseout", function(){ return tooltip.style("visibility", "hidden"); });

    node.append("text")
      .selectAll("tspan")
      .data(function(d) { return d.class.split(/(?=[A-Z][^A-Z])/g); })
      .enter().append("tspan")
        .attr("x", 0)
        .attr("y", function(d, i, nodes) { return 13 + (i - nodes.length / 2 - 0.5) * 10; })
        .text(function(d) { return d; });
}

function formatNumber(_number,scale) {
    var r=_number.toFixed(scale).replace(/./g, function(c,i,a) {
        return i>0 && c !== "." && ((a.length-i)%3 ===0)?','+c:c;
    })
    r=r.replace(".","#");
    r=r.replace(",",".");
    r=r.replace("#",",");
    return r;
}