package main
import (
    "text/template"
    "os"
)

type EntetiesClass struct {
    Name string 
    Value int32 
}
var textTemplate  = `{{range $index, $element := .}}{{$index}}
{{range $element}}{{.Value}}
{{end}}
{{end}}`


var textTemplate2 = `{{range .}}
        {{.Id}}
       {{.Name}}
   {{end}}`

type UserClass struct {
    Id string
    Name string
}


func main() {
    data := map[string][]EntetiesClass{
        "Yoga": {{"Yoga", 15}, {"Yoga", 51}},
        "Pilates": {{"Pilates", 3}, {"Pilates", 3}, {"Pilates", 6}, {"Pilates", 9}}, 
    }

    t := template.New("t")
    t, err := t.Parse(textTemplate)
    if err != nil {
        panic(err)
    } 

    err = t.Execute(os.Stdout, data)
    if err != nil {
        panic(err)
    }

    data2 := []UserClass{{"1","um"},{"2","dois"},{"3","tres"}}
    
    t2 := template.New("t2")
    t2, err = t.Parse(textTemplate2)
    if err != nil {
        panic(err)
    }
    err = t2.Execute(os.Stdout,data2)
    if err != nil {
        panic(err)
    }


}


