package main
import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "os"
)
type Page struct {
    ID int `json:"id"`
    Title string `json:"title"`
    Url string `json:"url"`
}
func (p Page) toString() string {
    return toJson(p)
}

func toJson(p interface{}) string {
    bytes, err := json.Marshal(p)
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)
    }
    return string(bytes)
}

func main() {
    pages := getPages()
    for _, p := range pages {
        fmt.Println(p.toString())
    }
    fmt.Println("Done!")
    fmt.Println(toJson(pages))
}


func getPages() []Page {
    raw, err := ioutil.ReadFile("./pages.json")
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)

    }
    fmt.Println("read ok")
    fmt.Println(string(raw))
    var c []Page 
    json.Unmarshal(raw, &c)
    fmt.Println("length: "+string(len(c)))
    for _, page := range c {
        fmt.Println("Title: "+page.Title)
    }
    return c
}
