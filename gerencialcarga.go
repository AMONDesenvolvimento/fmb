package main

import (
//  "encoding/json"
    "database/sql"
  _ "github.com/mattn/go-sqlite3"
    "time"
    "github.com/tealeg/xlsx"
//  "fmt"
    "log"
    "unicode/utf8"
    "strconv"
    "strings"
    "flag"
    "io/ioutil"
    gr "github.com/mikeshimura/goreport"

)

var mainDB *sql.DB
var agora time.Time

type Entrada struct {
    Conta string
    Descricao string
    Codred    string 
    Analitica string
    Orcamento string
}

type Gerencial struct {
        ID int64 `json:"id"`
        IdPai     int64  `json:"idpai"`
        Exercicio int64  `json:"exercicio"` 
        Conta     string `json:"conta"`
        Codred     int64  `json:"codred"`
	Descricao string `json:"descricao"`
        Analitica      string `json:"analitica"`
        SaldoAnterior float64 `json:"saldoanterior"`
        MovimentoDebitoCaixa float64 `json:"movimentodebitocaixa"`
        MovimentoCreditoCaixa float64 `json:"movimentocreditocaixa"`
        MovimentoDebitoCompetencia float64 `json:"movimentodebitocompetencia"`
        MovimentoCreditoCompetencia float64 `json:"movimentocreditocompetencia"`
        Orcamento  float64    `json:"orcamento"`
        DataCriacao string `json:"datacriacao"` 
        DataExclusao string `json:"dataexclusao"`
}
type GerencialArr []Gerencial


func main() {
    load :=flag.Bool( "l",false,"load file before sum")
    sum  :=flag.Bool("s",false,"sum")
    report := flag.Bool("r",false,"report")
    flag.Parse()
    if *load {
        log.Println("load")
    } else {
        log.Println("Do not load")
    }
    if *sum {
        log.Println("sum")
    } else {
        log.Println("Do not sum")
    }
    if *report {
         log.Println("generate report")
    } else {
         log.Println("Do not generate report")
    }


    db, errOpenDB := sql.Open("sqlite3", "fmbtransparencia.db")
    checkErr(errOpenDB)
    mainDB = db
    if *load {
    gerencialDeleteByExercicio(2017)
    var entrada Entrada
    excelFileName := "PlanodeContasGerencial.xlsx"
    xlFile, err := xlsx.OpenFile(excelFileName)
    if err != nil {
        panic(err)
    }
    for _, sheet := range xlFile.Sheets {
        for _, row := range sheet.Rows {
            if len(row.Cells)==0 {
                break;
            } 
            conta, _ := row.Cells[0].String()
            if strings.HasPrefix(conta,"Cod.Red") {
                continue
            }
            for j, cell := range row.Cells {
                text, _ := cell.String()
                if j==0 {
                    entrada.Codred = text
                    entrada.Descricao=""
                    entrada.Conta=""
                    entrada.Analitica=""
                    entrada.Orcamento=""
                    continue
                } else if j==1 {
                    entrada.Conta=text
                } else if j==2 {
                    entrada.Descricao=text
                } else if j==3 {
                    entrada.Orcamento=text
                }
            }
            gerencialInsert(entrada) 
        }
    }
    }
    if *sum { 
        gerencialSum(2017)
    }
    if *report {
        gerencialList()
    }
}

func gerencialInsert(entrada Entrada) {
       if utf8.RuneCountInString(entrada.Codred)==0 {
           entrada.Codred="0"
       }  
       if utf8.RuneCountInString(entrada.Orcamento)>0 {
           entrada.Analitica="X"
       } else {
           entrada.Orcamento="0"
       }
       stmt, err := mainDB.Prepare(
                                   `INSERT INTO planodecontasgerencial (
                                   idpai,
                                   exercicio,
                                   conta,
                                   descricao,
                                   codred,
                                   analitica,  
                                   saldoanterior,
                                   movimentodebitocaixa,
                                   movimentocreditocaixa,
                                   movimentodebitocompetencia,
                                   movimentocreditocompetencia,
                                   orcamento,
                                   datacriacao) values (?,?,?,?,?,?,?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           0,
                           2017,
                           entrada.Conta,
                           entrada.Descricao,
                           entrada.Codred,
                           entrada.Analitica,
                           0,
                           0,
                           0, 
                           0,
                           0,  
                           entrada.Orcamento,
                           time.Now().Format("2006-01-02 15:04:05"),
                           )
	checkErr(errExec)
//	newID, errLast := result.LastInsertId()
	_, errLast := result.LastInsertId()
	checkErr(errLast)
}

func gerencialGetByConta(conta string) Gerencial {
	stmt, err := mainDB.Prepare(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitocaixa,
                                            movimentocreditocaixa,
                                            movimentodebitocompetencia,
                                            movimentocreditocompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao  
                                            FROM planodecontasgerencial where conta = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(conta)
	checkErr(errQuery)
	var gerencial Gerencial
	for rows.Next() {
		err = rows.Scan(&gerencial.ID,
                                &gerencial.IdPai,
                                &gerencial.Exercicio,
                                &gerencial.Conta, 
                                &gerencial.Codred,
                                &gerencial.Descricao,
                                &gerencial.Analitica,
                                &gerencial.SaldoAnterior,
                                &gerencial.MovimentoDebitoCaixa,
                                &gerencial.MovimentoCreditoCaixa,
                                &gerencial.MovimentoDebitoCompetencia,
                                &gerencial.MovimentoCreditoCompetencia,
                                &gerencial.Orcamento, 
                                &gerencial.DataCriacao,
                                &gerencial.DataExclusao)
		//checkErr(err)
	}
        //log.Println("get by conta ID    : "+strconv.FormatInt(gerencial.ID,10))
        //log.Println("get by conta Conta : "+gerencial.Conta)
        //log.Println("get by conta Descrição : "+gerencial.Descricao)
	return gerencial
}

func gerencialSum(exercicio int64) {
	stmt, err := mainDB.Prepare(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM planodecontasgerencial where exercicio = ?`)
	checkErr(err)
	rows, errQuery := stmt.Query(exercicio)
	checkErr(errQuery)
	var gerencialArr GerencialArr
	for rows.Next() {
		var gerencial Gerencial
		err = rows.Scan(
                               &gerencial.ID,
                               &gerencial.IdPai,
                               &gerencial.Exercicio, 
                               &gerencial.Conta,
                               &gerencial.Codred,
                               &gerencial.Descricao,
                               &gerencial.Analitica,
                               &gerencial.SaldoAnterior,
                               &gerencial.MovimentoDebitoCaixa,
                               &gerencial.MovimentoCreditoCaixa,
                               &gerencial.MovimentoDebitoCompetencia,
                               &gerencial.MovimentoCreditoCompetencia,
                               &gerencial.Orcamento,
                               &gerencial.DataCriacao,
                               &gerencial.DataExclusao, 
                               )
		//checkErr(err)

		gerencialArr = append(gerencialArr, gerencial)
	}
        for _,gerencial := range gerencialArr {
            if len(strings.Trim(gerencial.Analitica," "))==0 {
                continue
            }
            parent := extractParent(gerencial.Conta) 
            //log.Println("Parent---->"+parent+" da conta "+gerencial.Conta)
            for len(parent)>0 {
                gerencialParent := gerencialGetByConta(parent) 
                //log.Println("conta parent....->"+gerencialParent.Conta)
                //log.Println("orcamento parent->"+gerencialParent.Conta+"|"+strconv.FormatFloat(gerencialParent.Orcamento,'f',2,64))
                //log.Println("orcamento conta ->"+gerencial.Conta+"|"+strconv.FormatFloat(gerencial.Orcamento,'f',2,64))
                sum := gerencialParent.Orcamento + gerencial.Orcamento
                gerencialUpdateByID(gerencialParent.ID, sum)
                parent = extractParent(parent)
            }
        }
}

func gerencialUpdateByID(id int64, orcamento float64) {
//      var gerencial Gerencial 
   
        //log.Println("update id-------->"+strconv.FormatInt(id,10))
        //log.Println("       orcamento->"+strconv.FormatFloat(orcamento,'f', 2, 64))

	stmt, err := mainDB.Prepare(`UPDATE planodecontasgerencial 
                                    SET (orcamento)=
                                    (?)     
                                    WHERE id = ?`)
	checkErr(err)
	result, errExec := stmt.Exec(
                                  orcamento,
                                  id)
	checkErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	checkErr(errLast)
 	if rowAffected > 0 {
		//jsonB, errMarshal := json.Marshal(gerencial)
		//checkErr(errMarshal)
		//fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		//fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}
}

func gerencialList() {
	rows, err := mainDB.Query(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao, 
                                          dataexclusao FROM planodecontasgerencial order by conta`)
	checkErr(err)
	var gerencialArr GerencialArr
        var gerencial Gerencial 
	for rows.Next() {
		err = rows.Scan(
                               &gerencial.ID,
                               &gerencial.IdPai,
                               &gerencial.Exercicio, 
                               &gerencial.Conta,
                               &gerencial.Codred,
                               &gerencial.Descricao,
                               &gerencial.Analitica,
                               &gerencial.SaldoAnterior,
                               &gerencial.MovimentoDebitoCaixa,
                               &gerencial.MovimentoCreditoCaixa,
                               &gerencial.MovimentoDebitoCompetencia,
                               &gerencial.MovimentoCreditoCompetencia,
                               &gerencial.Orcamento,
                               &gerencial.DataCriacao, 
                               &gerencial.DataExclusao, 
                               )
		//checkErr(err)

		gerencialArr = append(gerencialArr, gerencial)
	}

        var contents string = ""
        for _,gerencial = range gerencialArr {
           contents+=strconv.FormatInt(gerencial.ID, 10)+"\t"
           contents+=strconv.FormatInt(gerencial.IdPai, 10)+"\t"
           contents+=strconv.FormatInt(gerencial.Exercicio, 10)+"\t"
           contents+=gerencial.Conta+"\t"
           contents+=strconv.FormatInt(gerencial.Codred, 10)+"\t"
           contents+=gerencial.Descricao+"\t"
           contents+=gerencial.Analitica+"\t"
           contents+=strconv.FormatFloat(gerencial.SaldoAnterior,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(gerencial.MovimentoDebitoCaixa,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(gerencial.MovimentoCreditoCaixa,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(gerencial.MovimentoDebitoCompetencia,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(gerencial.MovimentoCreditoCompetencia,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(gerencial.Orcamento,'f',-1,64)+"\t"
           contents+=gerencial.DataCriacao+"\t"
           contents+=gerencial.DataExclusao+"\n"
         } 
        d1 := []byte(contents)
        err = ioutil.WriteFile("gerenciallist.txt",d1,0644)
        checkErr(err)
        gerencialReport()
}

func gerencialReport() {
        agora = time.Now()
	r := gr.CreateGoReport()
	r.SumWork["amountcum="] = 0.0
	font1 := gr.FontMap{
		FontName: "IPAexG",
		FileName: "ttf//ipaexg.ttf",
	}
	fonts := []*gr.FontMap{&font1}
	r.SetFonts(fonts)
	d := new(S1Detail)
	r.RegisterBand(gr.Band(*d), gr.Detail)
	h := new(S1Header)
	r.RegisterBand(gr.Band(*h), gr.PageHeader)
	s := new(S1Summary)
	r.RegisterBand(gr.Band(*s), gr.Summary)

	f := new(S1Footer)
	r.RegisterBand(gr.Band(*f), gr.PageFooter)


	r.Records = gr.ReadTextFile("gerenciallist.txt", 7) // 09 e 0A no final
//	fmt.Printf("Records %v \n", r.Records)
	r.SetPage("A4", "mm", "L")
	r.SetFooterY(190)
	r.Execute("static/gerenciallist.pdf")
	r.SaveText("static/gerencialsavetext.txt")
}

type S1Detail struct {
}

func (h S1Detail) GetHeight(report gr.GoReport) float64 {
	return 5
}
func (h S1Detail) Execute(report gr.GoReport) {
	cols := report.Records[report.DataPos].([]string)
	report.Font("IPAexG", 12, "")
	y := 5.0
	report.Cell(15, y, cols[3])
	report.Cell(50, y, identDescription(cols[3],cols[5]))
	report.CellRight(205, y,15, transformFloat(cols[12],2))
//	amt := ParseFloatNoError(cols[5]) * ParseFloatNoError(cols[6])
        report.CellRight(240, y,6, transformCodred(cols[4])) 
	report.SumWork["amountcum="] += 1
//	report.CellRight(180, y, 30, strconv.FormatFloat(amt, 'f', 2, 64))
}
func ParseFloatNoError(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}

type S1Header struct {
}

func (h S1Header) GetHeight(report gr.GoReport) float64 {
	return 30
}
func (h S1Header) Execute(report gr.GoReport) {
	report.Font("IPAexG", 14, "")
	report.Cell(80, 15, "Transparência - Plano Gerencial com Orçamento")
	report.Font("IPAexG", 12, "")
	report.Cell(255, 15, "Página")
	report.Cell(275, 15, strconv.Itoa(report.Page))
        y := 23.0
        filter := "Exercício: 2017"
        var x float64
        x = float64((265.0 - len(filter))/2.0)
        report.Cell(15+x,y,filter)
	y = 28.0
	report.Cell(15, y, "Conta")
	report.Cell(50, y, "Descrição")
	report.CellRight(205, y,15, "Orçamento")
	report.Cell(240, y, "Cod.Red.")
	report.LineType("straight", 0.2)
        report.LineH(15, y+5.0, 280)

      report.Image("static/Content/img/fmb.jpeg", 15, 10, 80, 20)
}

type S1Summary struct {
}

func (h S1Summary) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Summary) Execute(report gr.GoReport) {
	report.CellRight(100, 12,25, "Contas Listadas:")
	report.CellRight(130, 12, 30, strconv.FormatFloat(
		report.SumWork["amountcum="], 'f', 0, 64))
}

type S1Footer struct {
}

func (h S1Footer) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Footer) Execute(report gr.GoReport) {
	report.CellRight(200, 8,80, "Emitido por MASTER em "+formatTime(agora))
}

func gerencialDeleteByExercicio(exercicio int64) {
	stmt, err := mainDB.Prepare("DELETE FROM planodecontasgerencial WHERE exercicio = ?")
	checkErr(err)
	result, errExec := stmt.Exec(exercicio)
	checkErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	checkErr(errRow)
        log.Println("{row_affected=%d}",rowAffected)
//	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}



func transformFloat(value string, scale int) string {
    const zeros = "0000000000"
    var strint string 
    var strdec string
    r := value
    pos := strings.Index(r,".")
    if pos>=0 {
        strint=r[0:pos]
        strdec=r[pos+1:len(r)]
    } else {
        strint=r
        strdec=""
    }
    if len(strdec)<scale {
        strdec+=zeros[0:scale-len(strdec)]
    } else {
        strdec=strdec[0:scale]
    }
 
    r="";
    intlength :=len(strint)
    for i:=intlength-1;i>=0;i-- {
        r=string(strint[i])+r
        if (intlength-i)%3 ==0 && i!=0{
            r="."+r
        }
    }
    if scale>0 {
        r=r+","+strdec
    }
    return r 
}
func formatFloat(value float64, scale int) string {
    const zeros = "0000000000"
    var strint string 
    var strdec string
    r := strconv.FormatFloat(value,'f',-1,64)
    pos := strings.Index(r,".")
    if pos>=0 {
        strint=r[0:pos]
        strdec=r[pos+1:len(r)]
    } else {
        strint=r
        strdec=""
    }
    if len(strdec)<scale {
        strdec+=zeros[0:scale-len(strdec)]
    } else {
        strdec=strint[0:scale]
    }
 
    r="";
    intlength :=len(strint)
    for i:=intlength-1;i>=0;i-- {
        r=string(strint[i])+r
        if (intlength-i)%3 ==0 && i!=0{
            r="."+r
        }
    }
    if scale>0 {
        r=r+","+strdec
    }
    return r 
}



func extractParent(conta string) string {
    pos:=strings.LastIndex(conta,".") 
    if pos<0 {
        return ""
    } else {
        return conta[0:pos]
    }
}

func transformCodred(codred string) string {
    if codred=="0" {
        return ""
    } else {
        return codred
    }
}

func identDescription(conta string, descricao string) string {
    const spaces = "                                                                             "
    margin := strings.Count(conta,".")*10
    return spaces[0:margin]+descricao
}

func formatTime(date time.Time) string {
    r := date.Format("2006-01-02 15:04:05")
    year := r[0:4]
    month := r[5:7]
    day := r[8:10]
    time := r[11:19]
    return day + "/" + month + "/" + year+ " "+time 
}



func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

