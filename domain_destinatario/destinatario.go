package domain_destinatario
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
    "strconv"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
//	gr "github.com/mikeshimura/goreport"
    "../util"
)
type Record struct {
	ID   int64  `json:"id"`
        Nome string `json:"nome"`
        Descricao     string  `json:"descricao"`
        Expressao        string `json:"expressao"`
        DataCriacao string     `json:"datacriacao"` 
        DataExclusao string    `json:"dataexclusao"`
}

type Filter struct {
        ID string `json:"id"`
        Nome     string `json:"nome"`
}
type Arr []Record

func GetAll(w http.ResponseWriter, r *http.Request) {
	rows := util.QueryDB(`SELECT id,
                                          nome,
                                          descricao,
                                          expressao,
                                          datacriacao,
                                          dataexclusao FROM destinatario`)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.Nome,
                               &record.Descricao,
                               &record.Expressao, 
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB(` SELECT id,
                                             nome,
                                             descricao,
                                             expressao,
                                             datacriacao,
                                             dataexclusao FROM destinatario where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var  record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome,
                                &record.Descricao,
                                &record.Expressao, 
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func RowByID(id int64) Record {
	stmt := util.PrepareDB(` SELECT id,
                                             nome,
                                             descricao,
                                             expressao,
                                             datacriacao,
                                             dataexclusao FROM destinatario where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var  record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Nome,
                                &record.Descricao,
                                &record.Expressao, 
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
        return record
}


func Insert(w http.ResponseWriter, r *http.Request) {
        var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            fmt.Printf("erro %s",err)
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("nome---->"+record.Nome)
        record.DataCriacao = time.Now().Format("2006-01-02 15:04:05")


	stmt := util.PrepareDB(
                                   `INSERT INTO destinatario (
                                   nome,
                                   descricao,
                                   expressao,
                                   datacriacao) values (?,?,?,?)`)
	result, errExec := stmt.Exec(
                           record.Nome,  
                           record.Descricao,
                           record.Expressao,
                           record.DataCriacao,
                           )
	util.CheckErr(errExec)
	newID, errLast := result.LastInsertId()
	util.CheckErr(errLast)
	record.ID = newID
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func UpdateByID(w http.ResponseWriter, r *http.Request) {
      var record Record 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("nome---->"+record.Nome)


	stmt := util.PrepareDB(`UPDATE destinatario 
                                    SET (nome, descricao, expressao)=
                                    (?,?,?)     
                                    WHERE id = ?`)
	result, errExec := stmt.Exec(
                                  record.Nome, 
                                  record.Descricao,
                                  record.Expressao,
                                  record.ID)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(record)
		util.CheckErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func DeleteByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB("DELETE FROM destinatario WHERE id = ?")
	result, errExec := stmt.Exec(id)
	util.CheckErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	util.CheckErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func List(w http.ResponseWriter, r *http.Request) {
        var filter Filter 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }

        log.Println("id----->"+filter.ID)
        log.Println("nome---->"+filter.Nome)


	rows := util.QueryDB(`SELECT id,
                                          nome,
                                          descricao,
                                          expressao,
                                          datacriacao,
                                          dataexclusao FROM eventom`)
	var arr Arr
        var record Record
	for rows.Next() {
		rows.Scan(
                               &record.ID,
                               &record.Nome,  
                               &record.Descricao,
                               &record.Expressao, 
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )

		arr = append(arr, record)
	}

        var contents string = ""
        for _,record = range arr {
           contents+=strconv.FormatInt(record.ID, 10)+"\t"
           contents+=record.Nome+"\t"
           contents+=record.Descricao+"\t"
           contents+=record.Expressao+"\t"
           contents+=record.DataCriacao+"\t"
           contents+=record.DataExclusao+"\n"
         } 
        d1 := []byte(contents)
        err = ioutil.WriteFile("destinatariolist.txt",d1,0644)
        util.CheckErr(err)
        var retornoLista util.RetornoLista
        retornoLista.Arquivo="destinatariolist.txt"
        retornoLista.Status="ok"
	jsonB, errMarshal := json.Marshal(retornoLista)
	util.CheckErr(errMarshal)
        fmt.Printf("Lista-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}



