package main

import (
    "database/sql"
  _ "github.com/mattn/go-sqlite3"
    "time"
    "github.com/tealeg/xlsx"
    "fmt"
    "log"
    "unicode/utf8"
    "strconv"
    "strings"
)

var mainDB *sql.DB

type Entrada struct {
    Conta string
    Descricao string
    Codred    string 
    Analitica string
}

func main() {


    db, errOpenDB := sql.Open("sqlite3", "fmbtransparencia.db")
    checkErr(errOpenDB)
    mainDB = db

    var entrada Entrada
    excelFileName := "Plano de Contas FMB.xlsx"
    xlFile, err := xlsx.OpenFile(excelFileName)
    if err != nil {
        panic(err)
    }
    for _, sheet := range xlFile.Sheets {
        for _, row := range sheet.Rows {
            conta, _ := row.Cells[0].String()
            if strings.HasPrefix(conta,"Código") {
                continue
            }
            for j, cell := range row.Cells {
                text, _ := cell.String()
//              ct :=cell.Type()
                if j==0 {
                    log.Println("log")
                    fmt.Printf("[%d]",utf8.RuneCountInString(text))
                    log.Println("log2") 
                    entrada.Conta = text
                    entrada.Descricao=""
                    entrada.Codred="0"
                    entrada.Analitica=""
                    continue
                }
                if utf8.RuneCountInString(text)>0  {
                    if utf8.RuneCountInString(entrada.Descricao)==0 {
                        entrada.Descricao = text
                    }else if _, err := strconv.Atoi(text); err==nil {
                        entrada.Codred=text
                        entrada.Analitica="X"
                        break 
                    }     
                }  
            }
            planodecontasInsert(entrada) 
        }
    }
}

func planodecontasInsert(entrada Entrada) {
        log.Println("log3")
        fmt.Printf("[%s]",entrada.Conta);
        log.Println("log4") 
	stmt, err := mainDB.Prepare(
                                   `INSERT INTO planodecontas (
                                   idpai,
                                   exercicio,
                                   conta,
                                   descricao,
                                   codred,
                                   analitica,  
                                   datacriacao) values (?,?,?,?,?,?,?)`)
	checkErr(err)
	result, errExec := stmt.Exec(
                           0,
                           2017,
                           entrada.Conta,
                           entrada.Descricao,
                           entrada.Codred,
                           entrada.Analitica,
                           time.Now().Format("2006-01-02 15:04:05"),
                           )
        log.Println("log5")
	checkErr(errExec)
//	newID, errLast := result.LastInsertId()
	_, errLast := result.LastInsertId()
	checkErr(errLast)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

