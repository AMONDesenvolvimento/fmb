package domain_lancamento
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
    "strconv"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
	gr "github.com/mikeshimura/goreport"
    "../util"
    "../domain_evento"
)
type Record struct {
	ID   int64  `json:"id"`
        Evento     int64  `json:"evento"`
        NomeEvento string `json:nomeevento"`
        DescricaoEvento string `json:"descricaoevento"` 
        TipoEvento      string `json:"tipoevento"` 
        Valor       float64    `json:"valor"`
        Data time.Time  `json:"data"`
        Exercicio int64   `json:"exercicio"`
        MesReferencia int64   `json:"mesreferencia"`
        Historico  string     `json:"historico"`         
        DataCriacao string     `json:"datacriacao"` 
        DataExclusao string    `json:"dataexclusao"`
}
type Filter struct {
        ID string `json:"id"`
        Evento string `json:"evento"`
        NomeEvento string `json:"nomeevento"`
        DescricaoEvento string `json:"descricaoevento"`
        TipoEvento string `json:"tipoevento"`
        Valor string `json:"valor"`
        Data  string `json:"data"`
        Exercicio string `json:"exercicio"`
        MesReferencia string `json:"mesreferencia"`
        Historico     string `json:"historico"`  
}

var filter Filter
type Arr []Record
func GetAll(w http.ResponseWriter, r *http.Request) {
	rows := util.QueryDB("SELECT id, evento, valor, data, exercicio, mesreferencia, historico, datacriacao, dataexclusao FROM lancamento order by datacriacao desc")
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.Evento,
                               &record.Valor, 
                               &record.Data,
                               &record.Exercicio,
                               &record.MesReferencia,
                               &record.Historico,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
                evento :=domain_evento.RowByID(record.Evento)
                record.NomeEvento = evento.Nome
                record.DescricaoEvento = evento.Descricao  
                record.TipoEvento = evento.Tipo
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetByFilter(w http.ResponseWriter, r *http.Request) {
        var filter Filter 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
//        if err != nil {
//            panic(fmt.Sprintf("%s",string(body)))
//        }
   
        log.Println("id----->"+filter.ID)
        log.Println("evento---->"+filter.Evento)
        log.Println("valor---->"+filter.Valor)
        log.Println("data---->"+filter.Data)
        log.Println("exercicio---->"+filter.Exercicio)
        log.Println("mesreferencia---->"+filter.MesReferencia)
        log.Println("historico---->"+filter.Historico)
	selectText :=`SELECT lancamento.id as id,
                              evento,
                              evento.nome as nomeevento,
                              evento.descricao as descricaoevento,
                              evento.tipo as tipoevento, 
                              valor,
                              data,
                              exercicio,
                              mesreferencia,
                              historico,
                              lancamento.datacriacao as datacriacao,
                              lancamento.dataexclusao as exclusao FROM lancamento 
                              inner join evento on lancamento.evento = evento.id   
                              `
        whereText := generateWhereText(filter)
        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
        log.Println("where text->"+whereText)
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                          &record.ID,
                          &record.Evento,
                          &record.NomeEvento,
                          &record.DescricaoEvento,
                          &record.TipoEvento,
                          &record.Valor, 
                          &record.Data,
                          &record.Exercicio,
                          &record.MesReferencia,
                          &record.Historico,
                          &record.DataCriacao,
                          &record.DataExclusao, 
                          )
//                evento :=domain_evento.RowByID(record.Evento)
//                record.NomeEvento = evento.Nome
//                record.DescricaoEvento = evento.Descricao  
//                record.TipoEvento = evento.Tipo
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getByFilter-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}
/*
func GetCronologicalExplorer(w http.ResponseWriter, r *http.Request) {
        var filter Filter 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
//        if err != nil {
//            panic(fmt.Sprintf("%s",string(body)))
//        }
   
        log.Println("id----->"+filter.ID)
        log.Println("evento---->"+filter.Evento)
        log.Println("valor---->"+filter.Valor)
        log.Println("data---->"+filter.Data)
        log.Println("exercicio---->"+filter.Exercicio)
        log.Println("mesreferencia---->"+filter.MesReferencia)
        log.Println("historico---->"+filter.Historico)
	selectText :=`SELECT lancamento.id as id,
                              evento,
                              evento.nome as nomeevento,
                              evento.descricao as descricaoevento,
                              evento.tipo as tipoevento, 
                              valor,
                              data,
                              exercicio,
                              mesreferencia,
                              historico,
                              lancamento.datacriacao as datacriacao,
                              lancamento.dataexclusao as exclusao FROM lancamento 
                              inner join evento on lancamento.evento = evento.id order by lancamento.data, lancamento.id   
                              `
        whereText := generateWhereText(filter)
        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
        log.Println("where text->"+whereText)
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                          &record.ID,
                          &record.Evento,
                          &record.NomeEvento,
                          &record.DescricaoEvento,
                          &record.TipoEvento,
                          &record.Valor, 
                          &record.Data,
                          &record.Exercicio,
                          &record.MesReferencia,
                          &record.Historico,
                          &record.DataCriacao,
                          &record.DataExclusao, 
                          )
//                evento :=domain_evento.RowByID(record.Evento)
//                record.NomeEvento = evento.Nome
//                record.DescricaoEvento = evento.Descricao  
//                record.TipoEvento = evento.Tipo
		arr = append(arr, record)
	}

        r :=`<li>`
        tagMes := 0
        tagDia := 0 
        for _,record = range arr {
            mes := int(record.Data.Month)
            dia := int(record.Data.Day) 
            if mes!=tagMes {
                if tagMes>0 {
                }
            } else {
                if dia!=tagDia {
                } else {
                }
  
            }

        } 
        r+=`</li>`

//	jsonB, errMarshal := json.Marshal(arr)
//	util.CheckErr(errMarshal)
//      fmt.Printf("getByFilter-> %s",string(jsonB))
//	fmt.Fprintf(w, "%s", string(jsonB))
        fmt.Printf("GetCronologicalExplorer-> %s",r)
	fmt.Fprintf(w, "%s", r)
}
*/
func GetSumByContaCaixa(exercicio string, mesReferencia string, ultimoMes string, conta string) float64 {
       strMesReferencia :=mesReferencia
       if len(strMesReferencia)>0 && len(strMesReferencia)<2 {
           strMesReferencia="0"+strMesReferencia
       } 
       strUltimoMes :=ultimoMes
       if len(strUltimoMes)>0 && len(strUltimoMes)<2 {
           strUltimoMes="0"+strUltimoMes
       } 

        selectText :=`select sum(valor) as valor
                      from lancamento inner join 
                     (evento inner join orcamento on evento.gerencial = orcamento.codred)
                     on lancamento.evento = evento.id   
                     where orcamento.conta like '`+conta+`%'
                    `
        if len(strMesReferencia)>0 {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)='`+strMesReferencia+`')` 
        } else {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)<='`+strUltimoMes+`')` 
        }   
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
        var valor float64
	for rows.Next() {
		rows.Scan(
                          &valor,
                          )
	}
        return valor
}

func GetSumByContaCompetencia(exercicio string, mesReferencia string, ultimoMes string, conta string) float64 {
       strMesReferencia :=mesReferencia
       if len(strMesReferencia)>0 && len(strMesReferencia)<2 {
           strMesReferencia="0"+strMesReferencia
       } 
       strUltimoMes :=ultimoMes
       if len(strUltimoMes)>0 && len(strUltimoMes)<2 {
           strUltimoMes="0"+strUltimoMes
       } 

        selectText :=`select sum(valor) as valor
                      from lancamento inner join 
                     (evento inner join orcamento on evento.gerencial = orcamento.codred)
                     on lancamento.evento = evento.id   
                     where orcamento.conta like '`+conta+`%'
                    `
        if len(strMesReferencia)>0 {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)='`+strMesReferencia+`')` 
        } else {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)<='`+strUltimoMes+`')` 
        }   
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
        var valor float64
	for rows.Next() {
		rows.Scan(
                          &valor,
                          )
	}
        return valor
}

func GetSumByTipoCaixa(exercicio string, mesReferencia string,ultimoMes string, tipo string) float64 {
       strMesReferencia :=mesReferencia
       if len(strMesReferencia)>0 && len(strMesReferencia)<2 {
           strMesReferencia="0"+strMesReferencia
       } 
       strUltimoMes :=ultimoMes
       if len(strUltimoMes)>0 && len(strUltimoMes)<2 {
           strUltimoMes="0"+strUltimoMes
       } 

        selectText :=`select sum(valor) as valor
                      from lancamento inner join 
                     (evento inner join orcamento on evento.gerencial = orcamento.codred)
                     on lancamento.evento = evento.id   
                     where evento.tipo = '`+tipo+`'
                    `
        if len(strMesReferencia)>0 {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)='`+strMesReferencia+`')` 
        } else {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)<='`+strUltimoMes+`')` 
        }   
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
        var valor float64
	for rows.Next() {
		rows.Scan(
                          &valor,
                          )
	}
        return valor
}

func GetSumByTipoCompetencia(exercicio string, mesReferencia string,ultimoMes string, tipo string) float64 {
       strMesReferencia :=mesReferencia
       if len(strMesReferencia)>0 && len(strMesReferencia)<2 {
           strMesReferencia="0"+strMesReferencia
       } 
       strUltimoMes :=ultimoMes
       if len(strUltimoMes)>0 && len(strUltimoMes)<2 {
           strUltimoMes="0"+strUltimoMes
       } 

        selectText :=`select sum(valor) as valor
                      from lancamento inner join 
                     (evento inner join orcamento on evento.gerencial = orcamento.codred)
                     on lancamento.evento = evento.id   
                     where evento.tipo = '`+tipo+`'
                    `
        if len(strMesReferencia)>0 {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)='`+strMesReferencia+`')` 
        } else {
            selectText+=` and (strftime('%Y',data) = '`+exercicio+`' and strftime('%m',data)<='`+strUltimoMes+`')` 
        }   
        log.Println("select text->"+selectText) 
        rows := util.QueryDB(selectText)
        var valor float64
	for rows.Next() {
		rows.Scan(
                          &valor,
                          )
	}
        return valor
}


func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB(" SELECT id,evento,valor, data, exercicio, mesreferencia, historico, datacriacao, dataexclusao FROM lancamento where id = ?")
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Evento,
                                &record.Valor, 
                                &record.Data,
                                &record.Exercicio,
                                &record.MesReferencia,
                                &record.Historico,
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
        evento :=domain_evento.RowByID(record.Evento)
        record.NomeEvento = evento.Nome
        record.DescricaoEvento = evento.Descricao
        record.TipoEvento = evento.Tipo  
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func Insert(w http.ResponseWriter, r *http.Request) {
        var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            fmt.Printf("erro %s",err)
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("evento---->"+strconv.FormatInt(record.Evento,10))
        log.Println("valor---->"+strconv.FormatFloat(record.Valor,'f',-1,64))
        log.Println("data---->"+record.Data.Format("2006-01-02 15:04:05"))
        log.Println("exercicio---->"+strconv.FormatInt(record.Exercicio,10))
        log.Println("mesreferencia---->"+strconv.FormatInt(record.MesReferencia,10))
        log.Println("historico---->"+record.Historico)
        record.DataCriacao = time.Now().Format("2006-01-02 15:04:05")


	stmt := util.PrepareDB(
                                   `INSERT INTO lancamento (
                                   evento,
                                   valor,
                                   data,
                                   exercicio,
                                   mesreferencia,
                                   historico,
                                   datacriacao) values (?,?,?,?,?,?,?)`)
	result, errExec := stmt.Exec(
                           record.Evento,
                           record.Valor,
                           record.Data,
                           record.Exercicio,
                           record.MesReferencia,
                           record.Historico,
                           record.DataCriacao,
                           )
	util.CheckErr(errExec)
	newID, errLast := result.LastInsertId()
	util.CheckErr(errLast)
	record.ID = newID
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func UpdateByID(w http.ResponseWriter, r *http.Request) {
      var record Record 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("evento---->"+strconv.FormatInt(record.Evento,10))
        log.Println("valor---->"+strconv.FormatFloat(record.Valor,'f',-1,64))
        log.Println("data---->"+record.Data.Format("2006-01-02 15:04:05"))
        log.Println("exercicio---->"+strconv.FormatInt(record.Exercicio,10))
        log.Println("mesreferencia---->"+strconv.FormatInt(record.MesReferencia,10))
        log.Println("historico---->"+record.Historico)

	stmt := util.PrepareDB(`UPDATE lancamento 
                                    SET (valor, data,  mesreferencia, historico)=
                                    (?,?,?,?)     
                                    WHERE id = ?`)
	result, errExec := stmt.Exec(
                                  
                                  record.Valor,
                                  record.Data,
                                  record.MesReferencia,
                                  record.Historico,
                                  record.ID)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(record)
		util.CheckErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func DeleteByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB("DELETE FROM lancamento WHERE id = ?")
	result, errExec := stmt.Exec(id)
	util.CheckErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	util.CheckErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func List(w http.ResponseWriter, r *http.Request) {
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+filter.ID)
        log.Println("evento---->"+filter.Evento,10)
        log.Println("valor---->"+filter.Valor)
        log.Println("data---->"+filter.Data)
        log.Println("exercicio---->"+filter.Exercicio)
        log.Println("mesreferencia---->"+filter.MesReferencia)
        log.Println("historico---->"+filter.Historico)
	selectText :=`SELECT lancamento.id as id,
                              evento,
                              evento.nome as nomeevento,
                              evento.descricao as descricaoevento,
                              evento.tipo as tipoevento, 
                              valor,
                              data,
                              exercicio,
                              mesreferencia,
                              historico,
                              lancamento.datacriacao as datacriacao,
                              lancamento.dataexclusao as exclusao FROM lancamento 
                              inner join evento on lancamento.evento = evento.id`
          
        whereText := generateWhereText(filter)
        if len(whereText)>0 {
            selectText +=" where " + whereText
        }
	rows := util.QueryDB(selectText)
	var arr Arr
        var record Record 
	for rows.Next() {
		rows.Scan(
                          &record.ID,
                          &record.Evento,
                          &record.NomeEvento,
                          &record.DescricaoEvento,
                          &record.TipoEvento,
                          &record.Valor, 
                          &record.Data,
                          &record.Exercicio,
                          &record.MesReferencia,
                          &record.Historico,
                          &record.DataCriacao,
                          &record.DataExclusao, 
                          )
                evento :=domain_evento.RowByID(record.Evento)
                record.NomeEvento = evento.Nome
                record.DescricaoEvento = evento.Descricao 
                record.TipoEvento = evento.Tipo
		arr = append(arr, record)
	}

        var contents string = ""
        for _,record = range arr {
           //contents+=strconv.FormatInt(record.ID, 10)+"\t"
           //contents+=strconv.FormatInt(record.Evento, 10)+"\t"
           contents+=record.NomeEvento+"\t"
           //contents+=record.DescricaoEvento+"\t"
           contents+=record.TipoEvento+"\t"
           contents+=strconv.FormatFloat(record.Valor,'f',-1,64)+"\t"
           contents+=record.Data.Format("2006-01-02 15:04:05")+"\t"
           contents+=strconv.FormatInt(record.Exercicio,10)+"\t"
           contents+=strconv.FormatInt(record.MesReferencia,10)+"\t"
           contents+=record.Historico+"\t"
           contents+=record.DataCriacao+"\t"
           contents+=record.DataExclusao+"\n"
         } 
        d1 := []byte(contents)
        err = ioutil.WriteFile("lancamentolist.txt",d1,0644)
        util.CheckErr(err)
        Report()
        var retornoLista util.RetornoLista
        retornoLista.Arquivo="lancamentolist.pdf"
        retornoLista.Status="ok"
	jsonB, errMarshal := json.Marshal(retornoLista)
	util.CheckErr(errMarshal)
        fmt.Printf("Lista-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func Report() {
        util.Agora = time.Now()
	r := gr.CreateGoReport()
	r.SumWork["amountcum="] = 0.0
        r.SumWork["RF="]=0.0
        r.SumWork["RV="]=0.0
        r.SumWork["DF="]=0.0
        r.SumWork["DV="]=0.0
        r.SumWork["P="]=0.0
	font1 := gr.FontMap{
		FontName: "IPAexG",
		FileName: "ttf//ipaexg.ttf",
	}
	fonts := []*gr.FontMap{&font1}
	r.SetFonts(fonts)
	d := new(S1Detail)
	r.RegisterBand(gr.Band(*d), gr.Detail)
	h := new(S1Header)
	r.RegisterBand(gr.Band(*h), gr.PageHeader)
	s := new(S1Summary)
	r.RegisterBand(gr.Band(*s), gr.Summary)
	r.Records = gr.ReadTextFile("lancamentolist.txt", 7) // 09 e 0A no final

	f := new(S1Footer)
	r.RegisterBand(gr.Band(*f), gr.PageFooter)


//	fmt.Printf("Records %v \n", r.Records)
	r.SetPage("A4", "mm", "L")
	r.SetFooterY(190)
	r.Execute("static/lancamentolist.pdf")
	r.SaveText("static/lancamentolist.txt")
}

type S1Detail struct {
}

func (h S1Detail) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Detail) Execute(report gr.GoReport) {
	cols := report.Records[report.DataPos].([]string)
	report.Font("IPAexG", 9, "")
	y := 9.0
	//report.Cell(15, y, cols[0])
	report.Cell(15, y, cols[0])
	report.Cell(100, y, cols[1])
  report.Cell(120, y, util.TransformFloat(cols[2],2))
  report.Cell(143, y, util.TransformDate(cols[3]))
	//report.CellRight(150, y,15, util.TransformFloat(cols[3],2))
        if cols[1]=="RF" {
            report.SumWork["RF="] += util.ParseFloatNoError(cols[2])
        } else if cols[1]=="RV" {
            report.SumWork["RV="] += util.ParseFloatNoError(cols[2])
        } else if cols[1]=="DF" {
            report.SumWork["DF="] += util.ParseFloatNoError(cols[2])
        } else if cols[1]=="DV" {
            report.SumWork["DV="] += util.ParseFloatNoError(cols[2])
        } else if cols[1]=="P" {
            report.SumWork["P="] += util.ParseFloatNoError(cols[2])
        }
	//report.Cell(190, y, util.TransformDate(cols[6]))
  report.Cell(173, y,util.FormatReference(cols[4],cols[5]))
	//report.CellRight(151, y+4.0,15, util.FormatReference(cols[5],cols[6]))
	report.Cell(199, y, cols[6])

//	amt := ParseFloatNoError(cols[5]) * ParseFloatNoError(cols[6])
	report.SumWork["amountcum="] += 1
//	report.CellRight(180, y, 30, strconv.FormatFloat(amt, 'f', 2, 64))
}

type S1Header struct {
}

func (h S1Header) GetHeight(report gr.GoReport) float64 {
	return 30
}
func (h S1Header) Execute(report gr.GoReport) {
	report.Font("IPAexG", 14, "")
	report.Cell(80, 15, "Transparência - Listagem dos Lançamentos para Conferência")
	report.Font("IPAexG", 12, "")
	report.Cell(255, 15, "Página")
	report.Cell(275, 15, strconv.Itoa(report.Page))
        y := 23.0
        f := generateFilterText(filter) 
        var x float64
        x = float64((265.0 - len(f))/2.0)
        report.Cell(15+x,y,f)
	y = 28.0
	//report.Cell(15, y, "ID")
	report.Cell(15, y, "Evento")
	//report.Cell(60, y, "Nome")
  report.Cell(97,y,"Tipo")
	report.CellRight(117, y,15, "Valor")
  report.CellRight(140, y,15, "Data")
	report.Cell(170, y, "Referência")
  report.Cell(199, y, "Histórico")
	//report.CellRight(150, y+4.0,15, "Referência")
//	report.CellRight(160, y, 20, "Mes")
	//report.Cell(190, y+4.0, "Histórico")
	report.LineType("straight", 0.2)
        report.LineH(15, y+8.0, 280)

//      report.Image("static/Content/img/fmb.png", 220, 10, 240, 30)
      report.Image("static/Content/img/fmb.jpeg", 15, 10, 80, 20)

}

type S1Summary struct {
}

func (h S1Summary) GetHeight(report gr.GoReport) float64 {
	return 90
}
func (h S1Summary) Execute(report gr.GoReport) {
        report.NewPage(true) 

	report.CellRight(100, 12,25, "Lançamentos Listados:")
	report.CellRight(130, 12, 30, strconv.FormatFloat(
		report.SumWork["amountcum="], 'f', 0, 64))
        
	report.CellRight(100, 22,25, "Receitas Fixas:")
	report.CellRight(130, 22, 30, util.FormatFloat(
		report.SumWork["RF="],2))

	report.CellRight(100, 32, 25,"Receitas Variáveis:")
	report.CellRight(130, 32, 30, util.FormatFloat(
		report.SumWork["RV="],2))

	report.CellRight(100, 42, 25,"Total das Receitas:")
	report.CellRight(130, 42, 30, util.FormatFloat(
		report.SumWork["RF="]+report.SumWork["RV="],2))

	report.CellRight(100, 52,25, "Despesas Fixas:")
	report.CellRight(130, 52, 30, util.FormatFloat(
		report.SumWork["DF="],2))

	report.CellRight(100, 62,25, "Despesas Variáveis:")
	report.CellRight(130, 62, 30, util.FormatFloat(
		report.SumWork["DV="],2))

	report.CellRight(100, 72,25, "Total das Despesas:")
	report.CellRight(130, 72, 30, util.FormatFloat(
		report.SumWork["DF="]+report.SumWork["DV="],2))

	report.CellRight(100, 82,25, "Patrimoniais:")
	report.CellRight(130, 82, 30, util.FormatFloat(
		report.SumWork["P="],2))


}

type S1Footer struct {
}

func (h S1Footer) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Footer) Execute(report gr.GoReport) {
	report.CellRight(200, 8,80, "Emitido por MASTER em "+util.FormatTime(util.Agora))
}
func generateWhereText(filter Filter) string {
    r := ""
    if len(filter.Evento)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleInteger("evento", filter.Evento))
    }
    if len(filter.TipoEvento)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleString("tipoevento", filter.TipoEvento))
    }
    if len(filter.Data)>0 {
        r = util.ConcatWhere(r,util.GenerateInLineDate("data", filter.Data))
    }


    if len(filter.Exercicio)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleInteger("exercicio", filter.Exercicio))
    }
    if len(filter.MesReferencia)>0 {
        r = util.ConcatWhere(r,util.GenerateSingleInteger("mesreferencia", filter.MesReferencia))
    }

    return r

}
func generateFilterText(filter Filter) string {
    r := ""
    if len(filter.NomeEvento)>0 {
        r = util.ConcatFilter(r,filter.NomeEvento)
    }
    if len(filter.TipoEvento)>0 {
        r  = util.ConcatFilter(r, descricaoTipoEvento(filter.TipoEvento))    
    }
    if len(filter.Data)>0 {
        r = util.ConcatFilter(r, util.InLineDateFilter("Data",filter.Data)) 
    }



    if len(filter.Exercicio)>0 {
        r = util.ConcatFilter(r, util.InLineDateFilter("Exercício",filter.Exercicio)) 
    }
    if len(filter.MesReferencia)>0 {
        r = util.ConcatFilter(r, util.InLineDateFilter("Mês",descricaoMes(filter.MesReferencia))) 
    }

    if len(r)==0 {
        r = "Todos"
    }    
    return r
}

func descricaoTipoEvento(tipoEvento string) string {
    switch tipoEvento {
        case "D%":
            return "Despesas"
        case "DF":
            return "Despesas Fixas"
        case "DV":
            return "Despesas Variáveis"
        case "RF":
            return "Receitas Fixas"
        case "RV":
            return "Receitas Variáveis"
        case "R%":
            return "Receitas"
        default:
            return "***"
    }

}

func descricaoMes(mesReferencia string) string {
    switch mesReferencia {
        case "1":
            return "Janeiro"
        case "2":
            return "Fevereiro"
        case "3":
            return "Março"
        case "4":
            return "Abril"
        case "5":
            return "Maio"
        case "6":
            return "Junho"
        case "7":
            return "Julho"
        case "8":
            return "Agosto"
        case "9":
            return "Setembro"
        case "10":
            return "Outubro"
        case "11":
            return "Novembro"
        case "12":
            return "Dezembro"
        default:
            return "***"
    }
}




