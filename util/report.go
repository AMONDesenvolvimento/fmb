package util
import (
    "time"
    "strconv"
    _ "github.com/mattn/go-sqlite3"
    "strings"
)
var Agora time.Time 
type RetornoLista struct {
     Arquivo string `json:"arquivo"`
     Status  string  `json:"status"` 
}

func TransformFloat(value string, scale int) string {
    const zeros = "0000000000"
    var strint string 
    var strdec string
    r := value
    pos := strings.Index(r,".")
    if pos>=0 {
        strint=r[0:pos]
        strdec=r[pos+1:len(r)]
    } else {
        strint=r
        strdec=""
    }
    if len(strdec)<scale {
        strdec+=zeros[0:scale-len(strdec)]
    } else {
        strdec=strdec[0:scale]
    }
 
    r="";
    intlength :=len(strint)
    for i:=intlength-1;i>=0;i-- {
        r=string(strint[i])+r
        if (intlength-i)%3 ==0 && i!=0{
            r="."+r
        }
    }
    if scale>0 {
        r=r+","+strdec
    }
    return r 
}
func FormatFloat(value float64, scale int) string {
    const zeros = "0000000000"
    var strint string 
    var strdec string
    r := strconv.FormatFloat(value,'f',-1,64)
    pos := strings.Index(r,".")
    if pos>=0 {
        strint=r[0:pos]
        strdec=r[pos+1:len(r)]
    } else {
        strint=r
        strdec=""
    }
    if len(strdec)<scale {
        strdec+=zeros[0:scale-len(strdec)]
    } else {
        strdec=strint[0:scale]
    }
 
    r="";
    intlength :=len(strint)
    for i:=intlength-1;i>=0;i-- {
        r=string(strint[i])+r
        if (intlength-i)%3 ==0 && i!=0{
            r="."+r
        }
    }
    if scale>0 {
        r=r+","+strdec
    }
    return r 
}

func TransformDate(date string) string{
    year := date[0:4]
    month := date[5:7]
    day := date[8:10]
    return day + "/" + month + "/" + year 
}

func TransformCodred(codred string) string {
    if codred=="0" {
        return ""
    } else {
        return codred
    }
}

func IdentDescription(conta string, descricao string) string {
    const spaces = "                                                                             "
    margin := strings.Count(conta,".")*10
    return spaces[0:margin]+descricao
}

func FormatTime(date time.Time) string {
    r := date.Format("2006-01-02 15:04:05")
    year := r[0:4]
    month := r[5:7]
    day := r[8:10]
    time := r[11:19]
    return day + "/" + month + "/" + year+ " "+time 
}

func FormatReference(exercicio string, mes string) string {
    desmes := MonthDescription(mes) 
    var tmpexercicio string 
    if len(strings.Trim(exercicio," "))==0 || exercicio=="0" {
        tmpexercicio=""
    }
    if len(desmes)==0 && len(tmpexercicio)==0 {
        return "   -   "
    }
    if len(desmes)==0 {
        return exercicio
    } else {
        return desmes+"/"+exercicio
    }
}
func MonthDescription(month string) string{
    if len(strings.Trim(month," "))==0 || strings.Trim(month," ")=="0"{
        return ""
    }
    m,_ := strconv.Atoi(month)
    switch (m) {
    case 1:
        return "jan"
    case 2:
        return "fev"
    case 3:
        return "mar"
    case 4:
        return "abr"
    case 5:
        return "mai"
    case 6:
        return "jun"
    case 7:
        return "jul"
    case 8:
        return "ago"
    case 9:
        return "set"
    case 10:
        return "out"
    case 11:
        return "nov"
    case 12:
        return "dez"
    default:
        return "***"
    } 
}

func ParseFloatNoError(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}

func InLineDateFilter(columnName string, content string) (string){
    r := ""
    arr := strings.Split(content,";")
    for _,interval := range arr {
        limits := strings.Split(interval,"-")
        element := ""
        if len(limits)==1 {
            element = columnName + " "+ limits[0]
        } else {
            element = columnName + " entre " + limits[0]+ " e " + limits[1]
        }
        r = ConcatWhere(r,element) 
    }
    return r
}



