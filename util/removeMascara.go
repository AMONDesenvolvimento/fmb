package util

import (
    "strings"
)

func RemoveMascaraDeCampoNumerico(value string) (string) {
	retorno := strings.Replace(value, "(", "", -1)
	retorno = strings.Replace(retorno, ")", "", -1)
	retorno = strings.Replace(retorno, "-", "", -1)
	retorno = strings.Replace(retorno, ".", "", -1)
	retorno = strings.Replace(retorno, " ", "", -1)
	retorno = strings.Replace(retorno, "/", "", -1)
	return retorno
}
