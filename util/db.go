package util
import (
	"database/sql"
        "strings"
	_ "github.com/mattn/go-sqlite3"
)
var MainDB *sql.DB
func init() {
    db, errOpenDB := sql.Open("sqlite3", "fmbtransparencia.db")
    CheckErr(errOpenDB)
    MainDB = db
}
func PrepareDB(query string) (*sql.Stmt) {
    stmt, err := MainDB.Prepare(query)
    CheckErr(err)
    return stmt
}
func QueryDB(query string) (*sql.Rows) {
    rows, err := MainDB.Query(query)
    CheckErr(err)
    return rows
}

func ConcatWhere(clausule string,element string) (string) {
    r := clausule
    if len(clausule)>0 {
        r+=" and "
    }
    r+=element
    return r
}

func GenerateInLineDate(columnName string, content string) (string){
    r := ""
    arr := strings.Split(content,";")
    for _,interval := range arr {
        limits := strings.Split(interval,"-")
        element := ""
        if len(limits)==1 {
            element = columnName + ` = "`+ TransformDateToAmerican(limits[0])+`"`
        } else {
            element = `(`+columnName + ` >= "` + TransformDateToAmerican(limits[0])+ `" and ` + columnName + ` <= "` + TransformDateToAmerican(limits[1]) + `")`
        }
        r = ConcatWhere(r,element) 
    }
    return r
}

func GenerateInLineFloat(columnName string, content string) (string){
    r := ""
    return r
}

func GenerateInLineInteger(columnName string, content string) (string){
    r := ""
    return r
}

func GenerateInLineString(columnName string, content string) (string) {
    r := ""
    return r
}

func GenerateSingleInteger(columnName string, content string) (string){
    r := columnName + ` = `+content
    return r
}

func GenerateSingleString(columnName string, content string) (string) {
    r := ""
    if strings.HasPrefix(content,"%") || strings.HasSuffix(content,"%") {
        r = columnName+ ` like "` +content+`"`
    } else {
        r = columnName+ ` = "` +content+`"`
    } 
    return r
}
func TransformDateToAmerican(date string) string{
    year := date[6:10]
    month := date[3:5]
    day := date[0:2]
    return year +"-" + month + "-" + day + " 03:00:00+00:00" 
}




