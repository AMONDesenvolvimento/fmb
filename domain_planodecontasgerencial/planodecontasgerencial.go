package domain_planodecontasgerencial
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
    "strconv"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
    "strings"
	gr "github.com/mikeshimura/goreport"
    "../util"
)
type Record struct {
        ID int64 `json:"id"`
        IdPai     int64  `json:"idpai"`
        Exercicio int64  `json:"exercicio"` 
        Conta     string `json:"conta"`
        Codred     int64  `json:"codred"`
	Descricao string `json:"descricao"`
        Analitica      string `json:"analitica"`
        SaldoAnterior float64 `json:"saldoanterior"`
        MovimentoDebitoCaixa float64 `json:"movimentodebitocaixa"`
        MovimentoCreditoCaixa float64 `json:"movimentocreditocaixa"`
        MovimentoDebitoCompetencia float64 `json:"movimentodebitocompetencia"`
        MovimentoCreditoCompetencia float64 `json:"movimentocreditocompetencia"`
        Orcamento  float64    `json:"orcamento"`
        DataCriacao string `json:"datacriacao"` 
        DataExclusao string `json:"dataexclusao"`
}

type Filter struct {
        Exercicio string  `json:"exercicio"` 
        Codred    string  `json:"codred"`
}

type Arr []Record

//var agora time.Time

func GetAll(w http.ResponseWriter, r *http.Request) {
	rows, err := util.MainDB.Query(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao,
                                          dataexclusao FROM planodecontasgerencial`)
	util.CheckErr(err)
	var arr Arr
	for rows.Next() {
		var record Record
		err = rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}



func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitocaixa,
                                            movimentocreditocaixa,
                                            movimentodebitocompetencia,
                                            movimentocreditocompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao FROM planodecontasgerencial where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.IdPai,
                                &record.Exercicio, 
                                &record.Conta,
                                &record.Codred,
                                &record.Descricao,
                                &record.Analitica,
                                &record.SaldoAnterior,
                                &record.MovimentoDebitoCaixa,
                                &record.MovimentoCreditoCaixa,
                                &record.MovimentoDebitoCompetencia,
                                &record.MovimentoCreditoCompetencia,
                                &record.Orcamento,    
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetByConta(conta string) Record {
	stmt := util.PrepareDB(`SELECT id,
                                            idpai,
                                            exercicio,
                                            conta,
                                            codred,
                                            descricao,
                                            analitica,
                                            saldoanterior,
                                            movimentodebitocaixa,
                                            movimentocreditocaixa,
                                            movimentodebitocompetencia,
                                            movimentocreditocompetencia,
                                            orcamento,
                                            datacriacao,
                                            dataexclusao  
                                            FROM planodecontasgerencial where conta = ?`)
	rows, errQuery := stmt.Query(conta)
	util.CheckErr(errQuery)
	var record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.IdPai,
                                &record.Exercicio,
                                &record.Conta, 
                                &record.Codred,
                                &record.Descricao,
                                &record.Analitica,
                                &record.SaldoAnterior,
                                &record.MovimentoDebitoCaixa,
                                &record.MovimentoCreditoCaixa,
                                &record.MovimentoDebitoCompetencia,
                                &record.MovimentoCreditoCompetencia,
                                &record.Orcamento, 
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	return record
}



func extractParent(conta string) string {
    pos:=strings.LastIndex(conta,".") 
    if pos<0 {
        return ""
    } else {
        return conta[0:pos]
    }
}


func List(w http.ResponseWriter, r *http.Request) {
        var filter Filter 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("exercicio----->"+filter.Exercicio)
        log.Println("codred---->"+filter.Codred)


	rows := util.QueryDB(`SELECT id,
                                          idpai,
                                          exercicio,
                                          conta,
                                          codred,
                                          descricao,
                                          analitica,
                                          saldoanterior,
                                          movimentodebitocaixa,
                                          movimentocreditocaixa,
                                          movimentodebitocompetencia,
                                          movimentocreditocompetencia,
                                          orcamento,
                                          datacriacao, 
                                          dataexclusao FROM planodecontasgerencial order by conta`)
	var arr Arr
        var record Record 
	for rows.Next() {
		rows.Scan(
                               &record.ID,
                               &record.IdPai,
                               &record.Exercicio, 
                               &record.Conta,
                               &record.Codred,
                               &record.Descricao,
                               &record.Analitica,
                               &record.SaldoAnterior,
                               &record.MovimentoDebitoCaixa,
                               &record.MovimentoCreditoCaixa,
                               &record.MovimentoDebitoCompetencia,
                               &record.MovimentoCreditoCompetencia,
                               &record.Orcamento,
                               &record.DataCriacao, 
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}

        var contents string = ""
        for _,record = range arr {
           contents+=strconv.FormatInt(record.ID, 10)+"\t"
           contents+=strconv.FormatInt(record.IdPai, 10)+"\t"
           contents+=strconv.FormatInt(record.Exercicio, 10)+"\t"
           contents+=record.Conta+"\t"
           contents+=strconv.FormatInt(record.Codred, 10)+"\t"
           contents+=record.Descricao+"\t"
           contents+=record.Analitica+"\t"
           contents+=strconv.FormatFloat(record.SaldoAnterior,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoDebitoCaixa,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoCreditoCaixa,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoDebitoCompetencia,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.MovimentoCreditoCompetencia,'f',-1,64)+"\t"
           contents+=strconv.FormatFloat(record.Orcamento,'f',-1,64)+"\t"
           contents+=record.DataCriacao+"\t"
           contents+=record.DataExclusao+"\n"
         } 
        d1 := []byte(contents)
        err = ioutil.WriteFile("planodecontasgerenciallist.txt",d1,0644)
        util.CheckErr(err)
        Report()


        var retornoLista util.RetornoLista
        retornoLista.Arquivo="planodecontasgerenciallist.pdf"
        retornoLista.Status="ok"
	jsonB, errMarshal := json.Marshal(retornoLista)
	util.CheckErr(errMarshal)
        fmt.Printf("Lista-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func Report() {
        util.Agora = time.Now()
	r := gr.CreateGoReport()
	r.SumWork["amountcum="] = 0.0
	font1 := gr.FontMap{
		FontName: "IPAexG",
		FileName: "ttf//ipaexg.ttf",
	}
	fonts := []*gr.FontMap{&font1}
	r.SetFonts(fonts)
	d := new(S1Detail)
	r.RegisterBand(gr.Band(*d), gr.Detail)
	h := new(S1Header)
	r.RegisterBand(gr.Band(*h), gr.PageHeader)
	s := new(S1Summary)
	r.RegisterBand(gr.Band(*s), gr.Summary)

	f := new(S1Footer)
	r.RegisterBand(gr.Band(*f), gr.PageFooter)


	r.Records = gr.ReadTextFile("planodecontasgerenciallist.txt", 7) // 09 e 0A no final
//	fmt.Printf("Records %v \n", r.Records)
	r.SetPage("A4", "mm", "L")
	r.SetFooterY(190)
	r.Execute("static/orcamentolist.pdf")
	r.SaveText("static/orcamentosavetext.txt")
}

type S1Detail struct {
}

func (h S1Detail) GetHeight(report gr.GoReport) float64 {
	return 5
}
func (h S1Detail) Execute(report gr.GoReport) {
	cols := report.Records[report.DataPos].([]string)
	report.Font("IPAexG", 12, "")
	y := 5.0
	report.Cell(15, y, cols[3])
	report.Cell(50, y, util.IdentDescription(cols[3],cols[5]))
	report.CellRight(205, y,15, util.TransformFloat(cols[12],2))
//	amt := ParseFloatNoError(cols[5]) * ParseFloatNoError(cols[6])
        report.CellRight(240, y,6, util.TransformCodred(cols[4])) 
	report.SumWork["amountcum="] += 1
//	report.CellRight(180, y, 30, strconv.FormatFloat(amt, 'f', 2, 64))
}

type S1Header struct {
}

func (h S1Header) GetHeight(report gr.GoReport) float64 {
	return 30
}
func (h S1Header) Execute(report gr.GoReport) {
	report.Font("IPAexG", 14, "")
	report.Cell(80, 15, "Transparência - Plano Gerencial com Orçamento")
	report.Font("IPAexG", 12, "")
	report.Cell(255, 15, "Página")
	report.Cell(275, 15, strconv.Itoa(report.Page))
        y := 23.0
        filter := "Exercício: 2017"
        var x float64
        x = float64((265.0 - len(filter))/2.0)
        report.Cell(15+x,y,filter)
	y = 28.0
	report.Cell(15, y, "Conta")
	report.Cell(50, y, "Descrição")
	report.CellRight(205, y,15, "Orçamento")
	report.Cell(240, y, "Cod.Red.")
	report.LineType("straight", 0.2)
        report.LineH(15, y+5.0, 280)

      report.Image("static/Content/img/fmb.jpeg", 15, 10, 80, 20)
}

type S1Summary struct {
}

func (h S1Summary) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Summary) Execute(report gr.GoReport) {
	report.CellRight(100, 12,25, "Contas Listadas:")
	report.CellRight(130, 12, 30, strconv.FormatFloat(
		report.SumWork["amountcum="], 'f', 0, 64))
}

type S1Footer struct {
}

func (h S1Footer) GetHeight(report gr.GoReport) float64 {
	return 10
}
func (h S1Footer) Execute(report gr.GoReport) {
	report.CellRight(200, 8,80, "Emitido por MASTER em "+util.FormatTime(util.Agora))
}



