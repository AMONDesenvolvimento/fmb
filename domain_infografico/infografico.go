package domain_infografico
import (
    "time"
    "encoding/json"
    "net/http"
    "log"
    "strings"
    "strconv"
    "fmt"
    "io/ioutil"
    _ "github.com/mattn/go-sqlite3"
    "github.com/gorilla/mux"
//	gr "github.com/mikeshimura/goreport"
    "../util"
)
type Record struct {
	ID   int64  `json:"id"`
        Descricao     string  `json:"descricao"`
        Tema string `json:"tema"`
        Perspectivas string `json:"perspectivas"` 
        Variaveis    string    `json:"variaveis"`
        Filtros string  `json:"filtros"`
        Tipo string   `json:"tipo"`
        Visibilidade string   `json:"visibilidade"`
        Abrangencia  string     `json:"abrangencia"`         
        DataCriacao string     `json:"datacriacao"` 
        DataExclusao string    `json:"dataexclusao"`
}

type Filter struct {
	ID   string  `json:"id"`
        Descricao     string  `json:"descricao"`
        Tema string `json:"tema"`
        Perspectivas string `json:"perspectivas"` 
        Variaveis    string    `json:"variaveis"`
        Filtros string  `json:"filtros"`
        Tipo string   `json:"tipo"`
        Visibilidade string   `json:"visibilidade"`
        Abrangencia  string     `json:"abrangencia"`         
}
type Parms struct {
        ID int64 `json:"id"`
        ValoresFiltros string `json:"valoresfiltros"`
}
type Dashboard struct {
        Perspectiva1 string
        Perspectiva2 string 
        Variavel     float64
}


type Arr []Record

const ig_dashboard = "dashboard"
const ig_bubble    = "bolhas"
const ig_stackbar  = "barras"
const ig_weightedtree = "arvoreponderada"
const ig_bilevelpartition = "particaobinivel"

func GetAll(w http.ResponseWriter, r *http.Request) {
	rows := util.QueryDB(`SELECT id,
                                          descricao,
                                          tema,
                                          perspectivas,
                                          variaveis,
                                          filtros,
                                          tipo,
                                          visibilidade,
                                          abrangencia,
                                          datacriacao,
                                          dataexclusao FROM infografico`)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.Descricao,
                               &record.Tema, 
                               &record.Perspectivas,
                               &record.Variaveis,
                               &record.Filtros,
                               &record.Tipo,
                               &record.Visibilidade,
                               &record.Abrangencia,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}
	jsonB, errMarshal := json.Marshal(arr)
	util.CheckErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func GetMenu(w http.ResponseWriter, r *http.Request) {
	rows := util.QueryDB(`SELECT id,
                                          descricao,
                                          tema,
                                          perspectivas,
                                          variaveis,
                                          filtros,
                                          tipo,
                                          visibilidade,
                                          abrangencia,
                                          datacriacao,
                                          dataexclusao FROM infografico where visibilidade="publica"`)
	var arr Arr
	for rows.Next() {
		var record Record
		rows.Scan(
                               &record.ID,
                               &record.Descricao,
                               &record.Tema, 
                               &record.Perspectivas,
                               &record.Variaveis,
                               &record.Filtros,
                               &record.Tipo,
                               &record.Visibilidade,
                               &record.Abrangencia,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )
		arr = append(arr, record)
	}


       in := ""
       rawIn := json.RawMessage(in)
        jsonB, errMarshal := rawIn.MarshalJSON()
        util.CheckErr(errMarshal)
//	jsonB, errMarshal := json.Marshal(arr)
//	util.CheckErr(errMarshal)
        fmt.Printf("getAll-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}
func GetByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB(` SELECT id,
                                             descricao,
                                             tema,
                                             perspectivas,
                                             variaveis,
                                             filtros,
                                             tipo,
                                             visibilidade,
                                             abrangencia,
                                             datacriacao,
                                             dataexclusao FROM infografico where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var  record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Descricao,
                                &record.Tema, 
                                &record.Perspectivas,
                                &record.Variaveis,
                                &record.Filtros,
                                &record.Tipo,
                                &record.Visibilidade,
                                &record.Abrangencia,
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func RowByID(id int64) Record {
	stmt := util.PrepareDB(` SELECT id,
                                             descricao,
                                             tema,
                                             perspectivas,
                                             variaveis,
                                             filtros,
                                             tipo,
                                             visibilidade,
                                             abrangencia,
                                             datacriacao,
                                             dataexclusao FROM infografico where id = ?`)
	rows, errQuery := stmt.Query(id)
	util.CheckErr(errQuery)
	var  record Record
	for rows.Next() {
		rows.Scan(&record.ID,
                                &record.Descricao,
                                &record.Tema, 
                                &record.Perspectivas,
                                &record.Variaveis,
                                &record.Filtros,
                                &record.Tipo,
                                &record.Visibilidade,
                                &record.Abrangencia,
                                &record.DataCriacao,
                                &record.DataExclusao)
	}
        return record
}


func Insert(w http.ResponseWriter, r *http.Request) {
        var record Record
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            fmt.Printf("erro %s",err)
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("descricao---->"+record.Descricao)
        log.Println("tema---->"+record.Tema)
        log.Println("perspectivas---->"+record.Perspectivas)
        log.Println("variaveis---->"+record.Variaveis)
        log.Println("filtros---->"+record.Filtros)
        log.Println("tipo---->"+record.Tipo)
        log.Println("visibilidade---->"+record.Visibilidade)
        log.Println("abrangencia---->"+record.Abrangencia)
        record.DataCriacao = time.Now().Format("2006-01-02 15:04:05")


	stmt := util.PrepareDB(
                                   `INSERT INTO infografico (
                                   descricao,
                                   tema,
                                   perspectivas,
                                   variaveis,
                                   filtros,
                                   tipo,
                                   visibilidade,
                                   abrangencia,
                                   datacriacao) values (?,?,?,?,?,?,?,?,?)`)
	result, errExec := stmt.Exec(
                           record.Descricao,
                           record.Tema,
                           record.Perspectivas,
                           record.Variaveis,
                           record.Filtros,
                           record.Tipo,
                           record.Visibilidade,
                           record.Abrangencia,
                           record.DataCriacao,
                           )
	util.CheckErr(errExec)
	newID, errLast := result.LastInsertId()
	util.CheckErr(errLast)
	record.ID = newID
	jsonB, errMarshal := json.Marshal(record)
	util.CheckErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func UpdateByID(w http.ResponseWriter, r *http.Request) {
      var record Record 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &record)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+strconv.FormatInt(record.ID,10))
        log.Println("descricao---->"+record.Descricao)
        log.Println("tema---->"+record.Tema)
        log.Println("perspectivas---->"+record.Perspectivas)
        log.Println("variaveis---->"+record.Variaveis)
        log.Println("filtros---->"+record.Filtros)
        log.Println("tipo---->"+record.Tipo)
        log.Println("visibilidade---->"+record.Visibilidade)
        log.Println("abrangencia---->"+record.Abrangencia)



	stmt := util.PrepareDB(`UPDATE infografico 
                                    SET (descricao, tema, perspectivas, variaveis, filtros, tipo, visibilidade, abrangencia)=
                                    (?,?,?,?,?,?,?,?)     
                                    WHERE id = ?`)
	result, errExec := stmt.Exec(
                                  record.Descricao,
                                  record.Tema,
                                  record.Perspectivas,
                                  record.Variaveis,
                                  record.Filtros,
                                  record.Tipo,
                                  record.Visibilidade,
                                  record.Abrangencia,
                                  record.ID)
	util.CheckErr(errExec)
	rowAffected, errLast := result.RowsAffected()
	util.CheckErr(errLast)
	if rowAffected > 0 {
		jsonB, errMarshal := json.Marshal(record)
		util.CheckErr(errMarshal)
		fmt.Fprintf(w, "%s", string(jsonB))
	} else {
		fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
	}

}

func DeleteByID(w http.ResponseWriter, r *http.Request) {
        vars := mux.Vars(r)
        id := vars["id"]
        log.Println("id->"+id)
	stmt := util.PrepareDB("DELETE FROM infografico WHERE id = ?")
	result, errExec := stmt.Exec(id)
	util.CheckErr(errExec)
	rowAffected, errRow := result.RowsAffected()
	util.CheckErr(errRow)
	fmt.Fprintf(w, "{row_affected=%d}", rowAffected)
}

func List(w http.ResponseWriter, r *http.Request) {
        var filter Filter 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &filter)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   
        log.Println("id----->"+filter.ID)
        log.Println("descricao---->"+filter.Descricao)
        log.Println("tema---->"+filter.Tema)
        log.Println("perspectivas---->"+filter.Perspectivas)
        log.Println("variaveis---->"+filter.Variaveis)
        log.Println("filtros---->"+filter.Filtros)
        log.Println("tipo---->"+filter.Tipo)
        log.Println("visibilidade---->"+filter.Visibilidade)
        log.Println("abrangencia---->"+filter.Abrangencia)


	rows := util.QueryDB(`SELECT id,
                                          descricao,
                                          tema,
                                          perspectivas,
                                          variaveis,
                                          filtros,
                                          tipo,
                                          visibilidade,
                                          abrangencia,
                                          datacriacao,
                                          dataexclusao FROM infografico`)
	var arr Arr
        var record Record
	for rows.Next() {
		rows.Scan(
                               &record.ID,
                               &record.Descricao,
                               &record.Tema, 
                               &record.Perspectivas,
                               &record.Variaveis,
                               &record.Filtros,
                               &record.Tipo,
                               &record.Visibilidade,
                               &record.Abrangencia,
                               &record.DataCriacao,
                               &record.DataExclusao, 
                               )

		arr = append(arr, record)
	}

        var contents string = ""
        for _,record = range arr {
           contents+=strconv.FormatInt(record.ID, 10)+"\t"
           contents+=record.Descricao+"\t"
           contents+=record.Tema+"\t"
           contents+=record.Perspectivas+"\t"
           contents+=record.Variaveis+"\t"
           contents+=record.Filtros+"\t"
           contents+=record.Tipo+"\t"
           contents+=record.Visibilidade+"\t"
           contents+=record.Abrangencia+"\t"
           contents+=record.DataCriacao+"\t"
           contents+=record.DataExclusao+"\n"
         } 
        d1 := []byte(contents)
        err = ioutil.WriteFile("infograficolist.txt",d1,0644)
        util.CheckErr(err)
        var retornoLista util.RetornoLista
        retornoLista.Arquivo="infograficolist.txt"
        retornoLista.Status="ok"
	jsonB, errMarshal := json.Marshal(retornoLista)
	util.CheckErr(errMarshal)
        fmt.Printf("Lista-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}


func Data(w http.ResponseWriter, r *http.Request) {
        var parms Parms
        var jsonB []byte 
        body, err := ioutil.ReadAll(r.Body)
        if err != nil {
            panic(fmt.Sprintf("%s", r.Body))
        }
        log.Println("body---->"+string(body))
        err = json.Unmarshal(body, &parms)
        if err != nil {
            panic(fmt.Sprintf("%s",string(body)))
        }
   


        record := RowByID(parms.ID) 

        log.Println("id----->"+strconv.FormatInt(parms.ID,10))
        log.Println("valores filtros---->"+parms.ValoresFiltros)
        log.Println("descricao---->"+record.Descricao)
        log.Println("tipo---->"+record.Tipo)


        if strings.ToLower(record.Tipo)==ig_dashboard {
            jsonB = dashboardTemaDespesaData(record, parms.ValoresFiltros)
        } else if strings.ToLower(record.Tipo)==ig_weightedtree {
            jsonB = weightedData(record, parms.ValoresFiltros)
        }

        fmt.Printf("Data-> %s",string(jsonB))
	fmt.Fprintf(w, "%s", string(jsonB))
}

func dashboardTemaDespesaData(record Record, valoresFiltros string) []byte {
    var clausulaWhere=""
    var columns=""
    var groups=""
  
//select evento,mesreferencia,sum(valor)  from lancamento group by evento, mesreferencia order by evento, mesreferencia
    perspectivaArr := strings.Split(strings.ToLower(record.Perspectivas),";")
    variavel := strings.Split(strings.ToLower(record.Variaveis),";")[0] 
    filtroArr :=strings.Split(strings.ToLower(record.Filtros),";")
    filtroValArr := strings.Split(valoresFiltros,";")
   
   
    for i,filtro := range filtroArr {
        if filtro=="exercicio" && len(strings.Trim(filtroValArr[i]," "))>0{
             clausulaWhere = concatWhere(clausulaWhere,"exercicio = "+filtroValArr[i]) 
        } else if filtro=="mes" && len(strings.Trim(filtroValArr[i]," "))>0 {
            clausulaWhere = concatWhere(clausulaWhere,"mes = "+filtroValArr[i]) 
        } else if filtro=="tipo" && len(strings.Trim(filtroValArr[i]," "))>0{
            if filtroValArr[i]=="f" || filtroValArr[i]=="fixa" {
                clausulaWhere = concatWhere(clausulaWhere, `tipo = "F"`)
            } else {
                clausulaWhere = concatWhere(clausulaWhere, `tipo = "F"`)
            }
         } else if filtro=="conta1" && len(strings.Trim(filtroValArr[i]," "))>0{
            clausulaWhere = concatWhere(clausulaWhere, `conta1 = "`+filtroValArr[i]+`"`)
         } else if filtro=="conta2" && len(strings.Trim(filtroValArr[i]," "))>0{
            clausulaWhere = concatWhere(clausulaWhere, `conta2 = "`+filtroValArr[i]+`"`)
         } else if filtro=="conta3" && len(strings.Trim(filtroValArr[i]," "))>0{
            clausulaWhere = concatWhere(clausulaWhere, `conta3 = "`+filtroValArr[i]+`"`)
         } else if filtro=="conta4" && len(strings.Trim(filtroValArr[i]," "))>0{
            clausulaWhere = concatWhere(clausulaWhere, `conta4 = "`+filtroValArr[i]+`"`)
         }
    }
    for _,perspectiva:= range perspectivaArr {
        columns+=perspectiva+","
        groups+=perspectiva+","
    }
    columns+="sum("+variavel+")";
    if strings.HasSuffix(groups,"-") {
        columns = columns[0:len(groups)-1]
    }
    sqlselect := "select " +columns+ " from temadespesa "
    if len(clausulaWhere)>0 {
        sqlselect +=clausulaWhere + " "
    } 
    sqlselect+="group by "+groups+" order by "+groups
    sqldistinct := "select distinct "+perspectivaArr[1]+ "from temadespesa" 
    perspectiva2ValorMap := map[string]float64{}

    rows := util.QueryDB(sqldistinct)
    var perspectiva2Valor string 
    var perspectiva2ValorArr []string
    for rows.Next() {
	rows.Scan(&perspectiva2Valor,)
	//checkErr(err)
        perspectiva2ValorArr = append(perspectiva2ValorArr, perspectiva2Valor)
    }
    for _,p2v := range perspectiva2ValorArr {
        perspectiva2ValorMap[p2v] = 0.0
    }

    rows = util.QueryDB(sqlselect)
    var dashboard Dashboard 
    var dashboardArr []Dashboard
    for rows.Next() {
	rows.Scan(&dashboard.Perspectiva1,
                        &dashboard.Perspectiva2,
                        &dashboard.Variavel,
              )
	//checkErr(err)
        dashboardArr = append(dashboardArr, dashboard)
    }
    var strJson = "[" 
    var p1tag = "" 
    for _,dashboard = range dashboardArr {
        if dashboard.Perspectiva1 != p1tag { 
            if len(p1tag)>0 {
                for _,p2v := range perspectiva2ValorArr {
                    strVariavel := strconv.FormatFloat(perspectiva2ValorMap[p2v],'f',-1,64)
                    strJson+= `"`+p2v + `":`+strVariavel+`,` 
                }
                strJson+="}}"
            }
            strJson  = strJson[0:len(strJson)-1] 
            for _,p2v := range perspectiva2ValorArr {
                perspectiva2ValorMap[p2v] = 0.0
            }

            // {p1:'Jan',p2:{Sal:4786, Grat:1319, HE:249,Sobreaviso:324,Encargos:324}}
            strJson+=`{"p1":"`+dashboard.Perspectiva1+`","p2:{"`
            perspectiva2ValorMap[dashboard.Perspectiva2]=dashboard.Variavel
        } else {
            perspectiva2ValorMap[dashboard.Perspectiva2]=dashboard.Variavel
        }
    }
    for _,p2v := range perspectiva2ValorArr {
        strVariavel := strconv.FormatFloat(perspectiva2ValorMap[p2v],'f',-1,64)
        strJson+= `"`+p2v + `":`+strVariavel+`,` 
    }
    strJson+="}}"
    strJson+="]"
    rawIn := json.RawMessage(strJson)
    bytes, err := rawIn.MarshalJSON()
    if err != nil {
        panic(err)
    }
    fmt.Printf("Data-> %s",string(bytes))
    return bytes        
} 
func concatWhere(clausule string,element string) string {
    r := clausule
    if len(clausule)>0 {
        r+=" and "
    }
    r+=element
    return r
}

func weightedData(record Record, valoresFiltros string) []byte {
in :=`[
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Folha de Pagamento",
"Level3":"",
"Level4":"",
"Federal":42200.00,
"":"b",
"GovXFer":"",
"State":"8070.00",
"Local":"34130",
"Total":42200.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Comunicação",
"Level4":"CCR Comunicação",
"Federal":120000.00,
"":"b",
"GovXFer":"",
"State":"20000",
"Local":"100000",
"Total":120000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Comunicação",
"Level4":"Troia",
"Federal":6000,
"":"b",
"GovXFer":"",
"State":"1000",
"Local":"5000",
"Total":6000
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Jurídica",
"Level4":"",
"Federal":120000.00,
"":"b",
"GovXFer":"",
"State":"18777",
"Local":"101223.00",
"Total":120000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Parlamentar",
"Level4":"",
"Federal":36000.00,
"":"b",
"GovXFer":"",
"State":"6000",
"Local":"30000",
"Total":36000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Contabilidade",
"Level4":"",
"Federal":10560.00,
"":"b",
"GovXFer":"",
"State":"880",
"Local":"9680",
"Total":10560.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Informática",
"Level4":"",
"Federal":0,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":0
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Passagens",
"Level4":"",
"Federal":12000.00,
"":"b",
"GovXFer":"",
"State":"0.00",
"Local":"12000",
"Total":12000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Hospedagens",
"Level4":"",
"Federal":5800.00,
"":"b",
"GovXFer":"",
"State":"0.00",
"Local":"5800.00",
"Total":5800.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Diárias",
"Level4":"",
"Federal":5800.00,
"":"b",
"GovXFer":"",
"State":"0.00",
"Local":"5800.00",
"Total":5800.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Despesas Diretoria",
"Level3":"Passagens",
"Level4":"",
"Federal":132000.00,
"":"b",
"GovXFer":"",
"State":"7917.57",
"Local":"124082.43",
"Total":132000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Despesas Diretoria",
"Level3":"Hospedagens",
"Level4":"",
"Federal":54520.00,
"":"b",
"GovXFer":"",
"State":"732.59",
"Local":"53787.41",
"Total":54520.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Despesas Diretoria",
"Level3":"Diárias",
"Level4":"",
"Federal":178200.00,
"":"b",
"GovXFer":"",
"State":"0.00",
"Local":"178200",
"Total":178200.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Despesas com Telefone",
"Level3":"Oi Telemar 3222-7401",
"Level4":"",
"Federal":717.16,
"":"b",
"GovXFer":"",
"State":"131.80",
"Local":"585.36",
"Total":717.16
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Despesas com Telefone",
"Level3":"Claro celular 98416-2195",
"Level4":"",
"Federal":2856.00,
"":"b",
"GovXFer":"",
"State":"504.94",
"Local":"2351.06",
"Total":2856.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Provisionamento",
"Level3":"",
"Level4":"",
"Federal":92089.80,
"":"b",
"GovXFer":"",
"State":"0.00",
"Local":"92089.80",
"Total":92089.80
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Custeio de Instalações",
"Level3":"Sistema de Informática - AMON",
"Level4":"",
"Federal":20056.94,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":20056.94
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Custeio de Instalações",
"Level3":"Material de Expediente",
"Level4":"",
"Federal":700.00,
"":"b",
"GovXFer":"",
"State":"0.00",
"Local":"700.00",
"Total":700.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Custeio de Instalações",
"Level3":"Outras Despesas",
"Level4":"",
"Federal":500.00,
"":"b",
"GovXFer":"",
"State":"269.81",
"Local":"230.19",
"Total":500.00
}
]`

/*
in :=`[
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Jurídica",
"Level4":"Tróia",
"Federal":126000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":126000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Parlamentar",
"Level4":"",
"Federal":36000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":36000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Contabilidade",
"Level4":"",
"Federal":10560.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":10560.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Informática",
"Level4":"",
"Federal":0.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":1.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Passagens",
"Level4":"",
"Federal":12000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":12000.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Hospedagens",
"Level4":"",
"Federal":5800.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":5800.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Diárias",
"Level4":"",
"Federal":5800.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":5800.00
},













{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"",
"Level3":"",
"Level4":"",
"Federal":840000.00,
"":"b",
"GovXFer":0.0,
"State":0.0,
"Local":0.0,
"Total":840000.00
},
{
"Category":"--Despesa",
"Level1":"Despesa",
"Level2":"Folha de Pagamento",
"Level3":"",
"Level4":"",
"Federal":42200.00,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":42200.00
},
{
"Category":"--Despesa",
"Level1":"Despesa",
"Level2":"Custeio de Instalações",
"Level3":"",
"Level4":"",
"Federal":21256.84,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":21256.84
},
{
"Category":"--Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"",
"Level4":"",
"Federal":316160.00,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":316160.38
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Comunicação",
"Level4":"",
"Federal":120000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":120000.00
},
{
"Category":"----Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Comunicação",
"Level4":"CCR Comunicação",
"Federal":120000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":120000.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Jurídica",
"Level4":"",
"Federal":126000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":126000.00
},
{
"Category":"----Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Jurídica",
"Level4":"Tróia",
"Federal":126000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":126000.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Parlamentar",
"Level4":"",
"Federal":36000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":36000.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Contabilidade",
"Level4":"",
"Federal":10560.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":10560.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Informática",
"Level4":"",
"Federal":0.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":0.00
},
{
"Category":"-Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Passagens",
"Level4":"",
"Federal":12000.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":12000.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Hospedagens",
"Level4":"",
"Federal":5800.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":5800.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Assessorias",
"Level3":"Diárias",
"Level4":"",
"Federal":5800.00,
"":"b",
"GovXFed":"",
"State":"",
"Local":"",
"Total":5800.00
},
{
"Category":"--Despesa",
"Level1":"Despesa",
"Level2":"Despesas Diretoria",
"Level3":"",
"Level4":"",
"Federal":364720.00,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":364720.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Despesas Diretoria",
"Level3":"Passagens",
"Level4":"",
"Federal":54520.00,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":54520.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Despesas Diretoria",
"Level3":"Hospedagens",
"Level4":"",
"Federal":178200.00,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":178200.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Despesas Diretoria",
"Level3":"Diárias",
"Level4":"",
"Federal":"",
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":12345.38
},
{
"Category":"--Despesa",
"Level1":"Despesa",
"Level2":"Despesas com Telefone",
"Level3":"",
"Level4":"",
"Federal":12345.38,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":12345.38
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Despesas com Telefone",
"Level3":"Oi Telemar 3222-7401",
"Level4":"",
"Federal":12345.38,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":12345.38
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Despesas com Telefone",
"Level3":"Claro celular 98416-2195",
"Level4":"",
"Federal":12345.38,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":12345.38
},
{
"Category":"--Despesa",
"Level1":"Despesa",
"Level2":"Provisionamento",
"Level3":"",
"Level4":"",
"Federal":92089.80,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":92089.80
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Custeio de Instalações",
"Level3":"Sistema de Informática - AMON",
"Level4":"",
"Federal":2893,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":2893
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Custeio de Instalações",
"Level3":"Material de Expediente",
"Level4":"",
"Federal":700.00,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":700.00
},
{
"Category":"---Despesa",
"Level1":"Despesa",
"Level2":"Custeio de Instalações",
"Level3":"Outras Despesas",
"Level4":"",
"Federal":500.00,
"":"b",
"GovXFer":"",
"State":"",
"Local":"",
"Total":500.00
}
]`
*/


   rawIn := json.RawMessage(in)
    bytes, err := rawIn.MarshalJSON()
    if err != nil {
        panic(err)
    }

  fmt.Printf("Data-> %s",string(bytes))

  return bytes        

}


